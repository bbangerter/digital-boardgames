import { Component, Input, OnDestroy, AfterViewInit } from '@angular/core';
import { GameStart, CanvasStateService, GameComponent, GfxTexture, GfxLines, GfxClickData } from '../../core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'playground-game-controller',
  templateUrl: './playground-game-controller.component.html',
  styleUrls: ['./playground-game-controller.component.scss']
})
export class PlaygroundGameControllerComponent implements OnDestroy, AfterViewInit, GameComponent {
  @Input() gameData: GameStart;
  selectedValue: string = 'Line';
  gfxTypes: Array<string> = ['Line', 'Texture', 'TextureAtlas', 'FPS'];
  fpsValues: Array<number> = [0, 1, 30];
  selectedFPS: string = '0';
  canvasId: string = 'playground-canvas';

  private clickSubscription: Subscription;
  private background: GfxTexture;
  private lineSegments: GfxLines;

  constructor(private canvas: CanvasStateService) { }

  ngAfterViewInit(): void {
    this.background = this.canvas.createTexture(this.canvasId, './assets/playground/background.jpg', 0);
    this.background.setEnabled(true);
    this.canvas.createText(this.canvasId, 'Digital Boardgame Global Thermal Nuclear War', '50px Arial', 'black', 0).translate(75, 350);
    this.clickSubscription = this.canvas.subscribeClickEvent(this.canvasId, (data: GfxClickData) => this.onClick(data));
  }

  ngOnDestroy(): void {
    this.clickSubscription.unsubscribe();
    this.canvas.removeCanvas(this.canvasId);
  }

  public onChange() {
    this.canvas.setFPS(this.canvasId, parseInt(this.selectedFPS));
  }

  private onClick(data: GfxClickData) {
    if (data != null) {
      switch (this.selectedValue) {
        case 'Line':
          if (this.lineSegments == null) {
            this.lineSegments = this.canvas.createLineSegment(this.canvasId, data.x, data.y, 5, 'black', 0);
          }
          else {
            this.lineSegments.addTo(data.x, data.y);
          }
          break;
        case 'Texture':
          this.canvas.createTexture(this.canvasId, './assets/splendor/emerald-1.jpg', 0).translate(data.x, data.y);
          break;
        case 'TextureAtlas':
          this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/emerald-1.jpg', 3, 3, 4, 0).translate(data.x, data.y);
          break;
        case 'FPS':
          var color = `#${Math.floor(Math.random() * 16).toString(16) + Math.floor(Math.random() * 16).toString(16) +
            Math.floor(Math.random() * 16).toString(16) + Math.floor(Math.random() * 16).toString(16) +
            Math.floor(Math.random() * 16).toString(16) + Math.floor(Math.random() * 16).toString(16)}`;
          var circle = this.canvas.createCircle(this.canvasId, 0, 2 * Math.PI, Math.random() * 20, false, color, Math.random() * 2 > 1 ? true : false, 1, 0);
          circle.translate(data.x, data.y)
          circle.accelerate(Math.random() * 40 - 20, Math.random() * 40 - 20);
          break;
      }
    }
  }
}
