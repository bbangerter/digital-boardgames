import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaygroundGameControllerComponent } from './playground-game-controller.component';

describe('PlaygroundGameControllerComponent', () => {
  let component: PlaygroundGameControllerComponent;
  let fixture: ComponentFixture<PlaygroundGameControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaygroundGameControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaygroundGameControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
