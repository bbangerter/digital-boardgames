import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaygroundGameControllerComponent } from './playground-game-controller/playground-game-controller.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [PlaygroundGameControllerComponent],
  exports: [
    PlaygroundGameControllerComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatTableModule,
    MatSelectModule,
    MatCardModule,
    FormsModule,
    CoreModule
  ]
})
export class PlaygroundModule { }
