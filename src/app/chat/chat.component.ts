import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SignalRService, PlayerStateService } from '../core';
import { ChatMessage } from './dtos/chat-dtos.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  messages: Array<ChatMessage> = [];
  textMessage: string = '';
  private hubName: string = 'chat';
  private groupName: string = 'GroupChat';
  private chatSubscription: Subscription;

  constructor(private signalR: SignalRService, private playerState: PlayerStateService) { }

  ngOnInit(): void {
    this.startChat();
  }

  ngOnDestroy(): void {
    this.chatSubscription.unsubscribe();
    this.signalR.close(this.hubName);
  }

  public startChat() {
    this.signalR.connect(this.hubName, `PlayerName=${this.playerState.getPlayerName()}&GroupName=${this.groupName}&PlayerId=${this.playerState.getPlayerId()}`);
    this.chatSubscription = this.signalR.subscribeToMessage(this.hubName, (data) => this.onMessage(data));
  }

  public sendMessage() {
    this.signalR.sendMessage(this.hubName, this.textMessage);
    this.textMessage = '';
  }

  private onMessage(data: any) {
    if (data != null && data != '') {
      this.messages.unshift(data);
    }
  }
}
