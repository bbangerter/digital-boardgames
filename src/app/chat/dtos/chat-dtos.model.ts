export interface ChatMessage {
  playerName: string;
  message: string;
  serverMessage: boolean;
}
