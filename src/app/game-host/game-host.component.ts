import { Component, Input, ComponentFactoryResolver, AfterViewInit, ViewContainerRef } from '@angular/core';
import { GameComponentListService } from '../services/game-component-list.service';
import { GameStart, GameComponent } from '../core';

@Component({
  selector: 'app-game-host',
  templateUrl: './game-host.component.html',
  styleUrls: ['./game-host.component.scss']
})
export class GameHostComponent implements AfterViewInit {
  @Input() gameData: GameStart;

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private gameListService: GameComponentListService,
    private viewContainerRef: ViewContainerRef) { }

  ngAfterViewInit() {
    setTimeout(() => {
      this.loadComponent();
    }, 10);
  }

  loadComponent() {
    const gameItem = this.gameListService.getGameComponent(this.gameData.name);

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(gameItem);

    this.viewContainerRef.clear();

    const componentRef = this.viewContainerRef.createComponent(componentFactory);
    (<GameComponent>componentRef.instance).gameData = this.gameData;
  }
}
