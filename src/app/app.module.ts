import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

import { ChatComponent } from './chat/chat.component';
import { GameListComponent } from './game-list/game-list.component';
import { MasterControlComponent } from './master-control/master-control.component';
import { GameHostComponent } from './game-host/game-host.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSliderModule } from '@angular/material/slider';
import { FormsModule } from '@angular/forms';

import { OnenightModule } from './onenight';
import { SplendorModule } from './splendor/splendor.module';
import { PlaygroundModule } from './playground/playground.module';
import { TerraformingMarsModule } from './terraforming-mars/terraforming-mars.module';
import { MysteriumModule } from './mysterium/mysterium.module';
import { FavorOfThePharaohModule } from './favor-of-the-pharaoh/favor-of-the-pharaoh.module';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    GameListComponent,
    MasterControlComponent,
    GameHostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatTableModule,
    MatSelectModule,
    MatCardModule,
    MatTabsModule,
    MatSliderModule,
    FormsModule,
    CoreModule.forRoot(),
    OnenightModule,
    SplendorModule,
    PlaygroundModule,
    TerraformingMarsModule,
    MysteriumModule,
    FavorOfThePharaohModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
