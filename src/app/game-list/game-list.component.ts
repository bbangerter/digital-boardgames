import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { LobbyMessageType } from '../app-constants';
import { PlayerStateService, SignalRService, GameSessions, Game, GameStart } from '../core';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit, OnDestroy {
  availableGamesDisplayedColumns = ['Game', 'Players', 'Create'];
  waitingForPlayersDisplayedColumns = ['Game', 'Host', 'Players', 'Join'];
  games: Array<GameSessions> = [];
  availableGames: Array<Game> = [];
  private hubName: string = 'lobby';
  private groupName: string = 'Lobby';
  private lobbySubscription: Subscription;

  constructor(private signalR: SignalRService, private playerState: PlayerStateService/*, private cdr: ChangeDetectorRef*/) { }

  ngOnInit(): void {
    this.startLobby();
  }

  ngOnDestroy(): void {
    this.lobbySubscription.unsubscribe();
    this.signalR.close(this.hubName);
  }

  public createGame(game: string) {
    var message = this.lobbyMessage(LobbyMessageType.CreateGame, JSON.stringify({ Game: game }));
    this.signalR.sendMessage(this.hubName, message);
  }

  public joinGame(gameId: string) {
    var message = this.lobbyMessage(LobbyMessageType.JoinGame, JSON.stringify({ GameId: gameId }));
    this.signalR.sendMessage(this.hubName, message);
  }

  public cancelGame(gameId: string) {
    var message = this.lobbyMessage(LobbyMessageType.DestroyGame, JSON.stringify({ GameId: gameId }));
    this.signalR.sendMessage(this.hubName, message);
  }

  public leaveGame(gameId: string) {
    var message = this.lobbyMessage(LobbyMessageType.LeaveGame, JSON.stringify({ GameId: gameId }));
    this.signalR.sendMessage(this.hubName, message);
  }

  public startGame(gameId: string) {
    var message = this.lobbyMessage(LobbyMessageType.StartGame, JSON.stringify({ GameId: gameId }));
    this.signalR.sendMessage(this.hubName, message);
  }

  public canJoinGame(gameId: string): boolean {
    var game = this.games.find(g => g.gameId == gameId);
    var details = this.availableGames.find(a => a.name == game.gameName);
    if (details == null) return false;
    return game.players.length < details.maxPlayers;
  }

  public canStartGame(gameId: string): boolean {
    var game = this.games.find(g => g.gameId == gameId);
    var details = this.availableGames.find(a => a.name == game.gameName);
    return game.players.length >= details.minPlayers;
  }

  public ownsGame(): string {
    var playerId = this.playerState.getPlayerId();
    var game = this.games.find(g => g.creatorId == playerId);
    if (game === undefined) {
      return '';
    }

    return game.gameId;
  }

  public joinedGame(): string {
    var playerId = this.playerState.getPlayerId();
    var game = this.games.find(g => g.players.find(p => p.id == playerId));
    if (game === undefined) {
      return '';
    }

    return game.gameId;
  }

  public getPlayerList(gameId: string): string {
    var game = this.games.find(g => g.gameId == gameId);
    var players = game.players.map(p => p.name);
    return players.join(', ');
  }

  private lobbyMessage(type: number, message: string): Object {
    var lobbyMessage = {
      type: type,
      message: message
    };

    return lobbyMessage;
  }

  private gameStart(data: GameStart) {
    this.playerState.setActiveGame(data)
  }

  private startLobby() {
    this.signalR.connect(this.hubName, `PlayerName=${this.playerState.getPlayerName()}&GroupName=${this.groupName}&PlayerId=${this.playerState.getPlayerId()}`);
    this.lobbySubscription = this.signalR.subscribeToMessage(this.hubName, (data) => this.onMessage(data));
  }

  private onMessage(data: any) {
    if (data != null && data != '') {
      var message = JSON.parse(data.message);
      switch (data.type) {
        case LobbyMessageType.GameList:
          this.games = message.games;
          break;
        case LobbyMessageType.AvailableGames:
          this.availableGames = message.games;
          break;
        case LobbyMessageType.StartGame:
          this.gameStart(message);
          break;
      }

/*      this.cdr.detectChanges();*/
    }
  }
}
