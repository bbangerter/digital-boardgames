export const LobbyMessageType = {
  CreateGame: 0,
  JoinGame: 1,
  LeaveGame: 2,
  DestroyGame: 3,
  GameList: 4,
  StartGame: 5,
  EndGame: 6,
  AvailableGames: 7
}
