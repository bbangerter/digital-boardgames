import { TestBed } from '@angular/core/testing';

import { OneNightStateService } from './one-night-state.service';

describe('OneNightStateService', () => {
  let service: OneNightStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OneNightStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
