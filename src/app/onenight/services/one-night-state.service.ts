import { Injectable } from '@angular/core';
import { OneNightCard, OneNightSelectedCard, OneNightPlayerCard, OneNightPlayerMark, OneNightPlayerVote, OneNightPlayerData, OneNightAccuseRole, OneNightVoteTally, OneNightAccuseMark, OneNightArtifact } from '../dtos/one-night-dtos.model';
import { Subscription } from 'rxjs';
import { OneNightPhase, OneNightTeam, OneNightMessageType, OneNightMark, OneNightCardOrder, OneNightArtifacts } from '../one-night-constants';
import { PlayerStateService, SignalRService, GamePlayer, GameMessage, GameUtilsService } from '../../core';

@Injectable({
  providedIn: 'root'
})
export class OneNightStateService {
  allCards: Array<OneNightCard> = [];
  activePlayers: Array<GamePlayer> = [];
  useCard: Array<OneNightSelectedCard> = [];
  creatorId: string;
  discussionTime: number = 5;
  discussionTimeLeft: number;

  currentPhase: OneNightPhase;
  currentCardAction: OneNightCardOrder;        // the cardOrder action
  startingRoles: Array<OneNightPlayerCard>; // the roles players started with
  startingMarks: Array<OneNightPlayerMark>; // the marks players know they currently have
  currentRoles: Array<OneNightPlayerCard>;  // the current role each player has
  currentMarks: Array<OneNightPlayerMark>;  // the current marks given to players
  votes: Array<OneNightPlayerVote>;         // voting data when multiple players need to vote on something (vampires, and end game lynching)
  shieldToken: Array<string>;       // the player ids that the sentinel shield token was given to
  multiRoleDone: Array<string>;     // number of werewolves that have acknowledged they are done looking
  bumpedPlayer: Array<string>;      // the playerid of the player bumped by thing
  piTeam: OneNightTeam;                   // the team the PI is now on
  dopplegangerPiTeam: OneNightTeam;
  auras: Array<string>;             // list of player ids that have an aura for the aura seer
  revealed: Array<string>;          // player revealed by the revealer
  curated: Array<OneNightArtifact>; // players with artifacts placed on them
  copycat: OneNightCardOrder;                  // the role the copy cat has
  doppleganger: OneNightCardOrder;             // the role the doppleganger has
  claimedRole: Array<OneNightPlayerCard>;   // roles players claim to be
  otherClaimedRole: Array<OneNightAccuseRole>;  // role other players claim a target to be
  claimedMark: Array<OneNightPlayerMark>;   // marks claimed by players
  otherClaimedMark: Array<OneNightAccuseMark>;
  voteTally: Array<OneNightVoteTally>;

  dopplegangerTurn: boolean;

  private gameSubscription: Subscription;
  private hubName: string = 'game';

  constructor(private signalR: SignalRService, private playerState: PlayerStateService, private gameUtils: GameUtilsService) {
    this.initializeCards();
    for (var i = 0; i < this.allCards.length; i++) {
      this.useCard.push(new OneNightSelectedCard(false, this.allCards[i].minCount));
    }
    this.resetGame();
  }

  public startGameHub(playerName: string, groupName: string, playerId: string) {
    this.signalR.connect(this.hubName, `PlayerName=${playerName}&GroupName=${groupName}&PlayerId=${playerId}`);
    this.gameSubscription = this.signalR.subscribeToMessage(this.hubName, (data) => this.onMessage(data));
  }

  public closeGameHub() {
    this.gameSubscription.unsubscribe();
    this.signalR.close(this.hubName);
  }

  public updateRoles() {
    this.useCard = this.useCard.map<OneNightSelectedCard>((card, index) =>
      new OneNightSelectedCard(card.use, Math.max(Math.min(parseInt(card.count.toString()), this.allCards[index].maxCount), this.allCards[index].minCount)));
    this.sendGameMessage(OneNightMessageType.RoleSelect, this.useCard);
  }

  public updateDiscussionTime() {
    this.sendGameMessage(OneNightMessageType.DiscussionTime, this.discussionTime);
  }

  public getAllPlayers(includeSelf: boolean): Array<OneNightPlayerData> {
    return this.buildPlayerData(this.activePlayers.map((p) => p.id)).filter(p => p.Id != this.playerState.getPlayerId() || includeSelf);
  }

  public buildPlayerData(playerIds: Array<string>): Array<OneNightPlayerData> {
    var players = new Array<OneNightPlayerData>();
    for (var i = 0; i < playerIds.length; i++) {
      var role = this.currentRoles.find(p => p.playerId == playerIds[i]);
      var mark = this.currentMarks.find(m => m.playerId == playerIds[i]);
      players.push(new OneNightPlayerData(playerIds[i], role.playerName, role.card, mark != null ? mark.mark : -1));
    }

    return players;
  }

  public sendVotes() {
    this.sendGameMessage(OneNightMessageType.UpdateVote, this.votes);
  }

  public advancePhase() {
    if (this.currentPhase == OneNightPhase.Setup) {
      this.resetGame();
    }
    this.currentPhase++;
    switch (this.currentPhase) {
      case OneNightPhase.Shuffle:
        this.shuffle();
        break;
      case OneNightPhase.DayActions:
        this.playNextAction();
        break;
      case OneNightPhase.ReviewMarks:
        this.startingMarks = JSON.parse(JSON.stringify(this.currentMarks));
        this.advancePhase();
        break;
      case OneNightPhase.NightActions:
        if (!this.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Sentinel)) {
          this.playNextAction();
        }
        else if (this.shouldAutoAdvance()) {
          setTimeout(() => this.sendAdvanceAction(), Math.random() * 7000 + 3000);
        }
        break;
      case OneNightPhase.DetermineWinner:
        this.determineWinners();
        break;
      case OneNightPhase.Reset:
        this.currentPhase = OneNightPhase.Setup;
        this.sendGameMessage(OneNightMessageType.ResetGame, this.currentPhase);
        break;
    }
  }

  private getCurrentVampires(): Array<OneNightPlayerCard> {
    return this.currentRoles.filter(r => this.isVampire(r));
  }

  private getCurrentWerewolves(): Array<OneNightPlayerCard> {
    return this.currentRoles.filter(r => this.isWerewolf(r));
  }

  private determineWinners() {
    // tally votes
    for (var i = 0; i < this.activePlayers.length; i++) {
      this.voteTally.push(new OneNightVoteTally(this.activePlayers[i].id, this.votes.filter(v => v.targetId == this.activePlayers[i].id).length, false, false, false));
    }

    this.voteTally.sort((a, b) => b.count - a.count);

    var killCount = 1;
    if (this.getCurrentWerewolves().length > 0 && this.getCurrentVampires().length > 0) {
      killCount++;
    }

    var bodyguards = this.currentRoles.filter(r => this.isRole(r.playerId, r.card, OneNightCardOrder.Bodyguard));
    var vampires = this.getCurrentVampires();
    var killedPlayers = new Array<OneNightPlayerCard>();
    var index = 0;
    while (killedPlayers.length < killCount || index > 0 && this.voteTally[index].count == this.voteTally[index - 1].count) {
      var target = this.voteTally[index];
      var role = this.currentRoles.find(r => r.playerId == target.playerId);

      if (this.isProtected(bodyguards, vampires, target, role)) {
        target.unkillable = true;
        index++;
      }
      else {
        target.killed = true;
        killedPlayers.push(this.currentRoles.find(r => r.playerId == target.playerId));
      }
    }

    var killCount = killedPlayers.length;
    do {
      // if any killed players were hunters, kill their targets as well
      this.addHunterTargets(killedPlayers, bodyguards, vampires);

      // kill any lovers
      this.addLoverTargets(killedPlayers);
    } while (killCount != killedPlayers.length);

    if (this.voteTally[index].count <= 1) {
      for (var i = 0; i < this.voteTally.length; i++) {
        // TODO: only if no players on non-villager teams
        this.voteTally[i].victory = true;
      }
    }

    this.tannerVictories(killedPlayers);
    this.assassinVictories(killedPlayers);
    this.teamVictories(killCount, killedPlayers);

    // mark players who pointed at a diseased target as non-winners
    var losers = this.votes.filter(v => this.currentRoles.some(r => r.playerId == v.targetId && r.card.cardOrder == OneNightCardOrder.Diseased) ||
      this.currentMarks.some(m => m.playerId == v.targetId && m.mark == OneNightMark.Disease));
    this.voteTally.filter(t => losers.some(p => p.playerId == t.playerId)).forEach((t) => t.victory = false);
  }

  private teamVictories(killCount: number, killedPlayers: Array<OneNightPlayerCard>) {
    var killedVampires = killedPlayers.some(k => this.isRole(k.playerId, k.card, OneNightCardOrder.Vampire) || this.isRole(k.playerId, k.card, OneNightCardOrder.Master) ||
      this.isRole(k.playerId, k.card, OneNightCardOrder.Count));
    var killedWerewolves = killedPlayers.some(k => this.isRole(k.playerId, k.card, OneNightCardOrder.Werewolf) || this.isRole(k.playerId, k.card, OneNightCardOrder.AlphaWolf) ||
      this.isRole(k.playerId, k.card, OneNightCardOrder.MysticWolf) || this.isRole(k.playerId, k.card, OneNightCardOrder.Dreamwolf));
    var killedVillagers = killedPlayers.some(k => this.getPlayerTeam(k.playerId) == OneNightTeam.Villager);

    var traitors = this.currentRoles.filter(r => this.isTraitor(r.playerId));
    if (killCount == 1 && (killedVampires || killedWerewolves) || killCount == 2 && killedVampires && killedWerewolves) {
      this.markVillagerWins();
      traitors.forEach((t) => {
        if (this.getPlayerTeam(t.playerId) == OneNightTeam.Villager) {
          this.voteTally.find(v => v.playerId == t.playerId).victory = false;
        }
      });
    }

    if (this.getCurrentVampires().length > 0 && (killCount == 1 && !killedVampires || killCount == 2 && killedWerewolves && killedVillagers)) {
      this.markVampireWins();
      traitors.forEach((t) => {
        if (this.getPlayerTeam(t.playerId) == OneNightTeam.Vampire || this.getPlayerTeam(t.playerId) == OneNightTeam.VampireAlly) {
          this.voteTally.find(v => v.playerId == t.playerId).victory = false;
        }
      });
    }

    if (this.getCurrentWerewolves().length > 0 && (killCount == 1 && !killedWerewolves || killCount == 2 && killedVampires && killedVillagers)) {
      this.markWerewolfWins();
      traitors.forEach((t) => {
        if (this.getPlayerTeam(t.playerId) == OneNightTeam.Werewolf || this.getPlayerTeam(t.playerId) == OneNightTeam.WerewolfAlly) {
          this.voteTally.find(v => v.playerId == t.playerId).victory = false;
        }
      });
    }

    this.traitorVictories(traitors, killedVampires, killedWerewolves, killedVillagers);
  }

  private traitorVictories(traitors: OneNightPlayerCard[], killedVampires: boolean, killedWerewolves: boolean, killedVillagers: boolean) {
    // mark traitor victories
    if (killedVampires) {
      traitors.forEach((t) => {
        if (this.getPlayerTeam(t.playerId) == OneNightTeam.Vampire || this.getPlayerTeam(t.playerId) == OneNightTeam.VampireAlly) {
          this.voteTally.find(v => v.playerId == t.playerId).victory = true;
        }
      });
    }

    if (killedWerewolves) {
      traitors.forEach((t) => {
        if (this.getPlayerTeam(t.playerId) == OneNightTeam.Werewolf || this.getPlayerTeam(t.playerId) == OneNightTeam.WerewolfAlly) {
          this.voteTally.find(v => v.playerId == t.playerId).victory = true;
        }
      });
    }

    if (killedVillagers) {
      traitors.forEach((t) => {
        if (this.getPlayerTeam(t.playerId) == OneNightTeam.Villager) {
          this.voteTally.find(v => v.playerId == t.playerId).victory = true;
        }
      });
    }
  }

  private assassinVictories(killedPlayers: OneNightPlayerCard[]) {
    // mark assassin wins
    if (killedPlayers.some(k => this.currentMarks.some(m => m.playerId == k.playerId && m.mark == OneNightMark.Assassin))) {
      this.getPlayersWithRole(OneNightCardOrder.Assassin).forEach((r) => {
        this.voteTally.find(v => v.playerId == r.playerId).victory = true;
      });

      // if no assassins, mark appr assassin wins if marked targets were killed
      if (this.getPlayersWithRole(OneNightCardOrder.Assassin).length == 0) {
        this.getPlayersWithRole(OneNightCardOrder.ApprAssassin).forEach((r) => {
          this.voteTally.find(v => v.playerId == r.playerId).victory = true;
        });
      }
    }

    // mark appr assassin wins if any assassin died
    if (killedPlayers.some(k => this.isRole(k.playerId, k.card, OneNightCardOrder.Assassin))) {
      this.getPlayersWithRole(OneNightCardOrder.ApprAssassin).forEach((r) => {
        this.voteTally.find(v => v.playerId == r.playerId).victory = true;
      });
    }
  }

  private tannerVictories(killedPlayers: OneNightPlayerCard[]) {
    // mark dead tanners as winners
    killedPlayers.forEach((k) => {
      if (this.isRole(k.playerId, k.card, OneNightCardOrder.Tanner)) {
        this.voteTally.find(v => v.playerId == k.playerId).victory = true;
      }
    });

    // mark appr tanner winners if any tanners died
    if (killedPlayers.some(k => this.isRole(k.playerId, k.card, OneNightCardOrder.Tanner))) {
      this.getPlayersWithRole(OneNightCardOrder.ApprTanner).forEach((r) => {
        this.voteTally.find(v => v.playerId == r.playerId).victory = true;
      });
    }
    else if (this.getPlayersWithRole(OneNightCardOrder.Tanner).length == 0) {  // no tanners
      killedPlayers.forEach((k) => {
        if (this.isRole(k.playerId, k.card, OneNightCardOrder.ApprTanner)) {
          this.voteTally.find(t => t.playerId == k.playerId).victory = true;
        }
      });
    }
  }

  private addLoverTargets(killedPlayers: OneNightPlayerCard[]) {
    var lovers = this.currentMarks.filter(m => m.mark == OneNightMark.Love && killedPlayers.some(k => k.playerId == m.playerId));
    lovers.forEach((l) => {
      if (!killedPlayers.some(k => k.playerId == l.playerId)) {
        this.voteTally.find(t => t.playerId == l.playerId).killed = true;
        killedPlayers.push(this.currentRoles.find(r => r.playerId == l.playerId));
      }
    });
  }

  private addHunterTargets(killedPlayers: OneNightPlayerCard[], bodyguards: OneNightPlayerCard[], vampires: OneNightPlayerCard[]) {
    var killedHunters = killedPlayers.filter(p => this.isRole(p.playerId, p.card, OneNightCardOrder.Hunter));
    if (killedHunters.length > 0) {
      var hunterTargets = this.votes.filter(v => killedHunters.some(h => h.playerId == v.playerId));
      hunterTargets.forEach((t) => {
        var hunterTarget = this.currentRoles.find(r => r.playerId == t.targetId);
        var targetTally = this.voteTally.find(v => v.playerId == t.targetId);
        if (!this.isProtected(bodyguards, vampires, targetTally, hunterTarget) && !killedPlayers.some(p => p.playerId == t.targetId)) {
          killedPlayers.push(this.currentRoles.find(r => r.playerId == t.targetId));
          this.voteTally.find(v => v.playerId == t.targetId).killed = true;
        }
      });
    }
  }

  private isVampire(player: OneNightPlayerCard) {
    if (player.card.cardOrder >= OneNightCardOrder.Vampire && player.card.cardOrder <= OneNightCardOrder.Count ||
      player.card.cardOrder == OneNightCardOrder.Copycat && this.copycat >= OneNightCardOrder.Vampire && this.copycat <= OneNightCardOrder.Count ||
      player.card.cardOrder == OneNightCardOrder.Doppleganger && this.doppleganger >= OneNightCardOrder.Vampire && this.doppleganger <= OneNightCardOrder.Count ||
      this.currentMarks.some(m => m.playerId == player.playerId && m.mark == OneNightMark.Vampire)) {
      return true;
    }

    return this.curated.some(a => a.playerId == player.playerId && a.artifact == OneNightArtifacts.Vampire);
  }

  private isWerewolf(player: OneNightPlayerCard): boolean {
    if (player.card.cardOrder >= OneNightCardOrder.Werewolf && player.card.cardOrder <= OneNightCardOrder.Dreamwolf ||
      player.card.cardOrder == OneNightCardOrder.Copycat && this.copycat >= OneNightCardOrder.Werewolf && this.copycat <= OneNightCardOrder.Dreamwolf ||
      player.card.cardOrder == OneNightCardOrder.Doppleganger && this.doppleganger >= OneNightCardOrder.Werewolf && this.doppleganger <= OneNightCardOrder.Dreamwolf) {
      return true;
    }

    return this.curated.some(a => a.playerId == player.playerId && a.artifact == OneNightArtifacts.Werewolf);
  }

  private markVillagerWins() {
    this.voteTally.forEach((t) => {
      if (this.getPlayerTeam(t.playerId) == OneNightTeam.Villager) {
        t.victory = true;
      }
    });
  }

  private markVampireWins() {
    this.voteTally.forEach((t) => {
      if (this.getPlayerTeam(t.playerId) == OneNightTeam.Vampire || this.getPlayerTeam(t.playerId) == OneNightTeam.VampireAlly) {
        t.victory = true;
      }
    });
  }

  private markWerewolfWins() {
    this.voteTally.forEach((t) => {
      if (this.getPlayerTeam(t.playerId) == OneNightTeam.Werewolf || this.getPlayerTeam(t.playerId) == OneNightTeam.WerewolfAlly) {
        t.victory = true;
      }
    });
  }

  private getPlayersWithRole(role: number): Array<OneNightPlayerCard> {
    return this.currentRoles.filter(r => this.isRole(r.playerId, r.card, role) && this.activePlayers.some(p => p.id == r.playerId));
  }

  private getPlayerTeam(playerId: string): OneNightTeam {
    var role = this.currentRoles.find(r => r.playerId == playerId);
    var mark = this.currentMarks.find(m => m.playerId == playerId);

    var team = role.card.team;

    if (role.card.team == OneNightTeam.Varies) {
      team = this.getVariedTeam(playerId, role.card);
    }

    if (this.curated.some(a => a.playerId == playerId && (a.artifact == OneNightArtifacts.Bodyguard || a.artifact == OneNightArtifacts.Hunter || a.artifact == OneNightArtifacts.Prince ||
      a.artifact == OneNightArtifacts.Villager))) {
      team = OneNightTeam.Villager;
    }

    if (this.curated.some(a => a.playerId == playerId && a.artifact == OneNightArtifacts.Tanner)) {
      team = OneNightTeam.Tanner;
    }

    if (this.curated.some(a => a.playerId == playerId && a.artifact == OneNightArtifacts.Vampire)) {
      team = OneNightTeam.Vampire;
    }

    if (this.curated.some(a => a.playerId == playerId && a.artifact == OneNightArtifacts.Werewolf)) {
      team = OneNightTeam.Werewolf;
    }

    if (mark.mark == OneNightMark.Vampire) {
      team = OneNightTeam.Vampire;
    }

    return team;
  }

  private getVariedTeam(playerId: string, card: OneNightCard): number {
    if (card.cardOrder == OneNightCardOrder.Copycat) {
      var newCard = this.allCards[this.copycat];
      return this.getPlayerTeam(playerId);
    }

    if (card.cardOrder == OneNightCardOrder.Doppleganger) {
      var newCard = this.allCards[this.doppleganger];
      if (newCard.cardOrder == OneNightCardOrder.ParanormalInvestigator && this.dopplegangerPiTeam != -1) {
        return this.dopplegangerPiTeam;
      }
      else if (newCard.cardOrder != OneNightCardOrder.Copycat) {
        return this.getVariedTeam(playerId, newCard);
      }
    }

    if (card.cardOrder == OneNightCardOrder.ParanormalInvestigator) {
      if (this.piTeam != -1) {
        return this.piTeam;
      }
    }

    if (card.cardOrder == OneNightCardOrder.Cursed) {
      var votes = this.votes.filter(v => v.targetId == playerId);
      if (votes.some(v => this.isRole(v.playerId, this.currentRoles.find(r => r.playerId == v.playerId).card, OneNightCardOrder.Werewolf) ||
        this.isRole(v.playerId, this.currentRoles.find(r => r.playerId == v.playerId).card, OneNightCardOrder.AlphaWolf) ||
        this.isRole(v.playerId, this.currentRoles.find(r => r.playerId == v.playerId).card, OneNightCardOrder.MysticWolf) ||
        this.isRole(v.playerId, this.currentRoles.find(r => r.playerId == v.playerId).card, OneNightCardOrder.Dreamwolf))) {
        return OneNightTeam.Werewolf;
      }
    }

    return OneNightTeam.Villager;
  }

  public isRole(playerId: string, card: OneNightCard, role: OneNightCardOrder) {
    if (this.curated.some(a => a.playerId == playerId)) {
      var artifact = this.curated.find(a => a.playerId == playerId);
      if (artifact.artifact == OneNightArtifacts.Bodyguard) {
        return role == OneNightCardOrder.Bodyguard;
      }

      if (artifact.artifact == OneNightArtifacts.Hunter) {
        return role == OneNightCardOrder.Hunter;
      }

      if (artifact.artifact == OneNightArtifacts.Prince) {
        return role == OneNightCardOrder.Prince;
      }

      if (artifact.artifact == OneNightArtifacts.Tanner) {
        return role == OneNightCardOrder.Tanner;
      }

      if (artifact.artifact == OneNightArtifacts.Villager) {
        return role == OneNightCardOrder.Villager;
      }

      if (artifact.artifact == OneNightArtifacts.Vampire) {
        return role == OneNightCardOrder.Vampire;
      }

      if (artifact.artifact == OneNightArtifacts.Werewolf) {
        return role == OneNightCardOrder.Werewolf;
      }
    }

    if (card.cardOrder == role ||
      card.cardOrder == OneNightCardOrder.Copycat && this.copycat == role ||
      card.cardOrder == OneNightCardOrder.Doppleganger && this.doppleganger == role) {
      return true;
    }

    return false;
  }

  private isTraitor(playerId: string): boolean {
    return this.currentMarks.some(m => m.playerId == playerId && m.mark == OneNightMark.Traitor) || this.curated.some(a => a.playerId == playerId && a.artifact == OneNightArtifacts.Traitor);
  }

  public isProtected(bodyguards: Array<OneNightPlayerCard>, vampires: Array<OneNightPlayerCard>, target: OneNightVoteTally, role: OneNightPlayerCard): boolean {
    // protected by bodyguard
    if (this.votes.some(v => bodyguards.some(b => b.playerId == v.playerId) && v.targetId == target.playerId)) {
      return true;
    }

    // protected as prince
    if (this.isRole(role.playerId, role.card, OneNightCardOrder.Prince)) {
      return true;
    }

    // protected as master
    if (this.isRole(role.playerId, role.card, OneNightCardOrder.Master) && this.votes.some(v => vampires.some(vampire => v.playerId == vampire.playerId && v.targetId == target.playerId))) {
      return true;
    }

    return false;
  }

  public playNextAction() {
    this.votes = [];
    this.multiRoleDone = [];

    if (this.dopplegangerTurn) {
      this.dopplegangerTurn = false;
      this.currentCardAction++;
    }
    else if (this.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Doppleganger)) {
      if (this.currentCardAction == OneNightCardOrder.Count || this.currentCardAction == OneNightCardOrder.Renfield || this.currentCardAction == OneNightCardOrder.Priest ||
        this.currentCardAction == OneNightCardOrder.ApprAssassin || this.currentCardAction == OneNightCardOrder.Assassin || this.currentCardAction == OneNightCardOrder.Marksman ||
        this.currentCardAction == OneNightCardOrder.Pickpocket || this.currentCardAction == OneNightCardOrder.AuraSeer || this.currentCardAction == OneNightCardOrder.Gremlin ||
        this.currentCardAction == OneNightCardOrder.Insomniac || this.currentCardAction == OneNightCardOrder.Squire || this.currentCardAction == OneNightCardOrder.Beholder ||
        this.currentCardAction == OneNightCardOrder.Revealer || this.currentCardAction == OneNightCardOrder.Curator) {
        this.dopplegangerTurn = true;
      }
      else {
        this.currentCardAction++;
      }
    }
    else {
      this.currentCardAction++;
    }

    while (this.currentCardAction < this.allCards.length && !this.startingRoles.some(c => c.card.cardOrder == this.currentCardAction && c.card.hasAction)) {
      if (this.currentCardAction == OneNightCardOrder.Vampire && this.getVampires().length > 0) {
        break;
      }
      this.currentCardAction++;
      if (this.currentCardAction == OneNightCardOrder.Sentinel) // sentinal, first night phase role
      {
        this.advancePhase();
        return;
      }
    }

    if (this.currentCardAction >= this.allCards.length) {
      this.advancePhase();
    }

    if (this.shouldAutoAdvance()) {
      setTimeout(() => this.sendAdvanceAction(), Math.random() * 7000 + 3000);
    }
  }

  public getAdjacentPlayers(): Array<GamePlayer> {
    var player = this.activePlayers.findIndex(p => p.id == this.playerState.getPlayerId());
    var playerIds = new Array<string>();

    if (player == 0) {
      playerIds.push(this.activePlayers[this.activePlayers.length - 1].id);
    }
    else {
      playerIds.push(this.activePlayers[player - 1].id);
    }

    if (player == this.activePlayers.length - 1) {
      playerIds.push(this.activePlayers[0].id);
    }
    else {
      playerIds.push(this.activePlayers[player + 1].id);
    }

    return this.activePlayers.filter(p => playerIds.some(i => i == p.id));
  }

  private shouldAutoAdvance(): boolean {
    if (this.isGameCreator()) {
      if (this.dopplegangerTurn && this.doppleganger == this.currentCardAction) {
        return false;
      }

      var currentActionRole = this.startingRoles.find(r => r.card.cardOrder == this.currentCardAction || r.card.cardOrder == OneNightCardOrder.Copycat && this.copycat == this.currentCardAction);
      if (currentActionRole == null) {
        return false;
      }
      var isActivePlayer = this.activePlayers.some(p => p.id == currentActionRole.playerId) || (this.currentCardAction == OneNightCardOrder.Vampire && this.getVampires().length > 0 ||
        this.currentCardAction == OneNightCardOrder.Werewolf && this.getWerewolves(false).length > 0 ||
        this.currentCardAction == OneNightCardOrder.Mason && this.getMasons().length > 0);
      if (!isActivePlayer) return true;

      // no non-vampires to vote for
      if (this.currentCardAction == OneNightCardOrder.Vampire || this.currentCardAction == OneNightCardOrder.Count) {
        var extra = this.currentMarks.some(m => m.mark == OneNightMark.Vampire);
        if (this.getVampires().length + (extra ? 1 : 0) >= this.activePlayers.length) return true;
      }
    }

    return false;
  }

  public isGameCreator() {
    return this.creatorId == this.playerState.getPlayerId();
  }

  public sendAdvanceAction() {
    this.sendGameMessage(OneNightMessageType.AdvanceAction, '');
  }

  public sendGameMessage(type: OneNightMessageType, message: any) {
    var gameMessage = new GameMessage(type, JSON.stringify(message), this.playerState.getPlayerId());
    this.signalR.sendMessage(this.hubName, gameMessage);
  }

  private onMessage(data: GameMessage) {
    if (data != null) {
      var message = JSON.parse(data.message);
      switch (data.type) {
        case OneNightMessageType.UpdatePlayerList:
          this.activePlayers = message;
          break;
        case OneNightMessageType.RoleSelect:
          this.useCard = message;
          break;
        case OneNightMessageType.ShuffledDeck:
          this.startingRoles = message;
          this.currentRoles = JSON.parse(data.message);
          if (this.currentPhase < OneNightPhase.Shuffle) {
            this.currentPhase = OneNightPhase.Shuffle;
          }
          this.advancePhase();
          break;
        case OneNightMessageType.PlayerStartingMarks:
          this.startingMarks = message;
          break;
        case OneNightMessageType.PlayerCurrentMarks:
          this.currentMarks = message;
          break;
        case OneNightMessageType.AdvanceAction:
          this.playNextAction();
          break;
        case OneNightMessageType.UpdateVote:
          this.votes = message;
          this.processVotes();
          break;
        case OneNightMessageType.ShieldToken:
          this.shieldToken = message;
          break;
        case OneNightMessageType.MultiRoleDone:
          this.processMultiRole(message);
          break;
        case OneNightMessageType.SwapCards:
          this.currentRoles = message;
          break;
        case OneNightMessageType.BumpPlayer:
          this.bumpedPlayer = message;
          break;
        case OneNightMessageType.PITeam:
          this.piTeam = message;
          break;
        case OneNightMessageType.DopplegangerPITeam:
          this.dopplegangerPiTeam = message;
          break;
        case OneNightMessageType.SwapMarks:
          this.currentMarks = message;
          break;
        case OneNightMessageType.AddAura:
          this.auras = message;
          break;
        case OneNightMessageType.Revealed:
          this.revealed = message;
          break;
        case OneNightMessageType.Copycat:
          this.copycat = message;
          break;
        case OneNightMessageType.Doppleganger:
          this.doppleganger = message;
          break;
        case OneNightMessageType.DiscussionTime:
          this.discussionTime = parseInt(message);
          break;
        case OneNightMessageType.Claim:
          this.claimedRole = message;
          break;
        case OneNightMessageType.OtherClaim:
          this.otherClaimedRole = message;
          break;
        case OneNightMessageType.DiscussionTimer:
          if (!this.isGameCreator()) {
            this.discussionTimeLeft = message;
            if (this.discussionTimeLeft <= 0) {
              this.advancePhase();
            }
          }
          break;
        case OneNightMessageType.ClaimMark:
          this.claimedMark = message;
          break;
        case OneNightMessageType.OtherClaimMark:
          this.otherClaimedMark = message;
          break;
        case OneNightMessageType.ResetGame:
          this.currentPhase = message;
          this.resetGame();
          break;
        case OneNightMessageType.Artifacts:
          this.curated = message;
          break;
      }
    }
  }

  public addAura(playerId: string) {
    if (!this.auras.some(a => a == playerId)) {
      this.auras.push(playerId);
      this.sendGameMessage(OneNightMessageType.AddAura, this.auras);
    }
  }

  public swapCards(card1: string, card2: string) {
    var role1 = this.currentRoles.findIndex(r => r.playerId == card1);
    var role2 = this.currentRoles.findIndex(r => r.playerId == card2);
    var card = this.currentRoles[role1].card;
    this.currentRoles[role1].card = this.currentRoles[role2].card;
    this.currentRoles[role2].card = card;
    this.sendGameMessage(OneNightMessageType.SwapCards, this.currentRoles);
  }

  public swapMarks(mark1: string, mark2: string) {
    var role1 = this.currentMarks.findIndex(r => r.playerId == mark1);
    var role2 = this.currentMarks.findIndex(r => r.playerId == mark2);
    var mark = this.currentMarks[role1].mark;
    this.currentMarks[role1].mark = this.currentMarks[role2].mark;
    this.currentMarks[role2].mark = mark;
    this.sendGameMessage(OneNightMessageType.SwapMarks, this.currentMarks);
  }

  private processMultiRole(playerId: string) {
    if (this.isGameCreator() && !this.multiRoleDone.some(w => w == playerId)) {
      this.multiRoleDone.push(playerId);
      if (this.currentCardAction == OneNightCardOrder.Werewolf) {
        var werewolfCount = this.getWerewolves(false).length;
        if (this.multiRoleDone.length == werewolfCount) {
          this.sendAdvanceAction();
        }
      }
      if (this.currentCardAction == OneNightCardOrder.Mason) {
        var masonCount = this.getMasons().length;
        if (this.multiRoleDone.length == masonCount) {
          this.sendAdvanceAction();
        }
      }
    }
  }

  public getVampires(): Array<string> {
    return this.startingRoles.filter(r => (r.card.cardOrder >= OneNightCardOrder.Vampire && r.card.cardOrder <= OneNightCardOrder.Count ||
      r.card.cardOrder == OneNightCardOrder.Copycat && this.copycat >= OneNightCardOrder.Vampire && this.copycat <= OneNightCardOrder.Count ||
      r.card.cardOrder == OneNightCardOrder.Doppleganger && this.doppleganger >= OneNightCardOrder.Vampire && this.doppleganger <= OneNightCardOrder.Count) &&
      this.activePlayers.some(p => p.id == r.playerId)).map((r) => r.playerId);
  }

  public getWerewolves(includeDreamWolf: boolean): Array<string> {
    var fearMark = this.startingMarks.find(m => m.mark == OneNightMark.Fear);
    if (fearMark == null) fearMark = new OneNightPlayerMark('', '', 0);
    return this.startingRoles.filter(r => (r.card.cardOrder >= OneNightCardOrder.Werewolf && r.card.cardOrder <= (OneNightCardOrder.MysticWolf + (includeDreamWolf ? 1 : 0)) ||
      r.card.cardOrder == OneNightCardOrder.Copycat && this.copycat >= OneNightCardOrder.Werewolf && this.copycat <= (OneNightCardOrder.MysticWolf + (includeDreamWolf ? 1 : 0)) ||
      r.card.cardOrder == OneNightCardOrder.Doppleganger && this.doppleganger >= OneNightCardOrder.Werewolf && this.doppleganger <= (OneNightCardOrder.MysticWolf + (includeDreamWolf ? 1 : 0))) &&
      r.playerId != fearMark.playerId && this.activePlayers.some(p => p.id == r.playerId)).map((r) => r.playerId);
  }

  public getMasons(): Array<string> {
    var fearMark = this.startingMarks.find(m => m.mark == OneNightMark.Fear);
    if (fearMark == null) fearMark = new OneNightPlayerMark('', '', 0);
    return this.startingRoles.filter(r => r.card.cardOrder == OneNightCardOrder.Mason && r.playerId != fearMark.playerId &&
      this.activePlayers.some(p => p.id == r.playerId)).map((r) => r.playerId);
  }

  public getCenterCards(includeWolf: boolean): Array<OneNightPlayerData> {
    var centerCards = this.currentRoles.filter(r => r.playerId == 'Center 1' || r.playerId == 'Center 2' || r.playerId == 'Center 3' || includeWolf && r.playerId == 'Extra Werewolf').map((p) => p.playerId);
    return this.buildPlayerData(centerCards);
  }

  public getPlayerCards(): Array<OneNightPlayerCard> {
    return this.currentRoles.filter(r => r.playerId != 'Center 1' && r.playerId != 'Center 2' && r.playerId != 'Center 3' && r.playerId != 'Extra Werewolf');
  }

  public getPlayerMarks(): Array<OneNightPlayerMark> {
    return this.currentMarks;
  }

  public markToString(mark: OneNightMark): string {
    switch (mark) {
      case OneNightMark.Assassin:
        return "Assassin";
      case OneNightMark.Bat:
        return "Bat";
      case OneNightMark.Clarity:
        return "Clarity";
      case OneNightMark.Disease:
        return "Disease";
      case OneNightMark.Fear:
        return "Fear";
      case OneNightMark.Love:
        return "Love";
      case OneNightMark.Traitor:
        return "Traitor";
      case OneNightMark.Vampire:
        return "Vampire";
    }
  }

  public artifactToString(artifact: OneNightArtifacts): string {
    switch (artifact) {
      case OneNightArtifacts.Nothing:
        return 'Void of Nothingness';
      case OneNightArtifacts.Shame:
        return 'Shroud of Shame';
      case OneNightArtifacts.Muting:
        return 'Mask of Muting';
      case OneNightArtifacts.Werewolf:
        return 'Claw of the Werewolf';
      case OneNightArtifacts.Villager:
        return 'Brand of the Villager';
      case OneNightArtifacts.Tanner:
        return 'Cudgel of the Tanner';
      case OneNightArtifacts.Hunter:
        return 'Bow of the Hunter';
      case OneNightArtifacts.Prince:
        return 'Cloak of the Prince';
      case OneNightArtifacts.Bodyguard:
        return 'Sword of the Bodyguard';
      case OneNightArtifacts.Vampire:
        return 'Mist of the Vampire';
      case OneNightArtifacts.Traitor:
        return 'Dagger of the Traitor';
    }
  }

  private processVotes() {
    if (this.isGameCreator()) {
      if (this.currentCardAction == OneNightCardOrder.Vampire) {
        var voteCompleted = this.votes.every(v => v.targetId == this.votes[0].targetId) && this.getVampires().length == this.votes.length;
        if (voteCompleted) {
          // give mark of the vampire to the target
          var targetIndex = this.currentMarks.findIndex(m => m.playerId == this.votes[0].targetId);
          this.currentMarks[targetIndex].mark = OneNightMark.Vampire;
          this.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.currentMarks);
          this.sendAdvanceAction();
        }
      }
    }

    if (this.currentPhase == OneNightPhase.Vote) {
      var voteCompleted = this.votes.length == this.activePlayers.length;
      if (voteCompleted) {
        this.advancePhase();
      }
    }
  }

  private resetGame() {
    this.currentPhase = OneNightPhase.Setup;
    this.startingRoles = [];
    this.startingMarks = [];
    this.currentMarks = [];
    this.votes = [];
    this.shieldToken = [];
    this.bumpedPlayer = [];
    this.piTeam = -1;
    this.dopplegangerPiTeam = -1;
    this.multiRoleDone = [];
    this.currentCardAction = -1;
    this.auras = [];
    this.revealed = [];
    this.curated = [];
    this.copycat = -1;
    this.doppleganger = -1;
    this.dopplegangerTurn = false;
    this.claimedRole = [];
    this.claimedMark = [];
    this.otherClaimedRole = [];
    this.otherClaimedMark = [];
    this.voteTally = [];
    this.discussionTimeLeft = this.discussionTime * 60;
  }

  private shuffle() {
    // add cards to deck
    var deck = new Array<OneNightCard>();
    for (var i = 0; i < this.useCard.length; i++) {
      if (this.useCard[i].use) {
        for (var j = 0; j < this.useCard[i].count; j++) {
          deck.push(this.allCards[i]);
        }
      }
    }

    var skipShuffle = false;
    if (!skipShuffle) {
      this.gameUtils.shuffle(deck);
    }

    // assign to players
    for (var i = 0; i < this.activePlayers.length; i++) {
      this.startingRoles.push(new OneNightPlayerCard(this.activePlayers[i].id, this.activePlayers[i].name, deck[i]));
      this.startingMarks.push(new OneNightPlayerMark(this.activePlayers[i].id, this.activePlayers[i].name, OneNightMark.Clarity));
      this.currentMarks.push(new OneNightPlayerMark(this.activePlayers[i].id, this.activePlayers[i].name, OneNightMark.Clarity));
    }

    // assign extra cards to the middle
    for (var i = this.activePlayers.length; i < this.activePlayers.length + 3; i++) {
      this.startingRoles.push(new OneNightPlayerCard(`Center ${i - this.activePlayers.length + 1}`, `Center ${i - this.activePlayers.length + 1}`, deck[i]));
    }

    // add an extra werewolf card if alpha wolf is in play
    if (deck.some(c => c.cardName == 'Alpha Wolf')) {
      this.startingRoles.push(new OneNightPlayerCard('Extra Werewolf', 'Extra Werewolf', this.allCards.find(c => c.cardName == 'Werewolf')));
    }

    this.sendGameMessage(OneNightMessageType.ShuffledDeck, this.startingRoles);
    this.sendGameMessage(OneNightMessageType.PlayerStartingMarks, this.startingMarks);
    this.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.currentMarks);
  }

  private initializeCards() {
    // these must be added in card order
    this.allCards.push(new OneNightCard('Copycat',
      'Look at one of the center cards, you are now that role',
      1, 1, OneNightCardOrder.Copycat, OneNightTeam.Varies, false, true,
      './assets/onenight/onenight1.jpg', 436, 0));
    this.allCards.push(new OneNightCard('Doppleganger',
      'Look at another players card, you are now that role',
      1, 1, OneNightCardOrder.Doppleganger, OneNightTeam.Varies, false, true,
      './assets/onenight/onenight1.jpg', 218, 0));
    this.allCards.push(new OneNightCard('Vampire',
      'Look for other vampires, give any non-vampire a mark of the vampire',
      1, 2, OneNightCardOrder.Vampire, OneNightTeam.Vampire, true, true,
      './assets/onenight/onenight1.jpg', 0, 0));
    this.allCards.push(new OneNightCard('The Master',
      'Look for other vampires, give any non-vampire a mark of the vampire',
      1, 1, OneNightCardOrder.Master, OneNightTeam.Vampire, true, false,
      './assets/onenight/onenight1.jpg', 436, 301));
    this.allCards.push(new OneNightCard('Count',
      'Give a non-vampire player the mark of fear',
      1, 1, OneNightCardOrder.Count, OneNightTeam.Vampire, true, true,
      './assets/onenight/onenight1.jpg', 218, 301));
    this.allCards.push(new OneNightCard('Renfield',
      'Look for the vampires, then place the mark of the bat in front of you',
      1, 1, OneNightCardOrder.Renfield, OneNightTeam.VampireAlly, true, true,
      './assets/onenight/onenight1.jpg', 0, 301));
    this.allCards.push(new OneNightCard('Diseased',
      'Give a mark of disease to the player on your left or right',
      1, 1, OneNightCardOrder.Diseased, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight1.jpg', 436, 602));
    this.allCards.push(new OneNightCard('Cupid',
      'Give any two players the mark of love',
      1, 1, OneNightCardOrder.Cupid, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight1.jpg', 218, 602));
    this.allCards.push(new OneNightCard('Instigator',
      'You may give any player a mark of the traitor',
      1, 1, OneNightCardOrder.Instigator, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight1.jpg', 0, 602));
    this.allCards.push(new OneNightCard('Priest',
      'Give yourself a mark of clarity, then you may give any other player a mark of clarity',
      1, 1, OneNightCardOrder.Priest, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight2.jpg', 436, 0));
    this.allCards.push(new OneNightCard('Assassin',
      'Place the mark of the assassin in front of any player, you only win if the marked target is killed',
      1, 1, OneNightCardOrder.Assassin, OneNightTeam.Assassin, true, true,
      './assets/onenight/onenight2.jpg', 218, 0));
    this.allCards.push(new OneNightCard('Apprentice Assassin',
      'Look for the assassin, if there is no assassin you may give the mark of the assassin to another player',
      1, 1, OneNightCardOrder.ApprAssassin, OneNightTeam.ApprAssassin, true, true,
      './assets/onenight/onenight2.jpg', 0, 0));
    this.allCards.push(new OneNightCard('Sentinel',
      'You may place a shield token on any other player',
      1, 1, OneNightCardOrder.Sentinel, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight2.jpg', 436, 301));
    this.allCards.push(new OneNightCard('Werewolf',
      'Look for other werewolves, if there is only one werewolf, you may look at a card from the center',
      1, 2, OneNightCardOrder.Werewolf, OneNightTeam.Werewolf, false, true,
      './assets/onenight/onenight2.jpg', 218, 301));
    this.allCards.push(new OneNightCard('Alpha Wolf',
      'Exchange the werewolf card for any other players card',
      1, 1, OneNightCardOrder.AlphaWolf, OneNightTeam.Werewolf, false, true,
      './assets/onenight/onenight2.jpg', 0, 301));
    this.allCards.push(new OneNightCard('Mystic Wolf',
      'You may look at another players card',
      1, 1, OneNightCardOrder.MysticWolf, OneNightTeam.Werewolf, false, true,
      './assets/onenight/onenight2.jpg', 436, 602));
    this.allCards.push(new OneNightCard('Dreamwolf',
      'No actions',
      1, 1, OneNightCardOrder.Dreamwolf, OneNightTeam.Werewolf, false, false,
      './assets/onenight/onenight2.jpg', 218, 602));
    this.allCards.push(new OneNightCard('Minion',
      'Look to see who the werewolves are',
      1, 1, OneNightCardOrder.Minion, OneNightTeam.WerewolfAlly, false, true,
      './assets/onenight/onenight2.jpg', 0, 602));
    this.allCards.push(new OneNightCard('Tanner',
      'No actions. You only win if you die',
      1, 1, OneNightCardOrder.Tanner, OneNightTeam.Tanner, false, false,
      './assets/onenight/onenight3.jpg', 436, 0));
    this.allCards.push(new OneNightCard('Apprentice Tanner',
      'Look for the tanner',
      1, 1, OneNightCardOrder.ApprTanner, OneNightTeam.ApprTanner, false, true,
      './assets/onenight/onenight3.jpg', 218, 0));
    this.allCards.push(new OneNightCard('Mason',
      'Look for the other mason',
      2, 2, OneNightCardOrder.Mason, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight3.jpg', 0, 0));
    this.allCards.push(new OneNightCard('Thing',
      'Bump the player to your left or right',
      1, 1, OneNightCardOrder.Thing, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight3.jpg', 436, 301));
    this.allCards.push(new OneNightCard('Seer',
      'You may look at another players card or two of the center cards',
      1, 1, OneNightCardOrder.Seer, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight3.jpg', 218, 301));
    this.allCards.push(new OneNightCard('Apprentice Seer',
      'You may look at one of the center cards',
      1, 1, OneNightCardOrder.ApprSeer, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight3.jpg', 0, 301));
    this.allCards.push(new OneNightCard('Paranormal Investigator',
      'You may look at up to two cards of other players. If you see a role that isn\'t on the villager team, you are now that role, and must stop looking at cards',
      1, 1, OneNightCardOrder.ParanormalInvestigator, OneNightTeam.Varies, false, true,
      './assets/onenight/onenight3.jpg', 436, 602));
    this.allCards.push(new OneNightCard('Marksman',
      'You may view the mark of one player, and the card of another',
      1, 1, OneNightCardOrder.Marksman, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight3.jpg', 218, 602));
    this.allCards.push(new OneNightCard('Robber',
      'You may exchange your card with another players card, and then view your new card',
      1, 1, OneNightCardOrder.Robber, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight3.jpg', 0, 602));
    this.allCards.push(new OneNightCard('Witch',
      'You may look at one of the center cards, if you do you must exchange that card with another players card',
      1, 1, OneNightCardOrder.Witch, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight4.jpg', 436, 0));
    this.allCards.push(new OneNightCard('Pickpocket',
      'You may exchange your mark with another players mark, then view your new mark',
      1, 1, OneNightCardOrder.Pickpocket, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight4.jpg', 218, 0));
    this.allCards.push(new OneNightCard('Troublemaker',
      'You may exchange cards between two other players',
      1, 1, OneNightCardOrder.Troublemaker, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight4.jpg', 0, 0));
    this.allCards.push(new OneNightCard('Village Idiot',
      'You may move everyones card except your own to the left or right',
      1, 1, OneNightCardOrder.VillageIdiot, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight4.jpg', 436, 301));
    this.allCards.push(new OneNightCard('Aura Seer',
      'See which other players have moved, or looked at any card or mark',
      1, 1, OneNightCardOrder.AuraSeer, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight4.jpg', 218, 301));
    this.allCards.push(new OneNightCard('Gremlin',
      'You may switch cards or marks between any two players',
      1, 1, OneNightCardOrder.Gremlin, OneNightTeam.Villager, true, true,
      './assets/onenight/onenight4.jpg', 0, 301));
    this.allCards.push(new OneNightCard('Drunk',
      'Exchange your card with a card from the center',
      1, 1, OneNightCardOrder.Drunk, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight4.jpg', 436, 602));
    this.allCards.push(new OneNightCard('Insomniac',
      'Look at your card',
      1, 1, OneNightCardOrder.Insomniac, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight4.jpg', 218, 602));
    this.allCards.push(new OneNightCard('Squire',
      'You may look at the werewolves cards',
      1, 1, OneNightCardOrder.Squire, OneNightTeam.WerewolfAlly, false, true,
      './assets/onenight/onenight4.jpg', 0, 602));
    this.allCards.push(new OneNightCard('Beholder',
      'Look for the seer and apprentice seer, you may view their cards',
      1, 1, OneNightCardOrder.Beholder, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight5.jpg', 436, 0));
    this.allCards.push(new OneNightCard('Revealer',
      'You may turn any other card face up, if it is not on the villager team turn it back over',
      1, 1, OneNightCardOrder.Revealer, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight5.jpg', 218, 0));
    this.allCards.push(new OneNightCard('Curator',
      'You may place an artifact token face down on any player',
      1, 1, OneNightCardOrder.Curator, OneNightTeam.Villager, false, true,
      './assets/onenight/onenight5.jpg', 0, 0));
    this.allCards.push(new OneNightCard('Villager',
      'No actions',
      1, 3, OneNightCardOrder.Villager, OneNightTeam.Villager, false, false,
      './assets/onenight/onenight5.jpg', 436, 301));
    this.allCards.push(new OneNightCard('Hunter',
      'If the Hunter dies, the player he is pointing at dies as well.',
      1, 1, OneNightCardOrder.Hunter, OneNightTeam.Villager, false, false,
      './assets/onenight/onenight5.jpg', 218, 301));
    this.allCards.push(new OneNightCard('Cursed',
      'If a werewolf votes for you, you become a werewolf, otherwise you are a villager',
      1, 1, OneNightCardOrder.Cursed, OneNightTeam.Varies, false, false,
      './assets/onenight/onenight5.jpg', 0, 301));
    this.allCards.push(new OneNightCard('Prince',
      'Votes against you are discarded',
      1, 1, OneNightCardOrder.Prince, OneNightTeam.Villager, false, false,
      './assets/onenight/onenight5.jpg', 436, 602));
    this.allCards.push(new OneNightCard('Bodyguard',
      'Whoever you vote for cannot be killed',
      1, 1, OneNightCardOrder.Bodyguard, OneNightTeam.Villager, false, false,
      './assets/onenight/onenight5.jpg', 218, 602));
  }

  public getRoleCount(): number {
    var count = 0;
    for (var i = 0; i < this.useCard.length; i++) {
      if (this.useCard[i].use) {
        count += this.useCard[i].count;
      }
    }

    return count;
  }
}
