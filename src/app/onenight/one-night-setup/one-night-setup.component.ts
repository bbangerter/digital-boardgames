import { Component, Input } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';
import { OneNightSelectedCard } from '../dtos/one-night-dtos.model';
import { GameStart } from '../../core';

@Component({
  selector: 'onenight-setup',
  templateUrl: './one-night-setup.component.html',
  styleUrls: [
    './one-night-setup.component.scss',
    './../one-night.scss'
  ]
})
export class OneNightSetupComponent {
  @Input() gameData: GameStart;
  displayedColumns: string[] = ['Use', 'Name', 'Description', 'CardCount', 'MinCards', 'MaxCards'];

  constructor(public gameState: OneNightStateService) { }

  public getAllCards() {
    return this.gameState.allCards;
  }

  public useCard(): Array<OneNightSelectedCard> {
    return this.gameState.useCard;
  }

  public onCardSelectChanged() {
    this.gameState.updateRoles();
  }

  public onTimeChanged() {
    this.gameState.updateDiscussionTime();
  }

  public isGameCreator(): boolean {
    return this.gameState.isGameCreator();
  }

  public getNeededRoleCount(): number {
    return this.gameData.playerIds.length + 3;
  }

  public getRoleCount(): number {
    return this.gameState.getRoleCount();
  }

  public gameCanStart(): boolean {
    for (var i = 0; i < this.gameData.playerIds.length; i++) {
      if (!this.gameState.activePlayers.some(p => p.id == this.gameData.playerIds[i]))
        return false;
    }

    return this.gameState.getRoleCount() == this.gameData.playerIds.length + 3;
  }

  public startRound() {
    this.gameState.advancePhase();
  }
}
