import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneNightSetupComponent } from './one-night-setup.component';

describe('OneNightSetupComponent', () => {
  let component: OneNightSetupComponent;
  let fixture: ComponentFixture<OneNightSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneNightSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneNightSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
