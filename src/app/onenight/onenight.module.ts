import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { OneNightSetupComponent } from './one-night-setup/one-night-setup.component';
import { OneNightRoleComponent } from './one-night-role/one-night-role.component';
import { OneNightActionsComponent } from './one-night-actions/one-night-actions.component';
import { VampiresComponent } from './one-night-actions/vampires/vampires.component';
import { CountComponent } from './one-night-actions/count/count.component';
import { RenfieldComponent } from './one-night-actions/renfield/renfield.component';
import { DiseasedComponent } from './one-night-actions/diseased/diseased.component';
import { InstigatorComponent } from './one-night-actions/instigator/instigator.component';
import { CupidComponent } from './one-night-actions/cupid/cupid.component';
import { PriestComponent } from './one-night-actions/priest/priest.component';
import { AssassinComponent } from './one-night-actions/assassin/assassin.component';
import { ApprAssassinComponent } from './one-night-actions/appr-assassin/appr-assassin.component';
import { SentinelComponent } from './one-night-actions/sentinel/sentinel.component';
import { WerewolfComponent } from './one-night-actions/werewolf/werewolf.component';
import { AlphaWolfComponent } from './one-night-actions/alpha-wolf/alpha-wolf.component';
import { MysticWolfComponent } from './one-night-actions/mystic-wolf/mystic-wolf.component';
import { MinionComponent } from './one-night-actions/minion/minion.component';
import { ApprTannerComponent } from './one-night-actions/appr-tanner/appr-tanner.component';
import { MasonComponent } from './one-night-actions/mason/mason.component';
import { ThingComponent } from './one-night-actions/thing/thing.component';
import { SeerComponent } from './one-night-actions/seer/seer.component';
import { ApprSeerComponent } from './one-night-actions/appr-seer/appr-seer.component';
import { ParanormalInvestigatorComponent } from './one-night-actions/paranormal-investigator/paranormal-investigator.component';
import { MarksmanComponent } from './one-night-actions/marksman/marksman.component';
import { RobberComponent } from './one-night-actions/robber/robber.component';
import { WitchComponent } from './one-night-actions/witch/witch.component';
import { TroublemakerComponent } from './one-night-actions/troublemaker/troublemaker.component';
import { VillageIdiotComponent } from './one-night-actions/village-idiot/village-idiot.component';
import { PickpocketComponent } from './one-night-actions/pickpocket/pickpocket.component';
import { AuraSeerComponent } from './one-night-actions/aura-seer/aura-seer.component';
import { GremlinComponent } from './one-night-actions/gremlin/gremlin.component';
import { DrunkComponent } from './one-night-actions/drunk/drunk.component';
import { InsomniacComponent } from './one-night-actions/insomniac/insomniac.component';
import { SquireComponent } from './one-night-actions/squire/squire.component';
import { TableViewComponent } from './table-view/table-view.component';
import { BeholderComponent } from './one-night-actions/beholder/beholder.component';
import { RevealerComponent } from './one-night-actions/revealer/revealer.component';
import { DopplegangerComponent } from './one-night-actions/doppleganger/doppleganger.component';
import { CopycatComponent } from './one-night-actions/copycat/copycat.component';
import { CuratorComponent } from './one-night-actions/curator/curator.component';
import { OneNightDiscussionComponent } from './one-night-discussion/one-night-discussion.component';
import { OneNightVoteComponent } from './one-night-vote/one-night-vote.component';
import { OneNightWinnerComponent } from './one-night-winner/one-night-winner.component';
import { OneNightGameControllerComponent } from './one-night-game-controller/one-night-game-controller.component';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    OneNightGameControllerComponent,
    OneNightSetupComponent,
    OneNightRoleComponent,
    OneNightActionsComponent,
    VampiresComponent,
    CountComponent,
    RenfieldComponent,
    DiseasedComponent,
    CupidComponent,
    InstigatorComponent,
    PriestComponent,
    AssassinComponent,
    ApprAssassinComponent,
    SentinelComponent,
    WerewolfComponent,
    AlphaWolfComponent,
    MysticWolfComponent,
    MinionComponent,
    ApprTannerComponent,
    MasonComponent,
    ThingComponent,
    SeerComponent,
    ApprSeerComponent,
    ParanormalInvestigatorComponent,
    MarksmanComponent,
    RobberComponent,
    WitchComponent,
    PickpocketComponent,
    TroublemakerComponent,
    VillageIdiotComponent,
    AuraSeerComponent,
    GremlinComponent,
    DrunkComponent,
    InsomniacComponent,
    SquireComponent,
    BeholderComponent,
    TableViewComponent,
    RevealerComponent,
    CopycatComponent,
    DopplegangerComponent,
    CuratorComponent,
    OneNightDiscussionComponent,
    OneNightVoteComponent,
    OneNightWinnerComponent
  ],
  exports: [
    OneNightGameControllerComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatTableModule,
    MatSelectModule,
    MatCardModule,
    FormsModule,
    CoreModule
  ]
})
export class OnenightModule { }
