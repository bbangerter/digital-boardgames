import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-squire',
  templateUrl: './squire.component.html',
  styleUrls: [
    './squire.component.scss',
    './../../one-night.scss'
  ]
})
export class SquireComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getWerewolfPlayers(): Array<OneNightPlayerData> {
    this.revealed = this.gameState.getWerewolves(true);
    return this.gameState.buildPlayerData(this.revealed);
  }

  public acknowledge() {
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
