import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstigatorComponent } from './instigator.component';

describe('InstigatorComponent', () => {
  let component: InstigatorComponent;
  let fixture: ComponentFixture<InstigatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstigatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
