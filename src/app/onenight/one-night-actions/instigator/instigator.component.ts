import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightMark, OneNightMessageType } from '../../one-night-constants';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-instigator',
  templateUrl: './instigator.component.html',
  styleUrls: [
    './instigator.component.scss',
    './../../one-night.scss'
  ]
})
export class InstigatorComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public selectTarget(targetId: string) {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == targetId);
    this.gameState.currentMarks[mark].mark = OneNightMark.Traitor;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }
}
