import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightMessageType } from '../../one-night-constants';

@Component({
  selector: 'onenight-village-idiot',
  templateUrl: './village-idiot.component.html',
  styleUrls: [
    './village-idiot.component.scss',
    './../../one-night.scss'
  ]
})
export class VillageIdiotComponent {
  displayedColumns: string[] = ['Name', 'Choose'];
  selectedCard: string = '';

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public moveLeft() {
    var card = null;
    var nextCard = null;
    for (var index = 0; index < this.gameState.activePlayers.length; index++) {
      if (this.gameState.activePlayers[index].id == this.playerState.getPlayerId()) {
        continue;
      }

      var role = this.gameState.currentRoles.find(r => r.playerId == this.gameState.activePlayers[index].id);
      nextCard = role.card;
      role.card = card;
      card = nextCard;
    }
    this.gameState.currentRoles.find(r => r.card == null).card = card;
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendGameMessage(OneNightMessageType.SwapCards, this.gameState.currentRoles);
    this.acknowledge();
  }

  public moveRight() {
    var card = null;
    var nextCard = null;
    for (var index = this.gameState.activePlayers.length - 1; index >= 0; index--) {
      if (this.gameState.activePlayers[index].id == this.playerState.getPlayerId()) {
        continue;
      }

      var role = this.gameState.currentRoles.find(r => r.playerId == this.gameState.activePlayers[index].id);
      nextCard = role.card;
      role.card = card;
      card = nextCard;
    }
    this.gameState.currentRoles.find(r => r.card == null).card = card;
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendGameMessage(OneNightMessageType.SwapCards, this.gameState.currentRoles);
    this.acknowledge();
  }
}
