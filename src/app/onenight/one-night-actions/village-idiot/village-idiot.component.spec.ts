import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VillageIdiotComponent } from './village-idiot.component';

describe('VillageIdiotComponent', () => {
  let component: VillageIdiotComponent;
  let fixture: ComponentFixture<VillageIdiotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VillageIdiotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VillageIdiotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
