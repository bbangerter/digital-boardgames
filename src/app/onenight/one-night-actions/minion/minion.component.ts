import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';

@Component({
  selector: 'onenight-minion',
  templateUrl: './minion.component.html',
  styleUrls: [
    './minion.component.scss',
    './../../one-night.scss'
  ]
})
export class MinionComponent {
  constructor(private gameState: OneNightStateService) { }

  public getWerewolfPlayerNames(): string {
    var werewolves = this.gameState.getWerewolves(true);
    var players = this.gameState.activePlayers.filter(p => werewolves.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }
}
