import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { OneNightMark, OneNightMessageType } from '../../one-night-constants';

@Component({
  selector: 'onenight-count',
  templateUrl: './count.component.html',
  styleUrls: [
    './count.component.scss',
    './../../one-night.scss'
  ]
})
export class CountComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getVampirePlayerNames(): string {
    var vampires = this.gameState.getVampires();
    var players = this.gameState.activePlayers.filter(p => vampires.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public getNonVampirePlayers(): Array<OneNightPlayerData> {
    var vampires = this.gameState.getVampires();
    var mark = this.gameState.currentMarks.find(m => m.mark == OneNightMark.Vampire);
    if (mark != null) {
      vampires.push(mark.playerId);
    }

    var nonVampires = this.gameState.buildPlayerData(this.gameState.activePlayers.filter(p => !vampires.some(v => v == p.id)).map((p) => p.id));
    return nonVampires;
  }

  public getMarkedVampirePlayerName(): string {
    var mark = this.gameState.currentMarks.findIndex(m => m.mark == OneNightMark.Vampire);
    if (mark == -1) return "none";
    return this.gameState.activePlayers.find(p => p.id == this.gameState.currentMarks[mark].playerId).name;
  }

  public selectTarget(targetId: string) {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == targetId);
    this.gameState.currentMarks[mark].mark = OneNightMark.Fear;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
