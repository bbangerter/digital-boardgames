import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightMessageType } from '../../one-night-constants';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-sentinel',
  templateUrl: './sentinel.component.html',
  styleUrls: [
    './sentinel.component.scss',
    './../../one-night.scss'
  ]
})
export class SentinelComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public selectTarget(targetId: string) {
    this.gameState.shieldToken.push(targetId);
    this.gameState.sendGameMessage(OneNightMessageType.ShieldToken, this.gameState.shieldToken);
    this.gameState.sendAdvanceAction();
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }
}
