import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { OneNightMessageType, OneNightCardOrder } from '../../one-night-constants';

@Component({
  selector: 'onenight-doppleganger',
  templateUrl: './doppleganger.component.html',
  styleUrls: [
    './doppleganger.component.scss',
    './../../one-night.scss'
  ]
})
export class DopplegangerComponent{
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public selectTarget(targetId: string) {
    this.revealed.push(targetId);
    this.gameState.doppleganger = this.gameState.currentRoles.find(r => r.playerId == targetId).card.cardOrder;
    this.gameState.sendGameMessage(OneNightMessageType.Doppleganger, this.gameState.doppleganger);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public isCardRevealed(): boolean {
    // immediate action roles
    if (this.gameState.doppleganger == OneNightCardOrder.Sentinel || this.gameState.doppleganger == OneNightCardOrder.AlphaWolf || this.gameState.doppleganger == OneNightCardOrder.MysticWolf ||
      this.gameState.doppleganger == OneNightCardOrder.Seer || this.gameState.doppleganger == OneNightCardOrder.ApprSeer || this.gameState.doppleganger == OneNightCardOrder.ParanormalInvestigator ||
      this.gameState.doppleganger == OneNightCardOrder.Robber || this.gameState.doppleganger == OneNightCardOrder.Witch || this.gameState.doppleganger == OneNightCardOrder.Troublemaker ||
      this.gameState.doppleganger == OneNightCardOrder.VillageIdiot || this.gameState.doppleganger == OneNightCardOrder.Drunk || this.gameState.doppleganger == OneNightCardOrder.Diseased ||
      this.gameState.doppleganger == OneNightCardOrder.Cupid || this.gameState.doppleganger == OneNightCardOrder.Instigator || this.gameState.doppleganger == OneNightCardOrder.Thing ||
      this.gameState.doppleganger == OneNightCardOrder.Minion) {
      return false;
    }

    return this.revealed.length > 0;
  }
}
