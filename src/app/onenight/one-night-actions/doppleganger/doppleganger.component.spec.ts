import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DopplegangerComponent } from './doppleganger.component';

describe('DopplegangerComponent', () => {
  let component: DopplegangerComponent;
  let fixture: ComponentFixture<DopplegangerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DopplegangerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DopplegangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
