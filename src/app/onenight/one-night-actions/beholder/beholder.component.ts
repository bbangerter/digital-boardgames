import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { OneNightCardOrder } from '../../one-night-constants';

@Component({
  selector: 'onenight-beholder',
  templateUrl: './beholder.component.html',
  styleUrls: [
    './beholder.component.scss',
    './../../one-night.scss'
  ]
})
export class BeholderComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getSeerPlayers(): Array<OneNightPlayerData> {
    var seers = this.gameState.startingRoles.filter(r => (r.card.cardOrder == OneNightCardOrder.Seer || r.card.cardOrder == OneNightCardOrder.ApprSeer) && this.gameState.activePlayers.some(p => p.id == r.playerId));
    this.revealed = seers.map((s) => s.playerId);
    return this.gameState.buildPlayerData(this.revealed);
  }

  public acknowledge() {
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
