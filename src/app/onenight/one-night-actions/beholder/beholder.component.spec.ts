import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeholderComponent } from './beholder.component';

describe('BeholderComponent', () => {
  let component: BeholderComponent;
  let fixture: ComponentFixture<BeholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
