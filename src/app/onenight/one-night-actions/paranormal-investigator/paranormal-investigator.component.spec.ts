import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParanormalInvestigatorComponent } from './paranormal-investigator.component';

describe('ParanormalInvestigatorComponent', () => {
  let component: ParanormalInvestigatorComponent;
  let fixture: ComponentFixture<ParanormalInvestigatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParanormalInvestigatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParanormalInvestigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
