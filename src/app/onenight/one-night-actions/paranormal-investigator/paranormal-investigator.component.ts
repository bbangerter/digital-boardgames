import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { OneNightTeam, OneNightMessageType, OneNightCardOrder } from '../../one-night-constants';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-paranormal-investigator',
  templateUrl: './paranormal-investigator.component.html',
  styleUrls: [
    './paranormal-investigator.component.scss',
    './../../one-night.scss'
  ]
})
export class ParanormalInvestigatorComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];
  maxReveal: number = 2;

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getPlayerCards(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    this.gameState.addAura(this.playerState.getPlayerId());
    this.revealed.push(targetId);
    var role = this.getPlayerCards().find(c => c.Id == targetId);
    if (role.Card.team != OneNightTeam.Varies && role.Card.team != OneNightTeam.Villager) {
      if (this.gameState.currentCardAction == OneNightCardOrder.Doppleganger) {
        this.gameState.dopplegangerPiTeam = role.Card.team;
        this.gameState.sendGameMessage(OneNightMessageType.DopplegangerPITeam, this.gameState.dopplegangerPiTeam);
      }
      else {
        this.gameState.piTeam = role.Card.team;
        this.gameState.sendGameMessage(OneNightMessageType.PITeam, this.gameState.piTeam);
      }
      this.maxReveal = 1;
    }
  }
}
