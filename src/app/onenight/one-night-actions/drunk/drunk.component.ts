import { Component, OnInit } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-drunk',
  templateUrl: './drunk.component.html',
  styleUrls: [
    './drunk.component.scss',
    './../../one-night.scss'
  ]
})
export class DrunkComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getCards(): Array<OneNightPlayerData> {
    return this.gameState.getCenterCards(true);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public isShielded(): boolean {
    return this.gameState.shieldToken.some(s => s == this.playerState.getPlayerId());
  }

  public selectTarget(targetId: string) {
    this.gameState.swapCards(targetId, this.playerState.getPlayerId());
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
