import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightMark, OneNightMessageType, OneNightCardOrder } from '../../one-night-constants';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData, OneNightPlayerCard } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-assassin',
  templateUrl: './assassin.component.html',
  styleUrls: [
    './assassin.component.scss',
    './../../one-night.scss'
  ]
})
export class AssassinComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public isApprenticeAssassin(): boolean {
    return this.gameState.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.ApprAssassin);
  }

  private getApprenticeAssassins(): Array<OneNightPlayerCard> {
    return this.gameState.startingRoles.filter(r => this.gameState.isRole(r.playerId, r.card, OneNightCardOrder.ApprAssassin));
  }

  public getApprenticeAssassinName(): string {
    var appr = this.getApprenticeAssassins();
    if (!this.gameState.activePlayers.some(p => appr.some(a => a.playerId == p.id))) return "none, there is no apprentice assassin";
    return this.gameState.activePlayers.filter(p => appr.some(a => a.playerId == p.id)).map((p) => p.name).toString();
  }

  public selectTarget(targetId: string) {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == targetId);
    this.gameState.currentMarks[mark].mark = OneNightMark.Assassin;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
