import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightMessageType, OneNightMark } from '../../one-night-constants';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-priest',
  templateUrl: './priest.component.html',
  styleUrls: [
    './priest.component.scss',
    './../../one-night.scss'
  ]
})
export class PriestComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public selectTarget(targetId: string) {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == targetId);
    this.gameState.currentMarks[mark].mark = OneNightMark.Clarity;
    this.acknowledge();
  }

  public acknowledge() {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == this.playerState.getPlayerId());
    this.gameState.currentMarks[mark].mark = OneNightMark.Clarity;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
