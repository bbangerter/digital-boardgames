import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-troublemaker',
  templateUrl: './troublemaker.component.html',
  styleUrls: [
    './troublemaker.component.scss',
    './../../one-night.scss'
  ]
})
export class TroublemakerComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Roles'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public acknowledge(targetId: string) {
    if (this.isCardSelected()) {
      this.gameState.swapCards(this.revealed[0], targetId);
      this.gameState.addAura(this.playerState.getPlayerId());
    }
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    if (this.isCardSelected()) {
      this.acknowledge(targetId);
    }
    this.revealed.push(targetId);
  }

  public isCardSelected(): boolean {
    return this.revealed.length > 0;
  }
}
