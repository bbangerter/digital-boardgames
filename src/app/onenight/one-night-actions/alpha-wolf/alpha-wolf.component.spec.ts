import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlphaWolfComponent } from './alpha-wolf.component';

describe('AlphaWolfComponent', () => {
  let component: AlphaWolfComponent;
  let fixture: ComponentFixture<AlphaWolfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlphaWolfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlphaWolfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
