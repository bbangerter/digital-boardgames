import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightCardOrder, OneNightMark } from '../../one-night-constants';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-alpha-wolf',
  templateUrl: './alpha-wolf.component.html',
  styleUrls: [
    './alpha-wolf.component.scss',
    './../../one-night.scss'
  ]
})
export class AlphaWolfComponent {
  columnHeaders: string[] = ['Name', 'Turn into werewolf'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getWerewolfPlayerNames(): string {
    if (this.gameState.currentCardAction == OneNightCardOrder.Doppleganger) {
      return this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.AlphaWolf).playerName;
    }
    var werewolves = this.gameState.getWerewolves(true);
    var players = this.gameState.activePlayers.filter(p => werewolves.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public getDreamWolfName(): string {
    if (this.gameState.currentCardAction == OneNightCardOrder.Doppleganger) {
      return "unknown";
    }
    var dreamwolf = this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Dreamwolf ||
      r.card.cardOrder == OneNightCardOrder.Copycat && this.gameState.copycat == OneNightCardOrder.Dreamwolf).playerId;
    if (this.gameState.startingMarks.some(m => m.mark == OneNightMark.Fear && m.playerId == dreamwolf) ||
      !this.gameState.activePlayers.some(p => p.id == dreamwolf)) {
      return 'none';
    }

    return this.gameState.activePlayers.find(p => p.id == dreamwolf).name;
  }

  public getNonWerewolves(): Array<OneNightPlayerData> {
    if (this.gameState.currentCardAction == OneNightCardOrder.Doppleganger) {
      return this.gameState.getAllPlayers(false).filter(p => p.Id != this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.AlphaWolf).playerId);
    }
    var werewolves = this.gameState.getWerewolves(true);
    return this.gameState.buildPlayerData(this.gameState.activePlayers.filter(p => !werewolves.some(w => w == p.id)).map((p) => p.id));
  }

  public isDreamwolfInPlay(): boolean {
    return this.gameState.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Dreamwolf);
  }

  public selectTarget(targetId: string) {
    this.gameState.swapCards(targetId, 'Extra Werewolf');
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
