import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeerComponent } from './seer.component';

describe('SeerComponent', () => {
  let component: SeerComponent;
  let fixture: ComponentFixture<SeerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
