import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-seer',
  templateUrl: './seer.component.html',
  styleUrls: [
    './seer.component.scss',
    './../../one-night.scss'
  ]
})
export class SeerComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];
  maxReveal: number = 2;

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getCards(): Array<OneNightPlayerData> {
    var centerCards = this.gameState.getCenterCards(true);
    if (this.revealed.length == 0 || !this.canRevealMoreCards() && this.revealed.length != 2) {
      var playerCards = this.gameState.getAllPlayers(true);
      return centerCards.concat(playerCards);
    }
    return centerCards;
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    this.gameState.addAura(this.playerState.getPlayerId());
    this.revealed.push(targetId);
    if (!this.canRevealMoreCards()) {
      this.maxReveal = 1;
    }
  }

  public isCardRevealed(targetId: string): boolean {
    return this.revealed.some(c => c == targetId);
  }

  public canRevealMoreCards(): boolean {
    if (this.revealed.length == 0) return true;
    return !this.gameState.activePlayers.some(p => p.id == this.revealed[0]) && this.revealed.length < 2;
  }
}
