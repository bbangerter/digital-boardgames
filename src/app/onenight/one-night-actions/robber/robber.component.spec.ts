import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RobberComponent } from './robber.component';

describe('RobberComponent', () => {
  let component: RobberComponent;
  let fixture: ComponentFixture<RobberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RobberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RobberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
