import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-robber',
  templateUrl: './robber.component.html',
  styleUrls: [
    './robber.component.scss',
    './../../one-night.scss'
  ]
})
export class RobberComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public isShielded(): boolean {
    return this.gameState.shieldToken.some(s => s == this.playerState.getPlayerId());
  }

  public acknowledge() {
    if (this.isCardRevealed()) {
      this.gameState.swapCards(this.revealed[0], this.playerState.getPlayerId());
    }
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    this.revealed.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }

  public isCardRevealed(): boolean {
    return this.revealed.length > 0;
  }
}
