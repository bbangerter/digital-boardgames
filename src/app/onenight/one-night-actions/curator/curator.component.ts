import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerData, OneNightArtifact } from '../../dtos/one-night-dtos.model';
import { OneNightArtifacts, OneNightCardOrder, OneNightMessageType } from '../../one-night-constants';

@Component({
  selector: 'onenight-curator',
  templateUrl: './curator.component.html',
  styleUrls: [
    './curator.component.scss',
    './../../one-night.scss'
  ]
})
export class CuratorComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public selectTarget(targetId: string) {
    var artifact = Math.floor(Math.random() * (OneNightArtifacts.Traitor + 1));
    while (this.gameState.curated.some(a => a.artifact == artifact) ||
      this.gameState.curated.some(a => a.artifact == OneNightArtifacts.Werewolf) && !this.gameState.startingRoles.some(r => r.card.cardOrder >= OneNightCardOrder.Werewolf && r.card.cardOrder <= OneNightCardOrder.Dreamwolf) ||
      this.gameState.curated.some(a => a.artifact == OneNightArtifacts.Vampire) && !this.gameState.startingRoles.some(r => r.card.cardOrder >= OneNightCardOrder.Vampire && r.card.cardOrder <= OneNightCardOrder.Count)) {
      artifact = Math.floor(Math.random() * (OneNightArtifacts.Traitor + 1));
    }
    this.gameState.curated.push(new OneNightArtifact(targetId, artifact));
    this.gameState.sendGameMessage(OneNightMessageType.Artifacts, this.gameState.curated);
    this.acknowledge();
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }
}
