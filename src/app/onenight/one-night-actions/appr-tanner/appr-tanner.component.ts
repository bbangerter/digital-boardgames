import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightCardOrder } from '../../one-night-constants';

@Component({
  selector: 'onenight-appr-tanner',
  templateUrl: './appr-tanner.component.html',
  styleUrls: [
    './appr-tanner.component.scss',
    './../../one-night.scss'
  ]
})
export class ApprTannerComponent {
  constructor(private gameState: OneNightStateService) { }

  public isTanner(): boolean {
    return this.gameState.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Tanner);
  }

  public isActiveTanner(): boolean {
    if (!this.isTanner()) return false;
    var appr = this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Tanner).playerId;
    return this.gameState.activePlayers.some(p => p.id == appr);
  }

  public getTannerName(): string {
    var appr = this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Tanner).playerId;
    if (!this.gameState.activePlayers.some(p => p.id == appr)) return "none, there is no tanner";
    return this.gameState.activePlayers.find(p => p.id == appr).name;
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }
}
