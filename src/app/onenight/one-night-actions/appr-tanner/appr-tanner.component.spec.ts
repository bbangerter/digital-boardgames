import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprTannerComponent } from './appr-tanner.component';

describe('ApprTannerComponent', () => {
  let component: ApprTannerComponent;
  let fixture: ComponentFixture<ApprTannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprTannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprTannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
