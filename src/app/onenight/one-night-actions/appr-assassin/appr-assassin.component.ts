import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightCardOrder, OneNightMark, OneNightMessageType } from '../../one-night-constants';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData, OneNightPlayerCard } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-appr-assassin',
  templateUrl: './appr-assassin.component.html',
  styleUrls: [
    './appr-assassin.component.scss',
    './../../one-night.scss'
  ]
})
export class ApprAssassinComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public isAssassin(): boolean {
    return this.gameState.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Assassin);
  }

  public isActiveAssassin(): boolean {
    if (!this.isAssassin()) return false;
    var appr = this.getAssassins();
    return this.gameState.activePlayers.some(p => appr.some(a => a.playerId == p.id));
  }

  public getAssassinName() {
    var appr = this.getAssassins();
    if (!this.gameState.activePlayers.some(p => appr.some(a => a.playerId == p.id))) return "none, there is no assassin";
    return this.gameState.activePlayers.filter(p => appr.some(a => a.playerId == p.id)).map((p) => p.name).toString();
  }

  private getAssassins(): Array<OneNightPlayerCard> {
    return this.gameState.startingRoles.filter(r => this.gameState.isRole(r.playerId, r.card, OneNightCardOrder.Assassin));
  }

  public selectTarget(targetId: string) {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == targetId);
    this.gameState.currentMarks[mark].mark = OneNightMark.Assassin;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }
}
