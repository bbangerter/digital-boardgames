import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprAssassinComponent } from './appr-assassin.component';

describe('ApprAssassinComponent', () => {
  let component: ApprAssassinComponent;
  let fixture: ComponentFixture<ApprAssassinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprAssassinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprAssassinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
