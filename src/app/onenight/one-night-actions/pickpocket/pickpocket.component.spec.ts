import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickpocketComponent } from './pickpocket.component';

describe('PickpocketComponent', () => {
  let component: PickpocketComponent;
  let fixture: ComponentFixture<PickpocketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickpocketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickpocketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
