import { Component } from '@angular/core';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { OneNightStateService } from '../../services/one-night-state.service';

@Component({
  selector: 'onenight-pickpocket',
  templateUrl: './pickpocket.component.html',
  styleUrls: [
    './pickpocket.component.scss',
    './../../one-night.scss'
  ]
})
export class PickpocketComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public getMarkName(mark: number): string {
    return this.gameState.markToString(mark);
  }

  public acknowledge() {
    if (this.isMarkRevealed()) {
      this.gameState.swapMarks(this.revealed[0], this.playerState.getPlayerId());
    }
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    this.revealed.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }

  public isMarkRevealed(): boolean {
    return this.revealed.length > 0;
  }
}
