import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiseasedComponent } from './diseased.component';

describe('DiseasedComponent', () => {
  let component: DiseasedComponent;
  let fixture: ComponentFixture<DiseasedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiseasedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiseasedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
