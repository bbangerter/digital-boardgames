import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightMark, OneNightMessageType } from '../../one-night-constants';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-diseased',
  templateUrl: './diseased.component.html',
  styleUrls: [
    './diseased.component.scss',
    './../../one-night.scss'
  ]
})
export class DiseasedComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAdjacentPlayers(): Array<OneNightPlayerData> {
    return this.gameState.buildPlayerData(this.gameState.getAdjacentPlayers().map(p => p.id));
  }

  public selectTarget(targetId: string) {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == targetId);
    this.gameState.currentMarks[mark].mark = OneNightMark.Disease;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
