import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightCardOrder, OneNightMark } from '../../one-night-constants';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-mystic-wolf',
  templateUrl: './mystic-wolf.component.html',
  styleUrls: [
    './mystic-wolf.component.scss',
    './../../one-night.scss'
  ]
})
export class MysticWolfComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getWerewolfPlayerNames(): string {
    if (this.gameState.currentCardAction == OneNightCardOrder.Doppleganger) {
      return this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.MysticWolf).playerName;
    }
    var werewolves = this.gameState.getWerewolves(true);
    var players = this.gameState.activePlayers.filter(p => werewolves.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public getDreamWolfName(): string {
    var dreamwolf = this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Dreamwolf ||
      r.card.cardOrder == OneNightCardOrder.Copycat && this.gameState.copycat == OneNightCardOrder.Dreamwolf).playerId;
    if (this.gameState.startingMarks.some(m => m.mark == OneNightMark.Fear && m.playerId == dreamwolf) ||
      !this.gameState.activePlayers.some(p => p.id == dreamwolf)) {
      return 'none';
    }

    return this.gameState.activePlayers.find(p => p.id == dreamwolf).name;
  }

  public isDreamwolfInPlay(): boolean {
    return this.gameState.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Dreamwolf);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    this.revealed.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }

  public getPlayerCards(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }
}
