import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysticWolfComponent } from './mystic-wolf.component';

describe('MysticWolfComponent', () => {
  let component: MysticWolfComponent;
  let fixture: ComponentFixture<MysticWolfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysticWolfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysticWolfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
