import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerVote } from '../../dtos/one-night-dtos.model';
import { PlayerStateService, GamePlayer } from '../../../core';

@Component({
  selector: 'onenight-vampires',
  templateUrl: './vampires.component.html',
  styleUrls: [
    './vampires.component.scss',
    './../../one-night.scss'
  ]
})
export class VampiresComponent {
  displayedColumns: string[] = ['Name', 'Votes', 'YourVote', 'Choose'];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getVampirePlayerNames(): string {
    var vampires = this.gameState.getVampires();
    var players = this.gameState.activePlayers.filter(p => vampires.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public getNonVampirePlayers(): Array<GamePlayer> {
    var vampires = this.gameState.getVampires();
    var nonVampires = this.gameState.activePlayers.filter(p => !vampires.some(v => v == p.id));
    return nonVampires;
  }

  public getVoteCount(targetId: string): number {
    return this.gameState.votes.filter(v => v.targetId == targetId).length;
  }

  public getYourVote(targetId: string): string {
    if (this.gameState.votes.some(v => v.playerId == this.playerState.getPlayerId() && v.targetId == targetId)) {
      return 'X';
    }

    return '';
  }

  public voteForPlayer(targetId: string) {
    var currentVote = this.gameState.votes.findIndex(v => v.playerId == this.playerState.getPlayerId());
    if (currentVote == -1) {
      this.gameState.votes.push(new OneNightPlayerVote(this.playerState.getPlayerId(), targetId));
    }
    else {
      this.gameState.votes[currentVote].targetId = targetId;
    }

    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendVotes();
  }
}
