import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-appr-seer',
  templateUrl: './appr-seer.component.html',
  styleUrls: [
    './appr-seer.component.scss',
    './../../one-night.scss'
  ]
})
export class ApprSeerComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getCards(): Array<OneNightPlayerData> {
    return this.gameState.getCenterCards(true);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    this.revealed.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }
}
