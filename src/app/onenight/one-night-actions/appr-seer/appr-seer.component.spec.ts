import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprSeerComponent } from './appr-seer.component';

describe('ApprSeerComponent', () => {
  let component: ApprSeerComponent;
  let fixture: ComponentFixture<ApprSeerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprSeerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprSeerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
