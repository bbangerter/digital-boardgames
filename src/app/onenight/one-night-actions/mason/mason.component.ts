import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightMessageType } from '../../one-night-constants';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-mason',
  templateUrl: './mason.component.html',
  styleUrls: [
    './mason.component.scss',
    './../../one-night.scss'
  ]
})
export class MasonComponent {
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getMasonPlayerNames(): string {
    var masons = this.gameState.getMasons();
    var players = this.gameState.activePlayers.filter(p => masons.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public acknowledge() {
    this.gameState.sendGameMessage(OneNightMessageType.MultiRoleDone, this.playerState.getPlayerId());
  }
}
