import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightMark, OneNightMessageType } from '../../one-night-constants';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-cupid',
  templateUrl: './cupid.component.html',
  styleUrls: [
    './cupid.component.scss',
    './../../one-night.scss'
  ]
})
export class CupidComponent {
  columnHeaders: string[] = ['Name', 'Love'];
  displayedColumns: string[] = ['Name', 'Love'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public selectTarget(targetId: string) {
    this.gameState.addAura(this.playerState.getPlayerId());
    this.revealed.push(targetId);
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == targetId);
    this.gameState.currentMarks[mark].mark = OneNightMark.Love;
    if (this.revealed.length > 1) {
      this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);
      this.gameState.sendAdvanceAction();
    }
  }
}
