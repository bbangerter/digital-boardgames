import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneNightActionsComponent } from './one-night-actions.component';

describe('OneNightActionsComponent', () => {
  let component: OneNightActionsComponent;
  let fixture: ComponentFixture<OneNightActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneNightActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneNightActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
