import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightCardOrder, OneNightMark, OneNightMessageType } from '../../one-night-constants';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-werewolf',
  templateUrl: './werewolf.component.html',
  styleUrls: [
    './werewolf.component.scss',
    './../../one-night.scss'
  ]
})
export class WerewolfComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getWerewolfPlayerNames(): string {
    var werewolves = this.gameState.getWerewolves(true);
    var players = this.gameState.activePlayers.filter(p => werewolves.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public isSingleWerewolf(): boolean {
    return this.gameState.getWerewolves(true).length == 1;
  }

  public getDreamWolfName(): string {
    var dreamwolf = this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Dreamwolf ||
      r.card.cardOrder == OneNightCardOrder.Copycat && this.gameState.copycat == OneNightCardOrder.Dreamwolf).playerId;
    if (this.gameState.startingMarks.some(m => m.mark == OneNightMark.Fear && m.playerId == dreamwolf) ||
      !this.gameState.activePlayers.some(p => p.id == dreamwolf)) {
      return 'none';
    }

    return this.gameState.activePlayers.find(p => p.id == dreamwolf).name;
  }

  public isDreamwolfInPlay(): boolean {
    return this.gameState.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Dreamwolf);
  }

  public acknowledge() {
    this.gameState.sendGameMessage(OneNightMessageType.MultiRoleDone, this.playerState.getPlayerId());
  }

  public selectTarget(targetId: string) {
    this.revealed.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }

  public getCenterCards(): Array<OneNightPlayerData> {
    return this.gameState.getCenterCards(false);
  }
}
