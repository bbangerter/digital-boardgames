import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WerewolfComponent } from './werewolf.component';

describe('WerewolfComponent', () => {
  let component: WerewolfComponent;
  let fixture: ComponentFixture<WerewolfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WerewolfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WerewolfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
