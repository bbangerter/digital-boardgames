import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-insomniac',
  templateUrl: './insomniac.component.html',
  styleUrls: [
    './insomniac.component.scss',
    './../../one-night.scss'
  ]
})
export class InsomniacComponent {
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public isShielded(): boolean {
    return this.gameState.shieldToken.some(s => s == this.playerState.getPlayerId());
  }

  public getCurrentRole(): string {
    return this.gameState.currentRoles.find(r => r.playerId == this.playerState.getPlayerId()).card.cardName;
  }

  public acknowledge() {
    if (!this.isShielded()) {
      this.gameState.addAura(this.playerState.getPlayerId());
    }
    this.gameState.sendAdvanceAction();
  }
}
