import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsomniacComponent } from './insomniac.component';

describe('InsomniacComponent', () => {
  let component: InsomniacComponent;
  let fixture: ComponentFixture<InsomniacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsomniacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsomniacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
