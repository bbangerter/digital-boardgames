import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-gremlin',
  templateUrl: './gremlin.component.html',
  styleUrls: [
    './gremlin.component.scss',
    './../../one-night.scss'
  ]
})
export class GremlinComponent {
  columnHeaders: string[] = ['Name', 'Role', 'Mark'];
  displayedColumns: string[] = ['Name', 'Roles', 'Marks'];
  selectedRole: Array<string> = [];
  selectedMark: Array<string> = [];
  maxRoles: number = 2;
  maxMarks: number = 2;

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectRole(targetId: string) {
    if (this.isCardSelected()) {
      this.gameState.swapCards(this.selectedRole[0], targetId);
      this.gameState.sendAdvanceAction();
    }
    this.maxMarks = 0;
    this.selectedRole.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }

  public selectMark(targetId: string) {
    if (this.isMarkSelected()) {
      this.gameState.swapMarks(this.selectedMark[0], targetId);
      this.gameState.sendAdvanceAction();
    }
    this.maxRoles = 0;
    this.selectedMark.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }

  public isCardSelected(): boolean {
    return this.selectedRole.length > 0;
  }

  public isMarkSelected(): boolean {
    return this.selectedMark.length > 0;
  }
}
