import { Component } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';
import { PlayerStateService } from '../../core';
import { OneNightCardOrder, OneNightMark } from '../one-night-constants';

@Component({
  selector: 'onenight-actions',
  templateUrl: './one-night-actions.component.html',
  styleUrls: [
    './one-night-actions.component.scss',
    './../one-night.scss'
  ]
})
export class OneNightActionsComponent {
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getCurrentAction(): number {
    return this.gameState.currentCardAction;
  }

  public isDopplegangerCurrentAction(role: OneNightCardOrder): boolean {
    if (this.gameState.startingRoles.some(r => r.playerId == this.playerState.getPlayerId() && r.card.cardOrder == OneNightCardOrder.Doppleganger)) {
      if (this.gameState.doppleganger == -1) {
        return false;
      }

      if (role == this.gameState.doppleganger) {
        return this.isImmediateDopplegangerRole(role) || this.gameState.dopplegangerTurn;
      }
    }

    return false;
  }

  public isImmediateDopplegangerRole(role: OneNightCardOrder): boolean {
    if (role == OneNightCardOrder.Sentinel || role == OneNightCardOrder.AlphaWolf || role == OneNightCardOrder.MysticWolf ||
      role == OneNightCardOrder.Seer || role == OneNightCardOrder.ApprSeer || role == OneNightCardOrder.ParanormalInvestigator ||
      role == OneNightCardOrder.Robber || role == OneNightCardOrder.Witch || role == OneNightCardOrder.Troublemaker ||
      role == OneNightCardOrder.VillageIdiot || role == OneNightCardOrder.Drunk || role == OneNightCardOrder.Diseased ||
      role == OneNightCardOrder.Cupid || role == OneNightCardOrder.Instigator || this.gameState.doppleganger == OneNightCardOrder.Thing ||
      this.gameState.doppleganger == OneNightCardOrder.Minion) {
      return true;
    }

    return false;
  }

  public isDopplegangerCurrentRole(): boolean {
    if (!this.gameState.startingRoles.some(r => r.playerId == this.playerState.getPlayerId() && r.card.cardOrder == OneNightCardOrder.Doppleganger)) {
      return false;
    }

    if (this.gameState.currentCardAction == OneNightCardOrder.Vampire) {
      return this.gameState.doppleganger >= OneNightCardOrder.Vampire && this.gameState.doppleganger <= OneNightCardOrder.Count;
    }

    if (this.gameState.currentCardAction == OneNightCardOrder.Werewolf) {
      return this.gameState.doppleganger >= OneNightCardOrder.Werewolf && this.gameState.doppleganger <= OneNightCardOrder.MysticWolf;
    }

    return this.gameState.currentCardAction == this.gameState.doppleganger && this.gameState.dopplegangerTurn && !this.isImmediateDopplegangerRole(this.gameState.currentCardAction);;
  }

  public isCurrentActionPlayer(): boolean {
    if (this.gameState.dopplegangerTurn) return false;

    var playerRoleCard = this.gameState.startingRoles.find(r => r.playerId == this.playerState.getPlayerId()).card;
    if (this.gameState.currentCardAction == OneNightCardOrder.Copycat && playerRoleCard.cardOrder == OneNightCardOrder.Copycat) {
      return true;
    }
    if (playerRoleCard.cardOrder == OneNightCardOrder.Copycat && this.gameState.copycat != -1) {
      playerRoleCard = this.gameState.allCards[this.gameState.copycat];
    }
    var currentPlayersAction = playerRoleCard.cardOrder == this.gameState.currentCardAction;
    if (this.gameState.currentCardAction == OneNightCardOrder.Vampire) {
      if (playerRoleCard.cardOrder >= OneNightCardOrder.Master && playerRoleCard.cardOrder <= OneNightCardOrder.Count) {
        return true;
      }
    }

    if (this.gameState.currentCardAction == OneNightCardOrder.Werewolf) {
      if (playerRoleCard.cardOrder >= OneNightCardOrder.AlphaWolf && playerRoleCard.cardOrder <= OneNightCardOrder.MysticWolf) {
        return true;
      }
    }

    return currentPlayersAction;
  }

  public getCurrentRoleAction(): string {
    return this.gameState.allCards[this.gameState.currentCardAction].cardName + (this.gameState.dopplegangerTurn ? ' (doppleganger)' : '');
  }

  public isFeared(): boolean {
    return this.gameState.startingMarks.some(m => m.mark == OneNightMark.Fear && m.playerId == this.playerState.getPlayerId());
  }

  public acknowledgeFear() {
    this.gameState.sendAdvanceAction();
  }
}
