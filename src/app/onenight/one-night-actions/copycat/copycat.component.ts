import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { OneNightMessageType } from '../../one-night-constants';

@Component({
  selector: 'onenight-copycat',
  templateUrl: './copycat.component.html',
  styleUrls: [
    './copycat.component.scss',
    './../../one-night.scss'
  ]
})
export class CopycatComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getCenterCards(): Array<OneNightPlayerData> {
    return this.gameState.getCenterCards(true);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    this.revealed.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.copycat = this.getCenterCards().find(c => c.Id == targetId).Card.cardOrder;
    this.gameState.sendGameMessage(OneNightMessageType.Copycat, this.gameState.copycat);
  }

  public isCardSelected(): boolean {
    return this.revealed.length > 0;
  }
}
