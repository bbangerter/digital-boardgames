import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightMessageType } from '../../one-night-constants';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-thing',
  templateUrl: './thing.component.html',
  styleUrls: [
    './thing.component.scss',
    './../../one-night.scss'
  ]
})
export class ThingComponent {
  columnHeaders: string[] = ['Name', 'Mark'];
  displayedColumns: string[] = ['Name', 'Mark'];
  revealed: Array<string> = [];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAdjacentPlayers(): Array<OneNightPlayerData> {
    return this.gameState.buildPlayerData(this.gameState.getAdjacentPlayers().map(p => p.id));
  }

  public selectTarget(targetId: string) {
    this.gameState.bumpedPlayer.push(targetId);
    this.gameState.sendGameMessage(OneNightMessageType.BumpPlayer, this.gameState.bumpedPlayer);
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
