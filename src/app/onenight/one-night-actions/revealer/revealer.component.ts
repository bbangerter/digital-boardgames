import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { OneNightTeam, OneNightMessageType } from '../../one-night-constants';

@Component({
  selector: 'onenight-revealer',
  templateUrl: './revealer.component.html',
  styleUrls: [
    './revealer.component.scss',
    './../../one-night.scss'
  ]
})
export class RevealerComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public selectTarget(targetId: string) {
    this.gameState.addAura(this.playerState.getPlayerId());
    this.revealed.push(targetId);
    var role = this.getAllPlayers().find(c => c.Id == targetId);
    if (role.Card.team == OneNightTeam.Varies || role.Card.team == OneNightTeam.Villager) {
      this.gameState.revealed.push(targetId);
      this.gameState.sendGameMessage(OneNightMessageType.Revealed, this.gameState.revealed);
    }
  }

  public acknowledge() {
    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
