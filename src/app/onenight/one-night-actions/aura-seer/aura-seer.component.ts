import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';

@Component({
  selector: 'onenight-aura-seer',
  templateUrl: './aura-seer.component.html',
  styleUrls: [
    './aura-seer.component.scss',
    './../../one-night.scss'
  ]
})
export class AuraSeerComponent {
  constructor(private gameState: OneNightStateService) { }

  public getAuraPlayerNames(): string {
    var players = this.gameState.activePlayers.filter(p => this.gameState.auras.some(a => a == p.id)).map((p) => p.name).toString();
    return players;
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }
}
