import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuraSeerComponent } from './aura-seer.component';

describe('AuraSeerComponent', () => {
  let component: AuraSeerComponent;
  let fixture: ComponentFixture<AuraSeerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuraSeerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuraSeerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
