import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-witch',
  templateUrl: './witch.component.html',
  styleUrls: [
    './witch.component.scss',
    './../../one-night.scss'
  ]
})
export class WitchComponent {
  columnHeaders: string[] = ['Name', 'Role'];
  displayedColumns: string[] = ['Name', 'Role'];
  revealed: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getCenterCards(): Array<OneNightPlayerData> {
    return this.gameState.getCenterCards(true);
  }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectTarget(targetId: string) {
    if (this.isCardRevealed()) {
      this.gameState.swapCards(this.revealed[0], targetId);
      this.gameState.addAura(this.playerState.getPlayerId());
      this.gameState.sendAdvanceAction();
      return;
    }
    this.revealed.push(targetId);
  }

  public isCardRevealed(): boolean {
    return this.revealed.length > 0;
  }
}
