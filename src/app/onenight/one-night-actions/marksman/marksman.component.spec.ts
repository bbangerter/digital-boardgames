import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarksmanComponent } from './marksman.component';

describe('MarksmanComponent', () => {
  let component: MarksmanComponent;
  let fixture: ComponentFixture<MarksmanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarksmanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarksmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
