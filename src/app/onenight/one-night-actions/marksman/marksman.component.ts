import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { OneNightPlayerData } from '../../dtos/one-night-dtos.model';
import { PlayerStateService } from '../../../core';

@Component({
  selector: 'onenight-marksman',
  templateUrl: './marksman.component.html',
  styleUrls: [
    './marksman.component.scss',
    './../../one-night.scss'
  ]
})
export class MarksmanComponent {
  columnHeaders: string[] = ['Name', 'Role', 'Mark'];
  displayedColumns: string[] = ['Name', 'Role', 'Mark'];
  selectedRole: Array<string> = [];
  selectedMark: Array<string> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public acknowledge() {
    this.gameState.sendAdvanceAction();
  }

  public selectRole(targetId: string) {
    this.selectedRole.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }

  public selectMark(targetId: string) {
    this.selectedMark.push(targetId);
    this.gameState.addAura(this.playerState.getPlayerId());
  }
}
