import { Component } from '@angular/core';
import { OneNightStateService } from '../../services/one-night-state.service';
import { PlayerStateService } from '../../../core';
import { OneNightMark, OneNightMessageType } from '../../one-night-constants';

@Component({
  selector: 'onenight-renfield',
  templateUrl: './renfield.component.html',
  styleUrls: [
    './renfield.component.scss',
    './../../one-night.scss'
  ]
})
export class RenfieldComponent {
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getVampirePlayerNames(): string {
    var vampires = this.gameState.getVampires();
    var players = this.gameState.activePlayers.filter(p => vampires.some(v => v == p.id)).map((p) => p.name).toString();
    return players;
  }

  public getMarkedVampirePlayerName(): string {
    var mark = this.gameState.currentMarks.findIndex(m => m.mark == OneNightMark.Vampire);
    if (mark == -1) return "none";
    return this.gameState.activePlayers.find(p => p.id == this.gameState.currentMarks[mark].playerId).name;
  }

  public acknowledge() {
    var mark = this.gameState.currentMarks.findIndex(m => m.playerId == this.playerState.getPlayerId());
    this.gameState.currentMarks[mark].mark = OneNightMark.Bat;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerCurrentMarks, this.gameState.currentMarks);

    var startMark = this.gameState.startingMarks.findIndex(m => m.playerId == this.playerState.getPlayerId());
    this.gameState.startingMarks[startMark].mark = OneNightMark.Bat;
    this.gameState.sendGameMessage(OneNightMessageType.PlayerStartingMarks, this.gameState.startingMarks);

    this.gameState.addAura(this.playerState.getPlayerId());
    this.gameState.sendAdvanceAction();
  }
}
