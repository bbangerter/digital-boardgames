import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenfieldComponent } from './renfield.component';

describe('RenfieldComponent', () => {
  let component: RenfieldComponent;
  let fixture: ComponentFixture<RenfieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenfieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenfieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
