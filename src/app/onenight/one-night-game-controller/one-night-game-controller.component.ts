import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';
import { OneNightPhase } from '../one-night-constants';
import { PlayerStateService, GameStart, GamePlayer, GameComponent } from '../../core';

@Component({
  selector: 'onenight-game-controller',
  templateUrl: './one-night-game-controller.component.html',
  styleUrls: [
    './one-night-game-controller.component.scss',
    './../one-night.scss'
  ]
})
export class OneNightGameControllerComponent implements OnInit, OnDestroy, GameComponent {
  @Input() gameData: GameStart;

  constructor(private playerState: PlayerStateService, private gameState: OneNightStateService) { }

  ngOnInit(): void {
    this.gameState.startGameHub(this.playerState.getPlayerName(), this.gameData.gameId, this.playerState.getPlayerId());
    this.gameState.creatorId = this.gameData.creatorId;
  }

  ngOnDestroy(): void {
    this.gameState.closeGameHub();
  }

  public getCurrentPhase(): number {
    return this.gameState.currentPhase;
  }

  public getActivePlayers(): Array<GamePlayer> {
    return this.gameState.activePlayers;
  }

  public getPhaseName(): string {
    switch (this.gameState.currentPhase) {
      case OneNightPhase.Setup:
        return 'Setup';
      case OneNightPhase.Shuffle:
        return 'Shuffle';
      case OneNightPhase.DayActions:
        return 'Dusk Actions';
      case OneNightPhase.ReviewMarks:
        return 'Nightfall';
      case OneNightPhase.NightActions:
        return 'Night Actions';
      case OneNightPhase.Discussion:
        return 'Day (discuss who to lynch)';
      case OneNightPhase.Vote:
        return 'Voting';
      case OneNightPhase.DetermineWinner:
        return 'Victory';
      default:
        return '';
    }
  }
}
