import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneNightGameControllerComponent } from './one-night-game-controller.component';

describe('OneNightGameControllerComponent', () => {
  let component: OneNightGameControllerComponent;
  let fixture: ComponentFixture<OneNightGameControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneNightGameControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneNightGameControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
