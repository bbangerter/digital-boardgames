import { OneNightTeam, OneNightMark, OneNightCardOrder, OneNightArtifacts } from '../one-night-constants';

export class OneNightCard {
  constructor(public cardName: string, public cardDescription: string, public minCount: number, public maxCount: number,
    public cardOrder: OneNightCardOrder, public team: OneNightTeam, public usesMarks: boolean, public hasAction: boolean,
    public imageUrl: string, public left: number, public top: number) { }
}

export class OneNightSelectedCard {
  constructor(public use: boolean, public count: number) { }
}

export class OneNightPlayerData {
  constructor(public Id: string, public Name: string, public Card: OneNightCard, public Mark: OneNightMark) { }
}

export class OneNightPlayerCard {
  constructor(public playerId: string, public playerName: string, public card: OneNightCard) { }
}

export class OneNightPlayerMark {
  constructor(public playerId: string, public playerName: string, public mark: OneNightMark) { }
}

export class OneNightPlayerVote {
  constructor(public playerId: string, public targetId: string) { }
}

export class OneNightVoteTally {
  constructor(public playerId: string, public count: number, public victory: boolean, public killed: boolean, public unkillable: boolean) { }
}

export class OneNightAccuseRole {
  constructor(public playerId: string, public playerName: string, public targetId: string, public role: string) { }
}

export class OneNightAccuseMark {
  constructor(public playerId: string, public playerName: string, public targetId: string, public mark: string) { }
}

export class OneNightArtifact {
  constructor(public playerId: string, public artifact: OneNightArtifacts) { }
}
