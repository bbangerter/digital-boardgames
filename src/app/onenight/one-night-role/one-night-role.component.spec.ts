import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneNightRoleComponent } from './one-night-role.component';

describe('OneNightRoleComponent', () => {
  let component: OneNightRoleComponent;
  let fixture: ComponentFixture<OneNightRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneNightRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneNightRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
