import { Component } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';
import { OneNightCard } from '../dtos/one-night-dtos.model';
import { OneNightMark, OneNightCardOrder, OneNightPhase } from '../one-night-constants';
import { PlayerStateService } from '../../core';

@Component({
  selector: 'onenight-role',
  templateUrl: './one-night-role.component.html',
  styleUrls: [
    './one-night-role.component.scss',
    './../one-night.scss'
  ]
})
export class OneNightRoleComponent {
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getCurrentPhase(): OneNightPhase {
    return this.gameState.currentPhase;
  }

  public getPlayerRole(): OneNightCard {
    var playerCard = this.gameState.startingRoles.find(c => c.playerId == this.playerState.getPlayerId());
    if (playerCard == null) {
      return null;
    }

    return playerCard.card;
  }

  public getPlayerMark(): string {
    var playerMark = this.gameState.startingMarks.find(c => c.playerId == this.playerState.getPlayerId());
    if (playerMark == null) {
      return null;
    }

    return this.gameState.markToString(playerMark.mark);
  }

  public getImageSource(): string {
    var playerCard = this.gameState.startingRoles.find(c => c.playerId == this.playerState.getPlayerId());
    if (playerCard == null) {
      return null;
    }

    return playerCard.card.imageUrl;
  }

  public getImageStyle(): string {
    var playerCard = this.gameState.startingRoles.find(c => c.playerId == this.playerState.getPlayerId());
    if (playerCard == null) {
      return null;
    }

    var right = `right: ${playerCard.card.left - 436}px;`;
    var top = `top: -${playerCard.card.top}px;`;
    var clip = `clip: rect(${playerCard.card.top}px,${playerCard.card.left + 218}px,${playerCard.card.top + 301}px,${playerCard.card.left}px);`;
    return right + top + clip;
  }

  public inLoveWith(): string {
    if (this.gameState.startingMarks.some(m => m.mark == OneNightMark.Love && m.playerId == this.playerState.getPlayerId())) {
      var player = this.gameState.startingMarks.find(m => m.mark == OneNightMark.Love && m.playerId != this.playerState.getPlayerId());
      if (player != null) {
        return this.gameState.activePlayers.find(p => p.id == player.playerId).name;
      }

      return 'no one';
    }

    return null;
  }

  public usingMarks(): boolean {
    return this.gameState.startingRoles.some(c => c.card.usesMarks);
  }

  public isSentinelInPlay(): boolean {
    return this.gameState.startingRoles.some(r => r.card.cardOrder == OneNightCardOrder.Sentinel);
  }

  public shieldedPlayers(): string {
    if (this.gameState.shieldToken.length == 0) {
      return 'no player';
    }

    return this.gameState.activePlayers.filter(p => this.gameState.shieldToken.some(s => s == p.id)).map((p) => p.name).toString();
  }

  public bumpedPlayer(): string {
    if (this.gameState.bumpedPlayer.some(b => b == this.playerState.getPlayerId())) {
      if (this.gameState.doppleganger == OneNightCardOrder.Thing) {
        return this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Doppleganger).playerName;
      }
      if (this.gameState.copycat == OneNightCardOrder.Thing) {
        return this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Copycat).playerName;
      }
      return this.gameState.startingRoles.find(r => r.card.cardOrder == OneNightCardOrder.Thing).playerName;
    }

    return null;
  }

  public isRevealed(): boolean {
    return this.gameState.revealed.length > 0;
  }

  public revealedName(): string {
    return this.gameState.activePlayers.filter(p => this.gameState.revealed.some(r => r == p.id)).map((p) => p.name).toString();
  }

  public revealedRole(): string {
    return this.gameState.currentRoles.filter(p => this.gameState.revealed.some(r => r == p.playerId)).map((p) => p.card.cardName).toString();
  }

  public hasArtifact(): boolean {
    return this.gameState.curated.length > 0;
  }

  public curatedName(): string {
    return this.gameState.activePlayers.filter(p => this.gameState.curated.some(r => r.playerId == p.id)).map((p) => p.name).toString();
  }

  public getArtifactName(): string {
    if (this.getCurrentPhase() == OneNightPhase.DetermineWinner) {
      return this.gameState.curated.map(a => this.gameState.artifactToString(a.artifact) + ' (' + this.gameState.activePlayers.find(p => p.id == a.playerId).name + ')').toString();
    }

    if (!this.gameState.curated.some(a => a.playerId == this.playerState.getPlayerId())) {
      return '';
    }

    return this.gameState.artifactToString(this.gameState.curated.find(a => a.playerId == this.playerState.getPlayerId()).artifact);
  }
}
