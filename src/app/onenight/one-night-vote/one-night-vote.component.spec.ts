import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneNightVoteComponent } from './one-night-vote.component';

describe('OneNightVoteComponent', () => {
  let component: OneNightVoteComponent;
  let fixture: ComponentFixture<OneNightVoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneNightVoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneNightVoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
