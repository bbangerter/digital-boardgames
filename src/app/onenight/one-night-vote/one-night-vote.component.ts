import { Component } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';
import { PlayerStateService } from '../../core';
import { OneNightPlayerData, OneNightPlayerVote } from '../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-vote',
  templateUrl: './one-night-vote.component.html',
  styleUrls: [
    './one-night-vote.component.scss',
    './../one-night.scss'
  ]
})
export class OneNightVoteComponent {
  displayedColumns: string[] = ['Name', 'Claim', 'OtherClaim', 'Vote'];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(false);
  }

  public getClaim(id: string): string {
    var role = this.gameState.claimedRole.find(r => r.playerId == id);
    if (role == null) {
      return "none";
    }

    return role.card.cardName;
  }

  public getOtherClaim(id: string): string {
    var claims = this.gameState.otherClaimedRole.filter(r => r.targetId == id);
    return claims.map((c) => c.role + ' (' + c.playerName + ')').toString();
  }

  public getYourVote(targetId: string): string {
    if (this.gameState.votes.some(v => v.playerId == this.playerState.getPlayerId() && v.targetId == targetId)) {
      return 'X';
    }

    return '';
  }

  public hasVoted(): boolean {
    var currentVote = this.gameState.votes.findIndex(v => v.playerId == this.playerState.getPlayerId());
    return currentVote != -1;
  }

  public voteForPlayer(targetId: string) {
    var currentVote = this.gameState.votes.findIndex(v => v.playerId == this.playerState.getPlayerId());
    if (currentVote == -1) {
      this.gameState.votes.push(new OneNightPlayerVote(this.playerState.getPlayerId(), targetId));
    }
    else {
      this.gameState.votes[currentVote].targetId = targetId;
    }

    this.gameState.sendVotes();
  }

  public isShielded(playerId: string): boolean {
    return this.gameState.shieldToken.some(s => s == playerId);
  }
}
