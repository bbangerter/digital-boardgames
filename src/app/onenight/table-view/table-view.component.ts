import { Component, Input, Output, EventEmitter } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';

@Component({
  selector: 'onenight-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: [
    './table-view.component.scss',
    './../one-night.scss'
  ]
})
export class TableViewComponent {
  @Input() columnHeaders: Array<string>;
  @Input() displayedColumns: Array<string>;
  @Input() tableData: Array<any>;
  @Input() revealed1: Array<string>;
  @Input() revealed2: Array<string>;
  @Input() maxSelected1: number;
  @Input() maxSelected2: number;
  @Output() targetIdEmitter1 = new EventEmitter<string>();
  @Output() targetIdEmitter2 = new EventEmitter<string>();
  
  constructor(private gameState: OneNightStateService) { }

  public selectPlayer(targetId: string, index: number) {
    if (index == 1) {
      this.targetIdEmitter1.emit(targetId);
    }
    else {
      this.targetIdEmitter2.emit(targetId);
    }
  }

  public canSelect(playerId: string, index: number): boolean {
    if (index == 1) {
      if (this.revealed1.length >= this.maxSelected1) {
        return false;
      }
      if (this.revealed2 != null && this.revealed2.some(s => s == playerId)) {
        return false;
      }
    }
    else {
      if (this.revealed2.length >= this.maxSelected2) {
        return false;
      }
      if (this.revealed1 != null && this.revealed1.some(s => s == playerId)) {
        return false;
      }
    }

    return !this.isSelected(playerId, index);
  }

  public isSelected(playerId: string, index: number): boolean {
    if (index == 1) {
      return this.revealed1.some(r => r == playerId);
    }
    else {
      return this.revealed2.some(r => r == playerId);
    }
  }

  public isShielded(playerId: string): boolean {
    return this.gameState.shieldToken.some(s => s == playerId);
  }

  public getMarkName(mark: number): string {
    return this.gameState.markToString(mark);
  }
}
