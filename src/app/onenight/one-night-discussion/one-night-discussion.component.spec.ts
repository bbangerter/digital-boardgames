import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneNightDiscussionComponent } from './one-night-discussion.component';

describe('OneNightDiscussionComponent', () => {
  let component: OneNightDiscussionComponent;
  let fixture: ComponentFixture<OneNightDiscussionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneNightDiscussionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneNightDiscussionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
