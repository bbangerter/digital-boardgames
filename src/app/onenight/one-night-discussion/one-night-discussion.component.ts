import { Component, OnInit } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';
import { OneNightCard, OneNightPlayerCard, OneNightAccuseRole, OneNightPlayerMark, OneNightAccuseMark } from '../dtos/one-night-dtos.model';
import { OneNightMessageType, OneNightMark, OneNightCardOrder, OneNightArtifacts } from '../one-night-constants';
import { PlayerStateService, GamePlayer } from '../../core';

@Component({
  selector: 'onenight-discussion',
  templateUrl: './one-night-discussion.component.html',
  styleUrls: [
    './one-night-discussion.component.scss',
    './../one-night.scss'
  ]
})
export class OneNightDiscussionComponent implements OnInit {
  displayedColumns: string[] = ['Name', 'Claim', 'ClaimMark', 'OtherClaim', 'OtherClaimMark'];
  claimed: number;
  claimedMark: number;
  otherclaimed: Array<number> = [];
  otherclaimedmark: Array<number> = [];

  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  ngOnInit(): void {
    if (this.gameState.isGameCreator()) {
      this.setTimer();
    }
  }

  public getAllPlayers(): Array<GamePlayer> {
    return this.gameState.activePlayers;
  }

  public getCards(): Array<OneNightCard> {
    return this.gameState.allCards.filter((c, index) => this.gameState.useCard[index].use);
  }

  public getMarks(): Array<number> {
    var roles = this.gameState.startingRoles.filter(r => r.card.usesMarks);
    var usedMarks = new Array<OneNightMark>();
    usedMarks.push(OneNightMark.Clarity);

    if (roles.some(r => r.card.cardOrder >= OneNightCardOrder.Vampire && r.card.cardOrder <= OneNightCardOrder.Count)) {
      usedMarks.push(OneNightMark.Vampire);
    }

    if (roles.some(r => r.card.cardOrder == OneNightCardOrder.Renfield)) {
      usedMarks.push(OneNightMark.Bat);
    }

    if (roles.some(r => r.card.cardOrder == OneNightCardOrder.Assassin || r.card.cardOrder == OneNightCardOrder.ApprAssassin)) {
      usedMarks.push(OneNightMark.Assassin);
    }

    if (roles.some(r => r.card.cardOrder == OneNightCardOrder.Instigator)) {
      usedMarks.push(OneNightMark.Traitor);
    }

    if (roles.some(r => r.card.cardOrder == OneNightCardOrder.Diseased)) {
      usedMarks.push(OneNightMark.Disease);
    }

    if (roles.some(r => r.card.cardOrder == OneNightCardOrder.Count)) {
      usedMarks.push(OneNightMark.Fear);
    }

    if (roles.some(r => r.card.cardOrder == OneNightCardOrder.Cupid)) {
      usedMarks.push(OneNightMark.Love);
    }
    return usedMarks;
  }

  public getMark(mark: number): string {
    return this.gameState.markToString(mark);
  }

  public isPlayer(id: string): boolean {
    return this.playerState.getPlayerId() == id;
  }

  public getClaim(id: string): string {
    var role = this.gameState.claimedRole.find(r => r.playerId == id);
    if (role == null) {
      return 'none';
    }

    return role.card.cardName;
  }

  public getClaimMark(id: string): string {
    var mark = this.gameState.claimedMark.find(m => m.playerId == id);
    if (mark == null) {
      return 'none';
    }

    return this.gameState.markToString(mark.mark);
  }

  public getOtherClaim(id: string): string {
    var claims = this.gameState.otherClaimedRole.filter(r => r.targetId == id);
    return claims.map((c) => c.role + ' (' + c.playerName + ')').toString();
  }

  public getOtherClaimMark(id: string): string {
    var claims = this.gameState.otherClaimedMark.filter(m => m.targetId == id);
    return claims.map((m) => m.mark + ' (' + m.playerName + ')').toString();
  }

  public onClaimChanged() {
    var claim = this.gameState.claimedRole.find(c => c.playerId == this.playerState.getPlayerId());
    if (claim == null) {
      this.gameState.claimedRole.push(new OneNightPlayerCard(this.playerState.getPlayerId(), this.playerState.getPlayerName(), this.gameState.allCards[this.claimed]))
    }
    else {
      claim.card = this.gameState.allCards[this.claimed];
    }
    this.gameState.sendGameMessage(OneNightMessageType.Claim, this.gameState.claimedRole);
  }

  public onClaimMarkChanged() {
    var mark = this.gameState.claimedMark.find(m => m.playerId == this.playerState.getPlayerId());
    if (mark == null) {
      this.gameState.claimedMark.push(new OneNightPlayerMark(this.playerState.getPlayerId(), this.playerState.getPlayerName(), parseInt(this.claimedMark.toString())));
    }
    else {
      mark.mark = parseInt(this.claimedMark.toString());
    }

    this.gameState.sendGameMessage(OneNightMessageType.ClaimMark, this.gameState.claimedMark);
  }

  public onOtherClaimChanged(targetId: string, index: number) {
    var role = this.gameState.allCards[this.otherclaimed[index]];
    var claim = this.gameState.otherClaimedRole.find(r => r.playerId == this.playerState.getPlayerId() && r.targetId == targetId);
    if (claim == null) {
      this.gameState.otherClaimedRole.push(new OneNightAccuseRole(this.playerState.getPlayerId(), this.playerState.getPlayerName(), targetId, role.cardName));
    }
    else {
      if (role == null) {
        claim.role = 'none';
      }
      else {
        claim.role = role.cardName;
      }
    }

    this.gameState.sendGameMessage(OneNightMessageType.OtherClaim, this.gameState.otherClaimedRole);
  }

  public onOtherClaimMarkChanged(targetId: string, index: number) {
    var claim = this.gameState.otherClaimedMark.find(m => m.playerId == this.playerState.getPlayerId() && m.targetId == targetId);
    if (claim == null) {
      this.gameState.otherClaimedMark.push(new OneNightAccuseMark(this.playerState.getPlayerId(), this.playerState.getPlayerName(), targetId, this.gameState.markToString(parseInt(this.otherclaimedmark[index].toString()))));
    }
    else {
      claim.mark = this.gameState.markToString(this.otherclaimedmark[index]);
    }

    this.gameState.sendGameMessage(OneNightMessageType.OtherClaimMark, this.gameState.otherClaimedMark);
  }

  public getTimeLeft(): string {
    var minutes = Math.floor(this.gameState.discussionTimeLeft / 60);
    var seconds = this.gameState.discussionTimeLeft % 60;

    return minutes + ':' + seconds.toString().padStart(2, '0');
  }

  public isShamed(playerId: string, excludeSelf: boolean) {
    // returns true if the player on this system has the shroud of shame (excludeself returns false if the player id is this player)
    if (excludeSelf && playerId == this.playerState.getPlayerId()) {
      return false;
    }
    return this.gameState.curated.some(a => a.playerId == this.playerState.getPlayerId() && a.artifact == OneNightArtifacts.Shame);
  }

  private setTimer() {
    setTimeout(() => {
      this.decreaseTime();
    }, 1000);
  }

  private decreaseTime() {
    this.gameState.discussionTimeLeft--;
    if (this.gameState.discussionTimeLeft > 0) {
      this.setTimer();
    }
    else {
      this.gameState.advancePhase();
    }
    this.gameState.sendGameMessage(OneNightMessageType.DiscussionTimer, this.gameState.discussionTimeLeft);
  }
}
