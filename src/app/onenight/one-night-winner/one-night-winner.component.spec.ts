import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneNightWinnerComponent } from './one-night-winner.component';

describe('OneNightWinnerComponent', () => {
  let component: OneNightWinnerComponent;
  let fixture: ComponentFixture<OneNightWinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneNightWinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneNightWinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
