import { Component } from '@angular/core';
import { OneNightStateService } from '../services/one-night-state.service';
import { PlayerStateService } from '../../core';
import { OneNightPlayerData } from '../dtos/one-night-dtos.model';

@Component({
  selector: 'onenight-winner',
  templateUrl: './one-night-winner.component.html',
  styleUrls: [
    './one-night-winner.component.scss',
    './../one-night.scss'
  ]
})
export class OneNightWinnerComponent {
  displayedColumns: string[] = ['Name', 'Vote', 'Role', 'Mark', 'Killed', 'Victory'];
  constructor(private gameState: OneNightStateService, private playerState: PlayerStateService) { }

  public getAllPlayers(): Array<OneNightPlayerData> {
    return this.gameState.getAllPlayers(true);
  }

  public getVoteCount(targetId: string): number {
    return this.gameState.votes.filter(v => v.targetId == targetId).length;
  }

  public getMark(mark: number): string {
    return this.gameState.markToString(mark);
  }

  public wasKilled(playerId: string): string {
    var tally = this.gameState.voteTally.find(t => t.playerId == playerId);
    if (tally != null && tally.killed) {
      return 'X';
    }

    return '';
  }

  public isWinner(playerId: string): string {
    var tally = this.gameState.voteTally.find(t => t.playerId == playerId);
    if (tally != null && tally.victory) {
      return 'X';
    }

    return '';
  }

  public isGameCreator(): boolean {
    return this.gameState.isGameCreator();
  }

  public restart() {
    this.gameState.advancePhase();
  }
}
