export enum OneNightTeam {
  Villager = 0,
  Werewolf = 1,
  Vampire = 2,
  Varies = 3,
  Tanner = 4,
  Assassin = 5,
  ApprAssassin = 6,
  ApprTanner = 7,
  WerewolfAlly = 8,
  VampireAlly = 9
}

export enum OneNightPhase {
  Setup = 0,
  Shuffle = 1,
  DayActions = 2,
  ReviewMarks = 3,
  NightActions = 4,
  Discussion = 5,
  Vote = 6,
  DetermineWinner = 7,
  Reset = 8
}

export enum OneNightMessageType {
  UpdatePlayerList = 0,
  RoleSelect = 1,
  ShuffledDeck = 2,
  PlayerStartingMarks = 3,
  PlayerCurrentMarks = 4,
  AdvanceAction = 5,
  UpdateVote = 6,
  ShieldToken = 7,
  MultiRoleDone = 8,
  SwapCards = 9,
  BumpPlayer = 10,
  PITeam = 11,
  SwapMarks = 12,
  AddAura = 13,
  Revealed = 14,
  Copycat = 15,
  Doppleganger = 16,
  DiscussionTime = 17,
  Claim = 18,
  OtherClaim = 19,
  DiscussionTimer = 20,
  ClaimMark = 21,
  OtherClaimMark = 22,
  ResetGame = 23,
  Artifacts = 24,
  DopplegangerPITeam = 25
}

export enum OneNightMark {
  Clarity = 0,
  Vampire = 1,
  Bat = 2,
  Assassin = 3,
  Traitor = 4,
  Fear = 5,
  Disease = 6,
  Love = 7
}

export enum OneNightCardOrder {
  Copycat = 0,
  Doppleganger = 1,
  Vampire = 2,
  Master = 3,
  Count = 4,
  Renfield = 5,
  Diseased = 6,
  Cupid = 7,
  Instigator = 8,
  Priest = 9,
  Assassin = 10,
  ApprAssassin = 11,
  Sentinel = 12,
  Werewolf = 13,
  AlphaWolf = 14,
  MysticWolf = 15,
  Dreamwolf = 16,
  Minion = 17,
  Tanner = 18,
  ApprTanner = 19,
  Mason = 20,
  Thing = 21,
  Seer = 22,
  ApprSeer = 23,
  ParanormalInvestigator = 24,
  Marksman = 25,
  Robber = 26,
  Witch = 27,
  Pickpocket = 28,
  Troublemaker = 29,
  VillageIdiot = 30,
  AuraSeer = 31,
  Gremlin = 32,
  Drunk = 33,
  Insomniac = 34,
  Squire = 35,
  Beholder = 36,
  Revealer = 37,
  Curator = 38,
  Villager = 39,
  Hunter = 40,
  Cursed = 41,
  Prince = 42,
  Bodyguard = 43
}

export enum OneNightArtifacts {
  Nothing = 0,       // void of nothingness
  Shame = 1,         // shroud of shame
  Muting = 2,        // mask of muting
  Werewolf = 3,      // claw of the werewolf
  Villager = 4,      // brand of the villager
  Tanner = 5,        // cudgel of the tanner
  Hunter = 6,        // bow of the hunter
  Prince = 7,        // cloak of the prince
  Bodyguard = 8,     // sword of the bodyguard
  Vampire = 9,       // mist of the vampire
  Traitor = 10       // dagger of the traitor
}
