import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';

import { SplendorGameControllerComponent } from './splendor-game-controller/splendor-game-controller.component';
import { SplendorSetupComponent } from './splendor-setup/splendor-setup.component';
import { SplendorPlayerTurnComponent } from './splendor-player-turn/splendor-player-turn.component';
import { SplendorVictoryComponent } from './splendor-victory/splendor-victory.component';

@NgModule({
  declarations: [
    SplendorGameControllerComponent,
    SplendorSetupComponent,
    SplendorPlayerTurnComponent,
    SplendorVictoryComponent
  ],
  exports: [
    SplendorGameControllerComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatTableModule,
    MatSelectModule,
    MatCardModule,
    FormsModule,
    CoreModule
  ]
})
export class SplendorModule { }
