import { GfxText, PlayerScoreService, GfxTextureAtlas } from '../../core';
import { Subscription } from 'rxjs';
import { SplendorGemType } from '../splendor-constants';

export class SplendorCard {
  constructor(public cardId: number, public cardName: string, public gemType: number, public points: number, public diamondCost: number, public emeraldCost: number, public sapphireCost: number,
    public rubyCost: number, public topazCost: number, public imageUrl: string, public atlasIndex: number, public gfxId: number = -1, public containerId: number = -1,
    public claimId: number = -1, public reserveId: number = -1) { }
}

export class SplendorNoble {
  constructor(public nobleId: number, public diamondCost: number, public emeraldCost: number, public sapphireCost: number, public rubyCost: number, public topazCost: number, public points: number,
  public claimed: string, public imageUrl: string, public atlasIndex: number, public gfxId: number = -1, public selectionId: number = -1) { }
}

export interface SplendorClaimGems {
  gemType: number;
  amount: number;
}

export class SplendorClaimedGems {
  constructor(public gemType: SplendorGemType, public button: GfxTextureAtlas) { }
}

export class SplendorPlayerMat {
  baseGfxId: number;
  score: GfxText;
  diamondScore: GfxText;
  emeraldScore: GfxText;
  sapphireScore: GfxText;
  rubyScore: GfxText;
  topazScore: GfxText;
  goldScore: GfxText;
  diamondProjects: GfxText;
  emeraldProjects: GfxText;
  sapphireProjects: GfxText;
  rubyProjects: GfxText;
  topazProjects: GfxText;

  private scoreTracking: Subscription;
  private diamondTracking: Subscription;
  private emeraldTracking: Subscription;
  private sapphireTracking: Subscription;
  private rubyTracking: Subscription;
  private topazTracking: Subscription;
  private goldTracking: Subscription;

  constructor(public playerId: string) { }

  public scoreSubscriber(scoreTracker: PlayerScoreService) {
    this.scoreTracking = scoreTracker.subscribeScoreChange(this.playerId, 'gamescore', (data) => this.onScoreChange(data));
    this.diamondTracking = scoreTracker.subscribeScoreChange(this.playerId, 'diamond', (data) => this.onDiamondChange(data));
    this.emeraldTracking = scoreTracker.subscribeScoreChange(this.playerId, 'emerald', (data) => this.onEmeraldChange(data));
    this.sapphireTracking = scoreTracker.subscribeScoreChange(this.playerId, 'sapphire', (data) => this.onSapphireChange(data));
    this.rubyTracking = scoreTracker.subscribeScoreChange(this.playerId, 'ruby', (data) => this.onRubyChange(data));
    this.topazTracking = scoreTracker.subscribeScoreChange(this.playerId, 'topaz', (data) => this.onTopazChange(data));
    this.goldTracking = scoreTracker.subscribeScoreChange(this.playerId, 'gold', (data) => this.onGoldChange(data));
  }

  public cleanup() {
    this.scoreTracking.unsubscribe();
    this.diamondTracking.unsubscribe();
    this.emeraldTracking.unsubscribe();
    this.sapphireTracking.unsubscribe();
    this.rubyTracking.unsubscribe();
    this.topazTracking.unsubscribe();
    this.goldTracking.unsubscribe();
  }

  private onScoreChange(data) {
    this.score.setText(`Score: ${data}`);
  }

  private onDiamondChange(data) {
    this.diamondScore.setText(`${data}`);
  }

  private onEmeraldChange(data) {
    this.emeraldScore.setText(`${data}`);
  }

  private onSapphireChange(data) {
    this.sapphireScore.setText(`${data}`);
  }

  private onRubyChange(data) {
    this.rubyScore.setText(`${data}`);
  }

  private onTopazChange(data) {
    this.topazScore.setText(`${data}`);
  }

  private onGoldChange(data) {
    this.goldScore.setText(`${data}`);
  }
}
