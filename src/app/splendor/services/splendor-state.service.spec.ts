import { TestBed } from '@angular/core/testing';

import { SplendorStateService } from './splendor-state.service';

describe('SplendorStateService', () => {
  let service: SplendorStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SplendorStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
