import { Injectable } from '@angular/core';
import { SignalRService, PlayerStateService, GameUtilsService, GamePlayer, GameMessage, PlayerScoreService, CanvasStateService, GfxText } from '../../core';
import { Subscription } from 'rxjs';
import { SplendorMessageType, SplendorGemType, SplendorPhase } from '../splendor-constants';
import { SplendorCard, SplendorNoble, SplendorClaimGems, SplendorPlayerMat } from '../dtos/splendor-dtos.model';

@Injectable({
  providedIn: 'root'
})
export class SplendorStateService {
  creatorId: string;
  currentPhase: SplendorPhase;
  activePlayers: Array<GamePlayer> = [];
  tierOneCards: Array<SplendorCard> = [];
  tierTwoCards: Array<SplendorCard> = [];
  tierThreeCards: Array<SplendorCard> = [];
  nobles: Array<SplendorNoble> = [];
  firstPlayerId: string;
  currentPlayerId: string;
  dealtNobles: Array<SplendorNoble> = [];
  dealtTierOne: Array<SplendorCard> = [];
  dealtTierTwo: Array<SplendorCard> = [];
  dealtTierThree: Array<SplendorCard> = [];
  ownedCards: Map<string, Array<SplendorCard>>;

  // variables that pertain to the local player only
  reservedCards: Array<SplendorCard>;
  chooseNoble: boolean;
  canvasId: string = 'splendor-canvas';

  private firstRun: boolean = true;
  private gameSubscription: Subscription;
  private hubName: string = 'game';
  private lastEvent: string;
  private playerMats: Array<SplendorPlayerMat> = [];
  private bankGfxButton: Array<number> = [];
  private bankGfxText: Array<GfxText> = [];
  private bankSubscriptions: Array<Subscription> = [];

  constructor(private signalR: SignalRService, private gameUtils: GameUtilsService, private score: PlayerScoreService, private playerState: PlayerStateService,
    private canvas: CanvasStateService) {
    this.score.configureMinMax('gold', 0, 5);
    this.currentPhase = SplendorPhase.Setup;
  }

  public startGameHub(playerName: string, groupName: string, playerId: string) {
    this.signalR.connect(this.hubName, `PlayerName=${playerName}&GroupName=${groupName}&PlayerId=${playerId}`);
    this.gameSubscription = this.signalR.subscribeToMessage(this.hubName, (data) => this.onMessage(data));
  }

  public closeGameHub() {
    this.gameSubscription.unsubscribe();
    this.signalR.close(this.hubName);
  }

  public advancePhase() {
    if (this.currentPhase == SplendorPhase.Setup) {
      this.initializeCards();
      this.resetGame();
    }
    this.currentPhase++;
    switch (this.currentPhase) {
      case SplendorPhase.Shuffle:
        this.shuffle();
        break;
      case SplendorPhase.PlayerTurn:
        this.dealCards();
        break;
      case SplendorPhase.Victory:
        this.sortWinners();
        break;
      case SplendorPhase.Reset:
        this.currentPhase = SplendorPhase.Setup;
        this.cleanupImages();
        this.sendGameMessage(SplendorMessageType.ResetGame, this.currentPhase);
        break;
    }
  }

  public getLastEvent(): string {
    return this.lastEvent;
  }

  public isGameCreator() {
    return this.creatorId == this.playerState.getPlayerId();
  }

  private cleanupImages() {
    this.dealtNobles.forEach((n) => {
      this.canvas.removeGfxObject(this.canvasId, n.gfxId);
    });

    this.dealtTierOne.forEach((c) => {
      this.canvas.removeGfxObject(this.canvasId, c.gfxId);
    });
    this.dealtTierTwo.forEach((c) => {
      this.canvas.removeGfxObject(this.canvasId, c.gfxId);
    });
    this.dealtTierThree.forEach((c) => {
      this.canvas.removeGfxObject(this.canvasId, c.gfxId);
    });
    this.reservedCards.forEach((c) => {
      this.canvas.removeGfxObject(this.canvasId, c.gfxId);
    });
  }

  private sortWinners() {
    this.activePlayers.sort((a, b) => {
      if (this.score.getScore(a.id, 'gamescore') == this.score.getScore(b.id, 'gamescore')) {
        return this.ownedCards.get(a.id).length - this.ownedCards.get(b.id).length;
      }
      else {
        return this.score.getScore(b.id, 'gamescore') - this.score.getScore(a.id, 'gamescore');
      }
    });
  }

  private shuffle() {
    this.gameUtils.shuffle(this.tierOneCards);
    this.gameUtils.shuffle(this.tierTwoCards);
    this.gameUtils.shuffle(this.tierThreeCards);
    this.gameUtils.shuffle(this.nobles);

    var player = Math.floor(Math.random() * this.activePlayers.length);
    this.firstPlayerId = this.activePlayers[player].id;
    this.currentPlayerId = this.firstPlayerId;

    this.sendGameMessage(SplendorMessageType.ShuffledTierOne, this.tierOneCards);
    this.sendGameMessage(SplendorMessageType.ShuffledTierTwo, this.tierTwoCards);
    this.sendGameMessage(SplendorMessageType.ShuffledTierThree, this.tierThreeCards);
    this.sendGameMessage(SplendorMessageType.ShuffledNobles, this.nobles);
    this.sendGameMessage(SplendorMessageType.StartPlayer, this.firstPlayerId);
    this.sendGameMessage(SplendorMessageType.CurrentPlayer, this.currentPlayerId);
  }

  public sendGameMessage(type: SplendorMessageType, message: any) {
    var gameMessage = new GameMessage(type, JSON.stringify(message), this.playerState.getPlayerId());
    this.signalR.sendMessage(this.hubName, gameMessage);
  }

  private onMessage(data: GameMessage) {
    if (data != null) {
      var message = JSON.parse(data.message);
      switch (data.type) {
        case SplendorMessageType.UpdatePlayerList:
          this.activePlayers = message;
          break;
        case SplendorMessageType.ShuffledTierOne:
          this.tierOneCards = message;
          break;
        case SplendorMessageType.ShuffledTierTwo:
          this.tierTwoCards = message;
          break;
        case SplendorMessageType.ShuffledTierThree:
          this.tierThreeCards = message;
          break;
        case SplendorMessageType.ShuffledNobles:
          this.nobles = message;
          this.setupPlayerGfx();
          this.dealNobles();
          if (this.currentPhase == SplendorPhase.Setup) {
            this.resetGame();
            this.currentPhase = SplendorPhase.PlayerTurn;
            this.dealCards();
          }
          else {
            this.advancePhase();
          }
          break;
        case SplendorMessageType.StartPlayer:
          this.firstPlayerId = message;
          break;
        case SplendorMessageType.CurrentPlayer:
          this.currentPlayerId = message;
          this.updateGfxStates();
          break;
        case SplendorMessageType.ClaimGems:
          this.claimGems(message, data.playerId);
          break;
        case SplendorMessageType.AdvanceTurn:
          this.advanceTurn();
          break;
        case SplendorMessageType.RemoveCard:
          this.removeCard(parseInt(message), false);
          break;
        case SplendorMessageType.ClaimCard:
          this.claimCard(message, data.playerId);
          break;
        case SplendorMessageType.ClaimNoble:
          this.claimNoble(parseInt(message), data.playerId);
          break;
        case SplendorMessageType.ResetGame:
          this.currentPhase = SplendorPhase.Setup;
          if (this.isGameCreator()) {
            this.advancePhase();
          }
          else {
            this.currentPhase = SplendorPhase.Setup;
            this.cleanupImages();
            this.dealtNobles = new Array<SplendorNoble>();
            this.reservedCards = new Array<SplendorCard>();
            this.resetGame();
          }
          break;
        case SplendorMessageType.LastEvent:
          this.lastEvent = message;
          break;
      }
    }
  }

  public sendAdvanceTurn() {
    this.sendGameMessage(SplendorMessageType.AdvanceTurn, '');
  }
  
  public claimGems(message: SplendorClaimGems, playerId: string) {
    this.score.addScore(message.amount, playerId, this.getGemScoreId(message.gemType));
    this.score.subtractScore(message.amount, 'global', this.getGemScoreId(message.gemType));
  }

  public getGemScoreId(gemType: SplendorGemType) {
    switch (gemType) {
      case SplendorGemType.Diamond:
        return 'diamond';
      case SplendorGemType.Emerald:
        return 'emerald';
      case SplendorGemType.Sapphire:
        return 'sapphire';
      case SplendorGemType.Ruby:
        return 'ruby';
      case SplendorGemType.Topaz:
        return 'topaz';
      default:
        return 'gold';
    }
  }

  public addOwnedCard(card: SplendorCard, playerId: string) {
    this.ownedCards.get(playerId).push(card);
    this.score.addScore(card.points, playerId, 'gamescore');
    var mat = this.playerMats.find(m => m.playerId == playerId);
    switch (card.gemType) {
      case SplendorGemType.Diamond:
        mat.diamondProjects.setText(`${this.getPlayerMines(playerId, card.gemType)}`);
        break;
      case SplendorGemType.Emerald:
        mat.emeraldProjects.setText(`${this.getPlayerMines(playerId, card.gemType)}`);
        break;
      case SplendorGemType.Sapphire:
        mat.sapphireProjects.setText(`${this.getPlayerMines(playerId, card.gemType)}`);
        break;
      case SplendorGemType.Ruby:
        mat.rubyProjects.setText(`${this.getPlayerMines(playerId, card.gemType)}`);
        break;
      case SplendorGemType.Topaz:
        mat.topazProjects.setText(`${this.getPlayerMines(playerId, card.gemType)}`);
        break;
    }
  }

  public getPlayerGems(playerId: string, gemType: SplendorGemType): number {
    return this.score.getScore(playerId, this.getGemScoreId(gemType));
  }

  public getPlayerMines(playerId: string, gemType: SplendorGemType): number {
    if (playerId == 'global') {
      return 0;
    }
    return this.ownedCards.get(playerId).filter(c => c.gemType == gemType).length;
  }

  public canClaimCard(playerId: string, card: SplendorCard): boolean {
    var diamonds = Math.max(card.diamondCost - this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Diamond) - this.getPlayerGems(this.playerState.getPlayerId(), SplendorGemType.Diamond), 0);
    var emeralds = Math.max(card.emeraldCost - this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Emerald) - this.getPlayerGems(this.playerState.getPlayerId(), SplendorGemType.Emerald), 0);
    var sapphires = Math.max(card.sapphireCost - this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Sapphire) - this.getPlayerGems(this.playerState.getPlayerId(), SplendorGemType.Sapphire), 0);
    var rubies = Math.max(card.rubyCost - this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Ruby) - this.getPlayerGems(this.playerState.getPlayerId(), SplendorGemType.Ruby), 0);
    var topaz = Math.max(card.topazCost - this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Topaz) - this.getPlayerGems(this.playerState.getPlayerId(), SplendorGemType.Topaz), 0);

    return diamonds + emeralds + sapphires + rubies + topaz <= this.getPlayerGems(this.playerState.getPlayerId(), SplendorGemType.Gold) && this.currentPlayerId == playerId;
  }

  public canReserveCard(cardId: number): boolean {
    if (this.reservedCards.some(c => c.cardId == cardId)) {
      return false;
    }
    return this.reservedCards.length < 3 && this.currentPlayerId == this.playerState.getPlayerId();
  }

  public reserveCard(cardId: number): number {
    var card = this.findCardById(cardId, false);
    this.removeCard(cardId, true);
    this.reservedCards.push(card);
    this.createCardGfx(card, this.reservedCards.length - 1, 600, true);
    this.sendGameMessage(SplendorMessageType.RemoveCard, cardId);
    this.sendGameMessage(SplendorMessageType.LastEvent, `${this.playerState.getPlayerName()} reserved ${card.cardName}`);
    if (this.score.getScore('global', this.getGemScoreId(SplendorGemType.Gold)) > 0) {
      var claimed = {} as SplendorClaimGems;
      claimed.gemType = SplendorGemType.Gold;
      claimed.amount = 1;
      this.sendGameMessage(SplendorMessageType.ClaimGems, claimed);
      return 1;
    }

    return 0;
  }

  public claimedNoble(playerId: string): boolean {
    var claimableNobleCount = 0;
    this.dealtNobles.forEach((n, index) => {
      if (this.canClaimNoble(index, playerId)) {
        claimableNobleCount++;
      }
    });

    if (claimableNobleCount > 1) {
      this.chooseNoble = true;
      this.dealtNobles.forEach((n, index) => {
        var selection = this.canvas.getGfxObject(this.canvasId, n.selectionId);
        selection.setVisible(this.canClaimNoble(index, playerId));
      });
      return true;
    }
    else if (claimableNobleCount == 1) {
      this.dealtNobles.forEach((n, index) => {
        if (this.canClaimNoble(index, playerId)) {
          this.sendGameMessage(SplendorMessageType.ClaimNoble, index);
          this.chooseNoble = false;
        }
      });
    }

    return false;
  }

  public canClaimNoble(index: number, playerId: string): boolean {
    if (this.dealtNobles[index] != null && playerId == this.playerState.getPlayerId()) {
      return this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Diamond) >= this.dealtNobles[index].diamondCost &&
        this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Emerald) >= this.dealtNobles[index].emeraldCost &&
        this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Sapphire) >= this.dealtNobles[index].sapphireCost &&
        this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Ruby) >= this.dealtNobles[index].rubyCost &&
        this.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Topaz) >= this.dealtNobles[index].topazCost &&
        this.dealtNobles[index].claimed == '';
    }

    return false;
  }

  public findCardByClaimId(claimId: number): SplendorCard {
    var card = null;
    var cards = [this.dealtTierOne, this.dealtTierTwo, this.dealtTierThree, this.reservedCards];
    cards.forEach((findCard, index) => {
      var index = findCard.findIndex(c => c != null && c.claimId == claimId);
      if (index >= 0) {
        card = findCard[index];
      }
    });

    return card;
  }

  public findCardByReserveId(reserveId: number): SplendorCard {
    var card = null;
    var cards = [this.dealtTierOne, this.dealtTierTwo, this.dealtTierThree, this.reservedCards];
    cards.forEach((findCard, index) => {
      var index = findCard.findIndex(c => c != null && c.reserveId == reserveId);
      if (index >= 0) {
        card = findCard[index];
      }
    });

    return card;
  }
  
  public findCardById(cardId: number, includeReserve: boolean): SplendorCard {
    var card = null;
    var cards = [this.dealtTierOne, this.dealtTierTwo, this.dealtTierThree, this.reservedCards];
    cards.forEach((findCard, index) => {
      if (index < 3 || includeReserve) {
        var index = findCard.findIndex(c => c != null && c.cardId == cardId);
        if (index >= 0) {
          card = findCard[index];
        }
      }
    });

    return card;
  }

  public claimCard(card: SplendorCard, playerId: string) {
    this.removeCard(card.cardId, true);
    if (card != null) {
      this.addOwnedCard(card, playerId);
    }

    this.claimedNoble(playerId);
  }

  public claimNoble(index: number, playerId: string) {
    this.score.addScore(this.dealtNobles[index].points, playerId, 'gamescore');
    this.dealtNobles[index].claimed = playerId;
    var gfxText = this.canvas.createText(this.canvasId, `Claimed by ${this.getPlayerName(playerId)}`, '20px Arial', 'red', this.dealtNobles[index].gfxId)
    gfxText.rotate(45 * Math.PI / 180);
    gfxText.translate(10, 10);
  }

  public getPlayerName(playerId: string): string {
    return this.activePlayers.find(p => p.id == playerId).name;
  }

  public removeCard(cardId: number, includeReserve: boolean): SplendorCard {
    var card = null;
    var index = this.dealtTierOne.findIndex(c => c!= null && c.cardId == cardId);
    if (index >= 0) {
      card = this.dealtTierOne[index];
      this.dealtTierOne[index] = null;
      if (includeReserve) {
        this.canvas.removeGfxObject(this.canvasId, card.gfxId);
      }
    }
    index = this.dealtTierTwo.findIndex(c => c != null && c.cardId == cardId);
    if (index >= 0) {
      card = this.dealtTierTwo[index];
      this.dealtTierTwo[index] = null;
      if (includeReserve) {
        this.canvas.removeGfxObject(this.canvasId, card.gfxId);
      }
    }
    index = this.dealtTierThree.findIndex(c => c != null && c.cardId == cardId);
    if (index >= 0) {
      card = this.dealtTierThree[index];
      this.dealtTierThree[index] = null;
      if (includeReserve) {
        this.canvas.removeGfxObject(this.canvasId, card.gfxId);
      }
    }

    if (includeReserve) {
      index = this.reservedCards.findIndex(c => c != null && c.cardId == cardId);
      if (index >= 0) {
        card = this.reservedCards[index];
        this.reservedCards.splice(index, 1);  // we don't maintain a full list of empty cards in the reserved card list for the player
        this.canvas.removeGfxObject(this.canvasId, card.gfxId);
        for (var i = index; i < this.reservedCards.length; i++) {
          var reserve = this.reservedCards[index];
          var gfx = this.canvas.getGfxObject(this.canvasId, reserve.gfxId);
          gfx.setTranslation(635 + i * 120, 600); // shift remaining reserved cards down
        }
      }
    }

    return card;
  }

  private advanceTurn() {
    var index = this.activePlayers.findIndex(p => p.id == this.currentPlayerId);
    index++;
    if (index == this.activePlayers.length) {
      index = 0;
    }

    this.dealCards();

    this.currentPlayerId = this.activePlayers[index].id;
    this.updateGfxStates();

    if (this.isEndGame()) {
      this.advancePhase();
    }
  }

  private updateGfxStates() {
    this.dealtTierOne.forEach((c) => {
      if (c == null) {
        return;
      }

      this.setCardActionVisibility(c);
    });

    this.dealtTierTwo.forEach((c) => {
      if (c == null) {
        return;
      }

      this.setCardActionVisibility(c);
    });

    this.dealtTierThree.forEach((c) => {
      if (c == null) {
        return;
      }

      this.setCardActionVisibility(c);
    });

    this.reservedCards.forEach((c) => {
      if (c == null) {
        return;
      }

      this.setCardActionVisibility(c);
    })
  }

  private setCardActionVisibility(card: SplendorCard) {
    var container = this.canvas.getGfxObject(this.canvasId, card.containerId);
    if (container != null) {
      container.setVisible(this.playerState.getPlayerId() == this.currentPlayerId);
    }

    var claim = this.canvas.getGfxObject(this.canvasId, card.claimId);
    if (claim != null) {
      claim.setVisible(this.canClaimCard(this.playerState.getPlayerId(), card));
    }

    var reserve = this.canvas.getGfxObject(this.canvasId, card.reserveId);
    if (reserve != null) {
      reserve.setVisible(this.canReserveCard(card.cardId));
    }
  }

  private isEndGame(): boolean {
    for (var i = 0; i < this.activePlayers.length; i++) {
      if (this.score.getScore(this.activePlayers[i].id, 'gamescore') >= 15) {
        return this.currentPlayerId == this.firstPlayerId;
      }
    }

    return false;
  }

  private dealNobles() {
    while (this.dealtNobles.length < this.activePlayers.length + 1) {
      var noble = this.nobles.pop();
      var nobleImage = this.canvas.createTextureAtlas(this.canvasId, noble.imageUrl, 4, 3, noble.atlasIndex, 0);
      nobleImage.translate(15 + this.dealtNobles.length * 160, 5);
      noble.gfxId = nobleImage.getId();
      var selection = this.canvas.createButton(this.canvasId, 141, 142, 'yellow', false, 4, 0, 0, '', '', '', noble.gfxId);
      selection.setVisible(false);
      noble.selectionId = selection.getId();
      this.dealtNobles.push(noble);
    }
  }

  private setupPlayerGfx() {
    if (this.playerMats.length == 0) {
      // create the mats and the gfx
      this.activePlayers.forEach((p, i) => {
        var mat = new SplendorPlayerMat(p.id);
        var rect = this.canvas.createRectangle(this.canvasId, 540, 60, '#39120A', true, 1, 0);
        mat.baseGfxId = rect.getId();
        rect.setTranslation(650, 200 + i * 70);
        var name = this.canvas.createText(this.canvasId, p.name.substring(0, 9), '20px Arial',
          this.playerState.getPlayerId() == p.id ? 'yellow' : 'white', rect.getId());
        name.translate(5, 20);

        var score = this.canvas.createText(this.canvasId, 'Score: 0', '20px Arial', this.playerState.getPlayerId() == p.id ? 'yellow' : 'white', rect.getId());
        score.translate(5, 50);
        mat.score = score;

        var diamondContainer = this.canvas.createContainer(this.canvasId, rect.getId());
        diamondContainer.translate(100, 0);
        var diamond = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 0, diamondContainer.getId());
        diamond.setScale(0.5, 0.5);
        diamond.translate(0, 10);
        var diamondScore = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', diamondContainer.getId());
        mat.diamondScore = diamondScore;
        diamondScore.translate(15, 20);
        var diamondProjects = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', diamondContainer.getId());
        mat.diamondProjects = diamondProjects;
        diamondProjects.translate(15, 50)

        var emeraldContainer = this.canvas.createContainer(this.canvasId, rect.getId());
        emeraldContainer.translate(170, 0);
        var emerald = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 3, emeraldContainer.getId());
        emerald.setScale(0.5, 0.5);
        emerald.translate(0, 10);
        var emeraldScore = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', emeraldContainer.getId());
        mat.emeraldScore = emeraldScore;
        emeraldScore.translate(15, 20);
        var emeraldProjects = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', emeraldContainer.getId());
        mat.emeraldProjects = emeraldProjects;
        emeraldProjects.translate(15, 50)

        var sapphireContainer = this.canvas.createContainer(this.canvasId, rect.getId());
        sapphireContainer.translate(240, 0);
        var sapphire = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 1, sapphireContainer.getId());
        sapphire.setScale(0.5, 0.5);
        sapphire.translate(0, 10);
        var sapphireScore = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', sapphireContainer.getId());
        mat.sapphireScore = sapphireScore;
        sapphireScore.translate(15, 20);
        var sapphireProjects = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', sapphireContainer.getId());
        mat.sapphireProjects = sapphireProjects;
        sapphireProjects.translate(15, 50)

        var rubyContainer = this.canvas.createContainer(this.canvasId, rect.getId());
        rubyContainer.translate(310, 0);
        var ruby = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 2, rubyContainer.getId());
        ruby.setScale(0.5, 0.5);
        ruby.translate(0, 10);
        var rubyScore = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', rubyContainer.getId());
        mat.rubyScore = rubyScore;
        rubyScore.translate(15, 20);
        var rubyProjects = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', rubyContainer.getId());
        mat.rubyProjects = rubyProjects;
        rubyProjects.translate(15, 50)

        var topazContainer = this.canvas.createContainer(this.canvasId, rect.getId());
        topazContainer.translate(380, 0);
        var topaz = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 4, topazContainer.getId());
        topaz.setScale(0.5, 0.5);
        topaz.translate(0, 10);
        var topazScore = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', topazContainer.getId());
        mat.topazScore = topazScore;
        topazScore.translate(15, 20);
        var topazProjects = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', topazContainer.getId());
        mat.topazProjects = topazProjects;
        topazProjects.translate(15, 50)

        var goldContainer = this.canvas.createContainer(this.canvasId, rect.getId());
        goldContainer.translate(450, 0);
        var gold = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 5, goldContainer.getId());
        gold.setScale(0.5, 0.5);
        gold.translate(0, 10);
        var goldScore = this.canvas.createText(this.canvasId, '0', '20px Arial', 'white', goldContainer.getId());
        mat.goldScore = goldScore;
        goldScore.translate(15, 20);

        mat.scoreSubscriber(this.score);

        this.playerMats.push(mat);
      });
    }
    else {
      this.playerMats.forEach((m) => {
        m.diamondProjects.setText('0');
        m.emeraldProjects.setText('0');
        m.sapphireProjects.setText('0');
        m.rubyProjects.setText('0');
        m.topazProjects.setText('0');
      });
    }
  }

  private dealCards() {
    while (this.tierOneCards.length > 0 && this.dealtTierOne.some(c => c == null)) {
      var card = this.tierOneCards.pop();
      var index = this.dealtTierOne.findIndex(c => c == null);
      this.dealtTierOne[index] = card;
      this.createCardGfx(card, index, 600, false);
    }

    while (this.tierTwoCards.length > 0 && this.dealtTierTwo.some(c => c == null)) {
      var card = this.tierTwoCards.pop();
      var index = this.dealtTierTwo.findIndex(c => c == null);
      this.dealtTierTwo[index] = card;
      this.createCardGfx(card, index, 400, false);
    }

    while (this.tierThreeCards.length > 0 && this.dealtTierThree.some(c => c == null)) {
      var card = this.tierThreeCards.pop();
      var index = this.dealtTierThree.findIndex(c => c == null);
      this.dealtTierThree[index] = card;
      this.createCardGfx(card, index, 200, false);
    }
  }

  private createCardGfx(card: SplendorCard, index: number, y: number, isReserve: boolean) {
    var x = 135 + index * 120 + (isReserve ? 500 : 0);
    var gfx = this.canvas.createTextureAtlas(this.canvasId, card.imageUrl, 3, 3, card.atlasIndex, 0);
    gfx.translate(x, y);
    card.gfxId = gfx.getId();
    var container = this.canvas.createContainer(this.canvasId, gfx.getId());
    container.translate(0, 157);
    container.setVisible(true);
    card.containerId = container.getId();

    var claimButton = this.canvas.createButton(this.canvasId, 56, 20, 'white', true, 1, 10, 15, 'Claim', '10px Arial', 'black', container.getId());
    card.claimId = claimButton.getId();
    claimButton.setVisible(false);

    if (!isReserve) {
      var reserveButton = this.canvas.createButton(this.canvasId, 56, 20, 'gold', true, 1, 5, 15, 'Reserve', '10px Arial', 'black', container.getId());
      reserveButton.translate(57, 0);
      reserveButton.setEnabled(true);
      reserveButton.setVisible(false);
      card.reserveId = reserveButton.getId();
    }
    else {
      card.reserveId = -1;
    }
  }

  public getClickedGemType(id: number): SplendorGemType {
    return this.bankGfxButton.findIndex(g => g == id);
  }

  private resetGame() {
    this.lastEvent = 'Start of Game';

    this.chooseNoble = false;
    this.reservedCards = new Array<SplendorCard>();

    var gems = 3 + this.activePlayers.length;
    if (this.activePlayers.length < 4) {
      gems--;
    }
    this.score.configureMinMax('diamond', 0, gems);
    this.score.configureMinMax('emerald', 0, gems);
    this.score.configureMinMax('sapphire', 0, gems);
    this.score.configureMinMax('ruby', 0, gems);
    this.score.configureMinMax('topaz', 0, gems);
    this.score.setScore(gems, 'global', 'diamond');
    this.score.setScore(gems, 'global', 'emerald');
    this.score.setScore(gems, 'global', 'sapphire');
    this.score.setScore(gems, 'global', 'ruby');
    this.score.setScore(gems, 'global', 'topaz');
    this.score.setScore(5, 'global', 'gold');

    this.ownedCards = new Map<string, Array<SplendorCard>>();
    this.activePlayers.forEach((p) => {
      this.score.setScore(0, p.id, 'diamond');
      this.score.setScore(0, p.id, 'emerald');
      this.score.setScore(0, p.id, 'sapphire');
      this.score.setScore(0, p.id, 'ruby');
      this.score.setScore(0, p.id, 'topaz');
      this.score.setScore(0, p.id, 'gold');
      this.score.setScore(0, p.id, 'gamescore');
      this.ownedCards.set(p.id, new Array<SplendorCard>());
    });

    this.dealtTierOne = new Array<SplendorCard>();
    this.dealtTierTwo = new Array<SplendorCard>();
    this.dealtTierThree = new Array<SplendorCard>();

    for (var i = 0; i < 4; i++) {
      this.dealtTierOne.push(null);
      this.dealtTierTwo.push(null);
      this.dealtTierThree.push(null);
    }

    if (this.firstRun) {
      this.firstRun = false;
      var tier1 = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/card-backs.jpg', 1, 3, 0, 0);
      tier1.translate(15, 600);
      var tier2 = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/card-backs.jpg', 1, 3, 1, 0);
      tier2.translate(15, 400);
      var tier3 = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/card-backs.jpg', 1, 3, 2, 0);
      tier3.translate(15, 200);

      var bankDiamonds = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 0, 0);
      bankDiamonds.translate(820, 10);
      bankDiamonds.setEnabled(true);
      this.bankGfxButton.push(bankDiamonds.getId());
      var diamondText = this.canvas.createText(this.canvasId, '0', '30px Arial', 'white', bankDiamonds.getId());
      diamondText.translate(32, 47);
      this.bankGfxText.push(diamondText);
      this.bankSubscriptions.push(this.score.subscribeScoreChange('global', this.getGemScoreId(SplendorGemType.Diamond), (data) => this.onGemChange(data, SplendorGemType.Diamond)));

      var bankEmeralds = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 3, 0);
      bankEmeralds.translate(920, 10);
      bankEmeralds.setEnabled(true);
      this.bankGfxButton.push(bankEmeralds.getId());
      var emeraldText = this.canvas.createText(this.canvasId, '0', '30px Arial', 'white', bankEmeralds.getId());
      emeraldText.translate(32, 47);
      this.bankGfxText.push(emeraldText);
      this.bankSubscriptions.push(this.score.subscribeScoreChange('global', this.getGemScoreId(SplendorGemType.Emerald), (data) => this.onGemChange(data, SplendorGemType.Emerald)));

      var bankSapphires = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 1, 0);
      bankSapphires.translate(1020, 10);
      bankSapphires.setEnabled(true);
      this.bankGfxButton.push(bankSapphires.getId());
      var sapphireText = this.canvas.createText(this.canvasId, '0', '30px Arial', 'white', bankSapphires.getId());
      sapphireText.translate(32, 47);
      this.bankGfxText.push(sapphireText);
      this.bankSubscriptions.push(this.score.subscribeScoreChange('global', this.getGemScoreId(SplendorGemType.Sapphire), (data) => this.onGemChange(data, SplendorGemType.Sapphire)));

      var bankRubies = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 2, 0);
      bankRubies.translate(820, 100);
      bankRubies.setEnabled(true);
      this.bankGfxButton.push(bankRubies.getId());
      var rubyText = this.canvas.createText(this.canvasId, '0', '30px Arial', 'white', bankRubies.getId());
      rubyText.translate(32, 47);
      this.bankGfxText.push(rubyText);
      this.bankSubscriptions.push(this.score.subscribeScoreChange('global', this.getGemScoreId(SplendorGemType.Ruby), (data) => this.onGemChange(data, SplendorGemType.Ruby)));

      var bankTopaz = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 4, 0);
      bankTopaz.translate(920, 100);
      bankTopaz.setEnabled(true);
      this.bankGfxButton.push(bankTopaz.getId());
      var topazText = this.canvas.createText(this.canvasId, '0', '30px Arial', 'white', bankTopaz.getId());
      topazText.translate(32, 47);
      this.bankGfxText.push(topazText);
      this.bankSubscriptions.push(this.score.subscribeScoreChange('global', this.getGemScoreId(SplendorGemType.Topaz), (data) => this.onGemChange(data, SplendorGemType.Topaz)));

      var bankGold = this.canvas.createTextureAtlas(this.canvasId, './assets/splendor/gems.png', 2, 3, 5, 0);
      bankGold.translate(1020, 100);
      var goldText = this.canvas.createText(this.canvasId, '0', '30px Arial', 'white', bankGold.getId());
      goldText.translate(32, 47);
      this.bankGfxText.push(goldText);
      this.bankSubscriptions.push(this.score.subscribeScoreChange('global', this.getGemScoreId(SplendorGemType.Gold), (data) => this.onGemChange(data, SplendorGemType.Gold)));
    }
  }

  private onGemChange(data, gemType: SplendorGemType) {
    this.bankGfxText[gemType].setText(`${data}`);
  }

  private initializeCards() {
    this.tierOneCards = new Array<SplendorCard>();
    this.tierTwoCards = new Array<SplendorCard>();
    this.tierThreeCards = new Array<SplendorCard>();
    this.nobles = new Array<SplendorNoble>();
    this.dealtNobles = new Array<SplendorNoble>();

    this.tierOneCards.push(new SplendorCard(0, 'Diamond Mine', SplendorGemType.Diamond, 0, 0, 2, 1, 1, 1, './assets/splendor/diamond-1.jpg', 3));
    this.tierOneCards.push(new SplendorCard(1, 'Diamond Mine', SplendorGemType.Diamond, 0, 0, 0, 0, 2, 1, './assets/splendor/diamond-1.jpg', 0));
    this.tierOneCards.push(new SplendorCard(2, 'Diamond Mine', SplendorGemType.Diamond, 0, 0, 2, 2, 0, 1, './assets/splendor/diamond-1.jpg', 5));
    this.tierOneCards.push(new SplendorCard(3, 'Diamond Mine', SplendorGemType.Diamond, 0, 0, 1, 1, 1, 1, './assets/splendor/diamond-1.jpg', 4));
    this.tierOneCards.push(new SplendorCard(4, 'Diamond Mine', SplendorGemType.Diamond, 0, 0, 0, 2, 0, 2, './assets/splendor/diamond-1.jpg', 7));
    this.tierOneCards.push(new SplendorCard(5, 'Diamond Mine', SplendorGemType.Diamond, 0, 0, 0, 3, 0, 0, './assets/splendor/diamond-1.jpg', 6));
    this.tierOneCards.push(new SplendorCard(6, 'Diamond Mine', SplendorGemType.Diamond, 1, 0, 4, 0, 0, 0, './assets/splendor/diamond-1.jpg', 2));
    this.tierOneCards.push(new SplendorCard(7, 'Diamond Mine', SplendorGemType.Diamond, 0, 3, 0, 1, 0, 1, './assets/splendor/diamond-1.jpg', 1));

    this.tierOneCards.push(new SplendorCard(8, 'Emerald Mine', SplendorGemType.Emerald, 0, 1, 1, 3, 0, 0, './assets/splendor/emerald-1.jpg', 5));
    this.tierOneCards.push(new SplendorCard(9, 'Emerald Mine', SplendorGemType.Emerald, 0, 0, 0, 0, 3, 0, './assets/splendor/emerald-1.jpg', 6));
    this.tierOneCards.push(new SplendorCard(10, 'Emerald Mine', SplendorGemType.Emerald, 0, 1, 0, 1, 1, 1, './assets/splendor/emerald-1.jpg', 0));
    this.tierOneCards.push(new SplendorCard(11, 'Emerald Mine', SplendorGemType.Emerald, 0, 0, 0, 1, 2, 2, './assets/splendor/emerald-1.jpg', 2));
    this.tierOneCards.push(new SplendorCard(12, 'Emerald Mine', SplendorGemType.Emerald, 0, 0, 0, 2, 2, 0, './assets/splendor/emerald-1.jpg', 4));
    this.tierOneCards.push(new SplendorCard(13, 'Emerald Mine', SplendorGemType.Emerald, 1, 0, 0, 0, 0, 4, './assets/splendor/emerald-1.jpg', 1));
    this.tierOneCards.push(new SplendorCard(14, 'Emerald Mine', SplendorGemType.Emerald, 0, 2, 0, 1, 0, 0, './assets/splendor/emerald-1.jpg', 7));
    this.tierOneCards.push(new SplendorCard(15, 'Emerald Mine', SplendorGemType.Emerald, 0, 1, 0, 1, 1, 2, './assets/splendor/emerald-1.jpg', 3));

    this.tierOneCards.push(new SplendorCard(16, 'Sapphire Mine', SplendorGemType.Sapphire, 0, 0, 0, 0, 0, 3, './assets/splendor/sapphire-1.jpg', 5));
    this.tierOneCards.push(new SplendorCard(17, 'Sapphire Mine', SplendorGemType.Sapphire, 0, 1, 2, 0, 2, 0, './assets/splendor/sapphire-1.jpg', 4));
    this.tierOneCards.push(new SplendorCard(18, 'Sapphire Mine', SplendorGemType.Sapphire, 1, 0, 0, 0, 4, 0, './assets/splendor/sapphire-1.jpg', 7));
    this.tierOneCards.push(new SplendorCard(19, 'Sapphire Mine', SplendorGemType.Sapphire, 0, 1, 1, 0, 2, 1, './assets/splendor/sapphire-1.jpg', 3));
    this.tierOneCards.push(new SplendorCard(20, 'Sapphire Mine', SplendorGemType.Sapphire, 0, 0, 3, 1, 1, 0, './assets/splendor/sapphire-1.jpg', 1));
    this.tierOneCards.push(new SplendorCard(21, 'Sapphire Mine', SplendorGemType.Sapphire, 0, 1, 1, 0, 1, 1, './assets/splendor/sapphire-1.jpg', 0));
    this.tierOneCards.push(new SplendorCard(22, 'Sapphire Mine', SplendorGemType.Sapphire, 0, 0, 2, 0, 0, 2, './assets/splendor/sapphire-1.jpg', 6));
    this.tierOneCards.push(new SplendorCard(23, 'Sapphire Mine', SplendorGemType.Sapphire, 0, 1, 0, 0, 0, 2, './assets/splendor/sapphire-1.jpg', 2));

    this.tierOneCards.push(new SplendorCard(24, 'Ruby Mine', SplendorGemType.Ruby, 0, 3, 0, 0, 0, 0, './assets/splendor/ruby-1.jpg', 6));
    this.tierOneCards.push(new SplendorCard(25, 'Ruby Mine', SplendorGemType.Ruby, 0, 1, 0, 0, 1, 3, './assets/splendor/ruby-1.jpg', 1));
    this.tierOneCards.push(new SplendorCard(26, 'Ruby Mine', SplendorGemType.Ruby, 0, 2, 1, 0, 0, 2, './assets/splendor/ruby-1.jpg', 2));
    this.tierOneCards.push(new SplendorCard(27, 'Ruby Mine', SplendorGemType.Ruby, 0, 1, 1, 1, 0, 1, './assets/splendor/ruby-1.jpg', 0));
    this.tierOneCards.push(new SplendorCard(28, 'Ruby Mine', SplendorGemType.Ruby, 0, 2, 0, 0, 2, 0, './assets/splendor/ruby-1.jpg', 5));
    this.tierOneCards.push(new SplendorCard(29, 'Ruby Mine', SplendorGemType.Ruby, 1, 4, 0, 0, 0, 0, './assets/splendor/ruby-1.jpg', 3));
    this.tierOneCards.push(new SplendorCard(30, 'Ruby Mine', SplendorGemType.Ruby, 0, 0, 1, 2, 0, 0, './assets/splendor/ruby-1.jpg', 4));
    this.tierOneCards.push(new SplendorCard(31, 'Ruby Mine', SplendorGemType.Ruby, 0, 2, 1, 1, 0, 1, './assets/splendor/ruby-1.jpg', 7));

    this.tierOneCards.push(new SplendorCard(32, 'Topaz Mine', SplendorGemType.Topaz, 0, 0, 1, 0, 3, 1, './assets/splendor/topaz-1.jpg', 4));
    this.tierOneCards.push(new SplendorCard(33, 'Topaz Mine', SplendorGemType.Topaz, 0, 0, 3, 0, 0, 0, './assets/splendor/topaz-1.jpg', 7));
    this.tierOneCards.push(new SplendorCard(34, 'Topaz Mine', SplendorGemType.Topaz, 0, 2, 0, 2, 1, 0, './assets/splendor/topaz-1.jpg', 0));
    this.tierOneCards.push(new SplendorCard(35, 'Topaz Mine', SplendorGemType.Topaz, 0, 1, 1, 1, 1, 0, './assets/splendor/topaz-1.jpg', 5));
    this.tierOneCards.push(new SplendorCard(36, 'Topaz Mine', SplendorGemType.Topaz, 0, 2, 2, 0, 0, 0, './assets/splendor/topaz-1.jpg', 1));
    this.tierOneCards.push(new SplendorCard(37, 'Topaz Mine', SplendorGemType.Topaz, 1, 0, 0, 4, 0, 0, './assets/splendor/topaz-1.jpg', 3));
    this.tierOneCards.push(new SplendorCard(38, 'Topaz Mine', SplendorGemType.Topaz, 0, 1, 1, 2, 1, 0, './assets/splendor/topaz-1.jpg', 2));
    this.tierOneCards.push(new SplendorCard(39, 'Topaz Mine', SplendorGemType.Topaz, 0, 0, 2, 0, 1, 0, './assets/splendor/topaz-1.jpg', 6));

    this.tierTwoCards.push(new SplendorCard(40, 'Diamond Mine', SplendorGemType.Diamond, 2, 0, 0, 0, 5, 3, './assets/splendor/diamond-2.jpg', 2));
    this.tierTwoCards.push(new SplendorCard(41, 'Diamond Mine', SplendorGemType.Diamond, 2, 0, 0, 0, 5, 0, './assets/splendor/diamond-2.jpg', 4));
    this.tierTwoCards.push(new SplendorCard(42, 'Diamond Mine', SplendorGemType.Diamond, 1, 2, 0, 3, 3, 0, './assets/splendor/diamond-2.jpg', 5));
    this.tierTwoCards.push(new SplendorCard(43, 'Diamond Mine', SplendorGemType.Diamond, 2, 0, 1, 0, 4, 2, './assets/splendor/diamond-2.jpg', 3));
    this.tierTwoCards.push(new SplendorCard(44, 'Diamond Mine', SplendorGemType.Diamond, 1, 0, 3, 0, 2, 2, './assets/splendor/diamond-2.jpg', 1));
    this.tierTwoCards.push(new SplendorCard(45, 'Diamond Mine', SplendorGemType.Diamond, 3, 6, 0, 0, 0, 0, './assets/splendor/diamond-2.jpg', 0));

    this.tierTwoCards.push(new SplendorCard(46, 'Emerald Mine', SplendorGemType.Emerald, 2, 4, 0, 2, 0, 1, './assets/splendor/emerald-2.jpg', 1));
    this.tierTwoCards.push(new SplendorCard(47, 'Emerald Mine', SplendorGemType.Emerald, 1, 2, 0, 3, 0, 2, './assets/splendor/emerald-2.jpg', 0));
    this.tierTwoCards.push(new SplendorCard(48, 'Emerald Mine', SplendorGemType.Emerald, 1, 3, 2, 0, 3, 0, './assets/splendor/emerald-2.jpg', 2));
    this.tierTwoCards.push(new SplendorCard(49, 'Emerald Mine', SplendorGemType.Emerald, 2, 0, 3, 5, 0, 0, './assets/splendor/emerald-2.jpg', 5));
    this.tierTwoCards.push(new SplendorCard(50, 'Emerald Mine', SplendorGemType.Emerald, 2, 0, 5, 0, 0, 0, './assets/splendor/emerald-2.jpg', 4));
    this.tierTwoCards.push(new SplendorCard(51, 'Emerald Mine', SplendorGemType.Emerald, 3, 0, 6, 0, 0, 0, './assets/splendor/emerald-2.jpg', 3));

    this.tierTwoCards.push(new SplendorCard(52, 'Sapphire Mine', SplendorGemType.Sapphire, 2, 5, 0, 3, 0, 0, './assets/splendor/sapphire-2.jpg', 4));
    this.tierTwoCards.push(new SplendorCard(53, 'Sapphire Mine', SplendorGemType.Sapphire, 2, 0, 0, 5, 0, 0, './assets/splendor/sapphire-2.jpg', 2));
    this.tierTwoCards.push(new SplendorCard(54, 'Sapphire Mine', SplendorGemType.Sapphire, 1, 0, 3, 2, 0, 3, './assets/splendor/sapphire-2.jpg', 0));
    this.tierTwoCards.push(new SplendorCard(55, 'Sapphire Mine', SplendorGemType.Sapphire, 1, 0, 2, 2, 3, 0, './assets/splendor/sapphire-2.jpg', 5));
    this.tierTwoCards.push(new SplendorCard(56, 'Sapphire Mine', SplendorGemType.Sapphire, 2, 2, 0, 0, 1, 4, './assets/splendor/sapphire-2.jpg', 1));
    this.tierTwoCards.push(new SplendorCard(57, 'Sapphire Mine', SplendorGemType.Sapphire, 3, 0, 0, 6, 0, 0, './assets/splendor/sapphire-2.jpg', 3));

    this.tierTwoCards.push(new SplendorCard(58, 'Ruby Mine', SplendorGemType.Ruby, 1, 0, 0, 3, 2, 3, './assets/splendor/ruby-2.jpg', 0));
    this.tierTwoCards.push(new SplendorCard(59, 'Ruby Mine', SplendorGemType.Ruby, 2, 3, 0, 0, 0, 5, './assets/splendor/ruby-2.jpg', 5));
    this.tierTwoCards.push(new SplendorCard(60, 'Ruby Mine', SplendorGemType.Ruby, 2, 0, 0, 0, 0, 5, './assets/splendor/ruby-2.jpg', 2));
    this.tierTwoCards.push(new SplendorCard(61, 'Ruby Mine', SplendorGemType.Ruby, 1, 2, 0, 0, 2, 3, './assets/splendor/ruby-2.jpg', 4));
    this.tierTwoCards.push(new SplendorCard(62, 'Ruby Mine', SplendorGemType.Ruby, 2, 1, 2, 4, 0, 0, './assets/splendor/ruby-2.jpg', 1));
    this.tierTwoCards.push(new SplendorCard(63, 'Ruby Mine', SplendorGemType.Ruby, 3, 0, 0, 0, 6, 0, './assets/splendor/ruby-2.jpg', 3));

    this.tierTwoCards.push(new SplendorCard(64, 'Topaz Mine', SplendorGemType.Topaz, 2, 0, 5, 0, 3, 0, './assets/splendor/topaz-2.jpg', 2));
    this.tierTwoCards.push(new SplendorCard(65, 'Topaz Mine', SplendorGemType.Topaz, 2, 0, 4, 1, 2, 0, './assets/splendor/topaz-2.jpg', 3));
    this.tierTwoCards.push(new SplendorCard(66, 'Topaz Mine', SplendorGemType.Topaz, 1, 3, 3, 0, 0, 2, './assets/splendor/topaz-2.jpg', 5));
    this.tierTwoCards.push(new SplendorCard(67, 'Topaz Mine', SplendorGemType.Topaz, 1, 3, 2, 2, 0, 0, './assets/splendor/topaz-2.jpg', 4));
    this.tierTwoCards.push(new SplendorCard(68, 'Topaz Mine', SplendorGemType.Topaz, 2, 5, 0, 0, 0, 0, './assets/splendor/topaz-2.jpg', 0));
    this.tierTwoCards.push(new SplendorCard(69, 'Topaz Mine', SplendorGemType.Topaz, 3, 0, 0, 0, 0, 6, './assets/splendor/topaz-2.jpg', 1));

    this.tierThreeCards.push(new SplendorCard(70, 'Diamond Mine', SplendorGemType.Diamond, 4, 0, 0, 0, 0, 7, './assets/splendor/diamond-1.jpg', 8));
    this.tierThreeCards.push(new SplendorCard(71, 'Diamond Mine', SplendorGemType.Diamond, 4, 3, 0, 0, 3, 6, './assets/splendor/diamond-2.jpg', 7));
    this.tierThreeCards.push(new SplendorCard(72, 'Diamond Mine', SplendorGemType.Diamond, 5, 3, 0, 0, 0, 7, './assets/splendor/diamond-2.jpg', 8));
    this.tierThreeCards.push(new SplendorCard(73, 'Diamond Mine', SplendorGemType.Diamond, 3, 0, 3, 3, 5, 3, './assets/splendor/diamond-2.jpg', 6));

    this.tierThreeCards.push(new SplendorCard(74, 'Emerald Mine', SplendorGemType.Emerald, 4, 3, 3, 6, 0, 0, './assets/splendor/emerald-2.jpg', 6));
    this.tierThreeCards.push(new SplendorCard(75, 'Emerald Mine', SplendorGemType.Emerald, 3, 5, 0, 3, 3, 3, './assets/splendor/emerald-1.jpg', 8));
    this.tierThreeCards.push(new SplendorCard(76, 'Emerald Mine', SplendorGemType.Emerald, 4, 0, 0, 7, 0, 0, './assets/splendor/emerald-2.jpg', 8));
    this.tierThreeCards.push(new SplendorCard(77, 'Emerald Mine', SplendorGemType.Emerald, 5, 0, 3, 7, 0, 0, './assets/splendor/emerald-2.jpg', 7));

    this.tierThreeCards.push(new SplendorCard(78, 'Sapphire Mine', SplendorGemType.Sapphire, 4, 6, 0, 3, 0, 3, './assets/splendor/sapphire-1.jpg', 8));
    this.tierThreeCards.push(new SplendorCard(79, 'Sapphire Mine', SplendorGemType.Sapphire, 3, 3, 3, 0, 3, 5, './assets/splendor/sapphire-2.jpg', 7));
    this.tierThreeCards.push(new SplendorCard(80, 'Sapphire Mine', SplendorGemType.Sapphire, 4, 7, 0, 0, 0, 0, './assets/splendor/sapphire-2.jpg', 6));
    this.tierThreeCards.push(new SplendorCard(81, 'Sapphire Mine', SplendorGemType.Sapphire, 5, 7, 0, 3, 0, 0, './assets/splendor/sapphire-2.jpg', 8));

    this.tierThreeCards.push(new SplendorCard(82, 'Ruby Mine', SplendorGemType.Ruby, 4, 0, 6, 3, 3, 0, './assets/splendor/ruby-2.jpg', 6));
    this.tierThreeCards.push(new SplendorCard(83, 'Ruby Mine', SplendorGemType.Ruby, 5, 0, 7, 0, 3, 0, './assets/splendor/ruby-2.jpg', 8));
    this.tierThreeCards.push(new SplendorCard(84, 'Ruby Mine', SplendorGemType.Ruby, 4, 0, 7, 0, 0, 0, './assets/splendor/ruby-2.jpg', 7));
    this.tierThreeCards.push(new SplendorCard(85, 'Ruby Mine', SplendorGemType.Ruby, 3, 3, 3, 5, 0, 3, './assets/splendor/ruby-1.jpg', 8));

    this.tierThreeCards.push(new SplendorCard(86, 'Topaz Mine', SplendorGemType.Topaz, 4, 0, 3, 0, 6, 3, './assets/splendor/topaz-1.jpg', 8));
    this.tierThreeCards.push(new SplendorCard(87, 'Topaz Mine', SplendorGemType.Topaz, 5, 0, 0, 0, 7, 3, './assets/splendor/topaz-2.jpg', 6));
    this.tierThreeCards.push(new SplendorCard(88, 'Topaz Mine', SplendorGemType.Topaz, 4, 0, 0, 0, 7, 0, './assets/splendor/topaz-2.jpg', 7));
    this.tierThreeCards.push(new SplendorCard(89, 'Topaz Mine', SplendorGemType.Topaz, 3, 3, 5, 3, 3, 0, './assets/splendor/topaz-2.jpg', 8));

    this.nobles.push(new SplendorNoble(0, 4, 0, 4, 0, 0, 3, '', './assets/splendor/nobles.jpg', 8));
    this.nobles.push(new SplendorNoble(1, 0, 4, 0, 4, 0, 3, '', './assets/splendor/nobles.jpg', 1));
    this.nobles.push(new SplendorNoble(2, 3, 0, 0, 3, 3, 3, '', './assets/splendor/nobles.jpg', 7));
    this.nobles.push(new SplendorNoble(3, 0, 0, 0, 4, 4, 3, '', './assets/splendor/nobles.jpg', 4));
    this.nobles.push(new SplendorNoble(4, 0, 3, 0, 3, 3, 3, '', './assets/splendor/nobles.jpg', 5));
    this.nobles.push(new SplendorNoble(5, 0, 4, 4, 0, 0, 3, '', './assets/splendor/nobles.jpg', 3));
    this.nobles.push(new SplendorNoble(6, 3, 3, 3, 0, 0, 3, '', './assets/splendor/nobles.jpg', 6));
    this.nobles.push(new SplendorNoble(7, 0, 3, 3, 3, 0, 3, '', './assets/splendor/nobles.jpg', 9));
    this.nobles.push(new SplendorNoble(8, 3, 0, 3, 0, 3, 3, '', './assets/splendor/nobles.jpg', 0));
    this.nobles.push(new SplendorNoble(9, 4, 0, 0, 0, 4, 3, '', './assets/splendor/nobles.jpg', 2));
  }
}
