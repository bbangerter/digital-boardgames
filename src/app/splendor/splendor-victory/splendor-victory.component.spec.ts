import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplendorVictoryComponent } from './splendor-victory.component';

describe('SplendorVictoryComponent', () => {
  let component: SplendorVictoryComponent;
  let fixture: ComponentFixture<SplendorVictoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplendorVictoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplendorVictoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
