import { Component } from '@angular/core';
import { SplendorStateService } from '../services/splendor-state.service';
import { PlayerScoreService, GamePlayer } from '../../core';

@Component({
  selector: 'splendor-victory',
  templateUrl: './splendor-victory.component.html',
  styleUrls: [
    './splendor-victory.component.scss',
    './../splendor.scss'
  ]
})
export class SplendorVictoryComponent {
  displayedColumns: string[] = ['Players', 'Score', 'Projects'];
  constructor(private gameState: SplendorStateService, private score: PlayerScoreService) { }

  public getPlayerName(playerId: string): string {
    return this.gameState.getPlayerName(playerId);
  }

  public getPlayerScore(playerId: string): number {
    return this.score.getScore(playerId, 'gamescore');
  }

  public getPlayerProjects(playerId: string): number {
    return this.gameState.ownedCards.get(playerId).length;
  }

  public getPlayers(): Array<GamePlayer> {
    return this.gameState.activePlayers;
  }

  public isGameCreator(): boolean {
    return this.gameState.isGameCreator();
  }

  public restart() {
    this.gameState.advancePhase();
  }
}
