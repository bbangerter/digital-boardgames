import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplendorSetupComponent } from './splendor-setup.component';

describe('SplendorSetupComponent', () => {
  let component: SplendorSetupComponent;
  let fixture: ComponentFixture<SplendorSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplendorSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplendorSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
