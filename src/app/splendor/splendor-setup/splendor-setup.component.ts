import { Component, Input } from '@angular/core';
import { SplendorStateService } from '../services/splendor-state.service';
import { GameStart } from '../../core';

@Component({
  selector: 'splendor-setup',
  templateUrl: './splendor-setup.component.html',
  styleUrls: [
    './splendor-setup.component.scss',
    './../splendor.scss'
  ]
})
export class SplendorSetupComponent {
  @Input() gameData: GameStart;
  constructor(private gameState: SplendorStateService) { }

  public gameCanStart(): boolean {
    for (var i = 0; i < this.gameData.playerIds.length; i++) {
      if (!this.gameState.activePlayers.some(p => p.id == this.gameData.playerIds[i]))
        return false;
    }

    return true;
  }

  public startRound() {
    this.gameState.advancePhase();
  }

  public isGameCreator(): boolean {
    return this.gameState.isGameCreator();
  }
}
