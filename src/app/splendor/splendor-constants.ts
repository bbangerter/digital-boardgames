export enum SplendorMessageType {
  UpdatePlayerList = 0,
  ShuffledTierOne = 1,
  ShuffledTierTwo = 2,
  ShuffledTierThree = 3,
  ShuffledNobles = 4,
  StartPlayer = 5,
  CurrentPlayer = 6,
  ClaimGems = 7,
  AdvanceTurn = 8,
  RemoveCard = 9,
  ClaimCard = 10,
  ClaimNoble = 11,
  ResetGame = 12,
  LastEvent = 13
}

export enum SplendorGemType {
  Diamond = 0,
  Emerald = 1,
  Sapphire = 2,
  Ruby = 3,
  Topaz = 4,
  Gold = 5
}

export enum SplendorPhase {
  Setup = 0,
  Shuffle = 1,
  PlayerTurn = 2,
  Victory = 3,
  Reset = 4
}
