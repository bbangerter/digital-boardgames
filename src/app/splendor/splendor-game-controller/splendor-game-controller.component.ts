import { Component, OnInit, OnDestroy, Input, AfterViewInit } from '@angular/core';
import { GameComponent, GameStart, PlayerStateService, CanvasStateService } from '../../core';
import { SplendorStateService } from '../services/splendor-state.service';
import { SplendorPhase } from '../splendor-constants';

@Component({
  selector: 'splendor-game-controller',
  templateUrl: './splendor-game-controller.component.html',
  styleUrls: [
    './splendor-game-controller.component.scss',
    './../splendor.scss'
  ]
})
export class SplendorGameControllerComponent implements OnInit, OnDestroy, AfterViewInit, GameComponent {
  @Input() gameData: GameStart;

  constructor(private playerState: PlayerStateService, private gameState: SplendorStateService, private canvas: CanvasStateService) { }

  ngOnInit(): void {
    this.gameState.startGameHub(this.playerState.getPlayerName(), this.gameData.gameId, this.playerState.getPlayerId());
    this.gameState.creatorId = this.gameData.creatorId;
  }

  ngAfterViewInit(): void {
    this.canvas.createTexture(this.gameState.canvasId, './assets/splendor/background.jpg', 0);
  }

  ngOnDestroy(): void {
    this.gameState.closeGameHub();
  }

  public splendorCanvasId(): string {
    return this.gameState.canvasId;
  }

  public getCurrentPhase(): number {
    return this.gameState.currentPhase;
  }

  public getPhaseName(): string {
    switch (this.gameState.currentPhase) {
      case SplendorPhase.Setup:
        return '';
      case SplendorPhase.Shuffle:
        return 'Shuffle';
      case SplendorPhase.PlayerTurn:
        return '';
      default:
        return '';
    }
  }
}
