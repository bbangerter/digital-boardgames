import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplendorGameControllerComponent } from './splendor-game-controller.component';

describe('SplendorGameControllerComponent', () => {
  let component: SplendorGameControllerComponent;
  let fixture: ComponentFixture<SplendorGameControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplendorGameControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplendorGameControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
