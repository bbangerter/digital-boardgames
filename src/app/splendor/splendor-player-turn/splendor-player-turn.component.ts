import { Component, OnInit, OnDestroy } from '@angular/core';
import { SplendorNoble, SplendorClaimGems, SplendorClaimedGems } from '../dtos/splendor-dtos.model';
import { SplendorStateService } from '../services/splendor-state.service';
import { PlayerStateService, PlayerScoreService, GamePlayer, CanvasStateService, GfxRectangle, GfxClickData } from '../../core';
import { SplendorGemType, SplendorMessageType } from '../splendor-constants';
import { Subscription } from 'rxjs';

@Component({
  selector: 'splendor-player-turn',
  templateUrl: './splendor-player-turn.component.html',
  styleUrls: [
    './splendor-player-turn.component.scss',
    './../splendor.scss'
  ]
})
export class SplendorPlayerTurnComponent implements OnInit, OnDestroy {
  claimedGems: Array<SplendorClaimedGems> = [];

  private takeGemsButton: GfxRectangle;
  private clickSubscription: Subscription;

  constructor(private gameState: SplendorStateService, private playerState: PlayerStateService, private score: PlayerScoreService, private canvas: CanvasStateService) {
    this.takeGemsButton = this.canvas.createButton(this.gameState.canvasId, 60, 30, 'white', true, 1, 8, 22, 'Take', '20px Arial', 'black', 0);
    this.takeGemsButton.translate(1100, 150);
    this.takeGemsButton.setVisible(false);
    this.takeGemsButton.setEnabled(true);
  }

  ngOnInit(): void {
    this.clickSubscription = this.canvas.subscribeClickEvent(this.gameState.canvasId, (data: GfxClickData) => this.onClick(data));
  }

  ngOnDestroy(): void {
    this.clickSubscription.unsubscribe();
  }

  private onClick(data: GfxClickData) {
    if (data != null) {
      var gemType = this.gameState.getClickedGemType(data.objectId);
      if (gemType >= SplendorGemType.Diamond && gemType < SplendorGemType.Gold) {
        if (this.totalOwnedGems() > 10) {
          var claimed = {} as SplendorClaimGems;
          claimed.gemType = gemType;
          claimed.amount = -1;
          this.gameState.sendGameMessage(SplendorMessageType.ClaimGems, claimed);
          if (this.totalOwnedGems() < 12) {
            this.gameState.sendAdvanceTurn();
          }
        }
        else if (this.canClaimGem('global', gemType)) {
          this.claimGem(gemType);
          this.takeGemsButton.setVisible(true);
        }

        return;
      }

      // ignore all other actions while we have exceeded the max 10 gem count
      if (this.totalOwnedGems() > 10) {
        return;
      }

      var index = this.gameState.dealtNobles.findIndex(n => n.selectionId == data.objectId);
      if (index > -1) {
        this.gameState.dealtNobles.forEach((n) => {
          this.canvas.getGfxObject(this.gameState.canvasId, n.selectionId).setVisible(false);
        });
        this.gameState.chooseNoble = false;
        this.claimNoble(index);
        this.gameState.sendAdvanceTurn();
      }

      // player needs to choose a noble
      if (this.gameState.chooseNoble) {
        return;
      }

      if (this.takeGemsButton.getId() == data.objectId) {
        this.claimGems();

        return;
      }

      var card = this.gameState.findCardByClaimId(data.objectId);
      if (card != null) {
        this.claimCard(card.cardId);
        this.resetClaimedGems();
        return;
      }

      card = this.gameState.findCardByReserveId(data.objectId);
      if (card != null) {
        var gold = this.gameState.reserveCard(card.cardId);
        this.resetClaimedGems();
        if (!this.gameState.claimedNoble(this.playerState.getPlayerId()) && this.totalOwnedGems() + gold <= 10) {
          this.gameState.sendAdvanceTurn();
        }
        return;
      }

      if (this.claimedGems.some(g => g.button.getId() == data.objectId)) {
        var index = this.claimedGems.findIndex(g => g.button.getId() == data.objectId);
        this.canvas.removeGfxObject(this.gameState.canvasId, this.claimedGems[index].button.getId());
        for (var i = index + 1; i < this.claimedGems.length; i++) {
          this.claimedGems[i].button.translate(0, -50);
        }
        this.claimedGems.splice(index, 1);
        this.takeGemsButton.setVisible(this.claimedGems.length > 1);
        return;
      }
    }
  }

  public getLastEvent(): string {
    return this.gameState.getLastEvent();
  }

  public canClaimGem(playerId: string, gemType: number): boolean {
    if (playerId != 'global' || !this.isCurrentPlayer()) {
      return false;
    }

    var totalClaimed = this.totalClaimedGems();

    var count = this.getPlayerGems(playerId, gemType);
    if (count < 1 || count - this.claimedGems.filter(g => g.gemType == gemType).length < 1 || (totalClaimed > 1 || count < 4) &&
      this.claimedGems.filter(g => g.gemType == gemType).length > 0 || totalClaimed == 3) {
      return false;
    }

    if (this.gameState.chooseNoble) {
      return false;
    }

    return this.claimedGems.filter(g => g.gemType == SplendorGemType.Diamond).length != 2 && this.claimedGems.filter(g => g.gemType == SplendorGemType.Emerald).length != 2 &&
      this.claimedGems.filter(g => g.gemType == SplendorGemType.Sapphire).length != 2 && this.claimedGems.filter(g => g.gemType == SplendorGemType.Ruby).length != 2 &&
      this.claimedGems.filter(g => g.gemType == SplendorGemType.Topaz).length != 2;
  }

  public claimCard(cardId: number) {
    var card = this.gameState.findCardById(cardId, true);
    this.gameState.sendGameMessage(SplendorMessageType.ClaimCard, card);

    var diamonds = Math.max(card.diamondCost - this.gameState.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Diamond), 0);
    var emeralds = Math.max(card.emeraldCost - this.gameState.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Emerald), 0);
    var sapphires = Math.max(card.sapphireCost - this.gameState.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Sapphire), 0);
    var rubies = Math.max(card.rubyCost - this.gameState.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Ruby), 0);
    var topaz = Math.max(card.topazCost - this.gameState.getPlayerMines(this.playerState.getPlayerId(), SplendorGemType.Topaz), 0);

    var extras = this.spendGems(diamonds, SplendorGemType.Diamond) + this.spendGems(emeralds, SplendorGemType.Emerald) + this.spendGems(sapphires, SplendorGemType.Sapphire) +
      this.spendGems(rubies, SplendorGemType.Ruby) + this.spendGems(topaz, SplendorGemType.Topaz);
    this.spendGems(extras, SplendorGemType.Gold);

    this.gameState.sendGameMessage(SplendorMessageType.LastEvent, `${this.playerState.getPlayerName()} purchased ${card.cardName}`);

    if (!this.gameState.claimedNoble(this.playerState.getPlayerId())) {
      this.gameState.sendAdvanceTurn();
    }
  }

  public claimNoble(index: number) {
    this.gameState.sendGameMessage(SplendorMessageType.ClaimNoble, index);
    this.gameState.sendAdvanceTurn();
  }

  private spendGems(amount: number, gemType: number): number {
    var spent = Math.min(amount, this.getPlayerGems(this.playerState.getPlayerId(), gemType));
    if (spent > 0) {
      var claimed = {} as SplendorClaimGems;
      claimed.gemType = gemType;
      claimed.amount = -spent;
      this.gameState.sendGameMessage(SplendorMessageType.ClaimGems, claimed);
    }

    return amount - spent;
  }

  public totalClaimedGems(): number {
    return this.claimedGems.length;
  }

  public isCurrentPlayer(): boolean {
    return this.gameState.currentPlayerId == this.playerState.getPlayerId();
  }

  public getFirstPlayer(): string {
    return this.gameState.activePlayers.find(p => p.id == this.gameState.firstPlayerId).name;
  }

  public getCurrentPlayer(): string {
    return this.gameState.activePlayers.find(p => p.id == this.gameState.currentPlayerId).name;
  }

  public getPlayerName(playerId: string): string {
    return this.gameState.getPlayerName(playerId);
  }

  public getPlayerGems(playerId: string, gemType: SplendorGemType): number {
    return this.gameState.getPlayerGems(playerId, gemType);
  }

  public canClaimAnyGems(): boolean {
    for (var i = SplendorGemType.Diamond; i < SplendorGemType.Gold; i++) {
      if (this.canClaimGem('global', i)) {
        return true;
      }
    }

    return false;
  }

  public claimGem(gemType: number) {
    var claimGemGfx = this.canvas.createTextureAtlas(this.gameState.canvasId, './assets/splendor/gems.png', 2, 3, this.getGemAtlasIndex(gemType), 0);
    claimGemGfx.setScale(0.5, 0.5);
    claimGemGfx.translate(1100, 10 + this.totalClaimedGems() * 50);
    claimGemGfx.setEnabled(true);

    this.claimedGems.push(new SplendorClaimedGems(gemType, claimGemGfx));
  }

  private getGemAtlasIndex(gemType: SplendorGemType): number {
    switch (gemType) {
      case SplendorGemType.Diamond:
        return 0;
      case SplendorGemType.Emerald:
        return 3;
      case SplendorGemType.Sapphire:
        return 1;
      case SplendorGemType.Ruby:
        return 2;
      case SplendorGemType.Topaz:
        return 4;
      default:
        return 0;
    }
  }

  public resetClaimedGems() {
    this.claimedGems.forEach((g) => {
      this.canvas.removeGfxObject(this.gameState.canvasId, g.button.getId());
    });
    this.claimedGems = [];

    this.takeGemsButton.setVisible(false);
  }

  public claimGems() {
    var message = `${this.playerState.getPlayerName()} took ${this.preClaimedGems()}`;
    this.gameState.sendGameMessage(SplendorMessageType.LastEvent, message);

    for (var i = SplendorGemType.Diamond; i <= SplendorGemType.Gold; i++) {
      if (this.claimedGems.filter(g => g.gemType == i).length > 0) {
        var claimed = {} as SplendorClaimGems;
        claimed.gemType = i;
        claimed.amount = this.claimedGems.filter(g => g.gemType == i).length;
        this.gameState.sendGameMessage(SplendorMessageType.ClaimGems, claimed);
      }
    }

    if (!this.gameState.claimedNoble(this.playerState.getPlayerId()) && this.totalOwnedGems() + this.claimedGems.length < 11) {
      this.gameState.sendAdvanceTurn();
    }

    this.resetClaimedGems();
  }

  public preClaimedGems(): string {
    if (this.totalClaimedGems() == 0) {
      return '';
    }

    var claimed = '';
    for (var i = SplendorGemType.Diamond; i < SplendorGemType.Gold; i++) {
      if (this.claimedGems.filter(g => g.gemType == i).length > 0) {
        claimed += `${this.claimedGems.filter(g => g.gemType == i).length} ${this.gameState.getGemScoreId(i)} `;
      }
    }

    return claimed;
  }

  public totalOwnedGems(): number {
    var totalOwned = 0;
    for (var i = SplendorGemType.Diamond; i <= SplendorGemType.Gold; i++) {
      totalOwned += this.score.getScore(this.playerState.getPlayerId(), this.gameState.getGemScoreId(i));
    }

    return totalOwned;
  }
}
