import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplendorPlayerTurnComponent } from './splendor-player-turn.component';

describe('SplendorPlayerTurnComponent', () => {
  let component: SplendorPlayerTurnComponent;
  let fixture: ComponentFixture<SplendorPlayerTurnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplendorPlayerTurnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplendorPlayerTurnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
