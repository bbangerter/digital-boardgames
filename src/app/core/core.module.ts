import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerScoreService } from './services/player-score.service';
import { PlayerStateService } from './services/player-state.service';
import { SignalRService } from './services/signal-r.service';
import { CanvasComponent } from './canvas/canvas.component';
import { CanvasStateService } from './services/canvas-state.service';

@NgModule({
  declarations: [CanvasComponent],
  exports: [
    CanvasComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CoreModule {
  static forRoot() {
    return {
      ngModule: CoreModule,
      providers: [
        PlayerScoreService,
        PlayerStateService,
        SignalRService,
        CanvasStateService
      ]
    }
  }
}
