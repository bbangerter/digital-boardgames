import { Component, ViewChild, ElementRef, AfterViewInit, Input, OnDestroy } from '@angular/core';
import { CanvasStateService } from '../services/canvas-state.service';

@Component({
  selector: 'core-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnDestroy, AfterViewInit {
  @Input() width: number;
  @Input() height: number;
  @Input() id: string;
  @ViewChild('coreCanvas', { static: false }) coreCanvas: ElementRef<HTMLCanvasElement>;

  private context: CanvasRenderingContext2D;
  private canvas: HTMLCanvasElement;

  constructor(private canvasService: CanvasStateService) { }

  ngOnDestroy(): void {
    this.canvasService.removeCanvas(this.id);
  }

  ngAfterViewInit(): void {
    this.canvas = this.coreCanvas.nativeElement;
    this.context = this.canvas.getContext('2d');
    this.canvasService.addCanvas(this.id, this.context, 0);
    this.coreCanvas.nativeElement.addEventListener('click', this.onMouseClick.bind(this), false);
  }

  private onMouseClick(event) {
    var viewportOffset = this.canvas.getBoundingClientRect();
    var x = event.clientX - viewportOffset.left;
    var y = event.clientY - viewportOffset.top;
    if (x < 0 || y < 0 || x > this.canvas.width || y > this.canvas.height) {
      return;
    }
    this.canvasService.onClick(this.id, x, y);
  }
}
