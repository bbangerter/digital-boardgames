import { Subscription, Subject } from 'rxjs';

export class SceneGraph {
  private gfxList: Array<GfxObject>;
  private gfxIndex: number;
  private clickSubject: Subject<GfxClickData>;
  private refreshTimer: any = null;

  constructor(private context: CanvasRenderingContext2D, private framerate: number) {
    this.gfxList = new Array<GfxObject>();
    this.gfxIndex = 0;
    this.updateCanvas();
    this.clickSubject = new Subject<GfxClickData>();

    if (this.framerate > 0) {
      this.refreshTimer = setTimeout(this.updateCanvas.bind(this), 1000 / this.framerate);
    }
  }

  public setFPS(fps: number) {
    var prevFPS = this.framerate;
    this.framerate = fps;
    if (prevFPS == 0 && this.framerate > 0) {
      this.refreshTimer = setTimeout(this.updateCanvas.bind(this), 1000 / this.framerate);
    }
  }

  public clearSceneGraph() {
    this.gfxList = new Array<GfxObject>();
  }

  private updateCanvas() {
    this.animate(this.framerate > 0 ? 1 / this.framerate : 0);
    this.draw();
    if (this.framerate > 0) {
      this.refreshTimer = setTimeout(this.updateCanvas.bind(this), 1000 / this.framerate);
    }
    else {
      this.refreshTimer = null;
    }
  }

  public requestRefresh() {
    if (this.refreshTimer == null) {
      this.refreshTimer = setTimeout(this.updateCanvas.bind(this), 0);
    }
  }

  public getNextGfxId() {
    this.gfxIndex++;
    return this.gfxIndex;
  }

  public addGfx(gfxObject: GfxObject, parent: number) {
    if (parent == 0) {
      this.gfxList.push(gfxObject);
    }
    else {
      var parentGfx = this.getGfxObject(parent);
      if (parentGfx != null) {
        parentGfx.addChild(gfxObject);
      }
    }
  }

  public removeGfx(id: number) {
    var index = this.gfxList.findIndex(g => g.getId() == id);
    if (index > -1) {
      this.gfxList.splice(index, 1);
    }
    else {
      this.gfxList.forEach((g) => {
        g.removeChild(id);
      });
    }
  }

  public getGfxObject(id: number): GfxObject {
    for (var i = 0; i < this.gfxList.length; i++) {
      var gfx = this.gfxList[i].getGfxObject(id);
      if (gfx != null) {
        return gfx;
      }
    }

    return null;
  }

  public animate(deltaTime: number) {
    this.gfxList.forEach(g => g.animate(deltaTime));
  }

  public draw() {
    this.gfxList.forEach(g => g.draw(this.context));
  }

  public subscribeClickEvent(callback): Subscription {
    return this.clickSubject.subscribe((data) => callback(data));
  }

  public clickEvent(x: number, y: number) {
    for (var i = 0; i < this.gfxList.length; i++) {
      var clickData = this.gfxList[i].clicked(x, y);
      if (clickData != null) {
        this.clickSubject.next(clickData);
      }
    }
  }
}

export class GfxClickData {
  constructor(public objectId: number, public x: number, public y: number) { }
}

export class GfxObject {
  private id: number;
  protected xPosition: number;
  protected yPosition: number;
  protected rotation: number;
  protected xScale: number;
  protected yScale: number;
  protected visible: boolean;
  protected children: Array<GfxObject>;
  private parent: any;
  private xVelocity: number;
  private yVelocity: number;

  constructor(sceneGraph: SceneGraph, parent: number) {
    this.parent = sceneGraph;
    sceneGraph.addGfx(this, parent);
    this.id = sceneGraph.getNextGfxId();
    this.children = new Array<GfxObject>();
    this.visible = true;
    this.xVelocity = 0;
    this.yVelocity = 0;
    this.resetMatrix();
  }

  public getXPosition(): number {
    return this.xPosition;
  }

  public getYPosition(): number {
    return this.yPosition;
  }

  public requestRefresh() {
    if (this.parent != null) {
      this.parent.requestRefresh();
    }
  }

  public getId(): number {
    return this.id;
  }

  public getGfxObject(id: number): GfxObject {
    if (id == this.getId()) {
      return this;
    }

    for (var i = 0; i < this.children.length; i++) {
      var gfx = this.children[i].getGfxObject(id);
      if (gfx != null) {
        return gfx;
      }
    }

    return null;
  }

  public addChild(child: GfxObject) {
    this.children.push(child);
    child.parent = this;
    this.requestRefresh();
  }

  public removeChild(childId: number) {
    var index = this.children.findIndex(c => c.id == childId);
    if (index > -1) {
      this.children.splice(index, 1);
      this.requestRefresh();
    }
    else {
      this.children.forEach((g) => {
        g.removeChild(childId);
      });
    }
  }

  public removeAllChildren() {
    this.children = new Array<GfxObject>();
  }

  public accelerate(xVelocity: number, yVelocity: number) {
    this.xVelocity += xVelocity;
    this.yVelocity += yVelocity;
  }

  public setVisible(visible: boolean) {
    this.visible = visible;
    this.requestRefresh();
  }

  public isVisible(): boolean {
    return this.visible;
  }

  public resetMatrix() {
    this.xPosition = 0;
    this.yPosition = 0;
    this.rotation = 0;
    this.xScale = 1;
    this.yScale = 1;
    this.requestRefresh();
  }

  public translate(x: number, y: number) {
    this.xPosition += x;
    this.yPosition += y;
    this.requestRefresh();
  }

  public rotate(rotation: number) {
    this.rotation += rotation;
    this.requestRefresh();
  }

  public setTranslation(x: number, y: number) {
    this.xPosition = x;
    this.yPosition = y;
    this.requestRefresh();
  }

  public setRotation(rotation: number) {
    this.rotation = rotation;
    this.requestRefresh();
  }

  public setScale(xScale: number, yScale: number) {
    this.xScale = xScale;
    this.yScale = yScale;
    this.requestRefresh();
  }

  public animate(deltaTime: number) {
    this.updatePosition(deltaTime);
  }

  public draw(canvas: CanvasRenderingContext2D) {
    if (!this.isVisible()) {
      return;
    }

    canvas.save();
    
    canvas.rotate(this.rotation);
    canvas.translate(this.xPosition, this.yPosition);
    canvas.scale(this.xScale, this.yScale);
    this.render(canvas);
    this.children.forEach((c) => c.draw(canvas));

    canvas.restore();
  }

  public updatePosition(deltaTime: number) {
    this.translate(this.xVelocity * deltaTime, this.yVelocity * deltaTime);
  }

  public render(context: CanvasRenderingContext2D) { } // do nothing with base object

  public clicked(x: number, y: number): GfxClickData {
    for (var i = 0; i < this.children.length; i++) {
      if (this.children[i].isVisible()) {
        var gfx = this.children[i].clicked(x - this.xPosition, y - this.yPosition);
        if (gfx != null) {
          return gfx;
        }
      }
    }

    return null;
  }
}

export class GfxClickable extends GfxObject {
  protected enabled: boolean;

  constructor(sceneGraph: SceneGraph, protected width: number, protected height: number, parent: number) {
    super(sceneGraph, parent);
    this.enabled = false;
  }

  public getWidth(): number {
    return this.width;
  }

  public getHeight(): number {
    return this.height;
  }

  public setEnabled(enabled: boolean) {
    this.enabled = enabled;
  }

  public clicked(x: number, y: number): GfxClickData {
    var clickData = super.clicked(x, y);
    if (clickData == null && this.enabled && this.isVisible()) {
      if (x < this.xPosition || y < this.yPosition || x > this.xPosition + this.width * this.xScale || y > this.yPosition + this.height * this.yScale) {
        return null;
      }

      return new GfxClickData(this.getId(), x - this.xPosition, y - this.yPosition);
    }

    return clickData;
  }
}

export class GfxTexture extends GfxClickable {
  protected loaded: boolean = false;
  constructor(sceneGraph: SceneGraph, protected image: any, parent: number) {
    super(sceneGraph, 0, 0, parent);
    this.image.addEventListener('load', () => {
      this.onLoadComplete();
    });
    if (this.image.complete) {
      this.onLoadComplete();
    }
  }

  protected onLoadComplete() {
    this.loaded = true;
    this.width = this.image.width;
    this.height = this.image.height;
    this.requestRefresh();
  }

  public setTexture(image: any) {
    this.loaded = false;
    this.image = image;
    if (this.image.complete) {
      this.onLoadComplete();
    }
    else {
      this.image.addEventListener('load', () => {
        this.onLoadComplete();
      });
    }
  }

  public render(context: CanvasRenderingContext2D) {
    context.drawImage(this.image, 0, 0);
  }

  public isVisible(): boolean {
    if (!this.loaded) {
      return false;
    }

    return super.isVisible();
  }
}

export class GfxTextureAtlas extends GfxTexture {
  constructor(sceneGraph: SceneGraph, texturePath: string, protected rows: number, protected columns: number, protected index: number, parent: number) {
    super(sceneGraph, texturePath, parent);
    this.onLoadComplete();
  }

  protected onLoadComplete() {
    super.onLoadComplete();
    this.width = this.image.width / this.columns;
    this.height = this.image.height / this.rows;
  }

  public render(context: CanvasRenderingContext2D) {
    var sh = 0;
    var i = this.index;
    while (i >= this.columns) {
      i -= this.columns;
      sh += this.height;
    }
    context.drawImage(this.image, i * this.width, sh, this.width, this.height, 0, 0, this.width, this.height)
  }

  public setIndex(index: number) {
    if (index >= 0 && index < this.columns * this.rows) {
      this.index = index;
      this.requestRefresh();
    }
  }

  public getIndex(): number {
    return this.index;
  }
}

export class GfxText extends GfxObject {
  constructor(sceneGraph: SceneGraph, protected text: string, protected font: string, protected fillStyle: string, parent: number) {
    super(sceneGraph, parent);
  }

  public render(context: CanvasRenderingContext2D) {
    context.font = this.font;
    context.fillStyle = this.fillStyle;
    context.fillText(this.text, 0, 0);
  }

  public setText(text: string) {
    this.text = text;
    this.requestRefresh();
  }
}

export class GfxRectangle extends GfxClickable {
  constructor(sceneGraph: SceneGraph, width: number, height: number, protected fillStyle: string, protected fill: boolean, protected lineWidth: number, parent: number) {
    super(sceneGraph, width, height, parent);
  }

  public render(context: CanvasRenderingContext2D) {
    context.lineWidth = this.lineWidth;
    if (this.fill) {
      context.fillStyle = this.fillStyle;
      context.fillRect(0, 0, this.width, this.height);
    }
    else {
      context.strokeStyle = this.fillStyle;
      context.strokeRect(0, 0, this.width, this.height);
    }
  }

  public setFillStyle(fillStyle: string) {
    this.fillStyle = fillStyle;
    this.requestRefresh();
  }
}

export class GfxButton extends GfxRectangle {
  private text: GfxText;

  constructor(sceneGraph: SceneGraph, width: number, height: number, fillStyle: string, fill: boolean, lineWidth: number, xText: number, yText: number, text: string, font: string, textFillStyle: string, parent: number) {
    super(sceneGraph, width, height, fillStyle, fill, lineWidth, parent);
    this.text = new GfxText(sceneGraph, text, font, textFillStyle, this.getId());
    this.text.translate(xText, yText);
    this.setEnabled(true);
  }

  public setText(text: string) {
    this.text.setText(text);
  }
}

export class GfxTexturedButton extends GfxTexture {
  private text: GfxText;

  constructor(sceneGraph: SceneGraph, image: any, xText: number, yText: number, text: string, font: string, textFillStyle: string, parent: number) {
    super(sceneGraph, image, parent);
    this.text = new GfxText(sceneGraph, text, font, textFillStyle, this.getId());
    this.text.translate(xText, yText);
    this.setEnabled(true);
  }

  public setText(text: string) {
    this.text.setText(text);
  }
}

export class GfxLines extends GfxClickable {
  private lineTo: Array<any>;

  constructor(sceneGraph: SceneGraph, protected xStart: number, protected yStart: number, protected lineWidth: number, protected fillStyle: string, parent: number) {
    super(sceneGraph, -1, -1, parent);
    this.lineTo = new Array<any>();
    this.lineTo.push({ xTo: xStart, yTo: yStart });
  }

  public clicked(x: number, y: number): GfxClickData {
    var clickData = super.clicked(x, y);

    // must be a closed polygon
    if (this.lineTo.length > 1 && this.lineTo[0].xTo == this.lineTo[this.lineTo.length - 1].xTo && this.lineTo[0].yTo == this.lineTo[this.lineTo.length - 1].yTo) {
      var intersectionCount = 0;
      if (clickData == null && this.enabled) {
        x -= this.xPosition;
        y -= this.yPosition;
        for (var i = 0; i < this.lineTo.length - 1; i++) {
          var det, gamma, lambda;
          det = (5000) * (this.lineTo[i + 1].yTo - this.lineTo[i].yTo);
          if (det === 0) {
            return null;
          }
          else {
            lambda = ((this.lineTo[i + 1].yTo - this.lineTo[i].yTo) * (this.lineTo[i + 1].yTo - x) + (this.lineTo[i].xTo - this.lineTo[i + 1].xTo) * (this.lineTo[i + 1].yTo - y)) / det;
            gamma = 5000 * (this.lineTo[i + 1].yTo - y) / det;
            if (0 < lambda && lambda < 1 && 0 < gamma && gamma < 1) {
              intersectionCount++;
            }
          }
        }

        if (intersectionCount % 2 == 0) {
          return null;
        }

        return new GfxClickData(this.getId(), x, y);
      }
    }

    return clickData;
  }

  public addTo(xTo: number, yTo: number) {
    this.lineTo.push({ xTo: xTo, yTo: yTo });
    this.requestRefresh();
  }

  public render(context: CanvasRenderingContext2D) {
    context.lineWidth = this.lineWidth;
    context.beginPath();
    context.strokeStyle = this.fillStyle;
    context.moveTo(this.xStart, this.yStart);
    this.lineTo.forEach((s) => {
      context.lineTo(s.xTo, s.yTo);
    });
    context.stroke();
  }
}

export class GfxCircle extends GfxClickable {
  constructor(sceneGraph: SceneGraph, protected sAngle: number, protected eAngle: number, protected radius: number, protected ccw: boolean, protected fillStyle: string,
    protected fill: boolean, protected lineWidth: number, parent: number) {
    super(sceneGraph, -1, -1, parent);
  }

  public render(context: CanvasRenderingContext2D) {
    context.beginPath();
    context.arc(0, 0, this.radius, this.sAngle, this.eAngle, this.ccw);
    if (this.fill) {
      context.fillStyle = this.fillStyle;
      context.fill();
    }
    else {
      context.strokeStyle = this.fillStyle;
    }
    context.lineWidth = this.lineWidth;
    context.stroke();
  }

  public getRadius(): number {
    return this.radius;
  }

  public clicked(x: number, y: number): GfxClickData {
    var clickData = super.clicked(x, y);
    if (clickData == null && this.enabled) {
      var distanceSq = (x - this.xPosition) * (x - this.xPosition) + (y - this.yPosition) * (y - this.yPosition);
      if (this.radius * this.yScale * this.radius * this.yScale < distanceSq) {
        return null;
      }

      return new GfxClickData(this.getId(), x - this.xPosition, y - this.yPosition);
    }

    return clickData;
  }
}

export class GfxScrollable extends GfxObject {
  private scrollPosition: number = 0;

  constructor(sceneGraph: SceneGraph, protected width: number, protected height: number, protected vertical: boolean, protected upLeftButton: GfxTexture,
    protected upLeftButtonPosition: number, protected upLeftButtonSize: number, protected downRightButton: GfxTexture, protected downRightButtonPosition: number,
    protected downRightButtonSize: number, protected scrollAmount: number, parent: number) {
    super(sceneGraph, parent);
    if (this.vertical) {
      this.upLeftButton.translate(this.width / 2 - this.upLeftButtonSize / 2, this.upLeftButtonPosition);
      this.downRightButton.translate(this.width / 2 - this.upLeftButtonSize / 2, this.downRightButtonPosition);
    }
    else {
      this.upLeftButton.translate(this.upLeftButtonPosition, this.height / 2 - this.upLeftButtonSize / 2);
      this.downRightButton.translate(this.downRightButtonPosition, this.height / 2 - this.upLeftButtonSize / 2);
    }
    this.upLeftButton.setEnabled(true);
    this.upLeftButton.setVisible(false);
    this.downRightButton.setEnabled(true);
    this.downRightButton.setVisible(false);
    sceneGraph.subscribeClickEvent((data: GfxClickData) => this.onClick(data));
  }

  public addChild(child: GfxObject) {
    super.addChild(child);

    this.updateRightScrollButton();
  }

  public clicked(x: number, y: number): GfxClickData {
    for (var i = 0; i < this.children.length; i++) {
      if (this.children[i].isVisible()) {
        var gfx = this.children[i].clicked(x - this.xPosition, y - this.yPosition);
        if (gfx != null) {
          return gfx;
        }
      }
    }

    if (this.upLeftButton.isVisible()) {
      var left = this.upLeftButton.clicked(x - this.xPosition, y - this.yPosition);
      if (left != null) {
        return left;
      }
    }

    if (this.downRightButton.isVisible()) {
      var right = this.downRightButton.clicked(x - this.xPosition, y - this.yPosition);
      if (right != null) {
        return right;
      }
    }

    return null;
  }

  public draw(canvas: CanvasRenderingContext2D) {
    if (!this.isVisible()) {
      return;
    }
    
    canvas.save();
    canvas.rotate(this.rotation);
    canvas.translate(this.xPosition, this.yPosition);
    canvas.scale(this.xScale, this.yScale);

    this.upLeftButton.draw(canvas);
    this.downRightButton.draw(canvas);
    canvas.rect(0, 0, this.width, this.height);
    canvas.clip();

    if (this.vertical) {
      canvas.translate(0, -this.scrollPosition);
    }
    else {
      canvas.translate(-this.scrollPosition, 0);
    }
    
    this.render(canvas);
    this.children.forEach((c) => c.draw(canvas));

    canvas.restore();
  }

  private onClick(data: GfxClickData) {
    if (data.objectId == this.upLeftButton.getId()) {
      this.scrollPosition -= this.scrollAmount;
    }
    else if (data.objectId == this.downRightButton.getId()) {
      this.scrollPosition += this.scrollAmount;
    }

    if (this.scrollPosition <= 0) {
      this.scrollPosition = 0;
      this.upLeftButton.setVisible(false);
    }
    else {
      this.upLeftButton.setVisible(true);
    }

    this.updateRightScrollButton();
  }

  private updateRightScrollButton() {
    if (this.downRightButton == null) {
      return;
    }

    var visible = false;
    this.children.forEach((c) => {
      if (this.vertical) {
        if (c.getYPosition() > this.height + this.scrollPosition) {
          visible = true;
        }
        if (c instanceof GfxClickable) {
          var clk = c as GfxClickable;
          if (clk.getYPosition() + clk.getHeight() > this.height + this.scrollPosition) {
            visible = true;
          }

          if (c instanceof GfxCircle) {
            var circle = c as GfxCircle;
            if (circle.getYPosition() + circle.getRadius() > this.height + this.scrollPosition) {
              visible = true;
            }
          }
        }
      }
      else {
        if (c.getXPosition() > this.width + this.scrollPosition) {
          visible = true;
        }
        if (c instanceof GfxClickable) {
          var clk = c as GfxClickable;
          if (clk.getXPosition() + clk.getWidth() > this.width + this.scrollPosition) {
            visible = true;
          }

          if (c instanceof GfxCircle) {
            var circle = c as GfxCircle;
            if (circle.getXPosition() + circle.getRadius() > this.width + this.scrollPosition) {
              visible = true;
            }
          }
        }
      }
    });

    this.downRightButton.setVisible(visible);
  }
}
