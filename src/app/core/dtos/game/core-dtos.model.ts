export interface GameSessions {
  creatorName: string;
  creatorId: string;
  gameId: string;
  gameName: string;
  players: GamePlayer[];
}

export interface GamePlayer {
  name: string;
  id: string;
}

export interface Game {
  name: string;
  minPlayers: number;
  maxPlayers: number;
  component: string;
}

export interface GameStart {
  gameId: string;
  name: string;
  creatorId: string;
  playerIds: Array<string>;
}

export interface GameComponent {
  gameData: GameStart;
}

export class GameMessage {
  constructor(public type: number, public message: string, public playerId: string) { }
}
