import { Injectable } from '@angular/core';
import { SceneGraph, GfxTexture, GfxTextureAtlas, GfxObject, GfxText, GfxRectangle, GfxButton, GfxTexturedButton, GfxLines, GfxCircle, GfxScrollable } from '../dtos/graphics/graphics-dtos.model';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CanvasStateService {
  private sceneGraphList: Map<string, SceneGraph>;
  private imageList: Map<string, any>;

  constructor() {
    this.sceneGraphList = new Map<string, SceneGraph>();
    this.imageList = new Map<string, any>();
  }

  public addCanvas(canvasId: string, context: CanvasRenderingContext2D, framerate: number) {
    this.sceneGraphList.set(canvasId, new SceneGraph(context, framerate));
  }

  public removeCanvas(canvasId: string) {
    this.sceneGraphList.delete(canvasId);
  }

  public setFPS(canvasId: string, fps: number) {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      sceneGraph.setFPS(fps);
    }
  }

  public clearCanvas(canvasId: string) {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      sceneGraph.clearSceneGraph();
    }
  }

  public subscribeClickEvent(canvasId: string, callback): Subscription {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      return sceneGraph.subscribeClickEvent(callback);
    }

    return null;
  }

  public onClick(canvasId: string, x: number, y: number) {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      sceneGraph.clickEvent(x, y);
    }
  }

  public getGfxObject(canvasId: string, gfxId: number): GfxObject {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      return sceneGraph.getGfxObject(gfxId);
    }

    return null;
  }

  public removeGfxObject(canvasId: string, gfxId: number) {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      sceneGraph.removeGfx(gfxId);
    }
  }

  public createTexture(canvasId: string, texturePath: string, parent: number): GfxTexture {
    var image = this.getImage(texturePath);
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var texture = new GfxTexture(sceneGraph, image, parent);
      return texture;
    }

    return null;
  }

  public createTextureAtlas(canvasId: string, texturePath: string, rows: number, columns: number, index: number, parent: number): GfxTextureAtlas {
    var image = this.getImage(texturePath);
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var textureAtlas = new GfxTextureAtlas(sceneGraph, image, rows, columns, index, parent);
      return textureAtlas;
    }

    return null;
  }

  public createText(canvasId: string, text: string, font: string, fillStyle: string, parent: number): GfxText {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var gfxText = new GfxText(sceneGraph, text, font, fillStyle, parent);
      return gfxText;
    }

    return null;
  }

  public createContainer(canvasId: string, parent: number): GfxObject {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var container = new GfxObject(sceneGraph, parent);
      return container;
    }

    return null;
  }

  public createRectangle(canvasId: string, width: number, height: number, fillStyle: string, fill: boolean, lineWidth: number, parent: number): GfxRectangle {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var rect = new GfxRectangle(sceneGraph, width, height, fillStyle, fill, lineWidth, parent);
      return rect;
    }

    return null;
  }

  public createButton(canvasId: string, width: number, height: number, fillStyle: string, fill: boolean, lineWidth: number, xText: number, yText: number, text: string, font: string, textFillStyle: string, parent: number): GfxButton {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var button = new GfxButton(sceneGraph, width, height, fillStyle, fill, lineWidth, xText, yText, text, font, textFillStyle, parent);
      return button;
    }

    return null;
  }

  public createTexturedButton(canvasId: string, texturePath: string, xText: number, yText: number, text: string, font: string, textFillStyle: string, parent: number): GfxTexturedButton {
    var image = this.getImage(texturePath);
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var button = new GfxTexturedButton(sceneGraph, image, xText, yText, text, font, textFillStyle, parent);
      return button;
    }

    return null;
  }

  public createLineSegment(canvasId: string, xStart: number, yStart: number, lineWidth: number, fillStyle: string, parent: number): GfxLines {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var lines = new GfxLines(sceneGraph, xStart, yStart, lineWidth, fillStyle, parent);
      return lines;
    }

    return null;
  }

  public createCircle(canvasId: string, sAngle: number, eAngle: number, radius: number, ccw: boolean, fillStyle: string, fill: boolean, lineWidth: number, parent: number): GfxCircle {
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      var circle = new GfxCircle(sceneGraph, sAngle, eAngle, radius, ccw, fillStyle, fill, lineWidth, parent);
      return circle;
    }

    return null;
  }

  public createScrollArea(canvasId: string, width: number, height: number, vertical: boolean, upLeftImage: string, upLeftPosition: number, upLeftSize: number,
    downRightImage: string, downRightPosition: number, downRightSize: number, scrollAmount: number, parent: number): GfxScrollable {
    var left = this.createTexture(canvasId, upLeftImage, parent);
    var right = this.createTexture(canvasId, downRightImage, parent);
    var sceneGraph = this.sceneGraphList.get(canvasId);
    if (sceneGraph != null) {
      sceneGraph.removeGfx(left.getId());
      sceneGraph.removeGfx(right.getId());
      var scrollable = new GfxScrollable(sceneGraph, width, height, vertical, left, upLeftPosition, upLeftSize, right, downRightPosition, downRightSize, scrollAmount, parent);
      return scrollable;
    }

    return null;
  }

  public getImage(texturePath: string) {
    var image = this.imageList.get(texturePath);
    if (image == null) {
      image = new Image();
      image.src = texturePath;
      this.imageList.set(texturePath, image);
    }

    return image;
  }
}
