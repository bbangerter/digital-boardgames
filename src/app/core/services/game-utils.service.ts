import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GameUtilsService {

  constructor() { }

  public shuffle(deck: Array<any>) {
    // shuffle the deck with Fisher-Yates shuffle
    for (let i = deck.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i
      [deck[i], deck[j]] = [deck[j], deck[i]];
    }
  }
}
