import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { UUID } from 'angular2-uuid';
import { GameStart } from '..';

@Injectable({
  providedIn: 'root'
})
export class PlayerStateService {
  private actions: Map<string, number>;
  private actionsSubject: Map<string, BehaviorSubject<number>>;
  private playerName: string = '';
  private playerId: string = '';
  private activeGameData: GameStart;

  constructor() {
    this.actions = new Map<string, number>();
    this.actionsSubject = new Map<string, BehaviorSubject<number>>();
  }

  public setPlayerName(name: string) {
    this.playerName = name;
    this.playerId = UUID.UUID();
  }

  public getPlayerName(): string {
    return this.playerName;
  }

  public getPlayerId(): string {
    return this.playerId;
  }

  public setActiveGame(data: GameStart) {
    this.activeGameData = data;
  }

  public getActiveGameData(): GameStart {
    return this.activeGameData;
  }

  public getActions(playerId: string) : number {
    this.setupPlayer(playerId);
    return this.actions.get(playerId);
  }

  public useAction(playerId: string) {
    this.addActions(playerId, -1);
  }

  public resetActions(playerId: string, amount: number) {
    this.setupPlayer(playerId);
    this.actions.set(playerId, amount);
    this.actionsSubject.get(playerId).next(amount);
  }

  public addActions(playerId: string, amount: number) {
    this.setupPlayer(playerId);
    this.actions.set(playerId, this.actions.get(playerId) + amount);
    this.actionsSubject.get(playerId).next(this.actions.get(playerId));
  }

  public subscibeActionChange(playerId: string, callback) : Subscription {
    this.setupPlayer(playerId);
    return this.actionsSubject.get(playerId).subscribe((data) => callback(data));
  }

  private setupPlayer(key: string) {
    if (this.actions.get(key) === null) {
      this.actions.set(key, 0);
      this.actionsSubject.set(key, new BehaviorSubject<number>(0));
    }
  }
}
