import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerScoreService {
  private score: Map<string, number>;
  private scoreSubject: Map<string, BehaviorSubject<number>>;
  private minScore: Map<string, number>;
  private maxScore: Map<string, number>;
  private globalMin: number = -10000;
  private globalMax: number = 10000;

  constructor() {
    this.score = new Map<string, number>();
    this.scoreSubject = new Map<string, BehaviorSubject<number>>();
    this.minScore = new Map<string, number>();
    this.maxScore = new Map<string, number>();
  }

  public configureMinMax(scoreTrackId: string, min: number, max: number) {
    this.minScore.set(scoreTrackId, min);
    this.maxScore.set(scoreTrackId, max);
  }

  public configureGlobalMinMax(min: number, max: number) {
    this.globalMin = min;
    this.globalMax = max;
  }

  public getScore(playerId: string, scoreTrackId: string): number {
    var key = this.getScoreKey(playerId, scoreTrackId);
    this.setupPlayer(key);
    return this.score.get(key);
  }

  public canAdd(amount: number, playerId: string, scoreTrackId: string): boolean {
    var currentScore = this.getScore(playerId, scoreTrackId);
    if (this.maxScore !== null && this.maxScore.get(scoreTrackId) !== null && this.maxScore.get(scoreTrackId) < currentScore + amount) {
      return false
    }

    if (this.minScore !== null && this.minScore.get(scoreTrackId) !== null && this.minScore.get(scoreTrackId) > currentScore + amount) {
      return false;
    }

    if ((this.maxScore == null || this.maxScore.get(scoreTrackId) == null) && this.globalMax < currentScore + amount) {
      return false;
    }

    if ((this.minScore == null || this.minScore.get(scoreTrackId) == null) && this.globalMin > currentScore + amount) {
      return false;
    }

    return true;
  }

  public canSubtract(amount: number, playerId: string, scoreTrackId: string): boolean {
    return this.canAdd(-amount, playerId, scoreTrackId);
  }

  public setScore(amount: number, playerId: string, scoreTrackId: string) {
    var key = this.getScoreKey(playerId, scoreTrackId);
    this.setupPlayer(key);
    if (this.minScore !== null && this.minScore.get(scoreTrackId) !== null && this.minScore.get(scoreTrackId) > amount) {
      amount = this.minScore.get(scoreTrackId);
    }
    if (this.maxScore !== null && this.maxScore.get(scoreTrackId) !== null && this.maxScore.get(scoreTrackId) < amount) {
      amount = this.maxScore.get(scoreTrackId);
    }
    if ((this.minScore == null || this.minScore.get(scoreTrackId) == null) && this.globalMin > amount) {
      amount = this.globalMin;
    }
    if ((this.maxScore == null || this.maxScore.get(scoreTrackId) == null) && this.globalMax < amount) {
      amount = this.globalMax;
    }
    this.score.set(key, amount);
    this.scoreSubject.get(key).next(amount);
  }

  public addScore(amount: number, playerId: string, scoreTrackId: string) {
    var key = this.getScoreKey(playerId, scoreTrackId);
    this.setupPlayer(key);
    amount += this.score.get(key);
    this.setScore(amount, playerId, scoreTrackId);
  }

  public subtractScore(amount: number, playerId: string, scoreTrackId: string) {
    this.addScore(-amount, playerId, scoreTrackId);
  }

  public subscribeScoreChange(playerId: string, scoreTrackId: string, callback): Subscription {
    var key = this.getScoreKey(playerId, scoreTrackId);
    this.setupPlayer(key);
    return this.scoreSubject.get(key).subscribe((data) => callback(data));
  }

  private setupPlayer(key: string) {
    if (this.score.get(key) === undefined) {
      this.score.set(key, 0);
      this.scoreSubject.set(key, new BehaviorSubject<number>(0));
    }
  }

  private getScoreKey(playerId: string, scoreTrackId: string): string {
    if (scoreTrackId != null) {
      return playerId + ":" + scoreTrackId;
    }

    return playerId;
  }
}
