import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignalRService implements OnDestroy {
  private hubConnections: Map<string, HubConnection>;
  private hubSubject: Map<string, BehaviorSubject<any>>;

  constructor(private zone: NgZone) {
    this.hubConnections = new Map<string, HubConnection>();
    this.hubSubject = new Map<string, BehaviorSubject<any>>();
  }

  ngOnDestroy() {
    this.hubConnections.forEach((value, key) => this.close(key));
  }

  public connect(hubName: string, queryString: string) {
    var connection = new HubConnectionBuilder()
      //.withUrl(`https://localhost:32770/${hubName}?${queryString}`)
      //.withUrl(`https://digitalboardgames.azurewebsites.net/${hubName}?${queryString}`)
      .withUrl(`http://18.118.80.158/${hubName}?${queryString}`)
      .withAutomaticReconnect()
      .build();
    this.hubConnections.set(hubName, connection);

    connection.on('ClientMessage', (data: any) => {
      this.zone.run(() => {
        this.processMessage(hubName, data);
      });
    });

    connection.start()
      .then(() => console.log(hubName + ' connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  public close(hubName: string) {
    var connection = this.hubConnections.get(hubName);
    if (connection != undefined) {
      connection.stop();
    }
    this.hubConnections.delete(hubName);
    this.hubSubject.delete(hubName);
  }

  public sendMessage(hubName: string, data: any) {
    // send a message to the server
    var connection = this.hubConnections.get(hubName);
    if (connection != null) {
      connection.send('ServerMessage', data)
        .then(() => console.log(hubName + ' message sent'))
        .catch(err => console.log('Err: ' + err));
    }
  }

  public subscribeToMessage(hubName: string, callback) : Subscription {
    var subject = this.hubSubject.get(hubName);
    if (subject == null) {
      this.hubSubject.set(hubName, new BehaviorSubject<any>(null));
      subject = this.hubSubject.get(hubName);
    }
    if (subject != null) {
      return subject.subscribe((data) => callback(data));
    }

    return null;
  }

  private processMessage(hubName: string, data: any) {
    // process received messsages
    var subject = this.hubSubject.get(hubName);
    if (subject != null) {
      subject.next(data);
    }
  }
}
