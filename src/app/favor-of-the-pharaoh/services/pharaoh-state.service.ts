import { Injectable } from '@angular/core';
import { GamePlayer, SignalRService, GameMessage, PlayerStateService, CanvasStateService, PlayerScoreService, GfxText, GfxTextureAtlas, GfxClickData, GfxButton, GfxObject, GfxTexture } from '../../core';
import { Subscription } from 'rxjs';
import { PharaohMessageType, PharaohPhase, PharaohRequirementType, PharaohSides, PharaohScarabType, PharaohDiceType, PharaohTileAction } from '../pharaoh-constants';
import { PharaohSettings, PharaohSide, PharaohTile, PharaohTileSetting, PharaohSideSetting, PharaohRequirement, PharaohScarabScores, PharaohScarabChange, PharaohPlayerTile, PharaohDiceSet, PharaohDice, PharaohDiceRoll, PharaohLockDie, PharaohClaimTile, PharaohAddDie, PharaohAction } from '../dtos/pharaoh-dtos.model';

@Injectable({
  providedIn: 'root'
})
export class PharaohStateService {
  creatorId: string;
  activePlayers: Array<GamePlayer> = [];
  canvasId: string = 'pharaoh-canvas';
  currentPhase: PharaohPhase = PharaohPhase.Setup;
  settings: PharaohSettings;
  firstPlayer: string = null;
  currentPlayer: string = null;
  playerTiles: Array<PharaohPlayerTile>;

  private gameSubscription: Subscription;
  private hubName: string = 'game';
  private sides: Array<PharaohSide>;
  private tiles: Array<PharaohTile>;
  private scarabScores: Array<PharaohScarabScores>;
  private currentPlayerGfxContainer: GfxObject;
  private activeDice: Array<PharaohDice>;
  private rollButton: GfxButton;
  private genericButton: GfxButton;
  private increaseButton: GfxButton;
  private increaseButton2: GfxButton;
  private increaseButton3: GfxButton;
  private rerollButton: GfxButton;
  private lockButton: GfxButton;
  private doneButton: GfxButton;
  private wildButton1: GfxTextureAtlas;
  private wildButton2: GfxTextureAtlas;
  private wildButton3: GfxTextureAtlas;
  private wildButton4: GfxTextureAtlas;
  private wildButton5: GfxTextureAtlas;
  private wildButton6: GfxTextureAtlas;
  private pharaohLeader: GfxTextureAtlas;
  private leaderName: GfxText;
  private leaderValue: number;
  private leaderCount: number = 0;
  private pyramidGfx: GfxTexture;
  private clickListener: Subscription;
  private selectedDie: PharaohDice = null;
  private movePipsDie: Array<PharaohDice>;
  private movedPipsDice: Array<PharaohDice>;
  private dieLocked: boolean;
  private hasRolled: number;
  private bonusTurn: boolean = false;
  private finalTurn: boolean = false;
  private extraDice: number = 0;
  private freeWilds: number;
  private freeRerolls: number = 0;
  private activeAction: PharaohAction = null;
  private claimExtraTile: number = 0;
  private endTurn: boolean;
  private finalRollOff: string = null;
  private pyramidSlots = [{ x: 192, y: 55 }, { x: 162, y: 103 }, { x: 224, y: 103 }, { x: 131, y: 150 }, { x: 194, y: 150 }, { x: 255, y: 150 },
    { x: 99, y: 197 }, { x: 162, y: 197 }, { x: 224, y: 197 }, { x: 287, y: 197 }, { x: 70, y: 244 }, { x: 131, y: 244 }, { x: 194, y: 244 }, { x: 255, y: 244 }, { x: 318, y: 244 },
    { x: 39, y: 300 }, { x: 99, y: 300 }, { x: 162, y: 300 }, { x: 224, y: 300 }, { x: 287, y: 300 }, { x: 348, y: 300 }];
  constructor(private signalR: SignalRService, private playerState: PlayerStateService, private canvas: CanvasStateService, private score: PlayerScoreService) {
    this.initializeSettings();
  }

  public startGameHub(playerName: string, groupName: string, playerId: string) {
    this.signalR.connect(this.hubName, `PlayerName=${playerName}&GroupName=${groupName}&PlayerId=${playerId}`);
    this.gameSubscription = this.signalR.subscribeToMessage(this.hubName, (data) => this.onMessage(data));
  }

  public closeGameHub() {
    this.gameSubscription.unsubscribe();
    this.signalR.close(this.hubName);
  }

  public advancePhase() {
    this.currentPhase++;
  }

  public isGameCreator() {
    return this.creatorId == this.playerState.getPlayerId();
  }

  public getRequirement(tier: number, column: number): string {
    var side = this.settings.tierSides.get(tier);
    if (column == 0) {
      side = 0;
    }
    if (side == 2) {
      return 'Random Side';
    }
    else if (side == 3) {
      return 'Random';
    }
    return this.sides.find(s => s.tier == tier && s.column == column && s.side == side).description;
  }

  public getTiles(tier: number, column: number): Array<PharaohTile> {
    return this.tiles.filter(t => t.tier == tier && t.columns.some(c => c == column));
  }

  public getTile(id: number): PharaohTile {
    return this.tiles.find(t => t.tileId == id);
  }

  public updateSideSettings(tier: number, side: number) {
    this.sendGameMessage(PharaohMessageType.UpdateSideSettings, new PharaohSideSetting(tier, side));
  }

  public updateTileSettings(tier: number, column: number, value: number) {
    this.sendGameMessage(PharaohMessageType.UpdateTileSettings, new PharaohTileSetting(tier, column, value));
  }

  public startGame() {
    this.sendGameMessage(PharaohMessageType.FirstPlayer, this.activePlayers[Math.floor(Math.random() * this.activePlayers.length)].id);
    for (var i = 7; i >= 3; i--) {
      if (this.settings.tierSides.get(i) == PharaohSides.Random) {
        this.updateSideSettings(i, Math.floor(Math.random() * 2));
      }

      for (var c = 0; c < 4; c++) {
        if (this.settings.tiles.get(i * 10 + c) > 54) {
          var tiles = this.getTiles(i, c).filter(t => t.tileId < 54);
          var tileId = tiles[Math.floor(Math.random() * tiles.length)].tileId;
          while (c == 1 && tileId == this.settings.tiles.get(i * 10)) {
            tileId = tiles[Math.floor(Math.random() * tiles.length)].tileId;
          }
          this.settings.tiles.set(i * 10 + c, tileId);
          this.updateTileSettings(i, c, tileId);
        }
      }
    }
    
    this.sendGameMessage(PharaohMessageType.StartGame, '');
  }

  public setupBoard() {
    if (this.clickListener == null) {
      this.clickListener = this.canvas.subscribeClickEvent(this.canvasId, (data: GfxClickData) => this.onClick(data));
    }

    for (var tier = 7; tier >= 3; tier--) {
      var container = this.canvas.createRectangle(this.canvasId, 800, 30, 'black', true, 1, 0);
      container.translate(0, (7 - tier) * 155)
      for (var column = 0; column < 4; column++) {
        var splitter = this.canvas.createRectangle(this.canvasId, 10, 30, 'white', true, 1, container.getId());
        splitter.translate(column * 200 - 10, 0);

        var sideDetails = this.getSideColumn(tier, column);
        var text = this.canvas.createText(this.canvasId, sideDetails.description, '15px Arial', 'white', container.getId());
        text.translate(column * 200 + 5, 21);

        var tile = this.getTile(this.settings.tiles.get(tier * 10 + column));
        var tileTexture = this.canvas.createTexture(this.canvasId, tile.image, 0);
        tileTexture.translate(column * 200, (7 - tier) * 155 + 30);
        tile.gfxId = tileTexture.getId();
        tile.inColumn = column;
        tileTexture.setEnabled(true);
      }
    }

    var herder = this.getTile(54);
    var herderTexture = this.canvas.createTexture(this.canvasId, herder.image, 0);
    herderTexture.translate(800, 4 * 155 + 30);
    herderTexture.setEnabled(true);
    herder.gfxId = herderTexture.getId();

    this.pyramidGfx = this.canvas.createTexture(this.canvasId, './assets/pharaoh/pyramid.png', 0);
    this.pyramidGfx.translate(800, 0);

    this.scarabScores = new Array<PharaohScarabScores>();
    this.playerTiles = new Array<PharaohPlayerTile>();

    var scarabs = this.canvas.createContainer(this.canvasId, 0);
    scarabs.translate(1300, 5);
    var pip = this.canvas.createTexture(this.canvasId, './assets/pharaoh/pip.png', scarabs.getId());
    pip.translate(165, 0);
    var reroll = this.canvas.createTexture(this.canvasId, './assets/pharaoh/reroll.png', scarabs.getId());
    reroll.translate(233, 0);
    this.activePlayers.forEach((p, index) => {
      var row = this.canvas.createContainer(this.canvasId, scarabs.getId());
      row.translate(0, 75 + index * 22);
      var name = this.canvas.createText(this.canvasId, p.name, '25px Arial', 'black', row.getId());
      name.translate(0, 15);
      var pipCount = this.canvas.createText(this.canvasId, '0', '25px Arial', 'black', row.getId());
      pipCount.translate(187, 15);
      var rerollCount = this.canvas.createText(this.canvasId, '0', '25px Arial', 'black', row.getId());
      rerollCount.translate(255, 15);
      var playerTracker = new PharaohScarabScores(p.id, pipCount.getId(), rerollCount.getId(), this.score.subscribeScoreChange(p.id, 'scarab-pip', (data: number) => this.onPipChange(data, p.id)),
        this.score.subscribeScoreChange(p.id, 'scarab-reroll', (data: number) => this.onRerollChange(data, p.id)))
      this.scarabScores.push(playerTracker);

      this.playerTiles.push(new PharaohPlayerTile(p.id, this.tiles.length - 1));
    });

    this.genericButton = this.canvas.createButton(this.canvasId, 155, 20, 'red', true, 1, 5, 15, 'Take 2 Scarabs', '15px Arial', 'white', 0);
    this.genericButton.translate(1300, 5);
    this.genericButton.setVisible(false);

    this.rollButton = this.canvas.createButton(this.canvasId, 50, 20, 'red', true, 1, 5, 15, 'Roll', '15px Arial', 'white', 0);
    this.rollButton.translate(1300, 5);
    this.rollButton.setVisible(false);

    this.increaseButton = this.canvas.createButton(this.canvasId, 50, 20, 'red', true, 1, 5, 15, '+1 Pip', '15px Arial', 'white', 0);
    this.increaseButton.translate(1300, 30);
    this.increaseButton.setVisible(false);

    this.increaseButton2 = this.canvas.createButton(this.canvasId, 50, 20, 'red', true, 1, 5, 15, '+2 Pip', '15px Arial', 'white', 0);
    this.increaseButton2.translate(1355, 30);
    this.increaseButton2.setVisible(false);

    this.increaseButton3 = this.canvas.createButton(this.canvasId, 50, 20, 'red', true, 1, 5, 15, '+3 Pip', '15px Arial', 'white', 0);
    this.increaseButton3.translate(1300, 55);
    this.increaseButton3.setVisible(false);

    this.rerollButton = this.canvas.createButton(this.canvasId, 50, 20, 'red', true, 1, 5, 15, 'Reroll', '15px Arial', 'white', 0);
    this.rerollButton.translate(1300, 5);
    this.rerollButton.setVisible(false);

    this.lockButton = this.canvas.createButton(this.canvasId, 50, 20, 'red', true, 1, 5, 15, 'Lock', '15px Arial', 'white', 0);
    this.lockButton.translate(1355, 5);
    this.lockButton.setVisible(false);

    this.doneButton = this.canvas.createButton(this.canvasId, 50, 20, 'red', true, 1, 5, 15, 'Done', '15px Arial', 'white', 0);
    this.doneButton.translate(1355, 30);
    this.doneButton.setVisible(false);

    this.wildButton1 = this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(PharaohDiceType.Red), 2, 3, 0, 0);
    this.wildButton1.translate(1300, 55);
    this.wildButton1.setEnabled(true);
    this.wildButton1.setScale(0.5, 0.5);
    this.wildButton1.setVisible(false);

    this.wildButton2 = this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(PharaohDiceType.Red), 2, 3, 1, 0);
    this.wildButton2.translate(1325, 55);
    this.wildButton2.setEnabled(true);
    this.wildButton2.setScale(0.5, 0.5);
    this.wildButton2.setVisible(false);

    this.wildButton3 = this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(PharaohDiceType.Red), 2, 3, 2, 0);
    this.wildButton3.translate(1350, 55);
    this.wildButton3.setEnabled(true);
    this.wildButton3.setScale(0.5, 0.5);
    this.wildButton3.setVisible(false);

    this.wildButton4 = this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(PharaohDiceType.Red), 2, 3, 3, 0);
    this.wildButton4.translate(1375, 55);
    this.wildButton4.setEnabled(true);
    this.wildButton4.setScale(0.5, 0.5);
    this.wildButton4.setVisible(false);

    this.wildButton5 = this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(PharaohDiceType.Red), 2, 3, 4, 0);
    this.wildButton5.translate(1400, 55);
    this.wildButton5.setEnabled(true);
    this.wildButton5.setScale(0.5, 0.5);
    this.wildButton5.setVisible(false);

    this.wildButton6 = this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(PharaohDiceType.Red), 2, 3, 5, 0);
    this.wildButton6.translate(1425, 55);
    this.wildButton6.setEnabled(true);
    this.wildButton6.setScale(0.5, 0.5);
    this.wildButton6.setVisible(false);

    this.setupPlayerTurn();
  }

  public sendGameMessage(type: PharaohMessageType, message: any) {
    console.log(`Sending ${type} ${message} for ${this.playerState.getPlayerId()}`)
    var gameMessage = new GameMessage(type, JSON.stringify(message), this.playerState.getPlayerId());
    this.signalR.sendMessage(this.hubName, gameMessage);
  }

  private getSideColumn(tier: number, column: number): PharaohSide {
    var side = this.settings.tierSides.get(tier);
    return this.sides.find(s => s.tier == tier && s.side == side && s.column == column);
  }

  private onClick(data: GfxClickData) {
    if (!this.isCurrentPlayer()) {
      return;
    }

    this.clickedGenericButton(data.objectId);
    this.clickedClaimTile(data.objectId);
    this.clickedTileActivate(data.objectId);
    if (this.endTurn) {
      return;
    }
    this.clickedRollButton(data.objectId);
    this.clickedWildButton(data.objectId);
    this.clickedDie(data.objectId);
    this.clickedIncreasePips(data.objectId);
    this.clickedRerollDie(data.objectId);
    this.clickedLock(data.objectId);

    if (this.doneButton.getId() == data.objectId) {
      this.hideStandardDiceButtons();
    }

    this.updateActivatableTiles();
  }

  private clickedTileActivate(id: number) {
    var playerTile = this.playerTiles.find(t => t.gfxId == id);
    if (playerTile != null && !playerTile.used && this.activeAction == null) {
      // TODO Final roll off only addition
      var tile = this.getTile(playerTile.tileId);
      if (tile.dice.some(d => !d.mustRoll && (!d.onlyAfterRoll || this.hasRolled > 0) && (!d.addAfterPairLocked || this.hasLockedPair()) &&
        (!d.addAfterAllLocked || !this.activeDice.some(l => l.pyramidSlot == -1))) && !this.endTurn) {
        playerTile.used = true;
        this.genericButton.setVisible(false);
        tile.dice.forEach((d) => {
          if (!d.mustRoll) {
            for (var i = 0; i < d.amount; i++) {
              var value = d.startValue;
              if (d.incrementing) {
                value = Math.min(5, this.hasRolled + value - 1);
              }
              this.sendGameMessage(PharaohMessageType.AddActiveDie, new PharaohAddDie(d.diceType, value));
            }
          }
        });
        this.canvas.getGfxObject(this.canvasId, playerTile.gfxId).removeAllChildren();
        this.markTileUsed(tile);
      }
      if (tile.action.length > 0) {
        if (this.endTurn && tile.action.some(a => a.action == PharaohTileAction.FlipDice || a.action == PharaohTileAction.WhiteDiceWild || a.action == PharaohTileAction.AddPips ||
          a.action == PharaohTileAction.AnyDiceWild || a.action == PharaohTileAction.AnyDiceLocked || a.action == PharaohTileAction.AnyButRedWild || a.action == PharaohTileAction.SwapForRed ||
          a.action == PharaohTileAction.MovePips || a.action == PharaohTileAction.SplitDice)) {
          return;
        }

        if (tile.action[0].action == PharaohTileAction.BonusScarabAfterRoll && this.hasRolled < 2) {
          return;
        }
        if (tile.action.some(a => a.action == PharaohTileAction.AnyDiceLocked) && !this.activeDice.some(d => d.pyramidSlot != -1 && this.getDiceFaceValue(d) != -1)) {
          return;
        }

        playerTile.used = true;
        this.sendGameMessage(PharaohMessageType.ActivateTile, playerTile.tileId);

        if (tile.action[0].action == PharaohTileAction.MovePips) {
          this.movePipsDie = new Array<PharaohDice>();
          this.movedPipsDice = new Array<PharaohDice>();
        }
      }
    }
  }

  private clickedClaimTile(id: number) {
    if (this.finalRollOff != null) {
      return;
    }

    var tile = this.tiles.find(t => t.gfxId == id);
    if (!this.activeDiceRemaining() && !this.endTurn) {
      if (tile != null && this.requirementsMet(tile)) {
        this.sendGameMessage(PharaohMessageType.ClaimTile, new PharaohClaimTile(this.currentPlayer, tile.tileId));
      }
    }

    if (this.activeAction != null && this.activeAction.action == PharaohTileAction.ClaimTile && tile != null && this.hasRolled == 0) {
      if (this.playerTiles.some(p => p.playerId == this.currentPlayer && p.tileId == tile.tileId)) {
        return;
      }

      if (tile.tier <= this.activeAction.claimTier && tile.inColumn >= this.activeAction.min && tile.inColumn <= this.activeAction.max) {
        this.sendGameMessage(PharaohMessageType.ClaimTile, new PharaohClaimTile(this.currentPlayer, tile.tileId, false));
        this.claimExtraTile--;
        if (this.claimExtraTile == 0) {
          this.activeAction = null;
        }
      }
    }
  }

  private clickedLock(id: number) {
    if (this.lockButton.getId() == id) {
      var pyramidIndex = 0;
      this.activeDice.forEach((d) => {
        if (d.pyramidSlot >= pyramidIndex) {
          pyramidIndex = d.pyramidSlot + 1;
        }
      });
      var dieIndex = this.activeDice.findIndex(d => d == this.selectedDie);
      this.activeDice[dieIndex].pyramidSlot = pyramidIndex;
      this.activeDice[dieIndex].lockedOnRoll = this.hasRolled;
      this.sendGameMessage(PharaohMessageType.LockDie, new PharaohLockDie(dieIndex, pyramidIndex));
      this.dieLocked = true;
      if (this.selectedDie.type == PharaohDiceType.Captain && this.selectedDie.value == 5) {
        this.sendGameMessage(PharaohMessageType.AddActiveDie, new PharaohAddDie(PharaohDiceType.Red, -1));
        this.sendGameMessage(PharaohMessageType.AddActiveDie, new PharaohAddDie(PharaohDiceType.Red, -1));
      }
      // TODO Vizier locked
      this.hideStandardDiceButtons();
    }
  }

  private clickedRerollDie(id: number) {
    if (this.rerollButton.getId() == id) {
      var index = this.activeDice.findIndex(d => d == this.selectedDie);
      var roll = new PharaohDiceRoll(index, Math.floor(Math.random() * 6));
      this.sendGameMessage(PharaohMessageType.RerollDie, roll);
    }
  }

  private clickedIncreasePips(id: number) {
    if (this.increaseButton.getId() == id || this.increaseButton2.getId() == id || this.increaseButton3.getId() == id) {
      var increase = 1;
      if (this.increaseButton2.getId() == id) {
        increase++;
      }
      if (this.increaseButton3.getId() == id) {
        increase += 2;
      }
      var die = this.selectedDie;
      if (this.activeAction != null && this.activeAction.action == PharaohTileAction.MovePips) {
        die = this.movePipsDie[0];
      }
      var currentValue = this.getDiceFaceValue(die);
      for (var i = 0; i < increase; i++) {
        if (currentValue > -1) {
          if (die.type == PharaohDiceType.Serf) {
            if (currentValue == 0) {
              currentValue = 4;
            }
            else if (currentValue == 1) {
              currentValue = 2;
            }
            else {
              currentValue++;
            }
          }
          else if (die.type == PharaohDiceType.Noble) {
            if (currentValue == 4) {
              currentValue = 5;
            }
            else if (currentValue == 3) {
              currentValue = 0;
            }
          }
          else if (die.type == PharaohDiceType.Conspirator && currentValue < 4) {
            currentValue++;
          }
          else if (currentValue < 5) {
            currentValue++;
          }
        }
      }

      if (currentValue != this.getDiceFaceValue(die)) {
        var index = this.activeDice.findIndex(d => d == die);
        var roll = new PharaohDiceRoll(index, currentValue);
        this.sendGameMessage(PharaohMessageType.IncreasePip, roll);
      }

      if (this.activeAction != null && this.activeAction.action == PharaohTileAction.MovePips) {
        die = this.movePipsDie[1];
        var index2 = this.activeDice.findIndex(d => d == die);
        currentValue = this.getDiceFaceValue(die);
        if (die.type == PharaohDiceType.Serf) {
          if (currentValue == 1) {
            currentValue = 0;
          }
          else if (currentValue == 2) {
            currentValue = 4;
          }
          else {
            currentValue--;
          }
        }
        else if (die.type == PharaohDiceType.Noble) {
          if (currentValue == 5) {
            currentValue = 1;
          }
          else if (currentValue == 4) {
            currentValue = 3;
          }
          else {
            currentValue--;
          }
        }
        else if (die.type == PharaohDiceType.Vizier && currentValue > 1) {
          currentValue--;
        }
        else if (currentValue > 0) {
          currentValue--;
        }
        var roll = new PharaohDiceRoll(index2, currentValue);
        this.sendGameMessage(PharaohMessageType.IncreasePip, roll);
        if (!this.canRemovePips(die) || !this.canAddPips(1, this.movePipsDie[0])) {
          this.increaseButton.setVisible(false);
        }
      }

      if (this.activeAction != null && this.activeAction.action == PharaohTileAction.AddPips) {
        this.selectedDie.pipsAdded = true;
        this.hideStandardDiceButtons();
      }
    }
  }

  private clickedDie(id: number) {
    var die = this.activeDice.find(d => d.gfx.getId() == id);
    if (die != null) {
      var index = this.activeDice.findIndex(d => d == die);
      if ((die.value >= 0 || this.activeAction.action == PharaohTileAction.SwapForRed) && die.pyramidSlot == -1 && this.selectedDie == null) {
        if (this.activeAction != null && this.activeAction.action != PharaohTileAction.AnyButRedWild && this.activeAction.action != PharaohTileAction.AnyDiceLocked &&
          this.activeAction.action != PharaohTileAction.AnyDiceWild && this.activeAction.action != PharaohTileAction.WhiteDiceWild) {
          if (this.activeAction.action == PharaohTileAction.FlipDice) {
            this.freeWilds -= this.changeWilds(die, die.value);
            this.freeRerolls -= this.changeRerolls(die, die.value);
            this.sendGameMessage(PharaohMessageType.DiceRoll, new PharaohDiceRoll(index, 5 - die.value));
            this.freeWilds += this.changeWilds(die, 5 - die.value);
            this.freeRerolls += this.changeRerolls(die, 5 - die.value);
          }
          else if (this.activeAction.action == PharaohTileAction.AddPips) {
            if (this.activeAction.min == 1 && this.canAddPips(1, die)) {
              this.increaseButton.setVisible(true);
              this.selectedDie = die;
              die.highlightGfx.setVisible(true);
            }
            if (this.activeAction.min <= 2 && this.activeAction.max >= 2 && this.canAddPips(2, die)) {
              this.increaseButton2.setVisible(true);
              this.selectedDie = die;
              die.highlightGfx.setVisible(true);
            }
            if (this.activeAction.max == 3 && this.canAddPips(3, die)) {
              this.increaseButton3.setVisible(true);
              this.selectedDie = die;
              die.highlightGfx.setVisible(true);
            }
          }
          else if (this.activeAction.action == PharaohTileAction.SwapForRed && (die.type == PharaohDiceType.White || die.type == PharaohDiceType.Serf)) {
            this.sendGameMessage(PharaohMessageType.SwapToRed, index);
          }
          else if (this.activeAction.action == PharaohTileAction.SplitDice && die.type == PharaohDiceType.Red && die.value > 0) {
            this.selectedDie = die;
            this.showStandardDiceButtons();
          }
          else if (this.activeAction.action == PharaohTileAction.MovePips && (this.canAddPips(1, die) && this.movePipsDie.length == 0 || this.movePipsDie.length == 1 && this.canRemovePips(die)) &&
            (this.movedPipsDice.length < this.activeAction.max || this.movedPipsDice.some(d => d == die))) {
            this.movePipsDie.push(die);
            this.increaseButton.setVisible(this.movePipsDie.length > 1);
            die.highlightGfx.setVisible(true);
            if (!this.movedPipsDice.some(d => d == die)) {
              this.movedPipsDice.push(die);
            }
            this.genericButton.setText('Change Dice');
          }
        }
        else {
          // then we can do something with this die, figure out what options we have
          this.selectedDie = die;
          this.showStandardDiceButtons();
          die.highlightGfx.setVisible(true);
        }
      }
    }
  }

  private clickedWildButton(id: number) {
    if (this.wildButton1.getId() == id || this.wildButton2.getId() == id || this.wildButton3.getId() == id ||
      this.wildButton4.getId() == id || this.wildButton5.getId() == id || this.wildButton6.getId() == id) {
      var value = 0;
      if (this.wildButton1.getId() == id) {
        value = this.wildButton1.getIndex();
      }
      else if (this.wildButton2.getId() == id) {
        value = this.wildButton2.getIndex();
      }
      else if (this.wildButton3.getId() == id) {
        value = this.wildButton3.getIndex();
      }
      else if (this.wildButton4.getId() == id) {
        value = this.wildButton4.getIndex();
      }
      else if (this.wildButton5.getId() == id) {
        value = this.wildButton5.getIndex();
      }
      else if (this.wildButton6.getId() == id) {
        value = this.wildButton6.getIndex();
      }

      if (this.activeAction != null && this.activeAction.action == PharaohTileAction.AnyDiceLocked) {
        this.activeAction = null;
      }

      if (this.activeAction != null && this.activeAction.action == PharaohTileAction.SplitDice) {
        this.activeAction = null;
        var index = this.activeDice.findIndex(d => d == this.selectedDie);
        this.sendGameMessage(PharaohMessageType.SwapToWhite, index);
        this.sendGameMessage(PharaohMessageType.AddActiveDie, new PharaohAddDie(PharaohDiceType.White, this.selectedDie.value - value - 1));
      }

      if (this.selectedDie.value != value) {
        if (this.activeAction == null) {
          this.freeWilds--;
        }
        var index = this.activeDice.findIndex(d => d == this.selectedDie);
        this.freeWilds -= this.changeWilds(this.selectedDie, this.selectedDie.value);
        this.freeRerolls -= this.changeRerolls(this.selectedDie, this.selectedDie.value);
        this.sendGameMessage(PharaohMessageType.DiceRoll, new PharaohDiceRoll(index, value));
        this.freeWilds += this.changeWilds(this.selectedDie, value);
        this.freeRerolls += this.changeRerolls(this.selectedDie, value);

        this.hideStandardDiceButtons();
      }
    }
  }

  private clickedRollButton(id: number) {
    if (id == this.rollButton.getId()) {
      this.activeDice.forEach((d, index) => {
        if (d.pyramidSlot == -1) {
          var roll = new PharaohDiceRoll(index, Math.floor(Math.random() * 6));
          d.gfx.setEnabled(false);
          this.sendGameMessage(PharaohMessageType.DiceRoll, roll);
          d.value = roll.value;
          d.rolledValue = roll.value;
        }
      });

      this.rollButton.setVisible(false);
      this.dieLocked = false;
      this.hasRolled++;

      this.freeWilds = 0;
      this.freeRerolls = 0;
      this.activeDice.filter(d => d.pyramidSlot == -1).forEach((d) => {
        if (d.type == PharaohDiceType.Vizier && this.getDiceFaceValue(d) == -1 || (d.type == PharaohDiceType.Artisan || d.type == PharaohDiceType.Captain) && d.value == 0) {
          this.freeWilds++;
        }
        if (d.type == PharaohDiceType.Conspirator && this.getDiceFaceValue(d) == -1) {
          this.freeWilds += 2;
        }
        if (d.type == PharaohDiceType.Captain && d.value >= 3 && d.value <= 4) {
          this.freeRerolls++;
        }
      });
    }
  }

  private clickedGenericButton(id: number) {
    if (id == this.genericButton.getId()) {
      this.genericButton.setVisible(false);
      if (this.activeAction != null) {
        if (this.activeAction.action == PharaohTileAction.MovePips && this.movePipsDie.length > 0) {
          this.movePipsDie.forEach((d) => {
            d.highlightGfx.setVisible(false);
          });
          this.movePipsDie = new Array<PharaohDice>();
          this.genericButton.setText('Done Moving Pips');
          this.increaseButton.setVisible(false);
          this.genericButton.setVisible(true);
        }
        else {
          this.activeAction = null;
          this.activeDice.forEach(die => die.pipsAdded = false);
          this.hideStandardDiceButtons();
        }
      }
      else {
        if (this.endTurn || this.finalRollOff != null) {
          this.sendGameMessage(PharaohMessageType.EndTurn, '');
        }
        else {
          this.sendGameMessage(PharaohMessageType.TakeScarabs, '');
          this.genericButton.setText('End Turn');
        }
      }
    }
  }

  private updateActivatableTiles() {
    this.playerTiles.filter(t => t.playerId == this.currentPlayer).forEach((t) => {
      var tile = this.getTile(t.tileId);
      if (this.hasRolled > 1) {
        tile.dice.filter(d => d.incrementing).forEach((d) => {
          var gfx = this.canvas.getGfxObject(this.canvasId, t.dieId) as GfxTextureAtlas;
          if (gfx != null) {
            gfx.setIndex(Math.min(5, d.startValue + this.hasRolled - 1));
          }
        });
      }

      var tileGfx = this.canvas.getGfxObject(this.canvasId, t.gfxId) as GfxTexture;
      if (tileGfx != null) {
        tileGfx.setEnabled(!t.used && this.canActivateTile(tile));
      }
    });
  }

  private changeRerolls(die: PharaohDice, value: number): number {
    if (value != die.rolledValue) {
      if (die.type == PharaohDiceType.Captain && value >= 3 && value <= 4) {
        return 1;
      }
    }

    return 0;
  }

  private changeWilds(die: PharaohDice, value: number): number {
    var wilds = 0;
    if (value != die.rolledValue) {
      if ((die.type == PharaohDiceType.Artisan || die.type == PharaohDiceType.Captain || die.type == PharaohDiceType.Vizier) && value == 0) {
        wilds++;
      }
      if (die.type == PharaohDiceType.Conspirator && value == 5) {
        wilds += 2;
      }
    }

    return wilds;
  }

  private hasLockedPair(): boolean {
    var locked = this.activeDice.filter(d => d.lockedOnRoll == this.hasRolled && d.pyramidSlot != -1);
    var numbers = [0, 0, 0, 0, 0, 0];
    locked.forEach((l) => {
      if (this.getDiceFaceValue(l) != -1) {
        numbers[this.getDiceFaceValue(l)]++;
      }
    });

    return numbers.some(l => l >= 2);
  }

  private requirementsMet(tile: PharaohTile): boolean {
    // check if the player already owns this tile
    if (this.playerTiles.some(p => p.playerId == this.currentPlayer && p.tileId == tile.tileId)) {
      return false;
    }

    if (this.countedDice() < tile.tier) {
      return false;
    }

    if (tile.name == 'Herder') {
      return true;
    }

    var met = true;
    var sideDetails = this.getSideColumn(tile.tier, tile.inColumn);
    switch (sideDetails.requirements[0].requirement) {
      case PharaohRequirementType.Even:
        this.activeDice.forEach((d) => {
          if (!this.isEven(d)) {
            met = false;
          }
        });
        break;
      case PharaohRequirementType.Odd:
        this.activeDice.forEach((d) => {
          if (!this.isOdd(d)) {
            met = false;
          }
        });
        break;
      case PharaohRequirementType.GreaterThanEqual:
        this.activeDice.forEach((d) => {
          if (!this.isGreater(d, sideDetails.requirements[0].count)) {
            met = false;
          }
        });
        break;
      case PharaohRequirementType.LessThanEqual:
        this.activeDice.forEach((d) => {
          if (!this.isLess(d, sideDetails.requirements[0].count)) {
            met = false;
          }
        });
        break;
      case PharaohRequirementType.Unique:
        this.activeDice.forEach((d) => {
          if (!this.isUnique(d)) {
            met = false;
          }
        });
        break;
      case PharaohRequirementType.Straight:
        met = this.hasStraight(sideDetails.requirements[0].count);
        break;
      case PharaohRequirementType.Sum:
        var sum = this.activeDice.length;
        this.activeDice.forEach((d) => {
          sum += this.getDiceFaceValue(d);
        });
        met = sum >= sideDetails.requirements[0].count;
        break;
      case PharaohRequirementType.MatchingNumbers:
        met = this.hasMatches(sideDetails.requirements);
        break;
    }

    return met;
  }

  private hasMatches(requirements: Array<PharaohRequirement>): boolean {
    var filtered = this.activeDice.filter(d => d.type != PharaohDiceType.Captain && (d.type != PharaohDiceType.Vizier || d.value != 0) &&
      (d.type != PharaohDiceType.Conspirator || d.value != 5));

    var numbers = [0, 0, 0, 0, 0, 0];
    filtered.forEach((d) => {
      var faceValue = this.getDiceFaceValue(d);
      if (faceValue != -1) {
        numbers[faceValue]++;
      }
    });

    var met = true;
    requirements.forEach((r) => {
      if (r.exact != -1) {
        if (numbers[r.exact] < r.count) {
          met = false;
        }
        else {
          numbers[r.exact] -= r.count;
        }
      }
      else {
        var index = numbers.findIndex(n => n == r.count);
        if (index > -1) {
          numbers[index] -= r.count;
        }
        else {
          var count = 0;
          for (var i = 0; i < numbers.length; i++) {
            if (numbers[i] > count) {
              index = i;
              count = numbers[i];
              numbers[i] -= r.count;
            }
          }

          if (index == -1 || count < r.count) {
            met = false;
          }
        }
      }
    });

    return met;
  }

  private hasStraight(length: number): boolean {
    var filtered = this.activeDice.filter(d => d.type != PharaohDiceType.Captain && (d.type != PharaohDiceType.Vizier || d.value != 0) &&
      (d.type != PharaohDiceType.Conspirator || d.value != 5));

    var numbers = new Array<number>();
    filtered.forEach((d) => {
      var faceValue = this.getDiceFaceValue(d);
      if (!numbers.includes(faceValue)) {
        numbers.push(faceValue);
      }
    });

    numbers.sort();
    if (numbers.length < length) {
      return false;
    }

    for (var i = 0; i <= numbers.length - length; i++) {
      if (numbers[i + length - 1] - numbers[i] == length - 1) {
        return true;
      }
    }

    return false;
  }

  private getDiceFaceValue(die: PharaohDice): number {
    if (die.type == PharaohDiceType.Captain || die.type == PharaohDiceType.Vizier && die.value == 0 || die.type == PharaohDiceType.Conspirator && die.value == 5) {
      return -1;
    }

    if (die.type == PharaohDiceType.Serf) {
      if (die.value < 2) {
        return 0;
      }
      if (die.value > 3) {
        return 1;
      }
    }

    if (die.type == PharaohDiceType.Noble) {
      if (die.value < 2) {
        return 4;
      }
      if (die.value > 3) {
        return 5;
      }
    }

    return die.value;
  }

  private isUnique(die: PharaohDice): boolean {
    if (die.type == PharaohDiceType.Captain || die.type == PharaohDiceType.Vizier && die.value == 0 || die.type == PharaohDiceType.Conspirator && die.value == 5) {
      return true;
    }

    return this.activeDice.filter(d => d.type != PharaohDiceType.Captain && (d.type != PharaohDiceType.Vizier || d.value != 0) &&
      (d.type != PharaohDiceType.Conspirator || d.value != 5) && d.type != PharaohDiceType.Serf && d.type != PharaohDiceType.Noble && d.value == d.value).length < 2;
  }

  private isGreater(die: PharaohDice, min: number): boolean {
    if (die.type == PharaohDiceType.Captain) {
      return true;
    }

    if (die.type == PharaohDiceType.Vizier) {
      return die.value == 0 || die.value >= min - 1;
    }

    return die.value >= min - 1;
  }

  private isLess(die: PharaohDice, max: number): boolean {
    if (die.type == PharaohDiceType.Captain) {
      return true;
    }

    if (die.type == PharaohDiceType.Conspirator) {
      return die.value == 5 || die.value < max;
    }

    return die.value < max;
  }

  private countedDice(): number {
    var count = this.activeDice.length;
    this.activeDice.forEach((d) => {
      if (d.type == PharaohDiceType.Captain || d.type == PharaohDiceType.Conspirator && d.value == 5 || d.type == PharaohDiceType.Vizier && d.value == 0) {
        count--;
      }
    });

    return count;
  }

  private isEven(die: PharaohDice): boolean {
    if (die.type == PharaohDiceType.Captain) {
      return true;
    }

    if (die.type == PharaohDiceType.Serf || die.type == PharaohDiceType.Noble) {
      return die.value >= 3;
    }

    if (die.type == PharaohDiceType.Vizier) {
      return die.value % 2 == 1 || die.value == 0;
    }

    return die.value % 2 == 1;
  }

  private isOdd(die: PharaohDice): boolean {
    if (die.type == PharaohDiceType.Captain) {
      return true;
    }

    if (die.type == PharaohDiceType.Conspirator) {
      return die.value % 2 == 0 || die.value == 5
    }

    if (die.type == PharaohDiceType.Vizier) {
      return die.value % 2 == 0;
    }

    return !this.isEven(die);
  }

  private hideStandardDiceButtons() {
    this.deselectDie();

    this.increaseButton.setVisible(false);
    this.rerollButton.setVisible(false);
    this.lockButton.setVisible(false);
    this.doneButton.setVisible(false);
    this.increaseButton2.setVisible(false);
    this.increaseButton3.setVisible(false);

    this.wildButton1.setVisible(false);
    this.wildButton2.setVisible(false);
    this.wildButton3.setVisible(false);
    this.wildButton4.setVisible(false);
    this.wildButton5.setVisible(false);
    this.wildButton6.setVisible(false);

    if (this.activeDiceRemaining()) {
      this.rollButton.setVisible(this.dieLocked && !this.activeDice.some(d => d.type == PharaohDiceType.White && d.pyramidSlot == -1) || this.hasRolled == 0 && this.activeAction == null &&
        this.claimExtraTile == 0);
    }
    else {
      if (this.playerTiles.some(t => t.playerId == this.currentPlayer && t.tileId == 54)) {
        this.genericButton.setVisible(true);
        this.genericButton.setText('Take 2 Scarabs');
      }
      if (this.finalRollOff != null) {
        this.genericButton.setVisible(true);
        this.genericButton.setText('End Turn');
      }
    }
  }

  private activeDiceRemaining(): boolean {
    return this.activeDice.some(d => d.pyramidSlot == -1);
  }

  private showStandardDiceButtons() {
    if (!this.isCurrentPlayer() || this.selectedDie == null || this.activeAction != null && this.activeAction.action != PharaohTileAction.AnyButRedWild &&
      this.activeAction.action != PharaohTileAction.AnyDiceLocked && this.activeAction.action != PharaohTileAction.WhiteDiceWild && this.activeAction.action != PharaohTileAction.SplitDice) {
      return;
    }

    if (this.score.getScore(this.currentPlayer, 'scarab-pip') > 0 && this.activeAction == null && this.selectedDie.value != 6) {
      if (this.canAddPips(1, this.selectedDie)) {
        this.increaseButton.setVisible(true);
      }
    }
    if (this.score.getScore(this.currentPlayer, 'scarab-reroll') + this.freeRerolls > 0 && this.activeAction == null && this.selectedDie.value != 6) {
      if (this.freeRerolls > 0) {
        this.rerollButton.setFillStyle('black');
      }
      else {
        this.rerollButton.setFillStyle('red');
      }

      this.rerollButton.setVisible(true);
    }

    this.lockButton.setVisible(true && this.activeAction == null && this.selectedDie.value != 6);
    this.doneButton.setVisible(true && (this.activeAction == null || this.activeAction.action != PharaohTileAction.SplitDice));

    this.rollButton.setVisible(false);

    if (this.freeWilds > 0 || this.activeAction != null && (this.selectedDie.type == PharaohDiceType.White && this.activeAction.action == PharaohTileAction.WhiteDiceWild ||
      this.selectedDie.type != PharaohDiceType.Red && this.activeAction.action == PharaohTileAction.AnyButRedWild || this.activeAction.action == PharaohTileAction.AnyDiceLocked ||
      this.activeAction.action == PharaohTileAction.SplitDice) ||
      this.selectedDie.value == 6) {
      this.wildButton1.setTexture(this.canvas.getImage(this.getDiceTexturePath(this.selectedDie.type)));
      this.wildButton2.setTexture(this.canvas.getImage(this.getDiceTexturePath(this.selectedDie.type)));
      this.wildButton3.setTexture(this.canvas.getImage(this.getDiceTexturePath(this.selectedDie.type)));
      this.wildButton4.setTexture(this.canvas.getImage(this.getDiceTexturePath(this.selectedDie.type)));
      this.wildButton5.setTexture(this.canvas.getImage(this.getDiceTexturePath(this.selectedDie.type)));
      this.wildButton6.setTexture(this.canvas.getImage(this.getDiceTexturePath(this.selectedDie.type)));
      if (this.selectedDie.type == PharaohDiceType.Serf) {
        this.wildButton1.setVisible(this.showWildButton(0));
        this.wildButton5.setVisible(this.showWildButton(1));
        this.wildButton3.setVisible(this.showWildButton(2));
        this.wildButton4.setVisible(this.showWildButton(3));
      }
      else if (this.selectedDie.type == PharaohDiceType.Noble) {
        this.wildButton3.setVisible(this.showWildButton(3));
        this.wildButton4.setVisible(this.showWildButton(4));
        this.wildButton2.setVisible(this.showWildButton(5));
        this.wildButton6.setVisible(this.showWildButton(6));
      }
      else if (this.selectedDie.type == PharaohDiceType.Captain) {
        this.wildButton1.setVisible(this.showWildButton(6));
        this.wildButton2.setVisible(this.showWildButton(6));
        this.wildButton4.setVisible(this.showWildButton(6));
        this.wildButton6.setVisible(this.showWildButton(6));
      }
      else {
        this.wildButton1.setVisible(this.showWildButton(0));
        this.wildButton2.setVisible(this.showWildButton(1));
        this.wildButton3.setVisible(this.showWildButton(2));
        this.wildButton4.setVisible(this.showWildButton(3));
        this.wildButton5.setVisible(this.showWildButton(4));
        this.wildButton6.setVisible(this.showWildButton(5));
      }
    }
  }

  private showWildButton(faceValue: number): boolean {
    if (this.activeAction != null && this.activeAction.action == PharaohTileAction.SplitDice && faceValue >= this.selectedDie.value) {
      return false;
    }
    return this.activeAction == null || this.activeAction.action != PharaohTileAction.AnyDiceLocked || this.activeDice.some(d => d.pyramidSlot != -1 && this.getDiceFaceValue(d) == faceValue) ||
      this.getDiceFaceValue(this.selectedDie) == 6;
  }

  private canRemovePips(die: PharaohDice): boolean {
    if (this.getDiceFaceValue(die) == -1) {
      return false;
    }

    if ((die.type == PharaohDiceType.Red || die.type == PharaohDiceType.White || die.type == PharaohDiceType.Artisan || die.type == PharaohDiceType.Conspirator ||
      die.type == PharaohDiceType.Serf) && this.getDiceFaceValue(die) > 0) {
      return true;
    }

    if (die.type == PharaohDiceType.Vizier && this.getDiceFaceValue(die) > 1) {
      return true;
    }

    if (die.type == PharaohDiceType.Noble && this.getDiceFaceValue(die) > 2) {
      return true;
    }

    return false;
  }

  private canAddPips(pips: number, die: PharaohDice): boolean {
    if (this.getDiceFaceValue(die) == -1 || die.pipsAdded) {
      return false;
    }

    if ((die.type == PharaohDiceType.Red || die.type == PharaohDiceType.White || die.type == PharaohDiceType.Artisan || die.type == PharaohDiceType.Vizier) && this.getDiceFaceValue(die) < 6 - pips) {
      return true;
    }

    if ((die.type == PharaohDiceType.Conspirator || die.type == PharaohDiceType.Noble) && this.getDiceFaceValue(die) < 5 - pips) {
      return true;
    }

    if (die.type == PharaohDiceType.Serf && this.getDiceFaceValue(die) < 4 - pips) {
      return true;
    }

    return false;
  }

  private setupPlayerTurn() {
    this.hasRolled = 0;
    this.freeWilds = 0;
    this.activeAction = null;
    this.endTurn = false;

    if (this.finalRollOff != null && this.extraDice < 0) {
      this.extraDice = 0;
    }

    var diceCount = new Map<number, number>();
    for (var i = PharaohDiceType.Red; i <= PharaohDiceType.Captain; i++) {
      diceCount.set(i, 0);
    }

    if (this.currentPlayerGfxContainer != null) {
      this.currentPlayerGfxContainer.removeAllChildren();
    }
    else {
      this.currentPlayerGfxContainer = this.canvas.createContainer(this.canvasId, 0);
      this.currentPlayerGfxContainer.translate(800, 0);
    }
    this.playerTiles.filter(t => t.playerId == this.currentPlayer).forEach((t, index) => {
      this.setupTiles(t, index, diceCount);
    });

    if (this.extraDice != 0) {
      var redCount = diceCount.get(PharaohDiceType.Red) + this.extraDice;
      diceCount.set(PharaohDiceType.Red, redCount);
      this.extraDice = 0;
    }

    this.activeDice = new Array<PharaohDice>();
    this.setupDice(diceCount);

    this.rollButton.setVisible(this.isCurrentPlayer());
    this.updateActivatableTiles();

    this.dieLocked = false;
  }

  private setupDice(diceCount: Map<number, number>) {
    for (var i = PharaohDiceType.Red; i <= PharaohDiceType.Captain; i++) {
      var count = diceCount.get(i);
      for (var k = 0; k < count; k++) {
        this.addActiveDie(i, -1);
        if (this.activeDice.length >= 21) {
          break;
        }
      }
      if (this.activeDice.length >= 21) {
        break;
      }
    }
  }

  private setupTiles(t: PharaohPlayerTile, index: number, diceCount: Map<number, number>) {
    var tile = this.getTile(t.tileId);
    if (!tile.columns.some(c => c == 3)) {
      t.used = false;
    }
    this.dealScarabs(t.playerId, tile.scarabsPerTurn);
    var tileGfx = this.canvas.createTexture(this.canvasId, this.tiles[t.tileId].image, this.currentPlayerGfxContainer.getId());
    t.gfxId = tileGfx.getId();
    if (index > 10) {
      tileGfx.translate((3 - (index - 11) % 2) * 200, -Math.floor((index - 11) / 2) * 135 + 205)
    }
    else {
      tileGfx.translate((3 - index % 4) * 200, Math.floor(index / 4) * 135 + 340);
    }
    if (!t.used) {
      tile.dice.forEach((d) => {
        if (d.mustRoll) {
          var amount = diceCount.get(d.diceType) + d.amount;
          diceCount.set(d.diceType, amount);
        }
        else {
          for (var i = 0; i < d.amount; i++) {
            var die = this.createDieGfx(d.diceType, d.startValue, tileGfx.getId());
            die.translate(35 + i * 40, 35);
            t.dieId = die.getId();
          }
        }
      });
    }
    else {
      this.markTileUsed(tile);
    }
  }

  private canActivateTile(tile: PharaohTile): boolean {
    if (!this.isCurrentPlayer()) {
      return false;
    }

    if (tile.dice.some(d => !d.mustRoll)) {
      return true;
    }

    if (tile.action != null && tile.action.some(a => a.action == PharaohTileAction.BonusScarabs || a.action == PharaohTileAction.DoubleScarabs ||
      a.action == PharaohTileAction.BonusTurn && (a.onFinalRoll || this.finalRollOff == null) ||
      a.action == PharaohTileAction.SwapForRed && this.hasRolled == 0 && this.activeDice.some(d => d.type == PharaohDiceType.White || d.type == PharaohDiceType.Serf) ||
      a.action == PharaohTileAction.BonusScarabAfterRoll && this.hasRolled > 1 || a.action == PharaohTileAction.ClaimTile ||
      a.action == PharaohTileAction.DicePenalty || a.action == PharaohTileAction.StartFinalRoll ||
      this.hasRolled > 0 &&       // these tiles can only be played after at least one roll has been made in the turn
      (a.action == PharaohTileAction.AddPips && this.activeDice.some(d => d.pyramidSlot == -1) ||
      a.action == PharaohTileAction.AnyButRedWild && this.activeDice.some(d => d.pyramidSlot == -1 && d.type != PharaohDiceType.Red) ||
      a.action == PharaohTileAction.AnyDiceLocked && this.activeDice.some(d => d.pyramidSlot != -1) ||
      a.action == PharaohTileAction.AnyDiceWild && this.activeDice.some(d => d.pyramidSlot == -1) ||
      a.action == PharaohTileAction.FlipDice && this.activeDice.some(d => d.pyramidSlot == -1) ||
      a.action == PharaohTileAction.MovePips && this.activeDice.filter(d => d.pyramidSlot == -1).length > 1 ||
      a.action == PharaohTileAction.SplitDice && this.activeDice.some(d => d.pyramidSlot == -1) ||
      a.action == PharaohTileAction.WhiteDiceWild && this.activeDice.some(d => d.pyramidSlot == -1 && d.type == PharaohDiceType.White)))) {
      return true;
    }

    return false;
  }

  private addActiveDie(type: PharaohDiceType, value: number) {
    var columnsPerRow = [4, 3, 3, 2, 1, 1];
    var x = this.activeDice.length;
    var y = 0;
    for (var i = 0; i < columnsPerRow.length; i++) {
      if (x >= columnsPerRow[i]) {
        x -= columnsPerRow[i];
        y++;
      }
      else {
        break;
      }
    }
    if (y >= columnsPerRow.length) {
      y = 0;
      for (var i = 0; i < columnsPerRow.length; i++) {
        if (x >= columnsPerRow[i]) {
          x -= columnsPerRow[i];
          y++;
        }
        else {
          break;
        }
      }
    }
    var die = this.createDieGfx(type, value, this.currentPlayerGfxContainer.getId());

    if (this.activeDice.length < 14) {
      die.translate(x * 43, 5 + y * 43);
    }
    else {
      die.translate(390 - x * 43, 5 + y * 43);
    }
    die.setEnabled(this.isCurrentPlayer() && value >= 0);
    var highlight = this.canvas.createRectangle(this.canvasId, 38, 38, 'white', false, 3, die.getId());
    highlight.setVisible(false);
    this.activeDice.push(new PharaohDice(die, type, highlight, value));
  }

  private createDieGfx(type: PharaohDiceType, startValue: number, parent: number): GfxTextureAtlas {
    return this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(type), 2, 3, Math.min(5, Math.max(0, startValue)), parent);
  }

  private getDiceTexturePath(type: PharaohDiceType): string {
    switch (type) {
      case PharaohDiceType.Red:
        return './assets/pharaoh/red-dice.jpg';
      case PharaohDiceType.White:
        return './assets/pharaoh/white-dice.jpg';
      case PharaohDiceType.Artisan:
        return './assets/pharaoh/artisan-dice.jpg';
      case PharaohDiceType.Captain:
        return './assets/pharaoh/captain-dice.jpg';
      case PharaohDiceType.Conspirator:
        return './assets/pharaoh/conspirator-dice.jpg';
      case PharaohDiceType.Noble:
        return './assets/pharaoh/noble-dice.jpg';
      case PharaohDiceType.Serf:
        return './assets/pharaoh/serf-dice.jpg';
      case PharaohDiceType.Vizier:
        return './assets/pharaoh/vizier-dice.jpg';
    }
  }

  private onPipChange(data: number, playerId: string) {
    var scores = this.scarabScores.find(s => s.playerId == playerId);
    if (scores != null) {
      var text = this.canvas.getGfxObject(this.canvasId, scores.pipId) as GfxText;
      text.setText(data.toString());
    }
  }

  private onRerollChange(data: number, playerId: string) {
    var scores = this.scarabScores.find(s => s.playerId == playerId);
    if (scores != null) {
      var text = this.canvas.getGfxObject(this.canvasId, scores.rerollId) as GfxText;
      text.setText(data.toString());
    }
  }

  private onMessage(data: GameMessage) {
    if (data != null) {
      var message = JSON.parse(data.message);
      switch (data.type) {
        case PharaohMessageType.UpdatePlayerList:
          this.activePlayers = message;
          break;
        case PharaohMessageType.UpdateSideSettings:
          this.settings.tierSides.set(message.tier, message.side);
          break;
        case PharaohMessageType.UpdateTileSettings:
          this.settings.tiles.set(message.tier * 10 + message.column, message.tileId);
          break;
        case PharaohMessageType.StartGame:
          this.advancePhase();
          break;
        case PharaohMessageType.FirstPlayer:
          this.setupFirstTurn(message);
          break;
        case PharaohMessageType.ScarabChange:
          this.updateScarabs(message);
          break;
        case PharaohMessageType.DiceRoll:
          this.updateDiceRoll(message);
          break;
        case PharaohMessageType.LockDie:
          this.lockDie(message);
          break;
        case PharaohMessageType.ClaimTile:
          this.awardTile(message);
          break;
        case PharaohMessageType.IncreasePip:
          this.updateDiceRoll(message);
          if (this.activeAction == null) {
            this.score.subtractScore(1, data.playerId, 'scarab-pip');
          }
          else if (this.activeAction.diceForAddPips == 1) {
            this.activeAction = null;
          }
          if (this.activeAction == null || this.activeAction.action != PharaohTileAction.MovePips) {
            this.increaseButton.setVisible(false);
            this.showStandardDiceButtons();
          }
          break;
        case PharaohMessageType.RerollDie:
          this.updateDiceRoll(message);
          if (this.freeRerolls > 0) {
            this.freeRerolls--;
          }
          else {
            this.score.subtractScore(1, data.playerId, 'scarab-reroll');
          }
          this.rerollButton.setVisible(false);
          this.showStandardDiceButtons();
          break;
        case PharaohMessageType.AddActiveDie:
          this.addActiveDie(message.type, message.value);
          if (this.selectedDie == null && this.isCurrentPlayer()) {
            this.hideStandardDiceButtons();
          }
          break;
        case PharaohMessageType.TakeScarabs:
          this.addScarabs(data.playerId);
          break;
        case PharaohMessageType.ActivateTile:
          var tile = this.getTile(message);
          this.markTileUsed(tile);
          this.activeDice.forEach((d) => { d.pipsAdded = false; });
          tile.action.forEach((a) => {
            this.activateTileAction(a);
          });
          break;
        case PharaohMessageType.SwapToRed:
          this.changeDieType(message, PharaohDiceType.Red);
          break;
        case PharaohMessageType.SwapToWhite:
          this.changeDieType(message, PharaohDiceType.White);
          break;
        case PharaohMessageType.EndTurn:
          if (this.finalRollOff != null) {
            this.finalRolloffScore();
          }
          this.advanceTurn();
          break;
      }
    }
  }

  private markTileUsed(tile: PharaohTile) {
    var playerTile = this.playerTiles.find(t => t.playerId == this.currentPlayer && t.tileId == tile.tileId);
    this.canvas.createRectangle(this.canvasId, 189, 124, '#11111177', true, 1, playerTile.gfxId);
  }

  private changeDieType(index: number, type: PharaohDiceType) {
    this.activeDice[index].type = type;
    this.activeDice[index].gfx.setTexture(this.canvas.getImage(this.getDiceTexturePath(type)));
    this.activeDice[index].gfx.setEnabled(false);
    if (type == PharaohDiceType.Red) {
      this.dealScarabs(this.currentPlayer, 1);
    }
  }

  private addScarabs(playerId: string) {
    this.dealScarabs(playerId, 2);
  }

  private awardTile(claimedTile: PharaohClaimTile) {
    this.playerTiles.push(new PharaohPlayerTile(claimedTile.playerId, claimedTile.tileId));

    var tile = this.getTile(claimedTile.tileId);
    var claimedTiles = this.playerTiles.filter(t => t.tileId == claimedTile.tileId);
    if (claimedTiles.length >= Math.max(1, this.activePlayers.length - (tile.tier - 3))) {
      var texture = this.canvas.getGfxObject(this.canvasId, tile.gfxId) as GfxTexture;
      texture.setEnabled(false);
      this.canvas.createRectangle(this.canvasId, 189, 124, '#11111177', true, 1, tile.gfxId);
    }

    this.dealScarabs(claimedTile.playerId, tile.scarabsWhenClaimed);

    tile.action.filter(t => t.whenClaimed).forEach((t) => {
      this.playerTiles[this.playerTiles.length - 1].used = true;
      this.markTileUsed(tile);
      this.activateTileAction(t);
    });

    this.genericButton.setVisible(false);

    if (claimedTile.endTurn) {
      this.genericButton.setVisible(this.isCurrentPlayer());
      this.genericButton.setText('End Turn');
      this.endTurn = true;

      if (tile.tier == 7 && tile.inColumn == 0) {
        this.finalRolloffScore();
        this.startFinalRoll();
      }
    }

    // Add new tile to the player setup
    var diceCount = new Map<number, number>();
    for (var i = PharaohDiceType.Red; i <= PharaohDiceType.Captain; i++) {
      diceCount.set(i, 0);
    }
    var length = this.playerTiles.filter(t => t.playerId == this.currentPlayer).length;
    this.setupTiles(this.playerTiles[this.playerTiles.length - 1], length - 1, diceCount);
    this.setupDice(diceCount);
    this.updateActivatableTiles();
  }

  private startFinalRoll() {
    this.finalRollOff = this.currentPlayer;
    if (this.extraDice < 0) {
      this.extraDice = 0;
    }

    this.addFinalRollDice();
  }

  private finalRolloffScore() {
    var filtered = this.activeDice.filter(d => d.type != PharaohDiceType.Captain && (d.type != PharaohDiceType.Vizier || d.value != 0) &&
      (d.type != PharaohDiceType.Conspirator || d.value != 5));

    var numbers = [0, 0, 0, 0, 0, 0];
    filtered.forEach((d) => {
      var faceValue = this.getDiceFaceValue(d);
      if (faceValue != -1) {
        numbers[faceValue]++;
      }
    });

    var i = 0;
    var index = 0;
    for (i = 0; i < 6; i++) {
      if (numbers[i] > numbers[index]) {
        index = i;
      }
    }

    if (this.pharaohLeader == null) {
      this.pharaohLeader = this.canvas.createTextureAtlas(this.canvasId, this.getDiceTexturePath(PharaohDiceType.Red), 2, 3, index, this.pyramidGfx.getId());
      this.leaderName = this.canvas.createText(this.canvasId, '', '15px Arial', 'white', this.pharaohLeader.getId());
      this.leaderName.translate(-10, 10);
    }

    if (numbers[index] > this.leaderCount || numbers[index] == this.leaderCount && this.leaderValue < index) {
      this.pharaohLeader.setIndex(index);
      this.pharaohLeader.setTranslation(39 + 61 * (numbers[index] - 7), 302);
      this.leaderName.setText(this.activePlayers.find(p => p.id == this.currentPlayer).name);
      this.leaderValue = index;
      this.leaderCount = numbers[index];
    }
  }

  private addFinalRollDice() {
    var nextPlayer = this.getNextPlayer(this.finalRollOff);
    while (nextPlayer != this.firstPlayer) {
      if (this.playerState.getPlayerId() == nextPlayer) {
        this.extraDice = 1;
      }
      nextPlayer = this.getNextPlayer(nextPlayer);
    }
  }

  private activateTileAction(action: PharaohAction) {
    switch (action.action) {
      case PharaohTileAction.BonusTurn:
        this.bonusTurn = true;
        break;
      case PharaohTileAction.DicePenalty:
        if (action.penaltyToSelf && this.isCurrentPlayer() || !action.penaltyToSelf && !this.isCurrentPlayer() && this.finalRollOff != null) {
          this.extraDice -= action.min;
        }
        break;
      case PharaohTileAction.BonusScarabAfterRoll:
      case PharaohTileAction.BonusScarabs:
        this.dealScarabs(this.currentPlayer, action.min);
        break;
      case PharaohTileAction.DoubleScarabs:
        var count = this.score.getScore(this.currentPlayer, 'scarab-pip') + this.score.getScore(this.currentPlayer, 'scarab-reroll');
        this.dealScarabs(this.currentPlayer, count);
        break;
      case PharaohTileAction.FlipDice:
        this.activeAction = action;
        this.deselectDie();
        if (this.isCurrentPlayer()) {
          this.genericButton.setText("Done Flipping");
          this.genericButton.setVisible(true);
          this.hideStandardDiceButtons();
        }
        break;
      case PharaohTileAction.AddPips:
        this.activeAction = action;
        this.deselectDie();
        if (this.isCurrentPlayer()) {
          this.genericButton.setText('Done Adding');
          this.genericButton.setVisible(this.activeAction.diceForAddPips > 1);
          this.hideStandardDiceButtons();
        }
        break;
      case PharaohTileAction.ClaimTile:
        if (this.isCurrentPlayer()) {
          var claimableTiles = this.tiles.filter(t => t.inColumn >= action.min && t.inColumn <= action.max && t.tier <= action.claimTier &&
            !this.playerTiles.some(p => p.playerId == this.currentPlayer && p.tileId == t.tileId)).length;
          this.claimExtraTile++;
          if (this.claimExtraTile > claimableTiles) {
            this.claimExtraTile = claimableTiles;
          }
          this.hideStandardDiceButtons();
        }
      case PharaohTileAction.SplitDice:
      case PharaohTileAction.AnyDiceLocked:
        this.activeAction = action;
        break;
      case PharaohTileAction.AnyDiceWild:
        this.freeWilds += action.max;
        break;
      case PharaohTileAction.AnyButRedWild:
      case PharaohTileAction.WhiteDiceWild:
        this.activeAction = action;
        this.deselectDie();
        if (this.isCurrentPlayer()) {
          this.genericButton.setText('Done Setting Dice');
          this.genericButton.setVisible(true);
          this.hideStandardDiceButtons();
        }
        break;
      case PharaohTileAction.SwapForRed:
        this.activeAction = action;
        if (this.isCurrentPlayer()) {
          this.genericButton.setText('Done Swapping Dice');
          this.genericButton.setVisible(true);
          this.hideStandardDiceButtons();

          this.activeDice.filter(d => d.type == PharaohDiceType.White || d.type == PharaohDiceType.Serf).forEach((d) => {
            d.gfx.setEnabled(true);
          });
        }
        break;
      case PharaohTileAction.MovePips:
        this.activeAction = action;
        if (this.isCurrentPlayer()) {
          this.genericButton.setText('Done Moving Pips');
          this.genericButton.setVisible(true);
          this.hideStandardDiceButtons();
        }
        break;
      case PharaohTileAction.StartFinalRoll:
        this.startFinalRoll();
        break;
      /*
  SplitLocked = 16*/
    }
  }

  private isCurrentPlayer(): boolean {
    return this.currentPlayer == this.playerState.getPlayerId();
  }

  private deselectDie() {
    if (this.selectedDie != null) {
      this.selectedDie.highlightGfx.setVisible(false);
      this.selectedDie = null;
    }
  }

  private advanceTurn() {
    if (this.bonusTurn) {
      this.bonusTurn = false;
      if (this.finalRollOff != null) {
        this.addFinalRollDice();
      }
      this.setupPlayerTurn();
      return;
    }
    else {
      this.currentPlayer = this.getNextPlayer(this.currentPlayer);
      if (this.finalTurn && this.finalRollOff != this.currentPlayer) {
        this.advancePhase();
        return;
      }
    }

    this.setupPlayerTurn();
    if (this.finalRollOff == this.currentPlayer) {
      this.finalTurn = true;
    }
  }

  private lockDie(lock: PharaohLockDie) {
    this.activeDice[lock.index].pyramidSlot = lock.pyramid;
    this.activeDice[lock.index].gfx.setEnabled(false);
    this.activeDice[lock.index].gfx.setTranslation(this.pyramidSlots[lock.pyramid].x, this.pyramidSlots[lock.pyramid].y);
  }

  private updateDiceRoll(roll: PharaohDiceRoll) {
    this.activeDice[roll.index].value = roll.value;
    this.activeDice[roll.index].gfx.setIndex(roll.value);
    this.activeDice[roll.index].gfx.setEnabled(this.isCurrentPlayer());
  }

  private updateScarabs(change: PharaohScarabChange) {
    if (change.scarabType == PharaohScarabType.Pip) {
      this.score.addScore(change.change, change.playerId, 'scarab-pip');
    }
    else {
      this.score.addScore(change.change, change.playerId, 'scarab-reroll');
    }
  }

  private setupFirstTurn(firstPlayer: string) {
    this.firstPlayer = firstPlayer;
    this.currentPlayer = firstPlayer;
    var scarabPlayer = this.getNextPlayer(this.firstPlayer);
    var scarabCount = 1;
    while (scarabPlayer != this.firstPlayer) {
      this.dealScarabs(scarabPlayer, scarabCount);
      scarabPlayer = this.getNextPlayer(scarabPlayer);
      scarabCount++;
    }
  }

  private getNextPlayer(player: string): string {
    var index = this.activePlayers.findIndex(p => p.id == player);
    index++;
    if (index >= this.activePlayers.length) {
      index = 0;
    }

    return this.activePlayers[index].id;
  }

  private dealScarabs(player: string, amount: number) {
    if (this.isGameCreator()) {
      for (var i = 0; i < amount; i++) {
        var scarabType = Math.floor(Math.random() * 2);
        if (scarabType == PharaohScarabType.Pip) {
          this.sendGameMessage(PharaohMessageType.ScarabChange, new PharaohScarabChange(player, PharaohScarabType.Pip, 1));
        }
        else {
          this.sendGameMessage(PharaohMessageType.ScarabChange, new PharaohScarabChange(player, PharaohScarabType.Reroll, 1));
        }
      }
    }
  }

  private initializeSettings() {
    this.settings = new PharaohSettings();

    this.sides = new Array<PharaohSide>();
    this.sides.push(new PharaohSide(this.sides.length, 7, 0, 0, '7 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 7)]));
    this.sides.push(new PharaohSide(this.sides.length, 7, 1, 0, '7 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 7)]));
    this.sides.push(new PharaohSide(this.sides.length, 7, 0, 1, '3 of a Kind & 2 Pairs', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2), new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2)]));
    this.sides.push(new PharaohSide(this.sides.length, 7, 1, 1, '4 of a Kind & Three 1\'s', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3, 0),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 4)]));
    this.sides.push(new PharaohSide(this.sides.length, 7, 0, 2, '3 of a Kind & 4 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 4),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 7, 1, 2, 'Pair & 5 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 5),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 7, 0, 3, '40+', [new PharaohRequirement(PharaohRequirementType.Sum, 40)], 2));
    this.sides.push(new PharaohSide(this.sides.length, 7, 1, 3, '45+', [new PharaohRequirement(PharaohRequirementType.Sum, 45)], 2));

    this.sides.push(new PharaohSide(this.sides.length, 6, 0, 0, '6 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 6)]));
    this.sides.push(new PharaohSide(this.sides.length, 6, 1, 0, '6 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 6)]));
    this.sides.push(new PharaohSide(this.sides.length, 6, 0, 1, '3 Pairs', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2), new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2)]));
    this.sides.push(new PharaohSide(this.sides.length, 6, 1, 1, '1 2 3 4 5 6', [new PharaohRequirement(PharaohRequirementType.Straight, 6)]));
    this.sides.push(new PharaohSide(this.sides.length, 6, 0, 2, 'Pair & 4 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 4),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 6, 1, 2, 'Two 3 of a Kinds', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 6, 0, 3, '30+', [new PharaohRequirement(PharaohRequirementType.Sum, 30)], 2));
    this.sides.push(new PharaohSide(this.sides.length, 6, 1, 3, '35+', [new PharaohRequirement(PharaohRequirementType.Sum, 35)], 2));

    this.sides.push(new PharaohSide(this.sides.length, 5, 0, 0, '5 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 5)]));
    this.sides.push(new PharaohSide(this.sides.length, 5, 1, 0, '5 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 5)]));
    this.sides.push(new PharaohSide(this.sides.length, 5, 0, 1, '12345/23456', [new PharaohRequirement(PharaohRequirementType.Straight, 5)]));
    this.sides.push(new PharaohSide(this.sides.length, 5, 1, 1, 'All Dice <= 2', [new PharaohRequirement(PharaohRequirementType.LessThanEqual, 2)]));
    this.sides.push(new PharaohSide(this.sides.length, 5, 0, 2, 'Pair & 3 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 5, 1, 2, 'Three 6\'s & Pair of 1\'s', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3, 5),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2, 0)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 5, 0, 3, '25+', [new PharaohRequirement(PharaohRequirementType.Sum, 25)], 2));
    this.sides.push(new PharaohSide(this.sides.length, 5, 1, 3, 'All Different', [new PharaohRequirement(PharaohRequirementType.Unique, 0)], 2));

    this.sides.push(new PharaohSide(this.sides.length, 4, 0, 0, '4 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 4)]));
    this.sides.push(new PharaohSide(this.sides.length, 4, 1, 0, '4 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 4)]));
    this.sides.push(new PharaohSide(this.sides.length, 4, 0, 1, '1234/2345/3456', [new PharaohRequirement(PharaohRequirementType.Straight, 4)]));
    this.sides.push(new PharaohSide(this.sides.length, 4, 1, 1, 'All Dice >= 5', [new PharaohRequirement(PharaohRequirementType.GreaterThanEqual, 5)]));
    this.sides.push(new PharaohSide(this.sides.length, 4, 0, 2, '2 Pairs', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 4, 1, 2, 'Pair of 6\'s & Pair of 1\'s', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2, 5),
      new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2, 0)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 4, 0, 3, '20+', [new PharaohRequirement(PharaohRequirementType.Sum, 20)], 2));
    this.sides.push(new PharaohSide(this.sides.length, 4, 1, 3, 'All Different', [new PharaohRequirement(PharaohRequirementType.Unique, 0)], 2));

    this.sides.push(new PharaohSide(this.sides.length, 3, 0, 0, '3 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3)]));
    this.sides.push(new PharaohSide(this.sides.length, 3, 1, 0, '3 of a Kind', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 3)]));
    this.sides.push(new PharaohSide(this.sides.length, 3, 0, 1, 'Pair', [new PharaohRequirement(PharaohRequirementType.MatchingNumbers, 2)]));
    this.sides.push(new PharaohSide(this.sides.length, 3, 1, 1, 'All Dice >= 4', [new PharaohRequirement(PharaohRequirementType.GreaterThanEqual, 4)]));
    this.sides.push(new PharaohSide(this.sides.length, 3, 0, 2, 'All Even', [new PharaohRequirement(PharaohRequirementType.Even, 0)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 3, 1, 2, 'All Odd', [new PharaohRequirement(PharaohRequirementType.Odd, 0)], 1));
    this.sides.push(new PharaohSide(this.sides.length, 3, 0, 3, '10+', [new PharaohRequirement(PharaohRequirementType.Sum, 10)], 2));
    this.sides.push(new PharaohSide(this.sides.length, 3, 1, 3, '15+', [new PharaohRequirement(PharaohRequirementType.Sum, 15)], 2));

    this.tiles = new Array<PharaohTile>();
    this.tiles.push(new PharaohTile(this.tiles.length, 'Queen', 7, [0], './assets/pharaoh/queen.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 6)]));                   // 0
    this.tiles.push(new PharaohTile(this.tiles.length, 'Granary Master', 7, [1], './assets/pharaoh/granary-master.jpg', [new PharaohDiceSet(PharaohDiceType.Red),
      new PharaohDiceSet(PharaohDiceType.Red, false, 1, 0, true)]));          // 1
    this.tiles.push(new PharaohTile(this.tiles.length, 'Grand Vizier', 7, [1], './assets/pharaoh/grand-vizier.jpg', [new PharaohDiceSet(PharaohDiceType.Vizier)]));            // 2
    this.tiles.push(new PharaohTile(this.tiles.length, 'General', 7, [1], './assets/pharaoh/general.jpg', [new PharaohDiceSet(PharaohDiceType.Red, true, 2)]));          // 3
    this.tiles.push(new PharaohTile(this.tiles.length, 'Royal Astrologer', 7, [2], './assets/pharaoh/royal-astrologer.jpg', [], 0, 1,
      [new PharaohAction(PharaohTileAction.AnyButRedWild, 1, 100)]));          // 4
    this.tiles.push(new PharaohTile(this.tiles.length, 'Heir', 7, [2], './assets/pharaoh/heir.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.AddPips, 1, 2)]));          // 5
    this.tiles.push(new PharaohTile(this.tiles.length, 'Royal Mother', 7, [2], './assets/pharaoh/royal-mother.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.SwapForRed, 1, 100)]));          // 6
    this.tiles.push(new PharaohTile(this.tiles.length, 'Queen\'s Favor', 7, [3], './assets/pharaoh/queens-favor.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.ClaimTile, 0, 2, false, 6)]));          // 7
    this.tiles.push(new PharaohTile(this.tiles.length, 'Royal Death', 7, [3], './assets/pharaoh/royal-death.jpg', [new PharaohDiceSet(PharaohDiceType.White, false, 2, -1, false, false, false,
      false, true)], 0, 2, [new PharaohAction(PharaohTileAction.StartFinalRoll, 0, 0)]));          // 8
    this.tiles.push(new PharaohTile(this.tiles.length, 'Royal Power', 7, [3], './assets/pharaoh/royal-power.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.ClaimTile, 2, 2, false, 6),
      new PharaohAction(PharaohTileAction.ClaimTile, 2, 2, false, 6)]));          // 9

    this.tiles.push(new PharaohTile(this.tiles.length, 'Estate Overseer', 6, [0, 1], './assets/pharaoh/estate-overseer.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 0, true)], 1));          // 10
    this.tiles.push(new PharaohTile(this.tiles.length, 'Royal Attendants', 6, [0, 1], './assets/pharaoh/royal-attendants.jpg', [new PharaohDiceSet(PharaohDiceType.Red),
      new PharaohDiceSet(PharaohDiceType.White)]));          // 11
    this.tiles.push(new PharaohTile(this.tiles.length, 'Grain Trader', 6, [0, 1], './assets/pharaoh/grain-trader.jpg', [new PharaohDiceSet(PharaohDiceType.Red)], 2));          // 12
    this.tiles.push(new PharaohTile(this.tiles.length, 'Embalmer', 6, [0, 1], './assets/pharaoh/embalmer.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 5)]));          // 13
    this.tiles.push(new PharaohTile(this.tiles.length, 'Priest of the Dead', 6, [0, 1], './assets/pharaoh/priest-of-the-dead.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 6, false, true)]));          // 14
    this.tiles.push(new PharaohTile(this.tiles.length, 'Astrologer', 6, [2], './assets/pharaoh/astrologer.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.MovePips, 2, 3)]));          // 15
    this.tiles.push(new PharaohTile(this.tiles.length, 'Priestess', 6, [2], './assets/pharaoh/priestess.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.AddPips, 2, 2)]));          // 16
    this.tiles.push(new PharaohTile(this.tiles.length, 'Surveyor', 6, [2], './assets/pharaoh/surveyor.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.SplitDice, 0, 0)]));          // 17
    this.tiles.push(new PharaohTile(this.tiles.length, 'Pharaoh\s Gift', 6, [3], './assets/pharaoh/pharaohs-gift.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.BonusTurn, 0, 0, false, 0, true)]));          // 18
    this.tiles.push(new PharaohTile(this.tiles.length, 'Secret Passage', 6, [3], './assets/pharaoh/secret-passage.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.ClaimTile, 0, 3, false, 3),
      new PharaohAction(PharaohTileAction.ClaimTile, 0, 3, false, 3)]));          // 19
    this.tiles.push(new PharaohTile(this.tiles.length, 'Treasure', 6, [3], './assets/pharaoh/treasure.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.SplitLocked, 0, 2)]));          // 20

    this.tiles.push(new PharaohTile(this.tiles.length, 'Charioteer', 5, [0, 1], './assets/pharaoh/charioteer.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 4)]));          // 21
    this.tiles.push(new PharaohTile(this.tiles.length, 'Conspirator', 5, [0, 1], './assets/pharaoh/conspirator.jpg', [new PharaohDiceSet(PharaohDiceType.Conspirator)]));          // 22
    this.tiles.push(new PharaohTile(this.tiles.length, 'Tomb Builder', 5, [0, 1], './assets/pharaoh/tomb-builder.jpg', [new PharaohDiceSet(PharaohDiceType.Red)], 1));          // 23
    this.tiles.push(new PharaohTile(this.tiles.length, 'Ship Captain', 5, [0, 1], './assets/pharaoh/ship-captain.jpg', [new PharaohDiceSet(PharaohDiceType.Captain)]));          // 24
    this.tiles.push(new PharaohTile(this.tiles.length, 'Overseer', 5, [0, 1], './assets/pharaoh/overseer.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 3)]));          // 25
    this.tiles.push(new PharaohTile(this.tiles.length, 'Master Artisan', 5, [2], './assets/pharaoh/master-artisan.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.AnyDiceWild, 1, 1)]));          // 26
    this.tiles.push(new PharaohTile(this.tiles.length, 'Head Servant', 5, [2], './assets/pharaoh/head-servant.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.WhiteDiceWild, 1, 100)]));          // 27
    this.tiles.push(new PharaohTile(this.tiles.length, 'Priest', 5, [2], './assets/pharaoh/priest.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.AddPips, 1, 1)]));          // 28
    this.tiles.push(new PharaohTile(this.tiles.length, 'Royal Decree', 5, [3], './assets/pharaoh/royal-decree.jpg', [new PharaohDiceSet(PharaohDiceType.White, false, 3, 0, false, false, false, true)],
      0, 2));          // 29
    this.tiles.push(new PharaohTile(this.tiles.length, 'Bad Omen', 5, [3], './assets/pharaoh/bad-omen.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, -1)],
      0, 2, [new PharaohAction(PharaohTileAction.DicePenalty, 2, 2, false, 0, false, false)]));          // 30
    this.tiles.push(new PharaohTile(this.tiles.length, 'Burial Mask', 5, [3], './assets/pharaoh/burial-mask.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.BonusScarabs, 5, 5)]));          // 31

    this.tiles.push(new PharaohTile(this.tiles.length, 'Palace Servants', 4, [0, 1], './assets/pharaoh/palace-servants.jpg', [new PharaohDiceSet(PharaohDiceType.White, true, 2)]));          // 32
    this.tiles.push(new PharaohTile(this.tiles.length, 'Builder', 4, [0, 1], './assets/pharaoh/builder.jpg', [new PharaohDiceSet(PharaohDiceType.White)], 1));          // 33
    this.tiles.push(new PharaohTile(this.tiles.length, 'Artisan', 4, [0, 1], './assets/pharaoh/artisan.jpg', [new PharaohDiceSet(PharaohDiceType.Artisan)]));          // 34
    this.tiles.push(new PharaohTile(this.tiles.length, 'Soldier', 4, [0, 1], './assets/pharaoh/soldier.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 2)]));          // 35
    this.tiles.push(new PharaohTile(this.tiles.length, 'Noble Adoption', 4, [0, 1], './assets/pharaoh/noble-adoption.jpg', [new PharaohDiceSet(PharaohDiceType.Noble)]));          // 36
    this.tiles.push(new PharaohTile(this.tiles.length, 'Grain Merchant', 4, [2], './assets/pharaoh/grain-merchant.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.BonusScarabAfterRoll, 1, 1)]));          // 37
    this.tiles.push(new PharaohTile(this.tiles.length, 'Entertainer', 4, [2], './assets/pharaoh/entertainer.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.FlipDice, 1, 100)]));          // 38
    this.tiles.push(new PharaohTile(this.tiles.length, 'Matchmaker', 4, [2], './assets/pharaoh/matchmaker.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.AnyDiceLocked, 1, 1)]));          // 39
    this.tiles.push(new PharaohTile(this.tiles.length, 'Palace Key', 4, [3], './assets/pharaoh/palace-key.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 2, -1, false, false, false, false,
      false)], 0, 2));          // 40
    this.tiles.push(new PharaohTile(this.tiles.length, 'Spirit of the Dead', 4, [3], './assets/pharaoh/spirit-of-the-dead.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 6, false, true)], 0, 2));          // 41
    this.tiles.push(new PharaohTile(this.tiles.length, 'Good Omen', 4, [3], './assets/pharaoh/good-omen.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.BonusTurn, 0, 0, true)]));          // 42

    this.tiles.push(new PharaohTile(this.tiles.length, 'Worker', 3, [0, 1], './assets/pharaoh/worker.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 0)]));          // 43
    this.tiles.push(new PharaohTile(this.tiles.length, 'Indentured Worker', 3, [0, 1], './assets/pharaoh/indentured-worker.jpg', [new PharaohDiceSet(PharaohDiceType.White)], 0, 1));          // 44
    this.tiles.push(new PharaohTile(this.tiles.length, 'Guard', 3, [0, 1], './assets/pharaoh/guard.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, 1)]));          // 45
    this.tiles.push(new PharaohTile(this.tiles.length, 'Serf', 3, [0, 1], './assets/pharaoh/serf.jpg', [new PharaohDiceSet(PharaohDiceType.Serf)]));          // 46
    this.tiles.push(new PharaohTile(this.tiles.length, 'Farmer', 3, [0, 1], './assets/pharaoh/farmer.jpg', [new PharaohDiceSet(PharaohDiceType.Red)]));          // 47
    this.tiles.push(new PharaohTile(this.tiles.length, 'Soothsayer', 3, [2], './assets/pharaoh/soothsayer.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.MovePips, 2, 2)]));          // 48
    this.tiles.push(new PharaohTile(this.tiles.length, 'Beggar', 3, [2], './assets/pharaoh/beggar.jpg', [], 1, 1));          // 49
    this.tiles.push(new PharaohTile(this.tiles.length, 'Servant', 3, [2], './assets/pharaoh/servant.jpg', [], 0, 1, [new PharaohAction(PharaohTileAction.AddPips, 1, 3, false, 0, false, false, 1)]));          // 50
    this.tiles.push(new PharaohTile(this.tiles.length, 'Ankh', 3, [3], './assets/pharaoh/ankh.jpg', [], 0, 2, [new PharaohAction(PharaohTileAction.DoubleScarabs, 0, 0)]));          // 51
    this.tiles.push(new PharaohTile(this.tiles.length, 'Ancestral Guidance', 3, [3], './assets/pharaoh/ancestral-guidance.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, -1,
      false, false, false, false, false)], 0, 2, [new PharaohAction(PharaohTileAction.BonusScarabs, 2, 2)]));          // 52
    this.tiles.push(new PharaohTile(this.tiles.length, 'Omen', 3, [3], './assets/pharaoh/omen.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, -1)], 0, 2,
      [new PharaohAction(PharaohTileAction.BonusTurn, 0, 0, true), new PharaohAction(PharaohTileAction.DicePenalty, 1, 1, true)]));          // 53

    this.tiles.push(new PharaohTile(this.tiles.length, 'Herder', 0, [], './assets/pharaoh/herder.jpg', [new PharaohDiceSet(PharaohDiceType.Red, false, 1, -1, false, false, true)]));          // 54

    this.tiles.push(new PharaohTile(this.tiles.length, 'Random', 7, [1, 2, 3], './assets/pharaoh/random.jpg', []));
    this.tiles.push(new PharaohTile(this.tiles.length, 'Random', 6, [0, 1, 2, 3], './assets/pharaoh/random.jpg', []));
    this.tiles.push(new PharaohTile(this.tiles.length, 'Random', 5, [0, 1, 2, 3], './assets/pharaoh/random.jpg', []));
    this.tiles.push(new PharaohTile(this.tiles.length, 'Random', 4, [0, 1, 2, 3], './assets/pharaoh/random.jpg', []));
    this.tiles.push(new PharaohTile(this.tiles.length, 'Random', 3, [0, 1, 2, 3], './assets/pharaoh/random.jpg', []));

    this.tiles.push(new PharaohTile(this.tiles.length, 'Start', 0, [], './assets/pharaoh/start.jpg', [new PharaohDiceSet(PharaohDiceType.Red, true, 3)]));
  }
}
