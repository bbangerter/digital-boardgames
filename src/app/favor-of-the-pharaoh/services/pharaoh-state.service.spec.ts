import { TestBed } from '@angular/core/testing';

import { PharaohStateService } from './pharaoh-state.service';

describe('PharaohStateService', () => {
  let service: PharaohStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PharaohStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
