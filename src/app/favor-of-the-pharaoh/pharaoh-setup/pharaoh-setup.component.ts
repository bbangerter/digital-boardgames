import { Component, OnInit, Input } from '@angular/core';
import { PharaohStateService } from '../services/pharaoh-state.service';
import { PharaohTile } from '../dtos/pharaoh-dtos.model';
import { GameStart } from '../../core';
import { PharaohMessageType } from '../pharaoh-constants';

@Component({
  selector: 'pharaoh-setup',
  templateUrl: './pharaoh-setup.component.html',
  styleUrls: [
    './pharaoh-setup.component.scss',
    './../pharaoh.scss'
  ]
})
export class PharaohSetupComponent {
  @Input() gameData: GameStart;

  private tiers: Array<number> = [7, 6, 5, 4, 3];
  private columns: Array<number> = [0, 1, 2, 3];

  constructor(private gameState: PharaohStateService) { }

  public getTiers(): Array<number> {
    return this.tiers;
  }

  public getColumns(): Array<number> {
    return this.columns;
  }

  public allRandom() {
    this.setSide(7, 2);
    this.setSide(6, 2);
    this.setSide(5, 2);
    this.setSide(4, 2);
    this.setSide(3, 2);

    this.setTile(7, 1, 55);
    this.setTile(7, 2, 55);
    this.setTile(7, 3, 55);
    this.setTile(6, 0, 56);
    this.setTile(6, 1, 56);
    this.setTile(6, 2, 56);
    this.setTile(6, 3, 56);
    this.setTile(5, 0, 57);
    this.setTile(5, 1, 57);
    this.setTile(5, 2, 57);
    this.setTile(5, 3, 57);
    this.setTile(4, 0, 58);
    this.setTile(4, 1, 58);
    this.setTile(4, 2, 58);
    this.setTile(4, 3, 58);
    this.setTile(3, 0, 59);
    this.setTile(3, 1, 59);
    this.setTile(3, 2, 59);
    this.setTile(3, 3, 59);
  }

  public getSettingsSide(tier: number): string {
    return this.gameState.settings.tierSides.get(tier).toString();
  }

  public getSettingsRequirement(tier: number, column: number): string {
    return this.gameState.getRequirement(tier, column);
  }

  public getSettingsTile(tier: number, column: number): string {
    return this.gameState.settings.tiles.get(tier * 10 + column).toString();
  }

  public getTierTiles(tier: number, column: number): Array<PharaohTile> {
    return this.gameState.getTiles(tier, column);
  }

  public sideChange(side: any, tier: number) {
    var value = parseInt(side.value);
    this.setSide(tier, value);
  }

  private setSide(tier: number, value: number) {
    this.gameState.settings.tierSides.set(tier, value);
    this.gameState.updateSideSettings(tier, value);
  }

  public tileChange(tile: any, tier: number, column: number) {
    var value = parseInt(tile.value);
    this.setTile(tier, column, value);
  }

  private setTile(tier: number, column: number, value: number) {
    var previous = this.gameState.settings.tiles.get(tier * 10 + column);
    this.gameState.settings.tiles.set(tier * 10 + column, value);

    if (value < 54) {
      if (column == 0) {
        if (this.gameState.settings.tiles.get(tier * 10 + 1) == value) {
          this.gameState.settings.tiles.set(tier * 10 + 1, previous);
          this.gameState.updateTileSettings(tier, 1, previous);
        }
      }
      else if (column == 1) {
        if (this.gameState.settings.tiles.get(tier * 10) == value) {
          this.gameState.settings.tiles.set(tier * 10, previous);
          this.gameState.updateTileSettings(tier, 0, previous);
        }
      }
    }

    this.gameState.updateTileSettings(tier, column, value);
  }

  public getTileImage(tier: number, column: number): string {
    var tile = this.gameState.settings.tiles.get(tier * 10 + column);
    return this.gameState.getTile(tile).image;
  }

  public isGameCreator(): boolean {
    return this.gameState.isGameCreator();
  }

  public gameCanStart(): boolean {
    for (var i = 0; i < this.gameData.playerIds.length; i++) {
      if (!this.gameState.activePlayers.some(p => p.id == this.gameData.playerIds[i]))
        return false;
    }

    return true;
  }

  public startRound() {
    this.gameState.startGame();
  }
}
