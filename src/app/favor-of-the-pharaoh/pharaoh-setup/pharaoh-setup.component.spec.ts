import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PharaohSetupComponent } from './pharaoh-setup.component';

describe('PharaohSetupComponent', () => {
  let component: PharaohSetupComponent;
  let fixture: ComponentFixture<PharaohSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PharaohSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PharaohSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
