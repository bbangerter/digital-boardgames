import { Component, AfterViewInit } from '@angular/core';
import { PharaohStateService } from '../services/pharaoh-state.service';
import { CanvasStateService } from '../../core';

@Component({
  selector: 'pharaoh-play',
  templateUrl: './pharaoh-play.component.html',
  styleUrls: [
    './pharaoh-play.component.scss',
    './../pharaoh.scss'
  ]
})
export class PharaohPlayComponent implements AfterViewInit {
  constructor(private gameState: PharaohStateService, private canvas: CanvasStateService) { }

  ngAfterViewInit(): void {
    this.canvas.createTexture(this.gameState.canvasId, './assets/pharaoh/background.jpg', 0);
    this.gameState.setupBoard();
  }

  public pharaohCanvasId(): string {
    return this.gameState.canvasId;
  }

  public firstPlayerName(): string {
    if (this.gameState.firstPlayer != null) {
      return this.gameState.activePlayers.find(p => p.id == this.gameState.firstPlayer).name;
    }

    return '';
  }

  public currentPlayerName(): string {
    if (this.gameState.currentPlayer != null) {
      return this.gameState.activePlayers.find(p => p.id == this.gameState.currentPlayer).name;
    }

    return '';
  }

  public lastClaimedTile(): string {
    // Skip past starting player tiles to show the last tile claimed by a player
    if (this.gameState.playerTiles != null && this.gameState.playerTiles.length > this.gameState.activePlayers.length) {
      var tile = this.gameState.playerTiles[this.gameState.playerTiles.length - 1];

      return `${this.gameState.activePlayers.find(p => p.id == tile.playerId).name} claimed ${this.gameState.getTile(tile.tileId).name}`;
    }

    return '';
  }
}
