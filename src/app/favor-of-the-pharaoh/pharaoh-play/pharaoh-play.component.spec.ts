import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PharaohPlayComponent } from './pharaoh-play.component';

describe('PharaohPlayComponent', () => {
  let component: PharaohPlayComponent;
  let fixture: ComponentFixture<PharaohPlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PharaohPlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PharaohPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
