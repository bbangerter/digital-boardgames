import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';

import { PharaohGameControllerComponent } from './pharaoh-game-controller/pharaoh-game-controller.component';
import { PharaohSetupComponent } from './pharaoh-setup/pharaoh-setup.component';
import { PharaohPlayComponent } from './pharaoh-play/pharaoh-play.component';

@NgModule({
  declarations: [
    PharaohGameControllerComponent,
    PharaohSetupComponent,
    PharaohPlayComponent
  ],
  exports: [
    PharaohGameControllerComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatTableModule,
    MatRadioModule,
    MatSelectModule,
    MatCardModule,
    FormsModule,
    CoreModule
  ]
})
export class FavorOfThePharaohModule { }
