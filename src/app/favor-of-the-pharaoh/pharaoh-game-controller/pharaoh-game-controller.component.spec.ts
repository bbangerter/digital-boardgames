import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PharaohGameControllerComponent } from './pharaoh-game-controller.component';

describe('PharaohGameControllerComponent', () => {
  let component: PharaohGameControllerComponent;
  let fixture: ComponentFixture<PharaohGameControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PharaohGameControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PharaohGameControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
