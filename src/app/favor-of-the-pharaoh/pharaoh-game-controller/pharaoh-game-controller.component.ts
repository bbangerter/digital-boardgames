import { Component, OnInit, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { GameComponent, GameStart, PlayerStateService, CanvasStateService } from '../../core';
import { PharaohStateService } from '../services/pharaoh-state.service';

@Component({
  selector: 'pharaoh-game-controller',
  templateUrl: './pharaoh-game-controller.component.html',
  styleUrls: [
    './pharaoh-game-controller.component.scss',
    './../pharaoh.scss'
  ]
})
export class PharaohGameControllerComponent implements OnInit, OnDestroy, GameComponent {
  @Input() gameData: GameStart;

  constructor(private gameState: PharaohStateService, private playerState: PlayerStateService, private canvas: CanvasStateService) { }

  ngOnInit(): void {
    this.gameState.startGameHub(this.playerState.getPlayerName(), this.gameData.gameId, this.playerState.getPlayerId());
    this.gameState.creatorId = this.gameData.creatorId;
  }

  ngOnDestroy(): void {
    this.gameState.closeGameHub();
  }

  public getCurrentPhase(): number {
    return this.gameState.currentPhase;
  }
}
