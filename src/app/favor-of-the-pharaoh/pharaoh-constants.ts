export enum PharaohMessageType {
  UpdatePlayerList = 0,
  UpdateSideSettings = 1,
  UpdateTileSettings = 2,
  StartGame = 3,
  FirstPlayer = 4,
  ScarabChange = 5,
  DiceRoll = 6,
  LockDie = 7,
  ClaimTile = 8,
  IncreasePip = 9,
  RerollDie = 10,
  AddActiveDie = 11,
  TakeScarabs = 12,
  ActivateTile = 13,
  SwapToRed = 14,
  SwapToWhite = 15,
  EndTurn = 16
}

export enum PharaohPhase {
  Setup = 0,
  Play = 1
}

export enum PharaohRequirementType {
  MatchingNumbers = 0,
  Straight = 1,
  LessThanEqual = 2,
  GreaterThanEqual = 3,
  Sum = 4,
  Odd = 5,
  Even = 6,
  Unique = 7
}

export enum PharaohSides {
  A = 0,
  B = 1,
  Random = 2
}

export enum PharaohScarabType {
  Pip = 0,
  Reroll = 1
}

export enum PharaohDiceType {
  Red = 0,
  White = 1,
  Artisan = 2,
  Conspirator = 3,
  Vizier = 4,
  Noble = 5,
  Serf = 6,
  Captain = 7
}

export enum PharaohTileAction {
  BonusScarabs = 0,
  DoubleScarabs = 1,
  DicePenalty = 2,
  FlipDice = 3,
  BonusTurn = 4,
  BonusScarabAfterRoll = 5,
  WhiteDiceWild = 6,
  AddPips = 7,
  AnyDiceWild = 8,
  AnyDiceLocked = 9,
  ClaimTile = 10,
  AnyButRedWild = 11,
  StartFinalRoll = 12,
  SwapForRed = 13,
  MovePips = 14,
  SplitDice = 15,
  SplitLocked = 16
}
