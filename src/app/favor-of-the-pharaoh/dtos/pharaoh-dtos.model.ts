import { PharaohRequirementType, PharaohScarabType, PharaohDiceType, PharaohTileAction } from '../pharaoh-constants';
import { Subscription } from 'rxjs';
import { GfxTextureAtlas, GfxObject } from '../../core';

export class PharaohRequirement {
  constructor(public requirement: PharaohRequirementType, public count: number, public exact: number = -1) { }
}

export class PharaohSide {
  constructor(public index: number, public tier: number, public side: number, public column: number, public description: string, public requirements: Array<PharaohRequirement>, public scarabs: number = 0) { }
}

export class PharaohDiceSet {
  constructor(public diceType: PharaohDiceType, public mustRoll: boolean = true, public amount: number = 1, public startValue: number = 0, public incrementing: boolean = false,
    public addAfterAllLocked: boolean = false, public addAfterPairLocked: boolean = false, public onFinalRollOff: boolean = false, public onlyAfterRoll: boolean = true) { }
}

export class PharaohAction {
  constructor(public action: PharaohTileAction, public min: number, public max: number, public whenClaimed: boolean = false, public claimTier: number = 0, public onFinalRoll: boolean = false,
    public penaltyToSelf: boolean = true, public diceForAddPips: number = 100) { }
}

export class PharaohTile {
  constructor(public tileId: number, public name: string, public tier: number, public columns: Array<number>, public image: string, public dice: Array<PharaohDiceSet>,
    public scarabsPerTurn: number = 0, public scarabsWhenClaimed: number = 0, public action: Array<PharaohAction> = [], public inColumn: number = -1, public gfxId: number = -1) { }
}

export class PharaohSideSetting {
  constructor(public tier: number, public side: number) { }
}

export class PharaohTileSetting {
  constructor(public tier: number, public column: number, public tileId: number) { }
}

export class PharaohScarabScores {
  constructor(public playerId: string, public pipId: number, public rerollId: number, public pipSubscription: Subscription, public rerollSubscription: Subscription) { }
}

export class PharaohScarabChange {
  constructor(public playerId: string, public scarabType: PharaohScarabType, public change: number) { }
}

export class PharaohPlayerTile {
  constructor(public playerId: string, public tileId: number, public used: boolean = false, public gfxId: number = -1, public dieId: number = -1) { }
}

export class PharaohDice {
  constructor(public gfx: GfxTextureAtlas, public type: PharaohDiceType, public highlightGfx: GfxObject, public value: number = -1, public pyramidSlot: number = -1,
    public lockedOnRoll: number = -1, public pipsAdded: boolean = false, public rolledValue: number = -1) { }
}

export class PharaohDiceRoll {
  constructor(public index: number, public value: number) { }
}

export class PharaohLockDie {
  constructor(public index: number, public pyramid: number) { }
}

export class PharaohClaimTile {
  constructor(public playerId: string, public tileId: number, public endTurn: boolean = true) { }
}

export class PharaohAddDie {
  constructor(public type: PharaohDiceType, public value: number) { }
}

export class PharaohSettings {
  tierSides: Map<number, number>;
  tiles: Map<number, number>;

  constructor() {
    this.tierSides = new Map<number, number>();
    this.tierSides.set(7, 0);
    this.tierSides.set(6, 0);
    this.tierSides.set(5, 0);
    this.tierSides.set(4, 0);
    this.tierSides.set(3, 0);

    this.tiles = new Map<number, number>();
    this.tiles.set(70, 0);
    this.tiles.set(71, 1);
    this.tiles.set(72, 4);
    this.tiles.set(73, 7);
    this.tiles.set(60, 10);
    this.tiles.set(61, 11);
    this.tiles.set(62, 15);
    this.tiles.set(63, 18);
    this.tiles.set(50, 21);
    this.tiles.set(51, 22);
    this.tiles.set(52, 26);
    this.tiles.set(53, 29);
    this.tiles.set(40, 32);
    this.tiles.set(41, 33);
    this.tiles.set(42, 37);
    this.tiles.set(43, 40);
    this.tiles.set(30, 43);
    this.tiles.set(31, 44);
    this.tiles.set(32, 48);
    this.tiles.set(33, 51);
  }
}
