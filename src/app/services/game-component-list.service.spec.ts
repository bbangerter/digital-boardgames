import { TestBed } from '@angular/core/testing';

import { GameComponentListService } from './game-component-list.service';

describe('GameComponentListService', () => {
  let service: GameComponentListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GameComponentListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
