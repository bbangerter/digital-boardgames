import { Injectable, Type } from '@angular/core';
import { GameComponent } from '../core';
import { OneNightGameControllerComponent } from '../onenight/one-night-game-controller/one-night-game-controller.component';
import { SplendorGameControllerComponent } from '../splendor/splendor-game-controller/splendor-game-controller.component';
import { PlaygroundGameControllerComponent } from '../playground/playground-game-controller/playground-game-controller.component';
import { TerraformingMarsGameControllerComponent } from '../terraforming-mars/terraforming-mars-game-controller/terraforming-mars-game-controller.component';
import { MysteriumGameControllerComponent } from '../mysterium/mysterium-game-controller/mysterium-game-controller.component';
import { PharaohGameControllerComponent } from '../favor-of-the-pharaoh/pharaoh-game-controller/pharaoh-game-controller.component';

@Injectable({
  providedIn: 'root'
})
export class GameComponentListService {
  private gameList: Map<string, Type<GameComponent>>;

  constructor() {
    this.gameList = new Map<string, Type<GameComponent>>();
    this.gameList.set('One Night Ultimate Werewolf', OneNightGameControllerComponent);
    this.gameList.set('Splendor', SplendorGameControllerComponent);
    this.gameList.set('Playground', PlaygroundGameControllerComponent);
    this.gameList.set('Terraforming Mars', TerraformingMarsGameControllerComponent);
    this.gameList.set('Mysterium', MysteriumGameControllerComponent);
    this.gameList.set('Favor of the Pharaoh', PharaohGameControllerComponent);
  }

  public getGameComponent(name: string): Type<GameComponent> {
    return this.gameList.get(name);
  }
}
