import { Component } from '@angular/core';
import { PlayerStateService, GameStart } from '../core';

@Component({
  selector: 'app-master-control',
  templateUrl: './master-control.component.html',
  styleUrls: ['./master-control.component.scss']
})
export class MasterControlComponent {

  constructor(private playerState: PlayerStateService) { }

  public getPlayerName(): string {
    return this.playerState.getPlayerName();
  }

  public inGame(): boolean {
    return this.playerState.getActiveGameData() != undefined;
  }

  public getGameData(): GameStart {
    return this.playerState.getActiveGameData();
  }

  public validateLogin(name: string) {
    if (name.trim().length > 0) {
      this.playerState.setPlayerName(name.trim());
    }
  }
}
