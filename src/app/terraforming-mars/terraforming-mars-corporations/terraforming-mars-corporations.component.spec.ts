import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsCorporationsComponent } from './terraforming-mars-corporations.component';

describe('TerraformingMarsCorporationsComponent', () => {
  let component: TerraformingMarsCorporationsComponent;
  let fixture: ComponentFixture<TerraformingMarsCorporationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsCorporationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsCorporationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
