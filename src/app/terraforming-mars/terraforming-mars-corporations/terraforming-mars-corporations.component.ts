import { Component } from '@angular/core';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';
import { MarsCorp, MarsResourceCount } from '../dtos/terraforming-mars-dtos.model';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { TerraformingMarsDecksService } from '../services/terraforming-mars-decks.service';
import { PlayerStateService } from '../../core';
import { MarsTags, MarsResourceType } from '../terraforming-mars-constants';

@Component({
  selector: 'terraforming-mars-corporations',
  templateUrl: './terraforming-mars-corporations.component.html',
  styleUrls: [
    './terraforming-mars-corporations.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsCorporationsComponent {
  displayedColumns = ['Name', 'Production', 'Resources', 'Effect', 'Special', 'Tags'];
  playerCorps: Array<MarsCorp>;

  constructor(private marsPlayerState: TerraformingMarsPlayerStateService, private decks: TerraformingMarsDecksService, private gameState: TerraformingMarsStateService,
    private playerState: PlayerStateService) {
    var corpIds = this.marsPlayerState.getAllPlayerCorporations().map(c => c.cardId);
    this.playerCorps = this.decks.getCorporations(corpIds);
  }

  public getPlayerColorClass(corpId: number): string {
    var playerId = this.marsPlayerState.getAllPlayerCorporations().find(c => c.cardId == corpId).playerId;
    return this.gameState.getPlayerColorClass(playerId);
  }

  public getPlayerClass(corpId: number): string {
    var playerId = this.marsPlayerState.getAllPlayerCorporations().find(c => c.cardId == corpId).playerId;
    if (playerId == this.playerState.getPlayerId()) {
      return `player-highlight ${this.getPlayerColorClass(corpId)}`;
    }

    return this.getPlayerColorClass(corpId);
  }

  public getPlayerNameForCorp(corpId: number): string {
    var player = this.marsPlayerState.getAllPlayerCorporations().find(c => c.cardId == corpId).playerId;
    return this.gameState.getPlayerName(player);
  }

  public getStartingProduction(id: number): Array<MarsResourceCount> {
    return this.playerCorps.find(c => c.id == id).production;
  }

  public getResourceImage(resource: MarsResourceType): string {
    return this.gameState.getResourceImage(resource);
  }

  public getResourceExtra(resource: MarsResourceCount) {
    return this.gameState.getResourceExtra(resource);
  }

  public getStartingResources(id: number): Array<MarsResourceCount> {
    return this.playerCorps.find(c => c.id == id).resources;
  }

  public getCorpTags(id: number): Array<MarsTags> {
    return this.playerCorps.find(c => c.id == id).tags;
  }

  public getTagImage(tag: MarsTags): string {
    return this.gameState.getTagImage(tag);
  }
}
