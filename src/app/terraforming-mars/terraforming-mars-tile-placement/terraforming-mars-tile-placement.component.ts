import { Component, Input, EventEmitter, Output, OnChanges, SimpleChanges, AfterViewInit, OnDestroy } from '@angular/core';
import { TerraformingMarsBoardStateService } from '../services/terraforming-mars-board-state.service';
import { PlayerStateService, CanvasStateService, GfxClickData } from '../../core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { MarsTiles, MarsMessageType } from '../terraforming-mars-constants';
import { Subscription } from 'rxjs';
import { MarsPlayedTile } from '../dtos/terraforming-mars-dtos.model';

@Component({
  selector: 'terraforming-mars-tile-placement',
  templateUrl: './terraforming-mars-tile-placement.component.html',
  styleUrls: [
    './terraforming-mars-tile-placement.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsTilePlacementComponent implements OnChanges, AfterViewInit, OnDestroy {
  @Input() tilePlacement: Array<MarsTiles>;
  @Output() tilePlaced = new EventEmitter<any>();

  private gfxClickListener: Subscription;
  constructor(private marsBoardState: TerraformingMarsBoardStateService, private playerState: PlayerStateService, private gameState: TerraformingMarsStateService,
    private canvas: CanvasStateService) { }

  ngOnDestroy(): void {
    this.gfxClickListener.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.gfxClickListener = this.canvas.subscribeClickEvent(this.marsBoardState.canvasId, (data: GfxClickData) => this.onClick(data));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.tilePlacement.length > 0) {
      this.marsBoardState.highlightTileSelection(this.tilePlacement[0], this.playerState.getPlayerId(), this.gameState.getGeneration());
    }
  }

  public getTileType(): string {
    return this.gameState.getTilesString([this.tilePlacement[0]]);
  }

  private onClick(data: GfxClickData) {
    if (data != null && this.tilePlacement.length > 0) {
      var locationId = this.marsBoardState.getTileIdByGfxId(data.objectId, this.tilePlacement[0], this.playerState.getPlayerId(), this.gameState.getGeneration());
      if (locationId != -1) {
        this.gameState.sendGameMessage(MarsMessageType.PlaceTile, new MarsPlayedTile(this.playerState.getPlayerId(), locationId, this.tilePlacement[0]));
        this.marsBoardState.hideTileSelection();

        var tileLocation = this.marsBoardState.tileLocations.find(l => l.id == locationId);
        var tile = tileLocation.tile;
        if (this.tilePlacement.length > 1) {
          tile = this.tilePlacement[1];
        }

        if (tile != MarsTiles.None) {
          this.marsBoardState.highlightTileSelection(tile, this.playerState.getPlayerId());
        }
        this.tilePlaced.emit(locationId);
      }
    }
  }
}
