import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsTilePlacementComponent } from './terraforming-mars-tile-placement.component';

describe('TerraformingMarsTilePlacementComponent', () => {
  let component: TerraformingMarsTilePlacementComponent;
  let fixture: ComponentFixture<TerraformingMarsTilePlacementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsTilePlacementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsTilePlacementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
