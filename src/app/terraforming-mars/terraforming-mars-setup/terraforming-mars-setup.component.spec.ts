import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsSetupComponent } from './terraforming-mars-setup.component';

describe('TerraformingMarsSetupComponent', () => {
  let component: TerraformingMarsSetupComponent;
  let fixture: ComponentFixture<TerraformingMarsSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
