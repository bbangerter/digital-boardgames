import { Component, Input } from '@angular/core';
import { GameStart } from '../../core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { MarsExpansion, MarsMap } from '../terraforming-mars-constants';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'terraforming-mars-setup',
  templateUrl: './terraforming-mars-setup.component.html',
  styleUrls: [
    './terraforming-mars-setup.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsSetupComponent {
  @Input() gameData: GameStart;

  map: string = '3';

  private checked: Map<string, boolean>;
  constructor(private gameState: TerraformingMarsStateService) {
    this.checked = new Map<string, boolean>();

    var keys = Object.keys(MarsExpansion);
    keys.shift(); // remove base
    keys.forEach((k) => {
      this.checked.set(k, true);
    })
  }

  public getExpansionState(expansion: string) {
    return this.checked.get(expansion);
  }

  public setExpansionState(expansion: string, event: MatCheckboxChange) {
    this.checked.set(expansion, event.checked);
  }

  public expansions(): Array<MarsExpansion> {
    return [MarsExpansion.Prelude, MarsExpansion.Promos];
  }

  public gameCanStart(): boolean {
    for (var i = 0; i < this.gameData.playerIds.length; i++) {
      if (!this.gameState.getActivePlayers().some(p => p.id == this.gameData.playerIds[i]))
        return false;
    }

    return true;
  }

  public startGame() {
    var expansions = new Array<MarsExpansion>();
    expansions.push(MarsExpansion.Base);
    this.checked.forEach((value, key) => {
      if (value) {
        expansions.push(MarsExpansion[key]);
      }
    });
    var selectedMap = parseInt(this.map);
    if (selectedMap == 3) {
      selectedMap = Math.floor(Math.random() * 3);
    }
    this.gameState.startGame(expansions, selectedMap);
  }

  public isGameCreator(): boolean {
    return this.gameState.isGameCreator();
  }
}
