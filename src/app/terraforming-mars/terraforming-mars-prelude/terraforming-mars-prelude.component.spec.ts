import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsPreludeComponent } from './terraforming-mars-prelude.component';

describe('TerraformingMarsPreludeComponent', () => {
  let component: TerraformingMarsPreludeComponent;
  let fixture: ComponentFixture<TerraformingMarsPreludeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsPreludeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsPreludeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
