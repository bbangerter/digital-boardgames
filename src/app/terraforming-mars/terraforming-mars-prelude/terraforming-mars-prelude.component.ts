import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MarsPlayerCard, MarsResourceCount } from '../dtos/terraforming-mars-dtos.model';
import { PlayerStateService } from '../../core';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { TerraformingMarsDecksService } from '../services/terraforming-mars-decks.service';
import { MarsMessageType, MarsPrereq, MarsTags, MarsResourceType } from '../terraforming-mars-constants';

@Component({
  selector: 'terraforming-mars-prelude',
  templateUrl: './terraforming-mars-prelude.component.html',
  styleUrls: [
    './terraforming-mars-prelude.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsPreludeComponent {
  @Input() keep: number;
  @Input() dealt: boolean;
  @Input() action: boolean;
  @Output() preludeSelected = new EventEmitter<number>();

  displayedColumns = ['Name', 'Production', 'Resources', 'Tiles', 'Description', 'Tags', 'Select'];

  constructor(private playerState: PlayerStateService, private marsPlayerState: TerraformingMarsPlayerStateService, private gameState: TerraformingMarsStateService,
    private decks: TerraformingMarsDecksService) { }

  public getPlayerPreludes(): Array<MarsPlayerCard> {
    if (this.dealt) {
      return this.marsPlayerState.getPlayerDealtPreludes().filter(p => p.playerId == this.playerState.getPlayerId());
    }
    else {
      return this.marsPlayerState.getPlayerPreludePlayed().filter(p => !this.action || p.playerId == this.playerState.getPlayerId());
    }
  }

  public getPlayerColorClass(preludeId: number): string {
    var player = this.marsPlayerState.getPreludeOwner(preludeId);
    return this.gameState.getPlayerColorClass(player);
  }

  public getName(preludeId: number): string {
    return this.decks.getPrelude(preludeId).name;
  }

  public getProduction(preludeId: number): Array<MarsResourceCount> {
    return this.decks.getPrelude(preludeId).production;
  }

  public getResourceImage(resource: MarsResourceType): string {
    return this.gameState.getResourceImage(resource);
  }

  public getResourceExtra(resource: MarsResourceCount) {
    return this.gameState.getResourceExtra(resource);
  }

  public getResources(preludeId: number): Array<MarsResourceCount> {
    return this.decks.getPrelude(preludeId).resources;
  }

  public getTiles(preludeId: number): string {
    var tiles = this.decks.getPrelude(preludeId).tiles;
    if (tiles.length > 0) {
      return this.gameState.getTilesString(tiles);
    }

    return '';
  }

  public getTags(preludeId: number): Array<MarsTags> {
    return this.decks.getPrelude(preludeId).tags;
  }

  public getTagImage(tag: MarsTags): string {
    return this.gameState.getTagImage(tag);
  }

  public getDescription(preludeId: number): string {
    var prelude = this.decks.getPrelude(preludeId);
    if (prelude.playACard) {
      if (prelude.cardAdaptation) {
        return 'Play a card ignoring global requirements';
      }
      else if (prelude.cardDiscount) {
        return `Play a card, it costs ${prelude.cardDiscount} megacredits less.`;
      }
    }

    return '';
  }

  public getOwner(preludeId: number): string {
    if (preludeId < 0) {
      return this.playerState.getPlayerName();
    }
    var player = this.marsPlayerState.getPreludeOwner(preludeId);
    return this.gameState.getPlayerName(player);
  }

  public showButton(preludeId: number): boolean {
    if (this.gameState.getPendingTiles().length > 0 || this.gameState.getPlayingPreludeProject() != null) {
      return false;
    }

    if (!this.action) {
      return this.getPlayerPreludes().length > this.keep && this.dealt;
    }

    if (this.dealt) {
      return true;
    }

    if (this.gameState.getCurrentPlayer() != this.playerState.getPlayerId()) {
      return false;
    }

    var card = this.decks.getPrelude(preludeId);
    return !card.played && this.gameState.playerCanTakeActions();
  }

  public selectPrelude(preludeId: number) {
    if (this.action) {
      this.preludeSelected.emit(preludeId);
      this.decks.getPrelude(preludeId).played = true;
    }
    else {
      this.gameState.sendGameMessage(MarsMessageType.DiscardPrelude, preludeId);
    }
  }

  public getSelectLabel(): string {
    if (this.action) {
      return 'Play';
    }

    return 'Discard';
  }
}
