import { MarsResourceType, MarsTags, MarsExpansion, MarsTiles, MarsPrereq, MarsMap } from '../terraforming-mars-constants';
import { GfxRectangle, GfxLines } from '../../core';

export class MarsPlayerColor {
  constructor(public playerId: string, public color: string) { }
}

export class MarsPlayerTerraformingMarker {
  constructor(public playerId: string, public gfx: GfxRectangle) { }
}

export class MarsExpansionSet {
  constructor(public expansion: MarsExpansion) { }
}

export class MarsPrereqCondition {
  // tag: if type is tag, the list of tags needed
  // production: if type is production, the type of production needed
  constructor(public type: MarsPrereq, public amount: number, public max: boolean = false, public tag: Array<MarsTags> = [], public production: Array<MarsResourceType> = [],
    public tile: MarsTiles = MarsTiles.None, public owned: boolean = false, public or: boolean = false) { }
}

export class MarsResourceTarget {
  constructor(public resource: MarsResourceCount, public targetId: string, public projectId: number) { }
}

export class MarsTargetType {
  constructor(public playerId: string, public projectId: number, public projectIndex: number, public resourceIndex: number, public robotic: boolean = false,
    public resource: MarsResourceType = MarsResourceType.None) { }
}

export class MarsResourceCount {
  constructor(public resource: MarsResourceType, public amount: number, public anyPlayer: boolean = false, public anyCard: boolean = false, public or: Array<MarsResourceCount> = [],
    public condition: MarsPrereqCondition = null, public perTag: boolean = false, public tag: MarsTags = MarsTags.None, public perTile: boolean = false,
    public tile: MarsTiles = MarsTiles.None, public onlyOpponents: boolean = false, public onMars: boolean = true, public mustPurchase: boolean = false, 
    public keepProject: number = 100, public tokenScoreTrackId: string = null, public protectedAnimals: boolean = false, public adjacentPlayers: boolean = false,
    public targetPlayedCard: boolean = false, public steal: boolean = false) { }
}

export class MarsCorp extends MarsExpansionSet {
  constructor(public id: number, public name: string, public production: Array<MarsResourceCount>, public resources: Array<MarsResourceCount>, public tags: Array<MarsTags>,
    public effectText: string, public specialText: string, public action: MarsAction, public reaction: Array<MarsReaction>, public lastCorpAction: number = -1, public expansion: MarsExpansion = MarsExpansion.Base) {
    super(expansion);
  }
}

export class MarsPrelude extends MarsExpansionSet {
  constructor(public id: number, public name: string, public production: Array<MarsResourceCount>, public resources: Array<MarsResourceCount> = [], public tiles: Array<MarsTiles> = [],
    public tags: Array<MarsTags> = [], public playACard: boolean = false, public cardDiscount: number = 0, public cardAdaptation: boolean = false, public played: boolean = true,
    public expansion: MarsExpansion = MarsExpansion.Prelude) {
    super(expansion);
  }
}

export class MarsPlayerCard {
  constructor(public playerId: string, public cardId: number) { }
}

export class MarsScore {
  constructor(public score: number, public tokenType: MarsResourceType = MarsResourceType.None, public perTile: boolean = false, public tileType: MarsTiles = MarsTiles.None,
    public adjacentTile: boolean = false, public perTag: boolean = false, public tagType: MarsTags = MarsTags.None, public maxScore: number = 1000) { }
}

export class MarsProject extends MarsExpansionSet {
  constructor(public id: number, public name: string, public description: string, public cost: number, public production: Array<MarsResourceCount>, public resources: Array<MarsResourceCount>,
    public tags: Array<MarsTags>, public tile: Array<MarsTiles> = [], public prereqs: Array<MarsPrereqCondition> = [], public score: MarsScore = null,
    public specialDesign: boolean = false, public indenturedWorkers: boolean = false, expansion: MarsExpansion = MarsExpansion.Base) {
    super(expansion);
  }
}

export class MarsAction {
  constructor(public action: string, public productionCost: MarsResourceCount, public resourceCost: MarsResourceCount, public productionResult: MarsResourceCount,
    public resourceResult: Array<MarsResourceCount> = [], public tile: MarsTiles = MarsTiles.None, public firstTurn: boolean = false, public firstTurnUsed: boolean = false,
    public award: boolean = false, public allTurns: boolean = false) { }
}

export class MarsAlternateCredits {
  constructor(public resource: MarsResourceType, public amount: number, public scoreTrackId: string = null, public forTag: MarsTags = MarsTags.None) { }
}

export class MarsTagPlayed {
  constructor(public tag: Array<MarsTags>, public resource: Array<MarsResourceCount>, public production: Array<MarsResourceCount> = [], public additionalTag: MarsTags = MarsTags.None,
    public anyPlayer: boolean = false) { }
}

export class MarsTilePlayed {
  constructor(public tile: Array<MarsTiles>, public resource: MarsResourceCount, public production: MarsResourceCount = null, public onMars: boolean = false, public anyPlayer = false,
    public onPlacementBonus: Array<MarsResourceType> = []) { }
}

export class MarsReaction {
  constructor(public alternateCredits: MarsAlternateCredits, public discount: number = 0, public discountTag: MarsTags = MarsTags.None, public tagPlayed: MarsTagPlayed = null,
    public tilePlayed: MarsTilePlayed = null, public refund: number = 0, public adaptation: boolean = false, public protectedHabitat: boolean = false, public alloys: boolean = false,
    public discountedGreenery: boolean = false, public condition: MarsPrereqCondition = null) { }
}

export class MarsActionProject extends MarsProject {
  constructor(public id: number, public name: string, public description: string, public reaction: MarsReaction, public action: Array<MarsAction>, public cost: number,
    public production: Array<MarsResourceCount>, public resources: Array<MarsResourceCount>, public tags: Array<MarsTags>, public tile: Array<MarsTiles> = [],
    public prereqs: Array<MarsPrereqCondition> = [], public score: MarsScore = null,
    public expansion: MarsExpansion = MarsExpansion.Base, public triggerGeneration: number = -1) {
    super(id, name, description, cost, production, resources, tags, tile, prereqs, score, false, false, expansion);
  }
}

export class MarsPlayProject {
  constructor(public projectId: number, public megacredits: number, public steel: number, public titanium: number, public heat: number, public psychrophiles: number) { }
}

export class MarsTileLocation {
  constructor(public id: number, public gfx: GfxLines, public adjacentIds: Array<number>, public bonus: Array<MarsResourceCount> = [], public ocean: boolean = false, public noctis: boolean = false,
    public volcano: boolean = false, public ganymede: boolean = false, public phobos: boolean = false, public tile: MarsTiles = MarsTiles.None) { }
}

export class MarsPlayedTile {
  constructor(public playerId: string, public locationId: number, public tile: MarsTiles, public gfxId: number = -1) { }
}

export class MarsStandardProject {
  constructor(public id: number, public name: string, public cost: number, public production: MarsResourceCount, public resource: MarsResourceCount = null,
    public tile: MarsTiles = MarsTiles.None, public sellPatents: boolean = false, public gfxId: number = -1) { }
}

export class MarsMilestones {
  constructor(public id: number, public name: string, public condition: MarsPrereqCondition, public map: MarsMap = MarsMap.Base, public gfxId: number = -1) { }
}

export class MarsAwards {
  constructor(public id: number, public name: string, public condition: MarsPrereqCondition, public map: MarsMap = MarsMap.Base, public gfxId: number = -1) { }
}

export class MarsRank {
  constructor(public playerId: string, public count: number) { }
}

export class MarsAwardScore {
  constructor(public playerId: string, public score: number) { }
}

export class MarsFinalScore {
  constructor(public playerId: string, public score: number, public cardScore: number, public boardScore: number, public milestones: number, public awards: number) { }
}
