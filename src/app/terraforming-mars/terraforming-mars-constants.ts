export enum MarsMap {
  Base = 0,
  Hellas = 1,
  Elysium = 2
}

export enum MarsMessageType {
  UpdatePlayerList = 0,
  ShuffledCorps = 1,
  DiscardCorp = 2,
  ShuffledProjects = 3,
  DiscardProject = 4,
  PurchaseProjectCards = 5,
  FirstPlayer = 6,
  PlayProject = 7,
  ApplyProduction = 8,
  ApplyResources = 9,
  ApplyProductionTarget = 10,
  ApplyResourceTarget = 11,
  AdvanceTurn = 12,
  Pass = 13,
  PlaceTile = 14,
  RecordEvent = 15,
  PurchaseMidTurnCards = 16,
  ClaimMilestone = 17,
  ClaimAward = 18,
  SelectMidTurnCards = 19,
  ReshuffleGeneration = 20,
  Reshuffled = 21,
  ShuffledPrelude = 22,
  DiscardPrelude = 23,
  Expansions = 24,
  PlayPrelude = 25,
  Map = 26,
  TriggerProject = 27,
  TriggerCorp = 28
}

export enum MarsExpansion {
  Base = 'Base',
  Prelude = 'Prelude',
  Promos = 'Promos'
}

export enum MarsPhase {
  Setup = 0,
  DealStartingHand = 1,
  PlayPreludes = 2,
  PlayGeneration = 3,
  StartGeneration = 4,
  FinalScore = 5
}

export enum MarsResourceType {
  Megacredits = 0,
  Steel = 1,
  Titanium = 2,
  Plants = 3,
  Energy = 4,
  Heat = 5,
  Temperature = 6,
  Oxygen = 7,
  Project = 8,
  Terraforming = 9,
  AnimalToken = 10,
  MicrobeToken = 11,
  ScienceToken = 12,
  FleetToken = 13,
  RoboticWorkforce = 14,
  Prelude = 15,
  AnyBasic = 16,
  None = 100
}

export enum MarsTags {
  Earth = 0,
  Builder = 1,
  Science = 2,
  Jovian = 3,
  Space = 4,
  Energy = 5,
  Plant = 6,
  Microbe = 7,
  Animal = 8,
  City = 9,
  Event = 10,
  Wild = 11,
  None = 100
}

export enum MarsTiles {
  City = 0,
  Greenery = 1,
  Ocean = 2,
  Industrial = 3,
  Restricted = 4,
  Capital = 5,
  Mining = 6,
  AdjacentMining = 7,
  LandClaim = 8,
  NaturalPreserve = 9,
  EcologicalZone = 10,
  Commercial = 11,
  NuclearZone = 12,
  Mohole = 13,
  LavaFlows = 14,
  UrbanizedArea = 15,
  Noctis = 16,
  ProtectedValley = 17,
  ArtificialLake = 18,
  Ganymede = 19,
  Phobos = 20,
  Mangrove = 21,
  LavaTube = 22,
  ResearchOutpost = 23,
  CommunityArea = 24,
  Any = 99,
  None = 100
}

export enum MarsPrereq {
  Temperature = 0,
  Oxygen = 1,
  Ocean = 2,
  Tile = 3,
  Tags = 4,
  Production = 5,
  Token = 6,
  StandardProject = 7,
  ProjectCost = 8,
  Resource = 9,
  Landlord = 10,
  VictoryCard = 11,
  UniqueTags = 12,
  PrereqCards = 13,
  PolarTiles = 14,
  NonEventCards = 15,
  GreenCards = 16,
  EstateDealer = 17
}

export const MarsScoreTrack = {
  Megacredits: 'megacredits',
  Steel: 'steel',
  Titanium: 'titanium',
  Plants: 'plants',
  Energy: 'energy',
  Heat: 'heat',
  Terraforming: 'terraforming',
  Temperature: 'temperature',
  Oxygen: 'oxygen',
  Oceans: 'oceans',
  Cards: 'cards',
  Board: 'board',
  Awards: 'awards',
  Milestones: 'milestones'
}

export enum MarsCorporationProject {
  Recyclon = -3,
  Splice = -4
}
