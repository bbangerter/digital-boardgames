import { TestBed } from '@angular/core/testing';

import { TerraformingMarsPlayerStateService } from './terraforming-mars-player-state.service';

describe('TerraformingMarsPlayerStateService', () => {
  let service: TerraformingMarsPlayerStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TerraformingMarsPlayerStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
