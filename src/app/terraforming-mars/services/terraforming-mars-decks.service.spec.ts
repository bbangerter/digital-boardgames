import { TestBed } from '@angular/core/testing';

import { TerraformingMarsDecksService } from './terraforming-mars-decks.service';

describe('TerraformingMarsDecksService', () => {
  let service: TerraformingMarsDecksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TerraformingMarsDecksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
