import { TestBed } from '@angular/core/testing';

import { TerraformingMarsStateService } from './terraforming-mars-state.service';

describe('TerraformingMarsStateService', () => {
  let service: TerraformingMarsStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TerraformingMarsStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
