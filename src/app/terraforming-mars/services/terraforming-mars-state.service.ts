import { Injectable } from '@angular/core';
import { SignalRService, GamePlayer, GameMessage, PlayerStateService, PlayerScoreService } from '../../core';
import { Subscription } from 'rxjs';
import { MarsMessageType, MarsPhase, MarsResourceType, MarsTags, MarsExpansion, MarsTiles, MarsPrereq, MarsScoreTrack, MarsMap, MarsCorporationProject } from '../terraforming-mars-constants';
import { TerraformingMarsDecksService } from './terraforming-mars-decks.service';
import { MarsResourceCount, MarsPrereqCondition, MarsPlayProject, MarsResourceTarget, MarsPlayerColor, MarsPlayedTile, MarsActionProject, MarsPlayerCard, MarsProject, MarsPrelude } from '../dtos/terraforming-mars-dtos.model';
import { TerraformingMarsPlayerStateService } from './terraforming-mars-player-state.service';
import { TerraformingMarsBoardStateService } from './terraforming-mars-board-state.service';

@Injectable({
  providedIn: 'root'
})
export class TerraformingMarsStateService {
  private creatorId: string;
  private activePlayers: Array<GamePlayer> = [];
  private currentPhase: MarsPhase = MarsPhase.Setup;
  private generation: number;
  private firstPlayer: string;
  private currentPlayer: string;
  private recentEvents: Array<string> = [];
  private availableActions: number;
  private passedPlayers: Array<string> = [];
  private playerColors: Array<MarsPlayerColor>;
  private canTakeActions: boolean = true;
  private purchaseMidTurnCards: boolean = false;
  private selectMidTurnCards: number = 0;
  private unmiTerraformRating: number;
  private finalGeneration: boolean = false;
  private pendingProduction: Array<MarsResourceCount> = [];
  private pendingResources: Array<MarsResourceCount> = [];
  private pendingTiles: Array<MarsTiles> = [];
  private expansions: Array<MarsExpansion>;
  private map: MarsMap = MarsMap.Base;
  private playingPreludeProject: MarsPrelude = null;

  private gameSubscription: Subscription;
  private hubName: string = 'game';
  private playerReady: Array<string> = [];
  constructor(private signalR: SignalRService, private decks: TerraformingMarsDecksService, private playerState: PlayerStateService,
    private marsPlayerState: TerraformingMarsPlayerStateService, private score: PlayerScoreService, private marsBoardState: TerraformingMarsBoardStateService) {
  }

  public startGameHub(playerName: string, groupName: string, playerId: string) {
    this.signalR.connect(this.hubName, `PlayerName=${playerName}&GroupName=${groupName}&PlayerId=${playerId}`);
    this.gameSubscription = this.signalR.subscribeToMessage(this.hubName, (data) => this.onMessage(data));
  }

  public closeGameHub() {
    this.gameSubscription.unsubscribe();
    this.signalR.close(this.hubName);
  }

  public setCreatorId(id: string) {
    this.creatorId = id;
  }

  public getActivePlayers(): Array<GamePlayer> {
    return this.activePlayers;
  }

  public getCurrentPhase(): MarsPhase {
    return this.currentPhase;
  }

  public getGeneration(): number {
    return this.generation;
  }

  public getFirstPlayer(): string {
    return this.firstPlayer;
  }

  public getCurrentPlayer(): string {
    return this.currentPlayer;
  }

  public getRecentEvents(): Array<string> {
    return this.recentEvents;
  }

  public getAvailableActions(): number {
    return this.availableActions;
  }

  public getPlayerColors(): Array<MarsPlayerColor> {
    return this.playerColors;
  }

  public playerCanTakeActions(): boolean {
    return this.canTakeActions
  }

  public setCanTakeActions(can: boolean) {
    this.canTakeActions = can;
  }

  public getPurchaseMidTurnCards(): boolean {
    return this.purchaseMidTurnCards;
  }

  public setPurchaseMidTurnCards(set: boolean) {
    this.purchaseMidTurnCards = set;
  }

  public getSelectMidTurnCards(): number {
    return this.selectMidTurnCards
  }

  public setSelectMidTurnCards(set: number) {
    this.selectMidTurnCards = set;
  }

  public getUNMITerraformRating(): number {
    return this.unmiTerraformRating
  }

  public getFinalGeneration(): boolean {
    return this.finalGeneration;
  }

  public getPendingProduction(): Array<MarsResourceCount> {
    return this.pendingProduction;
  }

  public removePendingProduction() {
    this.pendingProduction.shift();
  }

  public addPendingProduction(add: MarsResourceCount) {
    this.pendingProduction.push(add);
  }

  public getPendingResources(): Array<MarsResourceCount> {
    return this.pendingResources;
  }

  public removePendingResource() {
    this.pendingResources.shift();
  }

  public addPendingResource(add: MarsResourceCount) {
    this.pendingResources.push(add);
  }

  public getPendingTiles(): Array<MarsTiles> {
    return this.pendingTiles;
  }

  public removePendingTile(): MarsTiles {
    return this.pendingTiles.shift();
  }

  public addPendingTile(add: MarsTiles) {
    this.pendingTiles.push(add);
  }

  public getMarsMap(): MarsMap {
    return this.map;
  }

  public getPlayingPreludeProject(): MarsPrelude {
    return this.playingPreludeProject
  }

  public setPlayingPreludeProject(card: MarsPrelude) {
    this.playingPreludeProject = card;
  }

  public hasFirstTurnAction(): boolean {
    // force first player action
    var corp = this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.action != null && corp.action.firstTurn && !corp.action.firstTurnUsed && this.generation == 1) {
      return true;
    }

    return false;
  }

  public roboticWorkforceTargets(): Array<MarsProject> {
    return this.marsPlayerState.getPlayerPlayedProjects(this.playerState.getPlayerId()).filter(p => p.production.length > 0 && !p.tags.some(t => t == MarsTags.Event) &&
      p.tags.some(t => t == MarsTags.Builder) && this.hasProduction(p));
  }

  public hasProduction(card: MarsProject): boolean {
    // if the card reduces the players production or resources, make sure they have enough
    return !card.production.some(p => p.amount < 0 && !p.anyPlayer && !this.score.canAdd(p.amount, this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(p.resource, false)));
  }

  public hasResource(card: MarsProject): boolean {
    return !card.resources.some(p => p.amount < 0 && !p.anyPlayer && !this.score.canAdd(p.amount, this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(p.resource, true)));
  }

  public getPlayerColorClass(player: string): string {
    var color = this.getPlayerColor(player);
    if (color == null) {
      return '';
    }
    switch (color) {
      case 'green':
        return 'player-color-green';
      case 'red':
        return 'player-color-red';
      case 'blue':
        return 'player-color-blue';
      case 'yellow':
        return 'player-color-yellow';
      default:
        return 'player-color-black';
    }
  }

  public getPlayerColor(playerId: string): string {
    var color = this.playerColors.find(p => p.playerId == playerId);
    return color.color;
  }

  private hasPreludesToPlay(): boolean {
    var preludes = this.marsPlayerState.getPlayerPreludePlayed().filter(p => p.playerId == this.currentPlayer);
    var morePreludes = false;
    preludes.forEach((p) => {
      var card = this.decks.getPrelude(p.cardId);
      if (!card.played) {
        morePreludes = true;
      }
    });

    return morePreludes;
  }

  public useAction() {
    if (this.currentPhase == MarsPhase.PlayPreludes) {
      if (!this.hasPreludesToPlay()) {
        this.sendGameMessage(MarsMessageType.AdvanceTurn, '');
      }
    }
    else {
      if (this.marsPlayerState.getPlayerDealtPreludes().length == 0) {
        this.availableActions--;
        if (this.availableActions <= 0) {
          this.sendGameMessage(MarsMessageType.AdvanceTurn, '');
        }
        else {
          this.showMapButtons();
        }
      }
    }
  }

  public allPlayersReady(): boolean {
    return this.playerReady.length == this.activePlayers.length;
  }

  public playerIsReady(id: string): boolean {
    return this.playerReady.some(p => p == id);
  }

  public isGameCreator(): boolean {
    return this.creatorId == this.playerState.getPlayerId();
  }

  public advancePhase() {
    this.currentPhase++;
    this.showMapButtons();
  }

  public usingExpansion(expansion: MarsExpansion): boolean {
    return this.expansions.some(e => e == expansion);
  }

  public startGame(expansions: Array<MarsExpansion>, map: MarsMap) {
    this.expansions = expansions;
    this.sendGameMessage(MarsMessageType.Map, map);
    this.sendGameMessage(MarsMessageType.Expansions, expansions);
    var index = Math.floor(Math.random() * this.activePlayers.length);
    this.sendGameMessage(MarsMessageType.FirstPlayer, this.activePlayers[index].id);
    var corps = this.decks.shuffleCorporations(expansions);
    this.sendGameMessage(MarsMessageType.ShuffledCorps, corps);
    if (this.usingExpansion(MarsExpansion.Prelude)) {
      var prelude = this.decks.shufflePrelude(expansions);
      this.sendGameMessage(MarsMessageType.ShuffledPrelude, prelude);
    }
    var projects = this.decks.shuffleProjects(expansions);
    this.sendGameMessage(MarsMessageType.ShuffledProjects, projects);
  }

  public sendGameMessage(type: MarsMessageType, message: any) {
    console.log(`Sending ${type} ${message} for ${this.playerState.getPlayerId()}`)
    var gameMessage = new GameMessage(type, JSON.stringify(message), this.playerState.getPlayerId());
    this.signalR.sendMessage(this.hubName, gameMessage);
  }

  public getResourceExtra(resource: MarsResourceCount, singleResource: boolean = false): string {
    var extra = '';
    if (resource.condition != null && !singleResource) {
      extra += ` (${this.getPrereqsString([resource.condition])})`;
    }
    if (resource.perTile) {
      if (resource.onMars) {
        extra += ` (per ${this.getTilesString([resource.tile])} tile on mars)`;
      }
      else {
        extra += ` (per ${this.getTilesString([resource.tile])} tile)`;
      }
    }
    if (resource.perTag) {
      if (resource.onlyOpponents) {
        extra += ` (per opponents ${this.getTagsString([resource.tag])} tags in play)`;
      }
      else if (resource.anyPlayer) {
        extra += ` (per ${this.getTagsString([resource.tag])} tag in play)`;
      }
      else {
        extra += ` (per your ${this.getTagsString([resource.tag])} tags in play)`;
      }
    }
    else if (resource.anyPlayer && !resource.perTile) {
      extra += ' (from any player)';
    }
    if (resource.anyCard) {
      extra += ' (to any card)';
    }
    if (!singleResource) {
      resource.or.forEach((o) => {
        extra += ` OR ${this.getResourceString([o], false)}`;
      })
    }

    return extra;
  }

  public getResourceString(resourceCount: Array<MarsResourceCount>, singleResource: boolean = false): string {
    var resources = '';
    resourceCount.forEach((p) => {
      if (resources != '') {
        resources += ', ';
      }
      resources += `${p.amount} ${this.getResourceName(p.resource, p.amount)}`;
      resources += this.getResourceExtra(p, singleResource);
    });

    return resources;
  }

  public getTilesString(tiles: Array<MarsTiles>): string {
    var tileString = '';
    tiles.forEach((t) => {
      if (tileString != '') {
        tileString += ', ';
      }
      tileString += `${this.getTileName(t)}`;
    });

    return tileString;
  }

  public getResourceImage(resource: MarsResourceType): string {
    switch (resource) {
      case MarsResourceType.Energy:
        return './assets/terraforming-mars/energy-icon.png';
      case MarsResourceType.Heat:
        return './assets/terraforming-mars/heat-icon.png';
      case MarsResourceType.Megacredits:
        return './assets/terraforming-mars/megacredit-icon.png';
      case MarsResourceType.Plants:
        return './assets/terraforming-mars/plant-icon.png';
      case MarsResourceType.Steel:
        return './assets/terraforming-mars/steel-icon.png';
      case MarsResourceType.Titanium:
        return './assets/terraforming-mars/titanium-icon.png';
      case MarsResourceType.AnimalToken:
        return './assets/terraforming-mars/animal-icon.png';
      case MarsResourceType.MicrobeToken:
        return './assets/terraforming-mars/microbe-icon.png';
      case MarsResourceType.Oxygen:
        return './assets/terraforming-mars/oxygen-icon.png';
      case MarsResourceType.Temperature:
        return './assets/terraforming-mars/temperature-icon.png';
      case MarsResourceType.Project:
        return './assets/terraforming-mars/project-icon.jpg';
      case MarsResourceType.Terraforming:
        return './assets/terraforming-mars/terraforming-icon.png';
    }

    return '';
  }

  public getTagImage(tag: MarsTags): string {
    switch (tag) {
      case MarsTags.Animal:
        return './assets/terraforming-mars/animal-tag.png';
      case MarsTags.Builder:
        return './assets/terraforming-mars/builder-tag.png';
      case MarsTags.City:
        return './assets/terraforming-mars/city-tag.png';
      case MarsTags.Earth:
        return './assets/terraforming-mars/earth-tag.png';
      case MarsTags.Energy:
        return './assets/terraforming-mars/energy-tag.png';
      case MarsTags.Event:
        return './assets/terraforming-mars/event-tag.png';
      case MarsTags.Jovian:
        return './assets/terraforming-mars/jovian-tag.png';
      case MarsTags.Microbe:
        return './assets/terraforming-mars/microbe-tag.png';
      case MarsTags.Plant:
        return './assets/terraforming-mars/plant-tag.png';
      case MarsTags.Science:
        return './assets/terraforming-mars/science-tag.png';
      case MarsTags.Space:
        return './assets/terraforming-mars/space-tag.png';
      case MarsTags.Wild:
        return './assets/terraforming-mars/wild-tag.png';
    }

    return '';
  }

  public getTagsString(tags: Array<MarsTags>): string {
    var tagString = '';
    tags.forEach((t) => {
      if (tagString != '') {
        tagString += ', ';
      }
      tagString += `${this.getTagName(t)}`;
    });

    return tagString;
  }

  public getPrereqsString(prereqs: Array<MarsPrereqCondition>): string {
    var prereqString = '';
    prereqs.forEach((p) => {
      if (prereqString != '') {
        prereqString += ', ';
      }
      prereqString += `${this.getPrereqName(p)}`;
    });

    return prereqString;
  }

  private getTagName(tag: MarsTags): string {
    switch (tag) {
      case MarsTags.Animal:
        return 'Animal';
      case MarsTags.Builder:
        return 'Builder';
      case MarsTags.City:
        return 'City';
      case MarsTags.Earth:
        return 'Earth';
      case MarsTags.Energy:
        return 'Energy';
      case MarsTags.Event:
        return 'Event';
      case MarsTags.Jovian:
        return 'Jovian';
      case MarsTags.Microbe:
        return 'Microbe';
      case MarsTags.Plant:
        return 'Plant';
      case MarsTags.Science:
        return 'Science';
      case MarsTags.Space:
        return 'Space';
      case MarsTags.Wild:
        return 'Wild';
      default:
        return '';
    }
  }

  private getPrereqName(prereq: MarsPrereqCondition) {
    var prereqString = '';
    if (prereq.max) {
      prereqString += 'Max: ';
    }
    switch (prereq.type) {
      case MarsPrereq.Ocean:
        prereqString += `${prereq.amount} ocean${prereq.amount > 1 ? 's' : ''}`;
        break;
      case MarsPrereq.Oxygen:
        prereqString += `${prereq.amount}% oxygen`;
        break;
      case MarsPrereq.Temperature:
        prereqString += `${prereq.amount} degrees`;
        break;
      case MarsPrereq.Production:
        prereq.production.forEach((r) => {
          prereqString += `${prereq.amount} ${this.getResourceName(r, prereq.amount)} production`;
        });
        break;
      case MarsPrereq.Tags:
        prereqString += `${prereq.amount > 0 ? prereq.amount : 'with a'} ${this.getTagsString(prereq.tag)} tag${prereq.amount > 1 ? 's' : ''}`;
        break;
      case MarsPrereq.Tile:
        prereqString += `${prereq.amount} ${this.getTileName(prereq.tile)}`;
        break;
      case MarsPrereq.Token:
        prereqString += `1 resource token on another card`;
        break;
      default:
        prereqString = 'none';
    }

    return prereqString;
  }

  private getTileName(tile: MarsTiles): string {
    switch (tile) {
      case MarsTiles.Capital:
        return 'Captial City';
      case MarsTiles.UrbanizedArea:
      case MarsTiles.Ganymede:
      case MarsTiles.Phobos:
      case MarsTiles.LavaTube:
      case MarsTiles.ResearchOutpost:
      case MarsTiles.City:
        return 'City';
      case MarsTiles.Noctis:
        return 'Noctis City';
      case MarsTiles.Commercial:
        return 'Commercial District';
      case MarsTiles.EcologicalZone:
        return 'Ecological Zone';
      case MarsTiles.Mangrove:
      case MarsTiles.ProtectedValley:
      case MarsTiles.Greenery:
        return 'Greenery';
      case MarsTiles.Industrial:
        return 'Industrial Complex';
      case MarsTiles.LandClaim:
        return 'Land Claim';
      case MarsTiles.LavaFlows:
        return 'Lava Flows';
      case MarsTiles.Mining:
      case MarsTiles.AdjacentMining:
        return 'Mining';
      case MarsTiles.Mohole:
        return 'Mohole Area';
      case MarsTiles.NaturalPreserve:
        return 'Nature Reserve';
      case MarsTiles.NuclearZone:
        return 'Nuclear Zone';
      case MarsTiles.ArtificialLake:
      case MarsTiles.Ocean:
        return 'Ocean';
      case MarsTiles.Restricted:
        return 'Restricted Area';
      case MarsTiles.CommunityArea:
        return 'Community Area';
      default:
        return '';
    }
  }

  public getResourceName(resource: MarsResourceType, amount: number): string {
    switch (resource) {
      case MarsResourceType.Energy:
        return 'energy';
      case MarsResourceType.Heat:
        return 'heat';
      case MarsResourceType.Megacredits:
        return `megacredit${amount > 1 || amount < -1 ? 's' : ''}`;
      case MarsResourceType.Plants:
        return `plant${amount > 1 || amount < -1 ? 's' : ''}`;
      case MarsResourceType.Steel:
        return 'steel';
      case MarsResourceType.Titanium:
        return 'titanium';
      case MarsResourceType.Temperature:
        return 'temperature';
      case MarsResourceType.Oxygen:
        return 'oxygen';
      case MarsResourceType.Project:
        return `card${amount > 1 || amount < -1 ? 's' : ''}`;
      case MarsResourceType.Terraforming:
        return 'terraforming';
      case MarsResourceType.AnimalToken:
        return `animal${amount > 1 || amount < -1 ? 's' : ''}`;
      case MarsResourceType.MicrobeToken:
        return `microbe${amount > 1 || amount < -1 ? 's' : ''}`;
      case MarsResourceType.ScienceToken:
        return 'science resource';
      case MarsResourceType.FleetToken:
        return 'fleet resource';
      case MarsResourceType.RoboticWorkforce:
        return 'duplicate production of a builder card';
      default:
        return '';
    }
  }

  public getPlayerName(playerId: string): string {
    return this.activePlayers.find(p => p.id == playerId).name;
  }

  private onMessage(data: GameMessage) {
    if (data != null) {
      var message = JSON.parse(data.message);
      switch (data.type) {
        case MarsMessageType.UpdatePlayerList:
          this.activePlayers = message;
          break;
        case MarsMessageType.ShuffledCorps:
          this.initializeGameState();
          this.marsPlayerState.dealCorporations(message, this.activePlayers);
          this.assignColors();
          this.advancePhase();
          break;
        case MarsMessageType.DiscardCorp:
          this.marsPlayerState.discardCorporation(message);
          break;
        case MarsMessageType.ShuffledProjects:
          this.decks.addShuffledProjects(message);
          this.marsPlayerState.dealProjects(10, this.activePlayers);
          break;
        case MarsMessageType.DiscardProject:
          this.marsPlayerState.discardProject(message);
          break;
        case MarsMessageType.PurchaseProjectCards:
          this.playerReady.push(message);
          this.startGeneration();
          break;
        case MarsMessageType.PurchaseMidTurnCards:
          this.marsPlayerState.purchaseCards(this.activePlayers);
          break;
        case MarsMessageType.SelectMidTurnCards:
          this.marsPlayerState.keepCards();
          break;
        case MarsMessageType.FirstPlayer:
          this.firstPlayer = message;
          this.currentPlayer = message;
          this.availableActions = 2;
          this.canTakeActions = true;
          break;
        case MarsMessageType.PlayProject:
          this.playProject(message);
          break;
        case MarsMessageType.ApplyProduction:
          this.applyProduction(message);
          break;
        case MarsMessageType.ApplyResources:
          this.applyResources(message);
          break;
        case MarsMessageType.ApplyProductionTarget:
          this.targetProduction(message, data.playerId);
          break;
        case MarsMessageType.ApplyResourceTarget:
          this.targetResource(message, data.playerId);
          break;
        case MarsMessageType.AdvanceTurn:
          this.advanceTurn();
          break;
        case MarsMessageType.Pass:
          this.passedPlayers.push(message);
          this.recordEvent(`${this.getPlayerName(message)} passes`);
          this.advanceTurn();
          break;
        case MarsMessageType.PlaceTile:
          this.placeTile(message);
          break;
        case MarsMessageType.RecordEvent:
          this.recordEvent(message);
          break;
        case MarsMessageType.ClaimMilestone:
          this.marsPlayerState.getClaimedMilestones().push(new MarsPlayerCard(data.playerId, message));
          this.claimAwardOrMilestoneGfx(data.playerId, message, true);
          break;
        case MarsMessageType.ClaimAward:
          this.marsPlayerState.getClaimedAwards().push(new MarsPlayerCard(data.playerId, message));
          this.claimAwardOrMilestoneGfx(data.playerId, message, false);
          break;
        case MarsMessageType.ReshuffleGeneration:
          this.decks.addShuffledProjects(message);
          break;
        case MarsMessageType.Reshuffled:
          this.decks.addShuffledProjects(message);
          break;
        case MarsMessageType.ShuffledPrelude:
          this.decks.addShuffledPrelude(message);
          this.marsPlayerState.dealPreludes(4, this.activePlayers);
          break;
        case MarsMessageType.DiscardPrelude:
          this.marsPlayerState.discardPrelude(message);
          break;
        case MarsMessageType.Expansions:
          this.expansions = message;
          break;
        case MarsMessageType.PlayPrelude:
          this.marsPlayerState.playPreludes(this.activePlayers, this.firstPlayer, message);
          break;
        case MarsMessageType.Map:
          this.map = message;
          break;
        case MarsMessageType.TriggerProject:
          var card = this.decks.getProject(message);
          if (card instanceof MarsActionProject) {
            var action = card as MarsActionProject;
            action.triggerGeneration = this.generation;
          }
          break;
        case MarsMessageType.TriggerCorp:
          var corp = this.marsPlayerState.getPlayerCorporation(data.playerId);
          corp.lastCorpAction = message;
          break;
      }
      this.hideMapButtons();
      this.showMapButtons();

      if (this.currentPlayer == this.playerState.getPlayerId() && this.pendingTiles.length > 0) {
        this.marsBoardState.hideTileSelection();
        if (this.pendingProduction.length == 0 && this.pendingResources.length == 0) {
          this.marsBoardState.highlightTileSelection(this.pendingTiles[0], this.currentPlayer, this.generation);
        }
      }
    }
  }

  private claimAwardOrMilestoneGfx(playerId: string, id: number, isMilestone: boolean) {
    if (isMilestone) {
      var milestone = this.decks.getMilestones(this.map).find(m => m.id == id);
      if (milestone != null) {
        this.marsBoardState.claimAwardOrMilestone(this.getPlayerColor(playerId), milestone.gfxId);
      }
    }
    else {
      var award = this.decks.getAwards(this.map).find(a => a.id == id);
      if (award != null) {
        this.marsBoardState.claimAwardOrMilestone(this.getPlayerColor(playerId), award.gfxId);
      }
    }
  }

  private placeTile(tile: MarsPlayedTile) {
    var bonusResources = this.marsBoardState.placeTile(tile, this.playerColors);
    if (tile.tile != MarsTiles.LandClaim && tile.tile != MarsTiles.CommunityArea) {
      bonusResources.forEach((b) => {
        this.applyResources(new MarsResourceTarget(b, tile.playerId, -1));

        if (b.resource == MarsResourceType.Steel && (tile.tile == MarsTiles.Mining || tile.tile == MarsTiles.AdjacentMining)) {
          this.applyProduction(new MarsResourceTarget(new MarsResourceCount(b.resource, 1), tile.playerId, -1));
        }
        if (b.resource == MarsResourceType.Titanium && (tile.tile == MarsTiles.Mining || tile.tile == MarsTiles.AdjacentMining)) {
          this.applyProduction(new MarsResourceTarget(new MarsResourceCount(b.resource, 1), tile.playerId, -1));
        }
      });

      if (tile.tile == MarsTiles.Greenery || tile.tile == MarsTiles.Mangrove || tile.tile == MarsTiles.ProtectedValley) {
        this.applyResources(new MarsResourceTarget(new MarsResourceCount(MarsResourceType.Oxygen, 1), tile.playerId, -1));
      }

      if (tile.tile == MarsTiles.ArtificialLake || tile.tile == MarsTiles.Ocean) {
        this.score.addScore(1, 'global', MarsScoreTrack.Oceans);
        this.score.addScore(1, tile.playerId, MarsScoreTrack.Terraforming);
      }

      this.marsPlayerState.applyTileBonuses(tile, bonusResources, this.activePlayers);
    }
  }

  private assignColors() {
    this.playerColors = new Array<MarsPlayerColor>();
    var colors = ['green', 'red', 'blue', 'yellow', 'black'];
    this.activePlayers.forEach((p, index) => {
      this.playerColors.push(new MarsPlayerColor(p.id, colors[index]));
    });
  }

  private advanceTurn() {
    this.canTakeActions = true;
    this.availableActions = 2;
    var index = this.activePlayers.findIndex(p => p.id == this.currentPlayer) + 1;
    if (index >= this.activePlayers.length) {
      index = 0;
    }
    this.currentPlayer = this.activePlayers[index].id;

    if (this.currentPhase == MarsPhase.PlayPreludes) {
      if (this.firstPlayer == this.currentPlayer) {
        this.advancePhase();
      }
      else if (!this.hasPreludesToPlay()) {
        this.advanceTurn();
      }

      return;
    }

    if (this.passedPlayers.length == this.activePlayers.length) {
      if (!this.nextGeneration()) {
        return;  // game over
      }
    }

    if (this.passedPlayers.some(p => p == this.currentPlayer)) {
      this.advanceTurn();
    }
    else {
      this.showMapButtons();
    }
  }

  private caluclateFinalScores() {
    this.advancePhase();
    this.marsPlayerState.awardPoints(this.activePlayers, this.map);
    this.marsPlayerState.milestonePoints();
    this.marsPlayerState.greeneryPoints(this.activePlayers);
    this.marsPlayerState.projectPoints(this.activePlayers);
  }

  private nextGeneration(): boolean {
    if (this.finalGeneration) {
      this.caluclateFinalScores();
      return false;
    }
    if (this.score.getScore('global', MarsScoreTrack.Temperature) == 8 && this.score.getScore('global', MarsScoreTrack.Oxygen) == 14 && this.score.getScore('global', MarsScoreTrack.Oceans) == 9) {
      this.finalGeneration = true;
    }
    this.marsPlayerState.collectIncome(this.activePlayers);
    if (!this.finalGeneration) {
      this.marsPlayerState.dealProjects(4, this.activePlayers);
      this.playerReady = [];
    }
    this.passedPlayers = [];
    var index = this.activePlayers.findIndex(p => p.id == this.firstPlayer) + 1;
    if (index >= this.activePlayers.length) {
      index = 0;
    }
    this.firstPlayer = this.activePlayers[index].id;
    this.currentPlayer = this.firstPlayer;
    this.generation++;
    this.hideMapButtons();

    return true;
  }

  private targetProduction(production: MarsResourceTarget, playerId: string) {
    this.applyProduction(production);
    var currentPlayerName = this.getPlayerName(playerId);
    var playerName = this.getPlayerName(production.targetId);
    var resource = this.getResourceName(production.resource.resource, production.resource.amount);
    var event = `${currentPlayerName} targets ${playerName} for ${production.resource.amount} ${resource} production`;
    if (production.projectId != -1) {
      event += ` on project ${this.decks.getProject(production.projectId).name}`;
    }
    this.recordEvent(event);
  }

  private targetResource(resource: MarsResourceTarget, playerId: string) {
    var card = this.decks.getProject(resource.projectId);
    if (card == null && resource.projectId == MarsCorporationProject.Recyclon) {
      var corp = this.marsPlayerState.getPlayerCorporation(this.marsPlayerState.getRecyclonPlayer());
      card = new MarsActionProject(MarsCorporationProject.Recyclon, 'Recyclon', '', corp.reaction[0], [], 0, [], [], []);
    }
    var scoreTrackId = null;
    if (card instanceof MarsActionProject) {
      scoreTrackId = this.marsPlayerState.getProjectScoreTrack(card as MarsActionProject);
    }
    this.marsPlayerState.applyResource(resource.resource, resource.targetId, this.activePlayers, scoreTrackId);
    var currentPlayerName = this.getPlayerName(playerId);
    var playerName = this.getPlayerName(resource.targetId);
    var resourceName = this.getResourceName(resource.resource.resource, resource.resource.amount);
    var event = `${currentPlayerName} targets ${playerName} for ${resource.resource.amount} ${resourceName}`;
    if (resource.projectId != -1) {
      event += ` on project ${card.name}`;
    }
    this.recordEvent(event);

    this.checkReshuffle();
  }

  private applyProduction(production: MarsResourceTarget) {
    this.marsPlayerState.applyProduction(production.resource, production.targetId, this.activePlayers);
  }

  private applyResources(resources: MarsResourceTarget) {
    this.marsPlayerState.applyResource(resources.resource, resources.targetId, this.activePlayers);

    this.checkReshuffle();
  }

  private checkReshuffle() {
    // check if there are fewer than 4 cards per player in the deck, if so, reshuffle
    if (this.decks.needReshuffle(4 * this.activePlayers.length) && this.isGameCreator()) {
      var reshuffled = this.decks.reshuffleProjects();
      this.sendGameMessage(MarsMessageType.Reshuffled, reshuffled);
    }
  }

  private playProject(playedProject: MarsPlayProject) {
    this.marsPlayerState.playProject(playedProject, this.activePlayers);
    var card = this.decks.getProject(playedProject.projectId);
    var playerId = this.marsPlayerState.getPlayerProjectPlayed().find(p => p.cardId == playedProject.projectId).playerId;
    var playerName = this.getPlayerName(playerId);
    this.recordEvent(`${playerName} played ${card.name}`);
  }

  private recordEvent(eventDetails: string) {
    if (this.recentEvents.length > 2) {
      this.recentEvents.shift();
    }

    this.recentEvents.push(eventDetails);
  }

  private startGeneration() {
    if (this.activePlayers.length == this.playerReady.length) {
      if (this.generation == 0) {
        this.advancePhase();

        // play corps
        this.marsPlayerState.playCorporations(this.activePlayers, this.firstPlayer);

        var unmi = this.marsPlayerState.getUNMIPlayer();
        if (unmi != null) {
          this.unmiTerraformRating = this.score.getScore(unmi, this.marsPlayerState.getScoreTrack(MarsResourceType.Terraforming, true));
        }

        if (this.usingExpansion(MarsExpansion.Prelude)) {
          this.marsPlayerState.playPreludes(this.activePlayers, this.firstPlayer);
          if (!this.hasPreludesToPlay()) {
            this.advanceTurn();
          }
        }
        else {
          // skip the play prelude phase
          this.advancePhase();
        }

        this.generation++;
      }
      else {
        var unmi = this.marsPlayerState.getUNMIPlayer();
        if (unmi != null) {
          this.unmiTerraformRating = this.score.getScore(unmi, this.marsPlayerState.getScoreTrack(MarsResourceType.Terraforming, true));
        }
      }
      this.marsPlayerState.purchaseCards(this.activePlayers);
      this.checkReshuffle();

      this.showMapButtons();
    }
  }

  public hideMapButtons() {
    var standardProjects = this.decks.getStandardProjects();
    this.marsBoardState.setActionButtonsVisible(standardProjects.map(s => s.gfxId), false);

    var milestones = this.decks.getMilestones(this.map);
    this.marsBoardState.setActionButtonsVisible(milestones.map(m => m.gfxId), false);

    var awards = this.decks.getAwards(this.map);
    this.marsBoardState.setActionButtonsVisible(awards.map(a => a.gfxId), false);
  }

  public showMapButtons() {
    if (this.finalGeneration || !this.allPlayersReady() || this.hasFirstTurnAction() || this.currentPhase == MarsPhase.PlayPreludes) {
      return;
    }

    if (this.currentPlayer == this.playerState.getPlayerId() && this.canTakeActions && this.availableActions > 0) {
      var megacredits = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true));
      if (this.marsPlayerState.isHelion()) {
        megacredits += this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, true));
      }
      this.showStandardProjectButtons(megacredits);
      this.showMilestoneButtons(megacredits);
      this.showAwardsButtons(megacredits);
    }
  }

  private showStandardProjectButtons(megacredits: number) {
    if (this.marsPlayerState.isHelion()) {
      megacredits += this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, true));
    }

    var standardProjects = this.decks.getStandardProjects();
    standardProjects.forEach((s) => {
      if (s.sellPatents) {
        if (this.marsPlayerState.getPlayerProjectHand().filter(p => p.playerId == this.playerState.getPlayerId()).length > 0) {
          this.marsBoardState.setActionButtonsVisible([s.gfxId], true);
        }
      }
      else if (s.cost - this.marsPlayerState.getStandardProjectDiscount(s.production) <= megacredits) {
        if ((this.score.getScore('global', MarsScoreTrack.Temperature) < 8 || s.resource == null) &&
          (s.tile == MarsTiles.None || this.canPlayStandardTile(s.tile))) {
          this.marsBoardState.setActionButtonsVisible([s.gfxId], true);
        }
      }
    });
  }

  private canPlayStandardTile(tile: MarsTiles): boolean {
    if (tile == MarsTiles.Ocean && this.score.getScore('global', MarsScoreTrack.Oceans) < 9) {
      return true;
    }

    if ((tile == MarsTiles.City || tile == MarsTiles.Greenery) && this.marsBoardState.validTileLocations(tile, this.playerState.getPlayerId()).length > 0) {
      return true;
    }

    return false;
  }

  private showMilestoneButtons(megacredits: number) {
    if (this.marsPlayerState.getClaimedMilestones().length < 3 && megacredits >= 8) {
      var milestones = this.decks.getMilestones(this.map);
      milestones.forEach((m) => {
        if (!this.marsPlayerState.getClaimedMilestones().some(p => p.cardId == m.id)) {
          var display = true;
          switch (m.condition.type) {
            case MarsPrereq.Resource:
              m.condition.production.forEach((p) => {
                if (p == MarsResourceType.Project) {
                  if (this.marsPlayerState.getPlayerProjectHand().filter(h => h.playerId == this.currentPlayer).length < m.condition.amount) {
                    display = false;
                  }
                }
                else if (this.score.getScore(this.currentPlayer, this.marsPlayerState.getScoreTrack(p, true)) < m.condition.amount) {
                  display = false;
                }
              });
              this.marsBoardState.setActionButtonsVisible([m.gfxId], display);
              break;
            case MarsPrereq.Tile:
              if (this.marsBoardState.getTotalTiles(m.condition.tile, false, this.currentPlayer) >= m.condition.amount) {
                this.marsBoardState.setActionButtonsVisible([m.gfxId], true);
              }
              break;
            case MarsPrereq.Tags:
              var tagCount = 0;
              m.condition.tag.forEach((t, index) => {
                tagCount += this.marsPlayerState.getTags(this.currentPlayer, t, index == 0);
              });
              if (tagCount >= m.condition.amount) {
                this.marsBoardState.setActionButtonsVisible([m.gfxId], true);
              }
              break;
            case MarsPrereq.UniqueTags:
              var uniqueCount = 0;
              [MarsTags.Animal, MarsTags.Builder, MarsTags.City, MarsTags.Earth, MarsTags.Energy, MarsTags.Jovian, MarsTags.Microbe, MarsTags.Plant, MarsTags.Science, MarsTags.Space, MarsTags.Wild].forEach((t) => {
                uniqueCount += this.marsPlayerState.getTags(this.currentPlayer, t, false) > 0 ? 1 : 0;
              });
              if (uniqueCount >= m.condition.amount) {
                this.marsBoardState.setActionButtonsVisible([m.gfxId], true);
              }
              break;
            case MarsPrereq.PrereqCards:
              if (this.marsPlayerState.getPlayerPlayedProjects(this.currentPlayer).filter(p => !p.tags.some(t => t == MarsTags.Event) && p.prereqs.length > 0).length >= m.condition.amount) {
                this.marsBoardState.setActionButtonsVisible([m.gfxId], true);
              }
              break;
            case MarsPrereq.PolarTiles:
              if (this.marsBoardState.tilesPlayed.filter(t => t.playerId == this.currentPlayer && t.locationId >= 50 && t.locationId <= 60 &&
                t.tile != MarsTiles.Ocean && t.tile != MarsTiles.ArtificialLake && t.tile != MarsTiles.LandClaim && t.tile != MarsTiles.CommunityArea).length >= m.condition.amount) {
                this.marsBoardState.setActionButtonsVisible([m.gfxId], true);
              }
              break;
            case MarsPrereq.Production:
              if (m.condition.or) {
                display = false;
                m.condition.production.forEach((p) => {
                  if (m.condition.amount <= this.score.getScore(this.currentPlayer, this.marsPlayerState.getScoreTrack(p, false))) {
                    display = true;
                  }
                });
              }
              else {
                m.condition.production.forEach((p) => {
                  if (m.condition.amount > this.score.getScore(this.currentPlayer, this.marsPlayerState.getScoreTrack(p, false))) {
                    display = false;
                  }
                });
              }
              this.marsBoardState.setActionButtonsVisible([m.gfxId], display);
              break;
            case MarsPrereq.NonEventCards:
              if (this.marsPlayerState.getPlayerPlayedProjects(this.currentPlayer).filter(c => !c.tags.some(t => t == MarsTags.Event)).length >= m.condition.amount) {
                this.marsBoardState.setActionButtonsVisible([m.gfxId], true);
              }
              break;
          }
        }
      });
    }
  }

  public showAwardsButtons(megacredits: number) {
    if (this.marsPlayerState.getClaimedAwards().length < 3 && megacredits >= 8 + 6 * this.marsPlayerState.getClaimedAwards().length) {
      var awards = this.decks.getAwards(this.map);
      awards.forEach((a) => {
        if (!this.marsPlayerState.getClaimedAwards().some(p => p.cardId == a.id)) {
          this.marsBoardState.setActionButtonsVisible([a.gfxId], true);
        }
      });
    }
  }

  private initializeGameState() {
    this.score.configureGlobalMinMax(0, 1000);
    this.score.configureMinMax(MarsScoreTrack.Energy, 0, 1000);
    this.score.configureMinMax(MarsScoreTrack.Heat, 0, 1000);
    this.score.configureMinMax(MarsScoreTrack.Megacredits, -5, 1000);
    this.score.configureMinMax(MarsScoreTrack.Oceans, 0, 9);
    this.score.configureMinMax(MarsScoreTrack.Oxygen, 0, 14);
    this.score.configureMinMax(MarsScoreTrack.Plants, 0, 1000);
    this.score.configureMinMax(MarsScoreTrack.Steel, 0, 1000);
    this.score.configureMinMax(MarsScoreTrack.Temperature, -30, 8);
    this.score.configureMinMax(MarsScoreTrack.Terraforming, 0, 1000);
    this.score.configureMinMax('s-' + MarsScoreTrack.Megacredits, 0, 1000);
    this.score.configureMinMax('s-' + MarsScoreTrack.Steel, 0, 1000);
    this.score.configureMinMax('s-' + MarsScoreTrack.Titanium, 0, 1000);
    this.score.configureMinMax('s-' + MarsScoreTrack.Plants, 0, 1000);
    this.score.configureMinMax('s-' + MarsScoreTrack.Energy, 0, 1000);
    this.score.configureMinMax('s-' + MarsScoreTrack.Heat, 0, 1000);
    this.activePlayers.forEach((p) => {
      this.score.setScore(20, p.id, MarsScoreTrack.Terraforming);
      this.score.setScore(0, p.id, MarsScoreTrack.Megacredits);
      this.score.setScore(0, p.id, MarsScoreTrack.Steel);
      this.score.setScore(0, p.id, MarsScoreTrack.Titanium);
      this.score.setScore(0, p.id, MarsScoreTrack.Plants);
      this.score.setScore(0, p.id, MarsScoreTrack.Energy);
      this.score.setScore(0, p.id, MarsScoreTrack.Heat);
      this.score.setScore(0, p.id, 's-' + MarsScoreTrack.Megacredits);
      this.score.setScore(0, p.id, 's-' + MarsScoreTrack.Steel);
      this.score.setScore(0, p.id, 's-' + MarsScoreTrack.Titanium);
      this.score.setScore(0, p.id, 's-' + MarsScoreTrack.Plants);
      this.score.setScore(0, p.id, 's-' + MarsScoreTrack.Energy);
      this.score.setScore(0, p.id, 's-' + MarsScoreTrack.Heat);
      this.score.setScore(0, p.id, 'psychrophiles');
    });

    this.score.setScore(-30, 'global', MarsScoreTrack.Temperature);
    this.score.setScore(0, 'global', MarsScoreTrack.Oxygen);
    this.score.setScore(0, 'global', MarsScoreTrack.Oceans);

    this.playerReady = [];
    this.generation = 0;
  }
}
