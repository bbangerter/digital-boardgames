import { Injectable } from '@angular/core';
import { MarsPlayerCard, MarsResourceCount, MarsTagPlayed, MarsReaction, MarsCorp, MarsActionProject, MarsAlternateCredits, MarsPlayProject, MarsTilePlayed, MarsPlayedTile, MarsProject, MarsPrereqCondition, MarsRank, MarsAwardScore } from '../dtos/terraforming-mars-dtos.model';
import { GamePlayer, PlayerScoreService, PlayerStateService } from '../../core';
import { TerraformingMarsDecksService } from './terraforming-mars-decks.service';
import { MarsResourceType, MarsScoreTrack, MarsTags, MarsTiles, MarsPrereq, MarsMap, MarsCorporationProject } from '../terraforming-mars-constants';
import { TerraformingMarsBoardStateService } from './terraforming-mars-board-state.service';

@Injectable({
  providedIn: 'root'
})
export class TerraformingMarsPlayerStateService {
  private playerCorps: Array<MarsPlayerCard>;
  private playerDealtPreludes: Array<MarsPlayerCard> = [];
  private playerDealtProjects: Array<MarsPlayerCard>;
  private playerProjectHand: Array<MarsPlayerCard>;
  private playerPreludePlayed: Array<MarsPlayerCard>;
  private playerProjectPlayed: Array<MarsPlayerCard>;
  private claimedMilestones: Array<MarsPlayerCard>;
  private claimedAwards: Array<MarsPlayerCard>;

  constructor(private decks: TerraformingMarsDecksService, private score: PlayerScoreService, private playerState: PlayerStateService, private marsBoardState: TerraformingMarsBoardStateService) {
    this.playerProjectHand = new Array<MarsPlayerCard>();
    this.playerProjectPlayed = new Array<MarsPlayerCard>();
    this.playerPreludePlayed = new Array<MarsPlayerCard>();
    this.claimedMilestones = new Array<MarsPlayerCard>();
    this.claimedAwards = new Array<MarsPlayerCard>();
  }

  public projectPoints(allPlayers: Array<GamePlayer>) {
    var projectScore = [0, 0, 0, 0, 0];
    this.playerProjectPlayed.forEach((p) => {
      var score = this.getProjectScore(p.cardId);
      this.score.addScore(score, p.playerId, this.getScoreTrack(MarsResourceType.Terraforming, true));
      var index = allPlayers.findIndex(a => a.id == p.playerId);
      projectScore[index] += score;
    });

    allPlayers.forEach((p, index) => {
      this.score.setScore(projectScore[index], p.id, MarsScoreTrack.Cards);
    });
  }

  public greeneryPoints(allPlayers: Array<GamePlayer>) {
    var boardScore = [0, 0, 0, 0, 0];
    allPlayers.forEach((p, index) => {
      var count = this.marsBoardState.getTotalTiles(MarsTiles.Greenery, false, p.id);
      this.score.addScore(count, p.id, this.getScoreTrack(MarsResourceType.Terraforming, true));
      boardScore[index] += count;
    });

    var cities = this.marsBoardState.getCityTiles();
    cities.forEach((c) => {
      var greeneries = this.marsBoardState.getAdjacentGreeneries(c.locationId).length;
      this.score.addScore(greeneries, c.playerId, this.getScoreTrack(MarsResourceType.Terraforming, true));
      var index = allPlayers.findIndex(p => p.id == c.playerId);
      boardScore[index] += greeneries;
    });

    allPlayers.forEach((p, index) => {
      this.score.setScore(boardScore[index], p.id, MarsScoreTrack.Board);
    });
  }

  public awardPoints(allPlayers: Array<GamePlayer>, map: MarsMap) {
    var awards = this.decks.getAwards(map);
    var playerScore = Array<MarsAwardScore>();
    allPlayers.forEach((p) => {
      playerScore.push(new MarsAwardScore(p.id, 0));
    });
    this.claimedAwards.forEach((a) => {
      var award = awards.find(i => i.id == a.cardId);
      var ranks = this.getPlayerAwardRanks(allPlayers, award.condition);
      ranks.forEach((r, index) => {
        if (index == 0) {
          playerScore.find(s => s.playerId == r.playerId).score += 5;
        }
        else if (index == 1) {
          playerScore.find(s => s.playerId == r.playerId).score += 2;
        }
      })
    });

    playerScore.forEach((s) => {
      this.score.addScore(s.score, s.playerId, this.getScoreTrack(MarsResourceType.Terraforming, true));
      this.score.addScore(s.score, s.playerId, MarsScoreTrack.Awards);
    });
  }

  private getPlayerAwardRanks(allPlayers: Array<GamePlayer>, condition: MarsPrereqCondition): Array<MarsRank> {
    var rank = new Array<MarsRank>();
    allPlayers.forEach((p) => {
      var score = 0;
      switch (condition.type) {
        case MarsPrereq.Landlord:
          rank.push(new MarsRank(p.id, this.marsBoardState.getTotalTiles(MarsTiles.Any, false, p.id)));
          break;
        case MarsPrereq.Resource:
        case MarsPrereq.Production:
          condition.production.forEach((r) => {
            score += this.score.getScore(p.id, this.getScoreTrack(r, condition.type == MarsPrereq.Resource)) 
          });
          rank.push(new MarsRank(p.id, score));
          break;
        case MarsPrereq.Tags:
          condition.tag.forEach((t) => {
            score += this.getTags(p.id, t, false);
          });
          rank.push(new MarsRank(p.id, score));
          break;
        case MarsPrereq.Tile:
          score = this.marsBoardState.getTotalTiles(MarsTiles.Greenery, false, p.id);
          rank.push(new MarsRank(p.id, score));
          break;
        case MarsPrereq.GreenCards:
          score = this.getPlayerNonActionProjects(p.id).length;
          rank.push(new MarsRank(p.id, score));
          break;
        case MarsPrereq.Token:
          this.getPlayersActionProjects(p.id).forEach((c) => {
            var track = this.getProjectScoreTrack(c);
            if (track != null) {
              score += this.score.getScore(p.id, track);
            }
          });
          rank.push(new MarsRank(p.id, score));
          break;
        case MarsPrereq.NonEventCards:
          score = this.getPlayerPlayedProjects(p.id).filter(c => !c.tags.some(t => t == MarsTags.Event) && c.cost >= condition.amount).length;
          rank.push(new MarsRank(p.id, score));
          break;
        case MarsPrereq.PolarTiles:
          score = this.marsBoardState.tilesPlayed.filter(t => t.playerId == p.id && t.locationId >= 36 && t.locationId <= 60).length;
          rank.push(new MarsRank(p.id, score));
          break;
        case MarsPrereq.EstateDealer:
          this.marsBoardState.tilesPlayed.filter(t => t.tile != MarsTiles.Ocean && t.tile != MarsTiles.ArtificialLake && t.tile != MarsTiles.LandClaim && t.tile != MarsTiles.CommunityArea &&
            t.playerId == p.id).forEach((i) => {
            score += this.marsBoardState.getAdjacentTiles(i.locationId).some(o => o.tile == MarsTiles.Ocean || o.tile == MarsTiles.ArtificialLake) ? 1 : 0;
          });
          rank.push(new MarsRank(p.id, score));
          break;
      }
    });

    rank.sort((a, b) => {
      return b.count - a.count;
    });
    if (allPlayers.length == 2) {
      if (rank[0].count == rank[1].count) {
        return [];
      }
      return [rank[0]];
    }
    else {
      if (rank[2].count == rank[1].count) {
        if (rank[1].count == rank[0].count) {
          return [];
        }
        return [rank[0]];
      }

      return [rank[0], rank[1]];
    }
  }

  public milestonePoints() {
    this.claimedMilestones.forEach((p) => {
      this.score.addScore(5, p.playerId, this.getScoreTrack(MarsResourceType.Terraforming, true));
      this.score.addScore(5, p.playerId, MarsScoreTrack.Milestones);
    });
  }

  public collectIncome(activePlayers: GamePlayer[]) {
    activePlayers.forEach((p) => {
      var energyToHeat = this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Energy, true));
      this.score.addScore(energyToHeat, p.id, this.getScoreTrack(MarsResourceType.Heat, true));
      this.score.subtractScore(energyToHeat, p.id, this.getScoreTrack(MarsResourceType.Energy, true));

      this.score.addScore(this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Megacredits, false)) +
        this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Terraforming, true)), p.id, this.getScoreTrack(MarsResourceType.Megacredits, true));
      this.score.addScore(this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Steel, false)), p.id, this.getScoreTrack(MarsResourceType.Steel, true));
      this.score.addScore(this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Titanium, false)), p.id, this.getScoreTrack(MarsResourceType.Titanium, true));
      this.score.addScore(this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Plants, false)), p.id, this.getScoreTrack(MarsResourceType.Plants, true));
      this.score.addScore(this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Energy, false)), p.id, this.getScoreTrack(MarsResourceType.Energy, true));
      this.score.addScore(this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Heat, false)), p.id, this.getScoreTrack(MarsResourceType.Heat, true));
    });
  }

  public applyTileBonuses(tile: MarsPlayedTile, bonus: Array<MarsResourceCount>, allPlayers: Array<GamePlayer>) {
    this.playerCorps.forEach((c) => {
      var corp = this.getPlayerCorporation(c.playerId);
      if (corp.reaction.length > 0) {
        corp.reaction.forEach((r) => {
          this.applyTileReactionBonuses(r, c.playerId, tile, bonus, allPlayers);
        });
      }
    });

    var actionProjects = this.getPlayedReactionCards();
    actionProjects.forEach((p) => {
      this.applyTileReactionBonuses(p.reaction, this.getProjectOwner(p.id), tile, bonus, allPlayers);
    });
  }

  private applyTileReactionBonuses(reaction: MarsReaction, playerId: string, tile: MarsPlayedTile, bonus: Array<MarsResourceCount>, allPlayers: Array<GamePlayer>) {
    if (reaction.tilePlayed != null && (reaction.tilePlayed.tile.some(t => t == tile.tile) || reaction.tilePlayed.tile.some(t => t == MarsTiles.Any))) {
      if ((reaction.tilePlayed.anyPlayer || tile.playerId == playerId && (!reaction.tilePlayed.onMars || tile.tile != MarsTiles.Ganymede && tile.tile != MarsTiles.Phobos))) {
        if (reaction.tilePlayed.onPlacementBonus.some(p => bonus.some(b => b.resource == p)) || reaction.tilePlayed.onPlacementBonus.length == 0) {
          if (reaction.tilePlayed.production != null) {
            this.applyProduction(reaction.tilePlayed.production, playerId, allPlayers);
          }
          if (reaction.tilePlayed.resource != null) {
            this.applyResource(reaction.tilePlayed.resource, playerId, allPlayers);
          }
        }
      }
    }
  }

  public getProjectScore(projectId: number): number {
    var card = this.decks.getProject(projectId);
    if (card.score == null) {
      return 0;
    }

    var score = 0;
    var playerId = this.getProjectOwner(card.id);
    if (card instanceof MarsActionProject) {
      if (card.score.tokenType != MarsResourceType.None) {
        var track = this.getProjectScoreTrack(card);
        this.getTokenCount(playerId, card.id);
        var tokens = this.score.getScore(playerId, track);
        score = Math.min(Math.floor(card.score.score * tokens), card.score.maxScore);
      }
      else {
        score = card.score.score;
      }
    }
    else {
      score = card.score.score;
    }

    if (card.score.adjacentTile) {
      score = this.marsBoardState.getTilesAdjacentTo(card.tile[0], card.score.tileType) * card.score.score;
    }
    else if (card.score.perTile) {
      score = this.marsBoardState.getTotalTiles(card.score.tileType, false) * card.score.score;
    }
    else if (card.score.perTag) {
      var tags = this.getTags(playerId, card.score.tagType, false);
      score = Math.min(Math.floor(card.score.score * tags), card.score.maxScore);
    }
    
    return Math.floor(score);
  }

  public getPreludeCards(playerId: string): Array<number> {
    return this.playerDealtPreludes.filter(p => p.playerId == playerId).map(c => c.cardId);
  }

  public getPlayedPreludeCards(playerId: string): Array<number> {
    return this.playerPreludePlayed.filter(p => p.playerId == playerId).map(c => c.cardId);
  }

  public getPreludeOwner(preludeId: number): string {
    if (this.playerDealtPreludes.find(c => c.cardId == preludeId)) {
      return this.playerDealtPreludes.find(c => c.cardId == preludeId).playerId;
    }

    if (this.playerPreludePlayed.find(c => c.cardId == preludeId)) {
      return this.playerPreludePlayed.find(c => c.cardId == preludeId).playerId;
    }

    return this.playerState.getPlayerId();
  }

  public getProjectOwner(projectId: number): string {
    if (this.playerDealtProjects.find(c => c.cardId == projectId)) {
      return this.playerDealtProjects.find(c => c.cardId == projectId).playerId;
    }

    if (this.playerProjectHand.find(c => c.cardId == projectId)) {
      return this.playerProjectHand.find(c => c.cardId == projectId).playerId;
    }

    if (this.playerProjectPlayed.find(c => c.cardId == projectId)) {
      return this.playerProjectPlayed.find(c => c.cardId == projectId).playerId;
    }

    if (projectId == MarsCorporationProject.Recyclon) {
      return this.getRecyclonPlayer();
    }

    if (projectId == MarsCorporationProject.Splice) {
      return this.getSplicePlayer();
    }

    return this.playerState.getPlayerId();
  }

  public getTokenCount(playerId: string, projectId: number): number {
    var card = this.getPlayersActionProjects(playerId).find(c => c.id == projectId);
    if (projectId == MarsCorporationProject.Recyclon) {
      var corp = this.getPlayerCorporation(playerId);
      card = new MarsActionProject(MarsCorporationProject.Recyclon, 'Recyclon', '', corp.reaction[0], [], 0, [], [], []);
    }
    if (card == null) {
      return 0;
    }
    var tokens = this.score.getScore(playerId, this.getProjectScoreTrack(card));
    return tokens;
  }

  public getProjectTokenType(card: MarsActionProject): MarsResourceType {
    for (var i = 0; i < card.action.length; i++) {
      var result = card.action[i].resourceResult.find(r => r.tokenScoreTrackId != null);
      if (result != null) {
        return result.resource;
      }
    }

    if (card.reaction != null) {
      if (card.reaction.tilePlayed != null && card.reaction.tilePlayed.resource != null &&
        card.reaction.tilePlayed.resource.tokenScoreTrackId != null) {
        return card.reaction.tilePlayed.resource.resource;
      }
      if (card.reaction.tagPlayed != null && card.reaction.tagPlayed.resource != null) {
        return card.reaction.tagPlayed.resource[0].resource;
      }
    }

    return MarsResourceType.None;
  }

  public getProjectScoreTrack(card: MarsActionProject): string {
    for (var i = 0; i < card.action.length; i++) {
      var result = card.action[i].resourceResult.find(r => r.tokenScoreTrackId != null);
      if (result != null) {
        return result.tokenScoreTrackId;
      }
    }

    if (card.reaction != null) {
      if (card.reaction.tilePlayed != null && card.reaction.tilePlayed.resource != null &&
        card.reaction.tilePlayed.resource.tokenScoreTrackId != null) {
        return card.reaction.tilePlayed.resource.tokenScoreTrackId;
      }
      if (card.reaction.tagPlayed != null && card.reaction.tagPlayed.resource != null) {
        return card.reaction.tagPlayed.resource[0].tokenScoreTrackId;
      }
    }

    return null;
  }

  public getPlayerPlayedProjects(playerId: string): Array<MarsProject> {
    var cardIds = this.playerProjectPlayed.filter(p => p.playerId == playerId).map(c => c.cardId);
    return this.decks.getProjects(cardIds);
  }

  public getTags(playerId: string, tag: MarsTags, includeWild: boolean): number {
    var count = 0;
    var corp = this.getPlayerCorporation(playerId);
    count += corp.tags.filter(t => t == tag || t == MarsTags.Wild && includeWild).length;

    var cards = this.getPlayerPlayedProjects(playerId);
    cards.forEach((c) => {
      if (tag == MarsTags.Event || !c.tags.some(t => t == MarsTags.Event)) {
        count += c.tags.filter(t => t == tag || t == MarsTags.Wild && includeWild).length;
      }
    });

    var prelude = this.getPlayedPreludeCards(playerId);
    prelude.forEach((p) => {
      count += this.decks.getPrelude(p).tags.filter(t => t == tag || t == MarsTags.Wild && includeWild).length;
    });

    return count;
  }

  public playProject(playedProject: MarsPlayProject, allPlayers: Array<GamePlayer>) {
    var project = this.playerProjectHand.find(p => p.cardId == playedProject.projectId);
    this.playerProjectPlayed.push(project);
    this.score.subtractScore(playedProject.megacredits, project.playerId, this.getScoreTrack(MarsResourceType.Megacredits, true));
    this.score.subtractScore(playedProject.steel, project.playerId, this.getScoreTrack(MarsResourceType.Steel, true));
    this.score.subtractScore(playedProject.titanium, project.playerId, this.getScoreTrack(MarsResourceType.Titanium, true));
    this.score.subtractScore(playedProject.psychrophiles, project.playerId, 'psychrophiles');
    this.score.subtractScore(playedProject.heat, project.playerId, this.getScoreTrack(MarsResourceType.Heat, true));
    var index = this.playerProjectHand.findIndex(p => p.cardId == playedProject.projectId);
    this.playerProjectHand.splice(index, 1);

    var card = this.decks.getProject(playedProject.projectId);
    card.tags.forEach((t) => {
      this.corporateTagReaction(t, project.playerId, card.tags, allPlayers, true);
    });

    this.corprateRefund(card, project.playerId);

    var projects = this.getPlayedReactionCards();
    projects.forEach((p) => {
      this.applyProjectTagReaction(p.reaction, card.tags, project.playerId, this.getProjectOwner(p.id), allPlayers);
    });
  }

  private corprateRefund(card: MarsProject, playerId: string) {
    var corp = this.getPlayerCorporation(playerId);
    corp.reaction.forEach((c) => {
      if (c.refund > 0 && c.condition != null) {
        if (c.condition.type == MarsPrereq.ProjectCost && card.cost >= c.condition.amount) {
          this.score.addScore(c.refund, playerId, this.getScoreTrack(MarsResourceType.Megacredits, true));
        }
        if (c.condition.type == MarsPrereq.VictoryCard && card.score != null && card.score.score >= 0) {
          this.score.addScore(c.refund, playerId, this.getScoreTrack(MarsResourceType.Megacredits, true));
        }
      }
    });
  }

  public applyProjectTagReaction(reaction: MarsReaction, tags: Array<MarsTags>, playerId: string, cardOwnerId: string, allPlayers: Array<GamePlayer>) {
    if (reaction.tagPlayed != null) {
      if (reaction.tagPlayed.resource.length > 0 && reaction.tagPlayed.resource[0].or.length > 0) {
        return;   // player choice needs to made in the front end to process this reaction
      }
      tags.forEach((t) => {
        if (reaction.tagPlayed.tag.some(p => p == t) && (reaction.tagPlayed.anyPlayer || playerId == cardOwnerId) &&
          (reaction.tagPlayed.additionalTag == MarsTags.None || tags.some(t => t == reaction.tagPlayed.additionalTag))) {
          reaction.tagPlayed.production.forEach((prod) => {
            this.applyProduction(prod, cardOwnerId, allPlayers);
          });
          reaction.tagPlayed.resource.forEach((r) => {
            this.applyResource(r, cardOwnerId, allPlayers);
          });
        }
      });
    }
  }

  public isHelion(id: string = this.playerState.getPlayerId()): boolean {
    var corp = this.getPlayerCorporation(id);
    if (corp.reaction.some(r => r.alternateCredits != null)) {
      return true;
    }

    return false;
  }

  public getUNMIPlayer(): string {
    var unmi = null;
    this.getAllPlayerCorporations().forEach((c) => {
      var action = this.getPlayerCorporation(c.playerId).action;
      if (action != null && action.resourceResult.length > 0 && action.resourceResult[0].resource == MarsResourceType.Terraforming) {
        unmi = c.playerId;
      }
    });

    return unmi;
  }

  public getSplicePlayer(): string {
    var splice = null;
    this.getAllPlayerCorporations().forEach((c) => {
      var corp = this.getPlayerCorporation(c.playerId);
      if (corp.name == 'Splice') {
        splice = c.playerId;
      }
    });

    return splice;
  }

  public getRecyclonPlayer(): string {
    var recyclon = null;
    this.getAllPlayerCorporations().forEach((c) => {
      var corp = this.getPlayerCorporation(c.playerId);
      if (corp.name == 'Recyclon') {
        recyclon = c.playerId;
      }
    });

    return recyclon;
  }

  public adaptationCount(): number {
    var value = 0;
    var corp = this.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.reaction.some(r => r.adaptation)) {
      value += 2;
    }

    if (this.getPlayersActionProjects(this.playerState.getPlayerId()).some(p => p.reaction != null && p.reaction.adaptation)) {
      value += 2;
    }

    return value;
  }

  private getAlternateCreditValue(tags: Array<MarsTags>, reaction: MarsReaction): number {
    if (reaction == null || reaction.alternateCredits == null) {
      return 0;
    }

    if (reaction.alternateCredits.forTag != MarsTags.None && !tags.some(t => t == reaction.alternateCredits.forTag)) {
      return 0;
    }

    return reaction.alternateCredits.amount * this.score.getScore(this.playerState.getPlayerId(),
      reaction.alternateCredits.scoreTrackId == null ? this.getScoreTrack(reaction.alternateCredits.resource, true) : reaction.alternateCredits.scoreTrackId);
  }

  public getAlternateCredits(tags: Array<MarsTags>): number {
    var corp = this.getPlayerCorporation(this.playerState.getPlayerId());
    var value = 0;
    corp.reaction.forEach((r) => {
      value += this.getAlternateCreditValue(tags, r);
    });

    var alternateCreditProjects = this.getPlayersActionProjects(this.playerState.getPlayerId()).filter(p => p.reaction != null && p.reaction.alternateCredits != null);
    alternateCreditProjects.forEach((p) => {
      value += this.getAlternateCreditValue(tags, p.reaction);
    });

    return value;
  }

  public getStandardProjectDiscount(production: MarsResourceCount): number {
    var discount = 0;
    if (production != null && production.resource == MarsResourceType.Energy) {
      var corp = this.getPlayerCorporation(this.playerState.getPlayerId());
      var discount = 0;
      corp.reaction.forEach((r) => {
        discount += this.getDiscountValue([MarsTags.Energy], r);
      });
    }

    return discount;
  }

  public getStandardProjectRefund(cost: number): number {
    var refund = 0;
    var corp = this.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.reaction.length > 0) {
      var standardRefund = corp.reaction.find(r => r.refund > 0 && r.condition != null && r.condition.type == MarsPrereq.StandardProject && cost >= r.condition.amount);
      if (standardRefund != null) {
        refund += standardRefund.refund;
      }
    }

    var project = this.getPlayersActionProjects(this.playerState.getPlayerId()).find(p => p.reaction != null && p.reaction.condition != null && p.reaction.condition.type == MarsPrereq.StandardProject);
    if (project != null) {
      refund += project.reaction.refund;
    }

    return refund;
  }

  public getProjectDiscount(tags: Array<MarsTags>): number {
    var corp = this.getPlayerCorporation(this.playerState.getPlayerId());
    var discount = 0;
    corp.reaction.forEach((r) => {
      discount += this.getDiscountValue(tags, r);
    });

    var projects = this.getPlayersActionProjects(this.playerState.getPlayerId()).filter(p => p.reaction != null && p.reaction.discount > 0);
    projects.forEach((p) => {
      discount += this.getDiscountValue(tags, p.reaction);
    });

    return discount;
  }

  private getDiscountValue(tags: Array<MarsTags>, reaction: MarsReaction): number {
    if (reaction == null || reaction.discount == 0) {
      return 0;
    }

    if (reaction.discountTag != MarsTags.None && !tags.some(t => t == reaction.discountTag)) {
      return 0;
    }

    return reaction.discount;
  }

  public isProtected(playerId: string): boolean {
    if (this.getPlayersActionProjects(playerId).some(p => p.reaction != null && p.reaction.protectedHabitat) && this.playerState.getPlayerId() != playerId) {
      return true;
    }

    return false;
  }

  public getSteelValue(): number {
    var value = 2;
    if (this.getPlayersActionProjects(this.playerState.getPlayerId()).some(p => p.reaction != null && p.reaction.alloys)) {
      value++;
    }

    return value;
  }

  public getTitaniumValue(): number {
    var value = 3;
    if (this.getPlayersActionProjects(this.playerState.getPlayerId()).some(p => p.reaction != null && p.reaction.alloys)) {
      value++;
    }

    var corp = this.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.reaction != null && corp.reaction.some(r => r.alloys)) {
      value++;
    }

    return value;
  }

  public getPsychrophileValue(): number {
    var reaction = this.getPlayersActionProjects(this.playerState.getPlayerId()).find(p => p.reaction != null && p.reaction.alternateCredits != null &&
      p.reaction.alternateCredits.forTag == MarsTags.Plant).reaction;
    return reaction.alternateCredits.amount;
  }

  public getPlayersActionProjects(playerId: string): Array<MarsActionProject> {
    var projects = this.getPlayerPlayedProjects(playerId);
    return projects.filter(c => c instanceof MarsActionProject).map(c => c as MarsActionProject);
  }

  public getPlayerNonActionProjects(playerId: string): Array<MarsProject> {
    var projects = this.getPlayerPlayedProjects(playerId);
    return projects.filter(c => !(c instanceof MarsActionProject) && !c.tags.some(t => t == MarsTags.Event));
  }

  public getPlayedProjects(): Array<MarsProject> {
    return this.decks.getProjects(this.playerProjectPlayed.map(c => c.cardId));
  }

  // this is both action and reaction projects
  public getPlayedActionProjects(): Array<MarsActionProject> {
    return this.getPlayedProjects().filter(c => c instanceof MarsActionProject).map(c => c as MarsActionProject);
  }

  // this is project cards with one or more actions
  public getPlayedActionCards(): Array<MarsActionProject> {
    var projects = this.getPlayedActionProjects();
    return projects.filter(p => p.action.length > 0);
  }

  // this is project cards with one or more reactions
  public getPlayedReactionCards(): Array<MarsActionProject> {
    var projects = this.getPlayedActionProjects();
    return projects.filter(p => p.reaction != null);
  }

  public getPlayerCorporation(playerId: string): MarsCorp {
    var playerCorp = this.playerCorps.find(c => c.playerId == playerId);
    return this.decks.getCorporation(playerCorp.cardId);
  }

  public getAllPlayerCorporations(): Array<MarsPlayerCard> {
    return this.playerCorps;
  }

  public getPlayerDealtPreludes(): Array<MarsPlayerCard> {
    return this.playerDealtPreludes;
  }

  public getPlayerDealtProjects(): Array<MarsPlayerCard> {
    return this.playerDealtProjects;
  }

  public getPlayerProjectHand(): Array<MarsPlayerCard> {
    return this.playerProjectHand;
  }

  public getPlayerPreludePlayed(): Array<MarsPlayerCard> {
    return this.playerPreludePlayed;
  }

  public getPlayerProjectPlayed(): Array<MarsPlayerCard> {
    return this.playerProjectPlayed;
  }

  public getClaimedMilestones(): Array<MarsPlayerCard> {
    return this.claimedMilestones;
  }

  public getClaimedAwards(): Array<MarsPlayerCard> {
    return this.claimedAwards;
  }

  public discardCorporation(selected: number) {
    var index = this.playerCorps.findIndex(c => c.cardId == selected);
    if (index > -1) {
      // remove the discarded corporation for this player
      this.playerCorps.splice(index, 1);
    }
  }

  public dealCorporations(corps: Array<number>, activePlayers: Array<GamePlayer>) {
    this.playerCorps = new Array<MarsPlayerCard>();
    activePlayers.forEach((p) => {
      this.playerCorps.push(new MarsPlayerCard(p.id, corps.pop()));
      this.playerCorps.push(new MarsPlayerCard(p.id, corps.pop()));
    })
  }

  public dealPreludes(cards: number, players: Array<GamePlayer>) {
    this.playerDealtPreludes = new Array<MarsPlayerCard>();
    players.forEach((p) => {
      for (var i = 0; i < cards; i++) {
        this.playerDealtPreludes.push(new MarsPlayerCard(p.id, this.decks.dealNextPrelude()));
      }
    });
  }

  public dealProjects(cards: number, players: Array<GamePlayer>, toHand: boolean = false, condition: MarsPrereqCondition = null) {
    if (!toHand) {
      this.playerDealtProjects = new Array<MarsPlayerCard>();
    }

    players.forEach((p) => {
      for (var i = 0; i < cards; i++) {
        if (toHand) {
          do {
            var nextCard = this.decks.dealNextProject();
          } while (condition != null && !this.dealCardCondition(nextCard, condition));
          this.playerProjectHand.push(new MarsPlayerCard(p.id, nextCard));
        }
        else {
          this.playerDealtProjects.push(new MarsPlayerCard(p.id, this.decks.dealNextProject()));
        }
      }
    });
  }

  private dealCardCondition(cardId: number, condition: MarsPrereqCondition): boolean {
    var card = this.decks.getProject(cardId);
    if (condition.type == MarsPrereq.Tags) {
      var keep = true;
      condition.tag.forEach((t) => {
        if (!card.tags.some(c => c == t)) {
          keep = false;
        }
      });

      if (!keep) {  // discard cards that don't match the condition
        this.discardProject(cardId);
      }

      return keep;
    }

    return true;
  }

  public discardPrelude(preludeId: number) {
    var index = this.playerDealtPreludes.findIndex(p => p.cardId == preludeId);
    if (index > -1) {
      this.playerDealtPreludes.splice(index, 1);
    }
  }

  public discardProject(projectId: number) {
    var index = this.playerDealtProjects.findIndex(p => p.cardId == projectId);
    if (index > -1) {
      this.playerDealtProjects.splice(index, 1);
    }

    index = this.playerProjectHand.findIndex(p => p.cardId == projectId);
    if (index > -1) {
      this.playerProjectHand.splice(index, 1);
    }

    this.decks.discardProject(projectId);
  }

  public purchaseCards(activePlayers: Array<GamePlayer>) {
    activePlayers.forEach((p) => {
      var cards = this.playerDealtProjects.filter(card => card.playerId == p.id).length;

      var megacredits = this.score.getScore(p.id, this.getScoreTrack(MarsResourceType.Megacredits, true));
      if (megacredits < cards * 3) {
        // Should only end up here if the player is helion
        this.score.subtractScore(megacredits, p.id, this.getScoreTrack(MarsResourceType.Megacredits, true));
        this.score.subtractScore(cards * 3 - megacredits, p.id, this.getScoreTrack(MarsResourceType.Heat, true))
      }
      else {
        this.score.subtractScore(cards * 3, p.id, this.getScoreTrack(MarsResourceType.Megacredits, true));
      }
    });
    this.keepCards();
  }

  public keepCards() {
    this.playerProjectHand = this.playerProjectHand.concat(this.playerDealtProjects);
    this.playerDealtProjects = new Array<MarsPlayerCard>();
  }

  public getScoreTrack(resource: MarsResourceType, supply: boolean): string {
    var track = supply ? 's-' : ''
    switch (resource) {
      case MarsResourceType.Megacredits:
        return track + MarsScoreTrack.Megacredits;
      case MarsResourceType.Steel:
        return track + MarsScoreTrack.Steel;
      case MarsResourceType.Titanium:
        return track + MarsScoreTrack.Titanium;
      case MarsResourceType.Plants:
        return track + MarsScoreTrack.Plants;
      case MarsResourceType.Energy:
        return track + MarsScoreTrack.Energy;
      case MarsResourceType.Heat:
        return track + MarsScoreTrack.Heat;
      case MarsResourceType.Terraforming:
        return MarsScoreTrack.Terraforming;
    }
  }

  public playPreludes(allPlayers: Array<GamePlayer>, firstPlayer: string, cardId: number = -1) {
    if (cardId != -1) {
      var prelude = this.playerDealtPreludes.find(p => p.cardId == cardId);
      this.playerPreludePlayed.push(prelude);
    }
    else {
      this.playerPreludePlayed = this.playerDealtPreludes;
    }
    this.playerDealtPreludes = new Array<MarsPlayerCard>();
    var startIndex = allPlayers.findIndex(p => p.id == firstPlayer);
    var index = startIndex;
    do {
      var preludes = this.playerPreludePlayed.filter(p => p.playerId == allPlayers[index].id && (cardId == -1 || p.cardId == cardId));
      preludes.sort((a, b) => { // make sure cards that give money get played before cards that take money
        var aCard = this.decks.getPrelude(a.cardId);
        var bCard = this.decks.getPrelude(b.cardId);
        if (aCard.resources.length > 0 && bCard.resources.length > 0) {
          var acredits = aCard.resources.find(r => r.resource == MarsResourceType.Megacredits);
          var bcredits = bCard.resources.find(r => r.resource == MarsResourceType.Megacredits);
          if (acredits != null && bcredits != null) {
            if (acredits.amount < 0 && bcredits.amount > 0) {
              return -1;
            }
          }
        }

        return 1;
      });
      preludes.forEach((c) => {
        var card = this.decks.getPrelude(c.cardId);
        card.production.forEach((p) => {
          this.applyProduction(p, allPlayers[index].id, allPlayers);
        });
        card.resources.forEach((r) => {
          this.applyResource(r, allPlayers[index].id, allPlayers);
        });
        card.tags.forEach((t) => {
          this.corporateTagReaction(t, allPlayers[index].id, card.tags, allPlayers, false);
        });
      });

      index++;
      if (index >= allPlayers.length) {
        index = 0;
      }
    } while (index != startIndex);
  }

  public playCorporations(allPlayers: Array<GamePlayer>, firstPlayer: string) {
    // play corporations in player order
    var startIndex = allPlayers.findIndex(p => p.id == firstPlayer);
    var index = startIndex;
    do {
      var corp = this.getPlayerCorporation(allPlayers[index].id);
      corp.production.forEach((prod) => {
        this.applyProduction(prod, allPlayers[index].id, allPlayers);
      });
      corp.resources.forEach((r) => {
        this.applyResource(r, allPlayers[index].id, allPlayers);
      });
      corp.tags.forEach((t) => {
        this.corporateTagReaction(t, allPlayers[index].id, corp.tags, allPlayers, false);
      });

      // special handling for Vitor
      if (corp.reaction.length > 0 && corp.reaction[0].condition != null && corp.reaction[0].condition.type == MarsPrereq.VictoryCard) {
        this.score.addScore(corp.reaction[0].refund, allPlayers[index].id, this.getScoreTrack(MarsResourceType.Megacredits, true));
      }

      index++;
      if (index >= allPlayers.length) {
        index = 0;
      }
    } while (index != startIndex);
  }

  public applyProduction(production: MarsResourceCount, playerId: string, allPlayers: Array<GamePlayer>) {
    if (production.perTile) {
      var tileCount = this.marsBoardState.getTotalTiles(production.tile, production.onMars);
      production.amount *= tileCount;
    }
    if (production.perTag) {
      var tagCount = 0;
      allPlayers.filter(a => (production.anyPlayer || production.onlyOpponents) && (!production.onlyOpponents || a.id != playerId) ||
        !production.anyPlayer && !production.onlyOpponents && a.id == playerId).forEach((p) => {
        tagCount += this.getTags(p.id, production.tag, p.id == playerId);
      });
      production.amount *= tagCount;
    }
    production.amount = Math.floor(production.amount);
    this.score.addScore(production.amount, playerId, this.getScoreTrack(production.resource, false));
  }

  public getPlayedResourceCards(token: MarsResourceType, withTokens: boolean, cardId: number, amount: number, anyCard: boolean): Array<MarsActionProject> {
    var cards = this.getPlayedActionProjects();
    if (this.getRecyclonPlayer() != null) {
      var corp = this.getPlayerCorporation(this.getRecyclonPlayer());
      cards.push(new MarsActionProject(MarsCorporationProject.Recyclon, 'Recyclon', '', corp.reaction[0], [], 0, [], [], []));
    }
    if (anyCard) {
      cards = cards.filter(c => (c.reaction != null && c.reaction.tagPlayed != null && (c.reaction.tagPlayed.resource.length > 0 && c.reaction.tagPlayed.resource[0].resource == token ||
        c.reaction.tilePlayed != null && c.reaction.tilePlayed.resource != null && c.reaction.tilePlayed.resource.resource == token) ||
        c.action.length > 0 && c.action[0].resourceResult.length > 0 && c.action[0].resourceResult[0].resource == token) && c.id != cardId);
    }
    else {
      cards = cards.filter(c => c.id == cardId);
    }
    cards = cards.filter(c => !withTokens || this.score.getScore(this.getProjectOwner(c.id), this.getProjectScoreTrack(c)) >= amount);
    cards = cards.filter(c => c.resources.length > 0 && !c.resources[0].protectedAnimals || !this.isProtected(this.getProjectOwner(c.id)));

    return cards;
  }

  public applyResource(resource: MarsResourceCount, playerId: string, allPlayers: Array<GamePlayer>, scoreTrackId: string = null) {
    if (resource.perTile) {
      var tileCount = this.marsBoardState.getTotalTiles(resource.tile, resource.onMars);
      resource.amount *= tileCount;
    }
    if (resource.perTag) {
      var tagCount = 0;
      allPlayers.filter(a => resource.anyPlayer || a.id == playerId).forEach((p) => {
        tagCount += this.getTags(p.id, resource.tag, p.id == playerId);
      });
      resource.amount *= tagCount;
    }
    resource.amount = Math.floor(resource.amount);

    if (resource.resource == MarsResourceType.AnimalToken || resource.resource == MarsResourceType.MicrobeToken || resource.resource == MarsResourceType.ScienceToken ||
      resource.resource == MarsResourceType.FleetToken) {
      if (resource.tokenScoreTrackId != null || scoreTrackId != null) {
        this.score.addScore(resource.amount, playerId, scoreTrackId != null ? scoreTrackId : resource.tokenScoreTrackId);
      }
    }
    else if (resource.resource == MarsResourceType.Temperature) {
      var currentTemp = this.score.getScore('global', MarsScoreTrack.Temperature);
      this.score.addScore(resource.amount * 2, 'global', MarsScoreTrack.Temperature);
      if (currentTemp < -24 && currentTemp + resource.amount * 2 >= -24) {
        this.score.addScore(1, playerId, this.getScoreTrack(MarsResourceType.Heat, false));
      }
      if (currentTemp < -20 && currentTemp + resource.amount * 2 >= -20) {
        this.score.addScore(1, playerId, this.getScoreTrack(MarsResourceType.Heat, false));
      }
      var newTemp = this.score.getScore('global', MarsScoreTrack.Temperature);
      var change = (newTemp - currentTemp) / 2;
      this.score.addScore(change, playerId, MarsScoreTrack.Terraforming);
    }
    else if (resource.resource == MarsResourceType.Oxygen) {
      var currentOxygen = this.score.getScore('global', MarsScoreTrack.Oxygen);
      this.score.addScore(resource.amount, 'global', MarsScoreTrack.Oxygen);
      if (currentOxygen < 8 && currentOxygen + resource.amount >= 8) {
        this.applyResource(new MarsResourceCount(MarsResourceType.Temperature, 1), playerId, allPlayers);
      }
      var newOxygen = this.score.getScore('global', MarsScoreTrack.Oxygen);
      var change = newOxygen - currentOxygen;
      this.score.addScore(change, playerId, MarsScoreTrack.Terraforming);
    }
    else if (resource.resource == MarsResourceType.Project) {
      if (resource.keepProject > 0) {
        this.dealProjects(resource.amount, [{ name: 'x', id: playerId }], resource.keepProject >= resource.amount && !resource.mustPurchase, resource.condition);
      }
      else if (resource.condition != null) { // search for life
        var life = this.decks.dealNextProject();
        var card = this.decks.getProject(life);
        if (resource.condition.type == MarsPrereq.Tags) {
          if (card.tags.some(t => resource.condition.tag.some(r => r == t))) {
            this.applyResource(resource.or[0], playerId, allPlayers);
          }
        }

        this.decks.discardProject(life);
      }
    }
    else if (resource.resource == MarsResourceType.Prelude) {
      this.dealPreludes(3, [{name: 'x', id: playerId}]);
    }
    else {
      this.score.addScore(resource.amount, playerId, this.getScoreTrack(resource.resource, true));
    }
  }

  private corporateTagReaction(tag: MarsTags, playerId: string, tags: Array<MarsTags>, allPlayers: Array<GamePlayer>, project: boolean) {
    for (var i = 0; i < this.playerCorps.length; i++) {
      var corporation = this.decks.getCorporation(this.playerCorps[i].cardId);
      corporation.reaction.forEach((r) => {
        if (r.tagPlayed != null && (r.tagPlayed.anyPlayer || playerId == this.playerCorps[i].playerId)) {
          this.applyCorpTagReaction(tag, tags, this.playerCorps[i].playerId, r, allPlayers, project, playerId);
        }
      });
    }
  }

  private applyCorpTagReaction(tag: MarsTags, tags: Array<MarsTags>, playerId: string, reaction: MarsReaction, allPlayers: Array<GamePlayer>, project: boolean, triggerPlayerId: string) {
    if (this.tagPlayed(tag, reaction.tagPlayed) &&
      (reaction.tagPlayed.additionalTag == MarsTags.None || tags.some(t => t == reaction.tagPlayed.additionalTag))) {
      if (reaction.tagPlayed.production != null) {
        reaction.tagPlayed.production.forEach((prod) => {
          this.applyProduction(prod, playerId, allPlayers);
        });
      }

      if (reaction.tagPlayed.resource != null) {
        reaction.tagPlayed.resource.forEach((res) => {
          if (res.tokenScoreTrackId != null) {
            // just assume recyclon for now
            if (this.score.getScore(playerId, res.tokenScoreTrackId) >= 2) {
              this.score.addScore(-2, playerId, res.tokenScoreTrackId);
              this.applyProduction(new MarsResourceCount(MarsResourceType.Plants, 1), playerId, allPlayers);
              return;
            }
          }

          // Splice corp reaction for other players
          if (this.getSplicePlayer() != null && !project && tag == MarsTags.Microbe) {
            this.applyResource(new MarsResourceCount(MarsResourceType.Megacredits, 2), triggerPlayerId, allPlayers);
          }

          this.applyResource(res, playerId, allPlayers);
        });
      }
    }
  }

  private tagPlayed(tag: MarsTags, tagReaction: MarsTagPlayed): boolean {
    if (tagReaction == null) {
      return false;
    }

    return tagReaction.tag.some(t => t == tag);
  }
}
