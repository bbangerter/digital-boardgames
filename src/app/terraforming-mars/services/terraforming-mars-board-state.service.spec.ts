import { TestBed } from '@angular/core/testing';

import { TerraformingMarsBoardStateService } from './terraforming-mars-board-state.service';

describe('TerraformingMarsBoardStateService', () => {
  let service: TerraformingMarsBoardStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TerraformingMarsBoardStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
