import { Injectable } from '@angular/core';
import { CanvasStateService, GfxLines, GfxRectangle, PlayerScoreService } from '../../core';
import { MarsTileLocation, MarsResourceCount, MarsPlayedTile, MarsPlayerColor } from '../dtos/terraforming-mars-dtos.model';
import { MarsResourceType, MarsTiles, MarsMap, MarsScoreTrack } from '../terraforming-mars-constants';

@Injectable({
  providedIn: 'root'
})
export class TerraformingMarsBoardStateService {
  canvasId: string = 'tm-canvas';
  tileLocations: Array<MarsTileLocation>;
  tilesPlayed: Array<MarsPlayedTile>;

  private boardId: number;
  private map: MarsMap;
  constructor(private canvas: CanvasStateService, private score: PlayerScoreService) { }

  public createBoard(map: MarsMap): number {
    switch (map) {
      case MarsMap.Base:
        this.boardId = this.canvas.createTexture(this.canvasId, './assets/terraforming-mars/mars.jpg', 0).getId();
        break;
      case MarsMap.Hellas:
        this.boardId = this.canvas.createTexture(this.canvasId, './assets/terraforming-mars/hellas.jpg', 0).getId();
        break;
      case MarsMap.Elysium:
        this.boardId = this.canvas.createTexture(this.canvasId, './assets/terraforming-mars/elysium.jpg', 0).getId();
        break;
    }

    this.map = map;
    
    return this.boardId;
  }

  public claimAwardOrMilestone(color: string, gfxId: number) {
    var marker = this.createPlayerMarker(color, this.boardId);
    var gfx = this.canvas.getGfxObject(this.canvasId, gfxId);
    marker.translate(gfx.getXPosition() + 35, gfx.getYPosition() + 5);
  }

  public createPlayerMarker(color: string, parent: number): GfxRectangle {
    return this.canvas.createRectangle(this.canvasId, 10, 10, color, true, 2, parent);
  }

  public getTileIdByGfxId(objectId: number, tile: MarsTiles, playerId: string, generation: number): number {
    var locations = this.validTileLocations(tile, playerId, generation);
    var location = locations.find(l => l.gfx.getId() == objectId);
    if (location != null) {
      return location.id;
    }
  }

  public getCityTiles(): Array<MarsPlayedTile> {
    return this.tilesPlayed.filter(t => t.tile == MarsTiles.City || t.tile == MarsTiles.Capital || t.tile == MarsTiles.LavaTube || t.tile == MarsTiles.Noctis ||
      t.tile == MarsTiles.ResearchOutpost || t.tile == MarsTiles.UrbanizedArea);
  }

  public placeTile(tile: MarsPlayedTile, colors: Array<MarsPlayerColor>): Array<MarsResourceCount> {
    switch (tile.tile) {
      case MarsTiles.Capital:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 7, this.boardId).getId();
        break;
      case MarsTiles.AdjacentMining:
      case MarsTiles.Mining:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 9, this.boardId).getId();
        break;
      case MarsTiles.ArtificialLake:
      case MarsTiles.Ocean:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 4, this.boardId).getId();
        break;
      case MarsTiles.City:
      case MarsTiles.Ganymede:
      case MarsTiles.LavaTube:
      case MarsTiles.Noctis:
      case MarsTiles.Phobos:
      case MarsTiles.ResearchOutpost:
      case MarsTiles.UrbanizedArea:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 3, this.boardId).getId();
        break;
      case MarsTiles.Commercial:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 5, this.boardId).getId();
        break;
      case MarsTiles.EcologicalZone:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 12, this.boardId).getId();
        break;
      case MarsTiles.Greenery:
      case MarsTiles.Mangrove:
      case MarsTiles.ProtectedValley:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 2, this.boardId).getId();
        break;
      case MarsTiles.Industrial:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 6, this.boardId).getId();
        break;
      case MarsTiles.LavaFlows:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 0, this.boardId).getId();
        break;
      case MarsTiles.CommunityArea:
      case MarsTiles.LandClaim:
        tile.gfxId = this.canvas.createContainer(this.canvasId, this.boardId).getId();
        break;
      case MarsTiles.Mohole:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 8, this.boardId).getId();
        break;
      case MarsTiles.NaturalPreserve:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 1, this.boardId).getId();
        break;
      case MarsTiles.NuclearZone:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 13, this.boardId).getId();
        break;
      case MarsTiles.Restricted:
        tile.gfxId = this.canvas.createTextureAtlas(this.canvasId, './assets/terraforming-mars/tiles.png', 3, 5, 14, this.boardId).getId();
        break;
    }

    var location = this.tileLocations.find(t => t.id == tile.locationId);
    this.canvas.getGfxObject(this.canvasId, tile.gfxId).translate(location.gfx.getXPosition(), location.gfx.getYPosition());
    if (tile.tile != MarsTiles.Ocean && tile.tile != MarsTiles.ArtificialLake) {
      this.createPlayerMarker(colors.find(c => c.playerId == tile.playerId).color, tile.gfxId).translate(25, 30);
    }

    var landClaim = this.tilesPlayed.findIndex(t => t.locationId == tile.locationId && t.tile == MarsTiles.LandClaim);
    if (landClaim != -1) {
      // remove land claim tile and marker
      this.canvas.removeGfxObject(this.canvasId, this.tilesPlayed[landClaim].gfxId);
      this.tilesPlayed.splice(landClaim, 1);
    }
    var communityMarker = this.tilesPlayed.findIndex(t => t.locationId == tile.locationId && t.tile == MarsTiles.CommunityArea);
    if (communityMarker != -1) {
      // remove land claim tile and marker
      this.canvas.removeGfxObject(this.canvasId, this.tilesPlayed[communityMarker].gfxId);
      this.tilesPlayed.splice(communityMarker, 1);
      location.bonus.push(new MarsResourceCount(MarsResourceType.Megacredits, 3));
    }

    this.tilesPlayed.push(tile);

    var adjacentOceans = 0;
    if (tile.tile != MarsTiles.LandClaim && tile.tile != MarsTiles.CommunityArea) {
      adjacentOceans = this.getTiles(location.adjacentIds).filter(t => t.tile == MarsTiles.Ocean || t.tile == MarsTiles.ArtificialLake).length;
    }
    location.bonus.push(new MarsResourceCount(MarsResourceType.Megacredits, adjacentOceans * 2));
    return location.bonus;
  }

  public highlightTileSelection(tile: MarsTiles, playerId: string, generation: number = -1) {
    var openTiles = this.validTileLocations(tile, playerId, generation);
    openTiles.forEach((t) => {
      t.gfx.setVisible(true);
    });
  }

  public hideTileSelection() {
    this.tileLocations.forEach((t) => {
      t.gfx.setVisible(false);
    });
  }

  public getTilesAdjacentTo(centerTile: MarsTiles, adjacentTile: MarsTiles): number {
    var tile = this.findPlayedTile(centerTile);
    if (tile == null) {
      return 0;
    }

    var adjacentTiles = this.getTiles(this.tileLocations.find(l => l.id == tile.locationId).adjacentIds);
    if (adjacentTile == MarsTiles.City) {
      return adjacentTiles.filter(a => a.tile == MarsTiles.City || a.tile == MarsTiles.Capital || a.tile == MarsTiles.LavaTube || a.tile == MarsTiles.Noctis ||
        a.tile == MarsTiles.ResearchOutpost || a.tile == MarsTiles.UrbanizedArea).length;
    }
    if (adjacentTile == MarsTiles.Greenery) {
      return adjacentTiles.filter(a => a.tile == MarsTiles.Greenery || a.tile == MarsTiles.Mangrove || a.tile == MarsTiles.ProtectedValley).length;
    }
    if (adjacentTile == MarsTiles.Ocean) {
      return adjacentTiles.filter(a => a.tile == MarsTiles.Ocean || a.tile == MarsTiles.ArtificialLake).length;
    }

    return 0;
  }

  private findPlayedTile(tile: MarsTiles): MarsPlayedTile {
    return this.tilesPlayed.find(t => t.tile == tile)
  }

  public getTotalTiles(tile: MarsTiles, onMars: boolean, playerId: string = 'any'): number {
    var list = [];
    if (tile == MarsTiles.City) {
      list = this.tilesPlayed.filter(p => p.tile == MarsTiles.City || p.tile == MarsTiles.Capital || p.tile == MarsTiles.LavaTube || p.tile == MarsTiles.Noctis ||
        p.tile == MarsTiles.ResearchOutpost || p.tile == MarsTiles.UrbanizedArea || !onMars && (p.tile == MarsTiles.Phobos || p.tile == MarsTiles.Ganymede));
    }
    if (tile == MarsTiles.Greenery) {
      list = this.tilesPlayed.filter(p => p.tile == MarsTiles.Greenery || p.tile == MarsTiles.Mangrove || p.tile == MarsTiles.ProtectedValley);
    }
    if (tile == MarsTiles.Any) {
      list = this.tilesPlayed.filter(p => (!onMars || p.tile != MarsTiles.Phobos && p.tile != MarsTiles.Ganymede) && p.tile != MarsTiles.Ocean && p.tile != MarsTiles.ArtificialLake);
    }

    return list.filter(p => playerId == 'any' || playerId == p.playerId).length;
  }

  public validTileLocations(tile: MarsTiles, playerId: string, generation: number = -1): Array<MarsTileLocation> {
    var openLocations = this.getOpenLocations(playerId);

    if (tile == MarsTiles.Capital || tile == MarsTiles.City || tile == MarsTiles.Noctis && this.tileLocations.filter(l => l.noctis).length == 0 ||
      tile == MarsTiles.LavaTube && this.tileLocations.filter(l => l.volcano).length == 0) {
      openLocations = openLocations.filter(t => !t.ocean && !t.noctis &&
        !this.getTiles(t.adjacentIds).some(p => p.tile == MarsTiles.City || p.tile == MarsTiles.Capital || p.tile == MarsTiles.LavaTube || p.tile == MarsTiles.Noctis ||
          p.tile == MarsTiles.ResearchOutpost || p.tile == MarsTiles.UrbanizedArea));
    }
    else if (tile == MarsTiles.LavaTube) {
      openLocations = openLocations.filter(t => t.volcano);
    }
    else if (tile == MarsTiles.Greenery || tile == MarsTiles.Restricted || tile == MarsTiles.LandClaim || tile == MarsTiles.EcologicalZone || tile == MarsTiles.Commercial ||
      tile == MarsTiles.NuclearZone || tile == MarsTiles.ArtificialLake || tile == MarsTiles.CommunityArea) {
      openLocations = openLocations.filter(t => !t.ocean && !t.noctis);
      if (tile == MarsTiles.Greenery) {
        // find areas adjacent to existing player pieces if possible, if none, then anywhere is fine
        var adjacentOpenLocations = openLocations.filter(t => this.getTiles(t.adjacentIds).some(p => p.playerId == playerId && p.tile != MarsTiles.Ocean && p.tile != MarsTiles.ArtificialLake &&
          p.tile != MarsTiles.LandClaim && p.tile != MarsTiles.CommunityArea));
        if (adjacentOpenLocations.length > 0) {
          openLocations = adjacentOpenLocations;
        }
      }
      else if (tile == MarsTiles.CommunityArea && generation != 1) {
        // find areas adjacent to player pieces or existing community areas
        openLocations = openLocations.filter(t => t.tile != MarsTiles.CommunityArea);
        openLocations = openLocations.filter(t => this.getTiles(t.adjacentIds, false, false).some(p => p.playerId == playerId && p.tile != MarsTiles.Ocean && p.tile != MarsTiles.ArtificialLake &&
          p.tile != MarsTiles.LandClaim));
      }
      else if (tile == MarsTiles.EcologicalZone) {
        openLocations = openLocations.filter(t => this.getTiles(t.adjacentIds).some(p => p.tile == MarsTiles.Greenery || p.tile == MarsTiles.Mangrove || p.tile == MarsTiles.ProtectedValley));
      }
    }
    else if (tile == MarsTiles.Ocean || tile == MarsTiles.Mohole || tile == MarsTiles.ProtectedValley || tile == MarsTiles.Mangrove) {
      openLocations = openLocations.filter(t => t.ocean);
    }
    else if (tile == MarsTiles.Industrial) {
      openLocations = openLocations.filter(t => !t.ocean && !t.noctis &&
        this.getTiles(t.adjacentIds).some(p => p.tile == MarsTiles.City || p.tile == MarsTiles.Capital || p.tile == MarsTiles.LavaTube || p.tile == MarsTiles.Noctis ||
          p.tile == MarsTiles.ResearchOutpost || p.tile == MarsTiles.UrbanizedArea));
    }
    else if (tile == MarsTiles.Mining || tile == MarsTiles.AdjacentMining) {
      openLocations = openLocations.filter(t => !t.ocean && !t.noctis && t.bonus.some(b => b.resource == MarsResourceType.Steel || b.resource == MarsResourceType.Titanium));
      if (tile == MarsTiles.AdjacentMining) {
        openLocations = openLocations.filter(t => this.getTiles(t.adjacentIds, true).some(p => p.playerId == playerId));
      }
    }
    else if (tile == MarsTiles.NaturalPreserve || tile == MarsTiles.ResearchOutpost) {
      openLocations = openLocations.filter(t => !t.ocean && !t.noctis && this.getTiles(t.adjacentIds).length == 0);
    }
    else if (tile == MarsTiles.LavaFlows) {
      if (this.tileLocations.filter(l => l.volcano).length > 0) {
        openLocations = openLocations.filter(t => t.volcano);
      }
      else {
        openLocations = openLocations.filter(t => !t.ocean);
      }
    }
    else if (tile == MarsTiles.UrbanizedArea) {
      openLocations = openLocations.filter(t => !t.ocean && !t.noctis &&
        this.getTiles(t.adjacentIds).filter(p => p.tile == MarsTiles.City || p.tile == MarsTiles.Capital || p.tile == MarsTiles.LavaTube || p.tile == MarsTiles.Noctis ||
          p.tile == MarsTiles.ResearchOutpost || p.tile == MarsTiles.UrbanizedArea).length >= 2);
    }
    else if (tile == MarsTiles.Noctis) {
      openLocations = openLocations.filter(t => t.noctis);
    }

    if (tile != MarsTiles.Ganymede && tile != MarsTiles.Phobos) {
      openLocations = openLocations.filter(t => !t.phobos && !t.ganymede);
    }

    if (tile == MarsTiles.Ganymede) {
      openLocations = openLocations.filter(t => t.ganymede);
    }
    else if (tile == MarsTiles.Phobos) {
      openLocations = openLocations.filter(t => t.phobos);
    }

    // Tile locations that cost credits
    var credits = this.score.getScore(playerId, 's-' + MarsScoreTrack.Megacredits);
    openLocations = openLocations.filter((t) => {
      var cost = t.bonus.find(b => b.resource == MarsResourceType.Megacredits);
      if (cost == null || tile == MarsTiles.LandClaim || tile == MarsTiles.CommunityArea) {
        return true;
      }
      return cost.amount + credits >= 0;
    });

    return openLocations;
  }

  public getAdjacentTiles(location: number): Array<MarsPlayedTile> {
    return this.getTiles(this.tileLocations.find(l => l.id == location).adjacentIds);
  }

  public getAdjacentGreeneries(location: number): Array<MarsPlayedTile> {
    return this.getAdjacentTiles(location).filter(t => t.tile == MarsTiles.Greenery || t.tile == MarsTiles.ProtectedValley || t.tile == MarsTiles.Mangrove);
  }

  public getTiles(locationIds: Array<number>, excludeOceans: boolean = false, excludeCommunity: boolean = true): Array<MarsPlayedTile> {
    return this.tilesPlayed.filter(t => locationIds.some(a => a == t.locationId) && t.tile != MarsTiles.LandClaim && (!excludeCommunity || t.tile != MarsTiles.CommunityArea) &&
      (!excludeOceans || t.tile != MarsTiles.Ocean && t.tile != MarsTiles.ArtificialLake));
  }

  private getOpenLocations(playerId: string): Array<MarsTileLocation> {
    var takenIds = this.tilesPlayed.filter(t => t.playerId != playerId || (t.tile != MarsTiles.LandClaim && t.tile != MarsTiles.CommunityArea)).map(i => i.locationId);
    return this.tileLocations.filter(l => !takenIds.some(t => t == l.id));
  }

  public initializeBoard(parent: number, map: MarsMap) {
    this.tilesPlayed = new Array<MarsPlayedTile>();
    this.tileLocations = new Array<MarsTileLocation>();

    if (map == MarsMap.Base) {
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 317, 145), [1, 5, 6], [new MarsResourceCount(MarsResourceType.Steel, 2)]));       // 0
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 380, 145), [0, 2, 6, 7], [new MarsResourceCount(MarsResourceType.Steel, 2)], true));     // 1
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 442, 145), [1, 3, 7, 8]));                                                               // 2
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 506, 145), [2, 4, 8, 9], [new MarsResourceCount(MarsResourceType.Project, 1)], true));   // 3
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 568, 145), [3, 9, 10], [], true));                                                     // 4

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 286, 200), [0, 6, 11, 12]));                                                               // 5
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 348, 200), [0, 1, 5, 7, 12, 13], [new MarsResourceCount(MarsResourceType.Steel, 1)], false, false, true)); // 6
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 411, 200), [1, 2, 6, 8, 13, 14]));                                                               // 7
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 474, 200), [2, 3, 7, 9, 14, 15]));                                                               // 8
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 537, 200), [3, 4, 8, 10, 15, 16]));                                                               // 9
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 600, 200), [4, 9, 16, 17], [new MarsResourceCount(MarsResourceType.Project, 2)], true));   // 10

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 254, 254), [5, 12, 18, 19], [new MarsResourceCount(MarsResourceType.Project, 1)], false, false, true)); // 11
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 317, 254), [5, 6, 11, 13, 19, 20]));                                                               // 12
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 380, 254), [6, 7, 12, 14, 20, 21]));                                                               // 13
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 442, 254), [7, 8, 13, 15, 21, 22]));                                                               // 14
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 506, 254), [8, 9, 14, 16, 22, 23]));                                                               // 15
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 568, 254), [9, 10, 15, 17, 23, 24]));                                                               // 16
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 631, 254), [10, 16, 24, 25], [new MarsResourceCount(MarsResourceType.Steel, 1)]));           // 17

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 221, 308), [11, 19, 26, 27], [new MarsResourceCount(MarsResourceType.Titanium, 1),           // 18
      new MarsResourceCount(MarsResourceType.Plants, 1)], false, false, true));
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 286, 308), [11, 12, 18, 20, 27, 28], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 19
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 348, 308), [12, 13, 19, 21, 28, 29], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 20
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 411, 308), [13, 14, 20, 22, 29, 30], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 21
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 474, 308), [14, 15, 21, 23, 30, 31], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 22
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 537, 308), [15, 16, 22, 24, 31, 32], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 23
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 600, 308), [16, 17, 23, 25, 32, 33], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 24
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 663, 308), [17, 25, 33, 34], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));    // 25

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 191, 363), [18, 27, 35], [new MarsResourceCount(MarsResourceType.Plants, 2)], false, false, true)); // 26
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 254, 363), [18, 19, 26, 28, 35, 36], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 27
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 317, 363), [19, 20, 27, 29, 36, 37], [new MarsResourceCount(MarsResourceType.Plants, 2)], false, true)); // 28
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 380, 363), [20, 21, 28, 30, 37, 38], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));    // 29
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 442, 363), [21, 22, 29, 31, 38, 39], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));    // 30
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 506, 363), [22, 23, 30, 32, 39, 40], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));    // 31
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 568, 363), [23, 24, 31, 33, 40, 41], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 32
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 631, 363), [24, 25, 32, 34, 41, 42], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 33
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 694, 363), [25, 33, 42], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 34

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 223, 417), [26, 27, 36, 43], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 35
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 286, 417), [27, 28, 35, 37, 43, 44], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 36
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 348, 417), [28, 29, 36, 38, 44, 45], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 37
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 411, 417), [29, 30, 37, 39, 45, 46], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 38
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 474, 417), [30, 31, 38, 40, 46, 47], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 39
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 537, 417), [31, 32, 39, 41, 47, 48], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));    // 40
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 600, 417), [32, 33, 40, 42, 48, 49], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));    // 41
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 663, 417), [33, 34, 41, 49], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));    // 42

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 254, 471), [35, 36, 44, 50]));                                                               // 43
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 317, 471), [36, 37, 43, 45, 50, 51]));                                                               // 44
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 380, 471), [37, 38, 44, 46, 51, 52]));                                                               // 45
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 442, 471), [38, 39, 45, 47, 52, 53]));                                                               // 46
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 506, 471), [39, 40, 46, 48, 53, 54]));                                                               // 47
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 568, 471), [40, 41, 47, 49, 54, 55], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 48
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 631, 471), [41, 42, 48, 55]));                                                               // 49

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 286, 527), [43, 44, 51, 56], [new MarsResourceCount(MarsResourceType.Steel, 2)]));           // 50
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 348, 527), [44, 45, 50, 52, 56, 57]));                                                               // 51
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 411, 527), [45, 46, 51, 53, 57, 58], [new MarsResourceCount(MarsResourceType.Project, 1)]));         // 52
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 474, 527), [46, 47, 52, 54, 58, 59], [new MarsResourceCount(MarsResourceType.Project, 1)]));         // 53
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 537, 527), [47, 48, 53, 55, 59, 60]));                                                               // 54
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 600, 527), [48, 49, 54, 60], [new MarsResourceCount(MarsResourceType.Titanium, 1)]));        // 55

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 317, 580), [50, 51, 57], [new MarsResourceCount(MarsResourceType.Steel, 1)]));           // 56
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 380, 580), [51, 52, 56, 58], [new MarsResourceCount(MarsResourceType.Steel, 2)]));           // 57
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 442, 580), [52, 53, 57, 59]));                                                               // 58
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 506, 580), [53, 54, 58, 60]));                                                               // 59
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 568, 580), [54, 55, 59], [new MarsResourceCount(MarsResourceType.Titanium, 2)], true));  // 60
    }
    else if (map == MarsMap.Hellas) {
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 149), [1, 5, 6], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));       // 0
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 149), [0, 2, 6, 7], [new MarsResourceCount(MarsResourceType.Plants, 2)], false));     // 1
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 149), [1, 3, 7, 8], [new MarsResourceCount(MarsResourceType.Plants, 2)], false));                                                               // 2
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 149), [2, 4, 8, 9],
        [new MarsResourceCount(MarsResourceType.Steel, 1), new MarsResourceCount(MarsResourceType.Plants, 1)], false));   // 3
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 149), [3, 9, 10], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));                                                     // 4

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 204), [0, 6, 11, 12], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));                                                               // 5
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 204), [0, 1, 5, 7, 12, 13], [new MarsResourceCount(MarsResourceType.Plants, 2)], false)); // 6
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 204), [1, 2, 6, 8, 13, 14], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));                                                               // 7
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 204), [2, 3, 7, 9, 14, 15],
        [new MarsResourceCount(MarsResourceType.Plants, 1), new MarsResourceCount(MarsResourceType.Steel, 1)], false));                                                               // 8
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 204), [3, 4, 8, 10, 15, 16], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));                                                               // 9
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 204), [4, 9, 16, 17], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));   // 10

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 255, 258), [5, 12, 18, 19], [new MarsResourceCount(MarsResourceType.Plants, 1)], true)); // 11
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 258), [5, 6, 11, 13, 19, 20], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));                                                               // 12
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 258), [6, 7, 12, 14, 20, 21], [new MarsResourceCount(MarsResourceType.Steel, 1)], false));                                                               // 13
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 258), [7, 8, 13, 15, 21, 22], [new MarsResourceCount(MarsResourceType.Steel, 1)], false));                                                               // 14
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 258), [8, 9, 14, 16, 22, 23], [], false));                                                               // 15
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 258), [9, 10, 15, 17, 23, 24], [new MarsResourceCount(MarsResourceType.Plants, 2)], false));                                                               // 16
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 632, 258), [10, 16, 24, 25],
        [new MarsResourceCount(MarsResourceType.Plants, 1), new MarsResourceCount(MarsResourceType.Project, 1)], false));           // 17

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 224, 312), [11, 19, 26, 27], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));           // 18
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 312), [11, 12, 18, 20, 27, 28], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));          // 19
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 312), [12, 13, 19, 21, 28, 29], [new MarsResourceCount(MarsResourceType.Steel, 1)], false));          // 20
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 312), [13, 14, 20, 22, 29, 30], [new MarsResourceCount(MarsResourceType.Steel, 2)], false));          // 21
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 312), [14, 15, 21, 23, 30, 31], [new MarsResourceCount(MarsResourceType.Steel, 1)], false));          // 22
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 312), [15, 16, 22, 24, 31, 32], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));          // 23
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 312), [16, 17, 23, 25, 32, 33], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));          // 24
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 664, 312), [17, 25, 33, 34], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));    // 25

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 192, 367), [18, 27, 35], [new MarsResourceCount(MarsResourceType.Project, 1)], false)); // 26
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 255, 367), [18, 19, 26, 28, 35, 36], [], false));          // 27
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 367), [19, 20, 27, 29, 36, 37], [], false)); // 28
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 367), [20, 21, 28, 30, 37, 38], [new MarsResourceCount(MarsResourceType.Steel, 2)], false));    // 29
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 367), [21, 22, 29, 31, 38, 39], [], false));    // 30
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 367), [22, 23, 30, 32, 39, 40], [new MarsResourceCount(MarsResourceType.Project, 1)], true));    // 31
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 367), [23, 24, 31, 33, 40, 41], [new MarsResourceCount(MarsResourceType.Heat, 3)], true));          // 32
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 632, 367), [24, 25, 32, 34, 41, 42], [], true));          // 33
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 695, 367), [25, 33, 42], [new MarsResourceCount(MarsResourceType.Plants, 1)], false));          // 34

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 224, 419), [26, 27, 36, 43], [new MarsResourceCount(MarsResourceType.Titanium, 1)], false));          // 35
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 419), [27, 28, 35, 37, 43, 44], [], false));          // 36
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 419), [28, 29, 36, 38, 44, 45], [new MarsResourceCount(MarsResourceType.Steel, 1)], false));          // 37
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 419), [29, 30, 37, 39, 45, 46], [], false));          // 38
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 419), [30, 31, 38, 40, 46, 47], [], false));          // 39
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 419), [31, 32, 39, 41, 47, 48], [], true));    // 40
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 419), [32, 33, 40, 42, 48, 49], [new MarsResourceCount(MarsResourceType.Steel, 1)], true));    // 41
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 664, 419), [33, 34, 41, 49], [], false));    // 42

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 255, 475), [35, 36, 44, 50], [new MarsResourceCount(MarsResourceType.Titanium, 2)], true));                                                               // 43
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 475), [36, 37, 43, 45, 50, 51], [], false));                                                               // 44
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 475), [37, 38, 44, 46, 51, 52], [], false));                                                               // 45
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 475), [38, 39, 45, 47, 52, 53], [new MarsResourceCount(MarsResourceType.Project, 1)], false));                                                               // 46
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 475), [39, 40, 46, 48, 53, 54], [], false));                                                               // 47
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 475), [40, 41, 47, 49, 54, 55], [], false));          // 48
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 632, 475), [41, 42, 48, 55], [new MarsResourceCount(MarsResourceType.Titanium, 1)], false));                                                               // 49

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 531), [43, 44, 51, 56], [new MarsResourceCount(MarsResourceType.Steel, 1)], false));           // 50
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 531), [44, 45, 50, 52, 56, 57], [new MarsResourceCount(MarsResourceType.Project, 1)], false));                                                               // 51
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 531), [45, 46, 51, 53, 57, 58], [new MarsResourceCount(MarsResourceType.Heat, 2)], false));                                                               // 52
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 531), [46, 47, 52, 54, 58, 59], [new MarsResourceCount(MarsResourceType.Heat, 2)], false));         // 53
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 531), [47, 48, 53, 55, 59, 60], [new MarsResourceCount(MarsResourceType.Titanium, 1)], false));                                                               // 54
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 531), [48, 49, 54, 60], [new MarsResourceCount(MarsResourceType.Titanium, 1)], false));        // 55

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 584), [50, 51, 57], [], false));           // 56
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 584), [51, 52, 56, 58], [new MarsResourceCount(MarsResourceType.Heat, 2)], false));           // 57
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 584), [52, 53, 57, 59],
        [new MarsResourceCount(MarsResourceType.Megacredits, -6)], false, false, false, false, false, MarsTiles.Ocean));                                                               // 58
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 584), [53, 54, 58, 60], [new MarsResourceCount(MarsResourceType.Heat, 2)], false));                                                               // 59
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 584), [54, 55, 59], [], false));  // 60
    }
    else if (map == MarsMap.Elysium) {
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 149), [1, 5, 6], [], true));       // 0
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 149), [0, 2, 6, 7], [new MarsResourceCount(MarsResourceType.Titanium, 1)], true));     // 1
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 149), [1, 3, 7, 8], [new MarsResourceCount(MarsResourceType.Project, 1)], true));                                                               // 2
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 149), [2, 4, 8, 9], [new MarsResourceCount(MarsResourceType.Steel, 1)], true));   // 3
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 149), [3, 9, 10], [new MarsResourceCount(MarsResourceType.Project, 1)]));                                                     // 4

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 204), [0, 6, 11, 12], [new MarsResourceCount(MarsResourceType.Titanium, 1)], false, false, true));                                                               // 5
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 204), [0, 1, 5, 7, 12, 13], [])); // 6
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 204), [1, 2, 6, 8, 13, 14], []));                                                               // 7
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 204), [2, 3, 7, 9, 14, 15], [], true));                                                               // 8
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 204), [3, 4, 8, 10, 15, 16], [], true));                                                               // 9
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 204), [4, 9, 16, 17], [new MarsResourceCount(MarsResourceType.Steel, 2)]));   // 10

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 255, 258), [5, 12, 18, 19], [new MarsResourceCount(MarsResourceType.Titanium, 2)], false, false, true)); // 11
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 258), [5, 6, 11, 13, 19, 20], []));                                                               // 12
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 258), [6, 7, 12, 14, 20, 21], [new MarsResourceCount(MarsResourceType.Project, 1)]));                                                               // 13
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 258), [7, 8, 13, 15, 21, 22], []));                                                               // 14
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 258), [8, 9, 14, 16, 22, 23], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));                                                               // 15
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 258), [9, 10, 15, 17, 23, 24], [], true));                                                               // 16
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 632, 258), [10, 16, 24, 25], [new MarsResourceCount(MarsResourceType.Project, 3)], false, false, true));           // 17

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 224, 312), [11, 19, 26, 27], [new MarsResourceCount(MarsResourceType.Plants, 1)]));           // 18
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 312), [11, 12, 18, 20, 27, 28], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 19
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 312), [12, 13, 19, 21, 28, 29], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 20
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 312), [13, 14, 20, 22, 29, 30], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));          // 21
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 312), [14, 15, 21, 23, 30, 31], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 22
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 312), [15, 16, 22, 24, 31, 32], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));          // 23
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 312), [16, 17, 23, 25, 32, 33], [new MarsResourceCount(MarsResourceType.Plants, 1)], true));          // 24
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 664, 312), [17, 25, 33, 34], [new MarsResourceCount(MarsResourceType.Plants, 1), new MarsResourceCount(MarsResourceType.Steel, 1)]));    // 25

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 192, 367), [18, 27, 35], [new MarsResourceCount(MarsResourceType.Plants, 2)])); // 26
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 255, 367), [18, 19, 26, 28, 35, 36], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 27
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 367), [19, 20, 27, 29, 36, 37], [new MarsResourceCount(MarsResourceType.Plants, 2)])); // 28
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 367), [20, 21, 28, 30, 37, 38], [new MarsResourceCount(MarsResourceType.Plants, 2)], true));    // 29
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 367), [21, 22, 29, 31, 38, 39], [new MarsResourceCount(MarsResourceType.Plants, 2)]));    // 30
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 367), [22, 23, 30, 32, 39, 40], [new MarsResourceCount(MarsResourceType.Plants, 3)]));    // 31
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 367), [23, 24, 31, 33, 40, 41], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 32
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 632, 367), [24, 25, 32, 34, 41, 42], [new MarsResourceCount(MarsResourceType.Plants, 2)]));          // 33
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 695, 367), [25, 33, 42],
        [new MarsResourceCount(MarsResourceType.Plants, 1), new MarsResourceCount(MarsResourceType.Titanium, 1)], false, false, true));          // 34

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 224, 421), [26, 27, 36, 43], [new MarsResourceCount(MarsResourceType.Steel, 1)]));          // 35
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 421), [27, 28, 35, 37, 43, 44], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 36
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 421), [28, 29, 36, 38, 44, 45], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 37
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 421), [29, 30, 37, 39, 45, 46], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 38
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 421), [30, 31, 38, 40, 46, 47], [new MarsResourceCount(MarsResourceType.Plants, 1)]));          // 39
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 421), [31, 32, 39, 41, 47, 48], [new MarsResourceCount(MarsResourceType.Plants, 1)]));    // 40
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 421), [32, 33, 40, 42, 48, 49], [new MarsResourceCount(MarsResourceType.Plants, 1)]));    // 41
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 664, 421), [33, 34, 41, 49], []));    // 42

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 255, 475), [35, 36, 44, 50], [new MarsResourceCount(MarsResourceType.Titanium, 1)]));                                                               // 43
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 475), [36, 37, 43, 45, 50, 51], [new MarsResourceCount(MarsResourceType.Steel, 1)]));                                                               // 44
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 475), [37, 38, 44, 46, 51, 52], []));                                                               // 45
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 475), [38, 39, 45, 47, 52, 53], []));                                                               // 46
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 475), [39, 40, 46, 48, 53, 54], [new MarsResourceCount(MarsResourceType.Steel, 1)]));                                                               // 47
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 475), [40, 41, 47, 49, 54, 55], []));          // 48
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 632, 475), [41, 42, 48, 55], []));                                                               // 49

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 287, 531), [43, 44, 51, 56], [new MarsResourceCount(MarsResourceType.Steel, 2)]));           // 50
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 349, 531), [44, 45, 50, 52, 56, 57], []));                                                               // 51
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 412, 531), [45, 46, 51, 53, 57, 58], []));                                                               // 52
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 475, 531), [46, 47, 52, 54, 58, 59], []));         // 53
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 538, 531), [47, 48, 53, 55, 59, 60], [new MarsResourceCount(MarsResourceType.Steel, 2)]));                                                               // 54
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 601, 531), [48, 49, 54, 60], []));        // 55

      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 318, 584), [50, 51, 57], [new MarsResourceCount(MarsResourceType.Steel, 1)]));           // 56
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 381, 584), [51, 52, 56, 58], []));           // 57
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 443, 584), [52, 53, 57, 59], [new MarsResourceCount(MarsResourceType.Project, 1)]));                                                               // 58
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 507, 584), [53, 54, 58, 60], [new MarsResourceCount(MarsResourceType.Project, 1)]));                                                               // 59
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 569, 584), [54, 55, 59], [new MarsResourceCount(MarsResourceType.Steel, 2)]));  // 60
    }

    // Ganymede and Phobos
    if (map == MarsMap.Elysium) {
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 794, 315), [], [], false, false, false, true));
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 782, 212), [], [], false, false, false, false, true));
    }
    else if (map == MarsMap.Hellas) {
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 105, 298), [], [], false, false, false, true));
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 114, 168), [], [], false, false, false, false, true));
    }
    else {
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 105, 291), [], [], false, false, false, true));
      this.tileLocations.push(new MarsTileLocation(this.tileLocations.length, this.createHex(parent, 124, 88), [], [], false, false, false, false, true));
    }
  }

  private createHex(parent: number, x: number, y: number): GfxLines {
    var hex = this.canvas.createLineSegment(this.canvasId, 0, 17, 2, 'white', parent);
    hex.addTo(31, 0);
    hex.addTo(62, 17);
    hex.addTo(62, 54);
    hex.addTo(31, 72);
    hex.addTo(0, 54);
    hex.addTo(0, 17);
    hex.translate(x, y);
    hex.setVisible(false);
    hex.setEnabled(true);

    return hex;
  }

  public createStandardProjectButton(standardProjectId: number, map: MarsMap): number {
    var x = 62;
    if (map == MarsMap.Elysium) {
      x = 751;
    }
    var button = this.canvas.createRectangle(this.canvasId, 135, 30, 'red', false, 1, this.boardId);
    button.translate(x, 455 + standardProjectId * 31);
    button.setEnabled(true);
    button.setVisible(false);
    return button.getId();
  }

  public createMilestoneButton(milestoneId: number): number {
    var button = this.canvas.createRectangle(this.canvasId, 82, 45, 'red', false, 1, this.boardId);
    button.translate(53 + (milestoneId % 5) * 82, 713);
    button.setEnabled(true);
    button.setVisible(false);
    return button.getId();
  }

  public createAwardButton(awardId: number): number {
    var button = this.canvas.createRectangle(this.canvasId, 82, 48, 'red', false, 1, this.boardId);
    button.translate(482 + (awardId % 5) * 82, 710);
    button.setEnabled(true);
    button.setVisible(false);
    return button.getId();
  }

  public setActionButtonsVisible(gfxIds: Array<number>, visible: boolean) {
    gfxIds.forEach((id) => {
      var gfx = this.canvas.getGfxObject(this.canvasId, id);
      if (gfx != null) {
        gfx.setVisible(visible);
      }
    });
  }
}
