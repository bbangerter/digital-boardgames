import { Injectable } from '@angular/core';
import { MarsCorp, MarsProject, MarsResourceCount, MarsPrereqCondition, MarsScore, MarsActionProject, MarsAction, MarsReaction, MarsAlternateCredits, MarsTagPlayed, MarsTilePlayed, MarsStandardProject, MarsMilestones, MarsAwards, MarsPrelude, MarsResourceTarget } from '../dtos/terraforming-mars-dtos.model';
import { MarsResourceType, MarsTags, MarsExpansion, MarsTiles, MarsPrereq, MarsMap } from '../terraforming-mars-constants';
import { GameUtilsService } from '../../core';
import { moveItemInArray } from '@angular/cdk/drag-drop';

@Injectable({
  providedIn: 'root'
})
export class TerraformingMarsDecksService {
  private corporations: Array<MarsCorp>;
  private preludes: Array<MarsPrelude>;
  private projects: Array<MarsProject>;
  private extraProjects: Array<MarsActionProject>;
  private standardProjects: Array<MarsStandardProject>;
  private milestones: Array<MarsMilestones>;
  private awards: Array<MarsAwards>;
  private shuffledProjects: Array<number>;
  private shuffledPreludes: Array<number>;
  private discardPile: Array<number>;
  constructor(private gameUtils: GameUtilsService) {
    this.shuffledProjects = new Array<number>();
    this.discardPile = new Array<number>();
    this.initializeCorporations();
    this.initializePrelude();
    this.initializeProjects();
    this.initializeStandardProjects();
    this.initializeMilestonesAndAwards();
  }

  public needReshuffle(needed: number): boolean {
    return this.shuffledProjects.length < needed;
  }

  public getStandardProjects(): Array<MarsStandardProject> {
    return this.standardProjects;
  }

  public getMilestones(map: MarsMap): Array<MarsMilestones> {
    return this.milestones.filter(m => m.map == map);
  }

  public getAwards(map: MarsMap): Array<MarsAwards> {
    return this.awards.filter(a => a.map == map);
  }

  public shuffleCorporations(expansions: Array<MarsExpansion>): Array<number> {
    var corpDeck = this.corporations.filter(c => expansions.some(e => e == c.expansion)).map(c => c.id);
    this.gameUtils.shuffle(corpDeck);
    return corpDeck;
  }

  public shufflePrelude(expansions: Array<MarsExpansion>): Array<number> {
    var preludeDeck = this.preludes.filter(p => expansions.some(e => e == p.expansion)).map(p => p.id);
    this.gameUtils.shuffle(preludeDeck);
    return preludeDeck;
  }

  public getCorporations(corpIds: Array<number>): Array<MarsCorp> {
    return this.corporations.filter(c => corpIds.some(id => id == c.id));
  }

  public getCorporation(corpId: number): MarsCorp {
    return this.corporations.find(c => c.id == corpId);
  }

  public shuffleProjects(expansions: Array<MarsExpansion>): Array<number> {
    var projectDeck = this.projects.filter(p => expansions.some(e => e == p.expansion)).map(p => p.id);
    this.gameUtils.shuffle(projectDeck);

    var debugCard = false;
    if (debugCard) {
      var cardsToDebug = ['Ants'];
        //['Aquifier Pumping', 'Mineral Deposit', 'Water Import from Europa', 'Asteroid'];

      cardsToDebug.forEach((c, index) => {
        var project = this.projects.find(p => p.name == c);
        if (project != null) {
          project.prereqs = []; // remove prereqs for playing this card
          var shuffledIndex = projectDeck.findIndex(d => d == project.id);
          var x = projectDeck[index];
          projectDeck[index] = projectDeck[shuffledIndex];
          projectDeck[shuffledIndex] = x;
        }
      });
    }

    return projectDeck;
  }

  public addShuffledProjects(projects: Array<number>) {
    this.shuffledProjects = this.shuffledProjects.concat(projects);
  }

  public addShuffledPrelude(preludes: Array<number>) {
    this.shuffledPreludes = preludes;
  }

  public reshuffleProjects(): Array<number> {
    this.gameUtils.shuffle(this.discardPile);
    var reshuffledProjects = this.discardPile;
    this.discardPile = new Array<number>();
    return reshuffledProjects;
  }

  public dealNextProject(): number {
    return this.shuffledProjects.shift();
  }

  public dealNextPrelude(): number {
    return this.shuffledPreludes.shift();
  }

  public getExtraProjects(): Array<MarsActionProject> {
    return this.extraProjects;
  }

  public getProjects(projectIds: Array<number>): Array<MarsProject> {
    return this.projects.filter(p => projectIds.some(id => id == p.id));
  }

  public dragAndDropProjectOrder(currentId: number, previousId: number) {
    var currentIndex = this.projects.findIndex(p => p.id == currentId);
    var previousIndex = this.projects.findIndex(p => p.id == previousId);
    moveItemInArray(this.projects, previousIndex, currentIndex);
  }

  public getPrelude(preludeId: number): MarsPrelude {
    return this.preludes.find(p => p.id == preludeId);
  }

  public getProject(projectId: number): MarsProject {
    var project = this.projects.find(p => p.id == projectId);
    if (project == null) {
      project = this.extraProjects.find(p => p.id == projectId);
    }

    return project;
  }

  public discardProject(projectId: number) {
    this.discardPile.push(projectId);
  }

  private initializeCorporations() {
    this.corporations = new Array<MarsCorp>();

    // base game
    this.corporations.push(new MarsCorp(this.corporations.length, 'Phob Log', [],
      [new MarsResourceCount(MarsResourceType.Titanium, 10), new MarsResourceCount(MarsResourceType.Megacredits, 23)],
      [MarsTags.Space], 'Your titanium resources are each worth 1 megacredit extra.', '', null, [new MarsReaction(null, 0, MarsTags.None, null, null, 0, false, false, true)]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Ecoline', [new MarsResourceCount(MarsResourceType.Plants, 2)],
      [new MarsResourceCount(MarsResourceType.Plants, 3), new MarsResourceCount(MarsResourceType.Megacredits, 36)], [MarsTags.Plant],
      'You may always pay 7 plants, instead of 8, to place 1 greenery.', '', null, [new MarsReaction(null, 0, MarsTags.None, null, null, 0, false, false, false, true)]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Tharsis Republic', [], [new MarsResourceCount(MarsResourceType.Megacredits, 40)], [MarsTags.Builder],
      'When any city tile is placed ON MARS increase your megacredit production by 1 step. When you place a city tile, gain 3 megacredits.',
      'As your first action in the game, place a city tile.', new MarsAction('Place a city tile', null, null, null, [], MarsTiles.City, true), [new MarsReaction(null, 0, MarsTags.None, null,
        new MarsTilePlayed([MarsTiles.City, MarsTiles.Capital, MarsTiles.LavaTube, MarsTiles.Noctis, MarsTiles.ResearchOutpost,
          MarsTiles.UrbanizedArea], null, new MarsResourceCount(MarsResourceType.Megacredits, 1), true, true)), new MarsReaction(null, 0, MarsTags.None, null,
            new MarsTilePlayed([MarsTiles.City, MarsTiles.Capital, MarsTiles.LavaTube, MarsTiles.Noctis, MarsTiles.ResearchOutpost,
            MarsTiles.UrbanizedArea], new MarsResourceCount(MarsResourceType.Megacredits, 3), null, false, false, []))]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Thorgate', [new MarsResourceCount(MarsResourceType.Energy, 1)],
      [new MarsResourceCount(MarsResourceType.Megacredits, 48)], [MarsTags.Energy],
      'When playing a power card OR THE STANDARD PROJECT POWER PLANT, you pay 3 megacredits less for it.', '', null, [new MarsReaction(null, 3, MarsTags.Energy)]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Saturn Systems',
      [new MarsResourceCount(MarsResourceType.Titanium, 1)], [new MarsResourceCount(MarsResourceType.Megacredits, 42)], [MarsTags.Jovian],
      'Each time any Jovian tag is put into play, including this, increase your megacredit production 1 step.', '', null,
      [new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Jovian], [], [new MarsResourceCount(MarsResourceType.Megacredits, 1)], MarsTags.None, true))]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Inventrix', [], [new MarsResourceCount(MarsResourceType.Megacredits, 45)], [MarsTags.Science],
      'Your temperature, oxygen, and oceanc requirements are +2 or -2 steps, your choice in each case.', 'As your first turn in the game, draw 3 cards.',
      new MarsAction('Draw 3 cards', null, null, null, [new MarsResourceCount(MarsResourceType.Project, 3)], MarsTiles.None, true), [new MarsReaction(null, 0, MarsTags.None, null, null, 0, true)]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Mining Guild', [new MarsResourceCount(MarsResourceType.Steel, 1)],
      [new MarsResourceCount(MarsResourceType.Steel, 5), new MarsResourceCount(MarsResourceType.Megacredits, 30)], [MarsTags.Builder, MarsTags.Builder],
      'Each time you get any steel or titanium as a placement bonus on the map, increase your steel production 1 step.', '', null,
      [new MarsReaction(null, 0, MarsTags.None, null,
        new MarsTilePlayed([MarsTiles.Any], null, new MarsResourceCount(MarsResourceType.Steel, 1), false, false, [MarsResourceType.Steel, MarsResourceType.Titanium]))]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Terractor', [], [new MarsResourceCount(MarsResourceType.Megacredits, 60)], [MarsTags.Earth],
      'When playing an Earth card, you may pay 3 megacredits less for it.', '', null, [new MarsReaction(null, 3, MarsTags.Earth)]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Helion', [new MarsResourceCount(MarsResourceType.Heat, 3)],
      [new MarsResourceCount(MarsResourceType.Megacredits, 42)], [MarsTags.Space],
      'You may use heat as megacredits. You may not use megacredits as heat.', '', null, [new MarsReaction(new MarsAlternateCredits(MarsResourceType.Heat, 1))]));
    this.corporations.push(new MarsCorp(this.corporations.length, 'United Nations Mars Initiative', [], [new MarsResourceCount(MarsResourceType.Megacredits, 40)], [MarsTags.Earth],
      'If your terraform rating was raised this generation, you may pay 3 megacredits to raise it 1 step more.', '',
      new MarsAction('Raise Terraforming', null, new MarsResourceCount(MarsResourceType.Megacredits, -3), null, [new MarsResourceCount(MarsResourceType.Terraforming, 1)]), []));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Interplanetary Cinematics', [],
      [new MarsResourceCount(MarsResourceType.Steel, 20), new MarsResourceCount(MarsResourceType.Megacredits, 30)], [MarsTags.Builder],
      'Each time you play an event, you get 2 megacredits.', '', null, [new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Event],
        [new MarsResourceCount(MarsResourceType.Megacredits, 2)]))], null));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Credicor', [], [new MarsResourceCount(MarsResourceType.Megacredits, 57)], [],
      'After you pay for a card or standard project with a basic cost of 20 megacredits or more, you gain 4 megacredts.', '', null,
      [new MarsReaction(null, 0, MarsTags.None, null, null, 4, false, false, false, false, new MarsPrereqCondition(MarsPrereq.StandardProject, 20)),
        new MarsReaction(null, 0, MarsTags.None, null, null, 4, false, false, false, false, new MarsPrereqCondition(MarsPrereq.ProjectCost, 20))]));

    // prelude corps
    this.corporations.push(new MarsCorp(this.corporations.length, 'Vitor', [], [new MarsResourceCount(MarsResourceType.Megacredits, 45)], [MarsTags.Earth],
      'When you play a card with a NON-NEGATIVE VP icon, including this, gain 3 megacredits', 'As your first action, fund an award for free.',
      new MarsAction('Fund an award', null, new MarsResourceCount(MarsResourceType.Megacredits, 8), null, [], MarsTiles.None, true, false, true),
      [new MarsReaction(null, 0, MarsTags.None, null, null, 3, false, false, false, false, new MarsPrereqCondition(MarsPrereq.VictoryCard, 0))], -1, MarsExpansion.Prelude));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Cheung Shing Mars', [new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [new MarsResourceCount(MarsResourceType.Megacredits, 44)], [MarsTags.Builder], 'When you play a building tag, you pay 2 megacredits less for it.', '', null,
      [new MarsReaction(null, 2, MarsTags.Builder)], -1, MarsExpansion.Prelude));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Valley Trust', [], [new MarsResourceCount(MarsResourceType.Megacredits, 37)], [MarsTags.Earth],
      'When you play a science tag, you pay 2 megacredits less for it.', 'As your first action, draw 3 prelude cards, and play one of them. Discard the other two.',
      new MarsAction('Draw 3 prelude cards', null, null, null, [new MarsResourceCount(MarsResourceType.Prelude, 3, false, false, [], null, false, MarsTags.None, false,
        MarsTiles.None, false, false, false, 1)], MarsTiles.None, true), [new MarsReaction(null, 2, MarsTags.Science)], -1, MarsExpansion.Prelude));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Robinson Industries', [], [new MarsResourceCount(MarsResourceType.Megacredits, 47)], [], 
      'Spend 4 megacredits to increase (one of) your LOWEST PRODUCTION 1 step.', '', new MarsAction('Increase lowest production', null, new MarsResourceCount(MarsResourceType.Megacredits, -4),
        new MarsResourceCount(MarsResourceType.AnyBasic, 1)), [], -1, MarsExpansion.Prelude));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Point Luna', [new MarsResourceCount(MarsResourceType.Titanium, 1)], [new MarsResourceCount(MarsResourceType.Megacredits, 38)],
      [MarsTags.Earth, MarsTags.Space], 'When you play an Earth tag, including this, draw a card.', '', null,
      [new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Earth], [new MarsResourceCount(MarsResourceType.Project, 1)]))], -1, MarsExpansion.Prelude));

    // promo corps
    this.corporations.push(new MarsCorp(this.corporations.length, 'Arcadian Communities', [], [new MarsResourceCount(MarsResourceType.Megacredits, 40),
      new MarsResourceCount(MarsResourceType.Steel, 10)], [], 'As your first action place a community marker on a non-reserved area',
      'Marked areas are reserved for you. When you place a tile there gain 3 MC.', new MarsAction('Place a community marker', null, null, null, [], MarsTiles.CommunityArea, true,
        false, false, true), [], -1, MarsExpansion.Promos));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Recyclon', [new MarsResourceCount(MarsResourceType.Steel, 1)], [new MarsResourceCount(MarsResourceType.Megacredits, 38)],
      [MarsTags.Builder, MarsTags.Microbe], 'When you play a building tag, including this, gain 1 microbe to this card, or remove 2 microbes here and raise your plant production 1 step.',
      '', null, [new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Builder], [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false,
        [new MarsResourceCount(MarsResourceType.MicrobeToken, -2, false, false, [new MarsResourceCount(MarsResourceType.Plants, 1)], new MarsPrereqCondition(MarsPrereq.Token, 2),
          false, MarsTags.Science, false, MarsTiles.None, false, false, false, 0, 'recyclon')], null, false, MarsTags.Science, false, MarsTiles.None, false, false, false, 0, 'recyclon')]))],
      -1, MarsExpansion.Promos));
    this.corporations.push(new MarsCorp(this.corporations.length, 'Splice', [], [new MarsResourceCount(MarsResourceType.Megacredits, 44)], [MarsTags.Microbe],
      'Each time a microbe tag is played, including this, THAT PLAYER gains 2 MC, or adds a microbe to THAT card, and you gain 2 MC.',
      'As your first action, reveal cards until you have revealed a microbe tag. Take that card into hand, and discard the rest.',
      new MarsAction('Draw 1 microbe card', null, null, null, [new MarsResourceCount(MarsResourceType.Project, 1, false, false, [],
        new MarsPrereqCondition(MarsPrereq.Tags, 0, false, [MarsTags.Microbe]))], MarsTiles.None, true),
      [new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Microbe], [new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], MarsTags.None, true))], -1, MarsExpansion.Promos));
  }

  private initializePrelude() {
    this.preludes = new Array<MarsPrelude>();

    this.preludes.push(new MarsPrelude(this.preludes.length, 'Eccentric Sponsor', [], [], [], [], true, 25, false, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Ecology Experts', [new MarsResourceCount(MarsResourceType.Plants, 1)], [], [], [MarsTags.Microbe, MarsTags.Plant],
      true, 0, true, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Nitrogen Shipment', [new MarsResourceCount(MarsResourceType.Plants, 1)], [new MarsResourceCount(MarsResourceType.Terraforming, 1),
      new MarsResourceCount(MarsResourceType.Megacredits, 5)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'IO Research Outpost', [new MarsResourceCount(MarsResourceType.Titanium, 1)], [new MarsResourceCount(MarsResourceType.Project, 1)],
      [], [MarsTags.Jovian, MarsTags.Science]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Mohole Excavation', [new MarsResourceCount(MarsResourceType.Steel, 1), new MarsResourceCount(MarsResourceType.Heat, 2)],
      [new MarsResourceCount(MarsResourceType.Heat, 2)], [], [MarsTags.Builder]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Biofuels', [new MarsResourceCount(MarsResourceType.Plants, 1), new MarsResourceCount(MarsResourceType.Energy, 1)],
      [new MarsResourceCount(MarsResourceType.Plants, 2)], [], [MarsTags.Microbe]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Great Aquifer', [], [], [MarsTiles.Ocean, MarsTiles.Ocean], [], false, 0, false, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Business Empire', [new MarsResourceCount(MarsResourceType.Megacredits, 6)], [new MarsResourceCount(MarsResourceType.Megacredits, -6)],
      [], [MarsTags.Earth]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Acquired Space Agency', [], [new MarsResourceCount(MarsResourceType.Titanium, 6), new MarsResourceCount(MarsResourceType.Project, 2,
      false, false, [], new MarsPrereqCondition(MarsPrereq.Tags, 0, false, [MarsTags.Space]))]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Biosphere Support', [new MarsResourceCount(MarsResourceType.Megacredits, -1), new MarsResourceCount(MarsResourceType.Plants, 2)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Metal-Rich Asteroid', [], [new MarsResourceCount(MarsResourceType.Temperature, 1), new MarsResourceCount(MarsResourceType.Titanium, 4),
      new MarsResourceCount(MarsResourceType.Steel, 4)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Research Network', [new MarsResourceCount(MarsResourceType.Megacredits, 1)], [new MarsResourceCount(MarsResourceType.Project, 3)],
      [], [MarsTags.Wild]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Supply Drop', [], [new MarsResourceCount(MarsResourceType.Titanium, 3), new MarsResourceCount(MarsResourceType.Steel, 8),
      new MarsResourceCount(MarsResourceType.Plants, 3)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Donation', [], [new MarsResourceCount(MarsResourceType.Megacredits, 21)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Self-Sufficient Settlement', [new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], [MarsTiles.City],
      [MarsTags.Builder, MarsTags.City], false, 0, false, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Smelting Plant', [], [new MarsResourceCount(MarsResourceType.Oxygen, 2), new MarsResourceCount(MarsResourceType.Steel, 5)], [],
      [MarsTags.Builder]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Aquifer Turbines', [new MarsResourceCount(MarsResourceType.Energy, 2)], [new MarsResourceCount(MarsResourceType.Megacredits, -3)],
      [MarsTiles.Ocean], [MarsTags.Energy], false, 0, false, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Mohole', [new MarsResourceCount(MarsResourceType.Heat, 3)], [new MarsResourceCount(MarsResourceType.Heat, 3)], [],
      [MarsTags.Builder]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Power Generation', [new MarsResourceCount(MarsResourceType.Energy, 3)], [], [], [MarsTags.Energy]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Galilean Mining', [new MarsResourceCount(MarsResourceType.Titanium, 2)], [new MarsResourceCount(MarsResourceType.Megacredits, -5)],
      [], [MarsTags.Jovian]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Mining Operations', [new MarsResourceCount(MarsResourceType.Steel, 2)], [new MarsResourceCount(MarsResourceType.Steel, 4)], [],
      [MarsTags.Builder]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Huge Asteroid', [], [new MarsResourceCount(MarsResourceType.Megacredits, -5),
      new MarsResourceCount(MarsResourceType.Temperature, 3)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Society Support', [new MarsResourceCount(MarsResourceType.Megacredits, -1), new MarsResourceCount(MarsResourceType.Plants, 1),
      new MarsResourceCount(MarsResourceType.Energy, 1), new MarsResourceCount(MarsResourceType.Heat, 1)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Supplier', [new MarsResourceCount(MarsResourceType.Energy, 2)], [new MarsResourceCount(MarsResourceType.Steel, 4)], [],
      [MarsTags.Energy]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Dome Farming', [new MarsResourceCount(MarsResourceType.Plants, 1), new MarsResourceCount(MarsResourceType.Megacredits, 2)], [],
      [], [MarsTags.Builder, MarsTags.Plant]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Experimental Forest', [], [new MarsResourceCount(MarsResourceType.Project, 2, false, false, [],
      new MarsPrereqCondition(MarsPrereq.Tags, 0, false, [MarsTags.Plant]))], [MarsTiles.Greenery], [MarsTags.Plant], false, 0, false, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Loan', [new MarsResourceCount(MarsResourceType.Megacredits, -2)],
      [new MarsResourceCount(MarsResourceType.Megacredits, 30)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Early Settlement', [new MarsResourceCount(MarsResourceType.Plants, 1)], [], [MarsTiles.City], [MarsTags.Builder, MarsTags.City],
      false, 0, false, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Biolab', [new MarsResourceCount(MarsResourceType.Plants, 1)], [new MarsResourceCount(MarsResourceType.Project, 3)], [],
      [MarsTags.Science]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Martian Industries', [new MarsResourceCount(MarsResourceType.Energy, 1), new MarsResourceCount(MarsResourceType.Steel, 1)],
      [new MarsResourceCount(MarsResourceType.Megacredits, 6)], [], [MarsTags.Builder]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Polar Industries', [new MarsResourceCount(MarsResourceType.Heat, 2)], [], [MarsTiles.Ocean], [MarsTags.Builder],
      false, 0, false, false));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Orbital Construction Yard', [new MarsResourceCount(MarsResourceType.Titanium, 1)],
      [new MarsResourceCount(MarsResourceType.Titanium, 4)], [], [MarsTags.Space]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Metals Company', [new MarsResourceCount(MarsResourceType.Titanium, 1), new MarsResourceCount(MarsResourceType.Steel, 1),
      new MarsResourceCount(MarsResourceType.Megacredits, 1)]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'UNMI Contractor', [], [new MarsResourceCount(MarsResourceType.Terraforming, 3), new MarsResourceCount(MarsResourceType.Project, 1)],
      [], [MarsTags.Earth]));
    this.preludes.push(new MarsPrelude(this.preludes.length, 'Allied Bank', [new MarsResourceCount(MarsResourceType.Megacredits, 4)], [new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [], [MarsTags.Earth]));

  }

  private initializeProjects() {
    // greeneries and raising the temperature
    this.extraProjects = new Array<MarsActionProject>();
    this.extraProjects.push(new MarsActionProject(-1, 'Greenery', '', null, [new MarsAction('Action 8 plants -> Spend 8 plants to place a greenery', null,
      new MarsResourceCount(MarsResourceType.Plants, -8), null, [], MarsTiles.Greenery)], 0, [], [], []));
    this.extraProjects.push(new MarsActionProject(-2, 'Temperature', '', null, [new MarsAction('Action 8 heat -> Spend 8 heat to raise the temperature', null,
      new MarsResourceCount(MarsResourceType.Heat, -8), null, [new MarsResourceCount(MarsResourceType.Temperature, 1)])], 0, [], [], []));

    this.projects = new Array<MarsProject>();

    // promo - blue cards
    this.projects.push(new MarsActionProject(this.projects.length, 'Penguins', '1 VP for each animal on this card.', null,
      [new MarsAction('Action -> Add 1 animal to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false, MarsTags.None,
        false, MarsTiles.None, false, false, false, 0, 'penguins')])],
      7, [], [], [MarsTags.Animal], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 8)], new MarsScore(1, MarsResourceType.AnimalToken), MarsExpansion.Promos));
    // self replicating robots
    this.projects.push(new MarsActionProject(this.projects.length, 'Orbital Cleanup', '', null,
      [new MarsAction('Action -> Gain 1 MC per science tag you have.', null, null, null,
        [new MarsResourceCount(MarsResourceType.Megacredits, 1, false, false, [], null, true, MarsTags.Science)])],
      14, [new MarsResourceCount(MarsResourceType.Megacredits, -2)], [], [MarsTags.Earth, MarsTags.Space], [], [], new MarsScore(2), MarsExpansion.Promos));

    // base game - blue cards
    this.projects.push(new MarsActionProject(this.projects.length, 'Fish', '1 VP for each animal on this card', null,
      [new MarsAction('Action -> Add 1 animal to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'fish')])],
      9, [new MarsResourceCount(MarsResourceType.Plants, -1, true)], [], [MarsTags.Animal], [],
      [new MarsPrereqCondition(MarsPrereq.Temperature, 2)], new MarsScore(1, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Business Network', '', null,
      [new MarsAction('Action -> Look a the top card and either buy it or discard it.', null, null, null,
        [new MarsResourceCount(MarsResourceType.Project, 1, false, false, [], null, false, MarsTags.Animal, false, MarsTiles.None, false, false, true)])],
      4, [new MarsResourceCount(MarsResourceType.Megacredits, -1)], [], [MarsTags.Earth], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Martian Rails', '', null,
      [new MarsAction('Action 1 energy -> Spend 1 energy to gain 1 megacredit for each city tile ON MARS.', null, new MarsResourceCount(MarsResourceType.Energy, -1),
        null, [new MarsResourceCount(MarsResourceType.Megacredits, 1, false, false, [], null, false, MarsTags.None, true, MarsTiles.City, false, true)])], 13,
      [], [], [MarsTags.Builder], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Industrial Center', 'Place this tile ADJACENT TO ANY CITY TILE.', null,
      [new MarsAction('Action 7 megacredits -> Spend 7 megacredits to increase your steel production by 1 step', null, new MarsResourceCount(MarsResourceType.Megacredits, -7),
        new MarsResourceCount(MarsResourceType.Steel, 1))], 4, [], [], [MarsTags.Builder], [MarsTiles.Industrial], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Inventor\'s Guild', '', null,
      [new MarsAction('Action -> Look a the top card and either buy it or discard it.', null, null, null,
        [new MarsResourceCount(MarsResourceType.Project, 1, false, false, [], null, false, MarsTags.Animal, false, MarsTiles.None, false, false, true)])],
      9, [], [], [MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'AI Central', '', null,
      [new MarsAction('Action -> Draw 2 cards.', null, null, null, [new MarsResourceCount(MarsResourceType.Project, 2)])],
      21, [new MarsResourceCount(MarsResourceType.Energy, -1)], [], [MarsTags.Builder, MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Tags, 3, false, [MarsTags.Science])],
      new MarsScore(1)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Restricted Area', '', null,
      [new MarsAction('Action 2 megacredits-> Spend 2 megacredits to draw a card.', null, new MarsResourceCount(MarsResourceType.Megacredits, -2),
        null, [new MarsResourceCount(MarsResourceType.Project, 1)])], 11, [], [], [MarsTags.Science], [MarsTiles.Restricted], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Underground Detonations', '', null,
      [new MarsAction('Action 10 megacredits -> Spend 10 megacredits to increase your heat production 2 steps.', null, new MarsResourceCount(MarsResourceType.Megacredits, -10),
        new MarsResourceCount(MarsResourceType.Heat, 2))],
      6, [], [], [MarsTags.Earth], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Aquifier Pumping', '', null,
      [new MarsAction('Action 8 megacredits -> Spend 8 megacredits to place 1 ocean tile. STEEL MAY BE USED as if you were playing a building card.', null,
        new MarsResourceCount(MarsResourceType.Megacredits, -8, false, false, [new MarsResourceCount(MarsResourceType.Steel, 0)]), null, [], MarsTiles.Ocean)],
      18, [], [], [MarsTags.Builder], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Ironworks', '', null,
      [new MarsAction('Action 4 energy -> Spend 4 energy to gain 1 steel and increase oxygen 1 step.', null,
        new MarsResourceCount(MarsResourceType.Energy, -4), null, [new MarsResourceCount(MarsResourceType.Steel, 1), new MarsResourceCount(MarsResourceType.Oxygen, 1)])],
      11, [], [], [MarsTags.Builder], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Security Fleet', '1 VP for each figter resource on this card.', null,
      [new MarsAction('Action 1 titanium -> Spend 1 titanium to add 1 figther resource to this card.', null,
        new MarsResourceCount(MarsResourceType.Titanium, -1), null, [new MarsResourceCount(MarsResourceType.FleetToken, 1, false, false, [], null, false, MarsTags.None, false,
          MarsTiles.None, false, false, false, 0, 'security')])],
      12, [], [], [MarsTags.Space], [], [], new MarsScore(1, MarsResourceType.FleetToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Search for Life', '3 VPs if you have one or more science resources on this card.', null,
      [new MarsAction('Action 1 megacredit -> Spend 1 megacredit to reveal and discard the top card of the draw deck. If that card has a microbe tag, add a science resource here.', null,
        new MarsResourceCount(MarsResourceType.Megacredits, -1), null, [new MarsResourceCount(MarsResourceType.Project, 1, false, false, [new MarsResourceCount(MarsResourceType.ScienceToken, 1,
          false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'life')],
          new MarsPrereqCondition(MarsPrereq.Tags, 1, false, [MarsTags.Microbe]), false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'life')])],
      3, [], [], [MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 6, true)],
      new MarsScore(3, MarsResourceType.ScienceToken, false, MarsTiles.None, false, false, MarsTags.None, 3)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Tardigrades', '1 VP per 4 microbes on this card.', null,
      [new MarsAction('Action -> Add 1 microbe to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'tardigrades')])],
      4, [], [], [MarsTags.Microbe], [], [], new MarsScore(0.25, MarsResourceType.MicrobeToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Ants', '1 VP per 2 microbes on this card.', null,
      [new MarsAction('Action 1 microbe (from any player) -> Remove 1 microbe from any card to add 1 to this card.', null,
        new MarsResourceCount(MarsResourceType.MicrobeToken, -1, true, true), null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false, MarsTags.None, false,
          MarsTiles.None, false, false, false, 0, 'ants')])],
      9, [], [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 4)], new MarsScore(0.5, MarsResourceType.MicrobeToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Regolith Eaters', '', null,
      [new MarsAction('Action -> Add 1 microbe to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'regolith')]),
        new MarsAction('Action 2 microbes -> Remove 2 microbes from this card to raise oxygen level 1 step.', null, new MarsResourceCount(MarsResourceType.MicrobeToken, -2,
          false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'regolith'), null,
        [new MarsResourceCount(MarsResourceType.Oxygen, 1)])], 13, [], [], [MarsTags.Microbe, MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Caretaker Contract', '', null,
      [new MarsAction('Action 8 heat -> Spend 8 heat to increase your terraform rating 1 step.', null,
        new MarsResourceCount(MarsResourceType.Heat, -8), null, [new MarsResourceCount(MarsResourceType.Terraforming, 1)])],
      3, [], [], [], [], [new MarsPrereqCondition(MarsPrereq.Temperature, 0)]));
    this.projects.push(new MarsActionProject(this.projects.length, 'Development Center', '', null,
      [new MarsAction('Action 1 energy -> Spend 1 energy to draw a card.', null,
        new MarsResourceCount(MarsResourceType.Energy, -1), null, [new MarsResourceCount(MarsResourceType.Project, 1)])],
      11, [], [], [MarsTags.Builder, MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Physics Complex', '2 VP for each science resource on this card.', null,
      [new MarsAction('Action 6 energy -> Spend 6 energy to add a science resource to this card.', null,
        new MarsResourceCount(MarsResourceType.Energy, -6), null, [new MarsResourceCount(MarsResourceType.ScienceToken, 1, false, false, [], null, false, MarsTags.None, false,
          MarsTiles.None, false, false, false, 0, 'physics')])],
      12, [], [], [MarsTags.Builder, MarsTags.Science], [], [], new MarsScore(2, MarsResourceType.ScienceToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Small Animals', '1 VP per 2 animals on this card.', null,
      [new MarsAction('Action -> Add 1 animal to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'small animal')])],
      6, [new MarsResourceCount(MarsResourceType.Plants, -1, true)], [], [MarsTags.Animal, MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 6)],
      new MarsScore(0.5, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Space Mirrors', '', null,
      [new MarsAction('Action 7 megacredits -> Spend 7 megacredits to increase your energy production 1 step.', null,
        new MarsResourceCount(MarsResourceType.Megacredits, -7), new MarsResourceCount(MarsResourceType.Energy, 1))],
      3, [], [], [MarsTags.Space, MarsTags.Energy], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Power Infrastructure', '', null,
      [new MarsAction('Action energy -> Spend any amount of energy to gain that amount of megacredits.', null,
        new MarsResourceCount(MarsResourceType.Energy, 0), null, [new MarsResourceCount(MarsResourceType.Megacredits, 0)])],
      11, [], [], [MarsTags.Builder, MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Equatorial Magnetizer', '', null,
      [new MarsAction('Action 1 energy production -> Decrease your energy production 1 step to increase your terraform rating 1 step.', 
        new MarsResourceCount(MarsResourceType.Energy, -1), null, null, [new MarsResourceCount(MarsResourceType.Terraforming, 1)])],
      11, [], [], [MarsTags.Builder], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Predators', '1 VP per animal on this card.', null,
      [new MarsAction('Action 1 animal (from any player) -> Remove 1 animal from any card to add 1 animal to this card.', null,
        new MarsResourceCount(MarsResourceType.AnimalToken, -1, true, true, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'predators'),
        null, [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'predators')])],
      14, [], [], [MarsTags.Animal], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 11)], new MarsScore(1, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Livestock', '1 VP for each animal on this card.', null,
      [new MarsAction('Action -> Add 1 animal to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'livestock')])],
      13, [new MarsResourceCount(MarsResourceType.Plants, -1), new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], [MarsTags.Animal], [],
      [new MarsPrereqCondition(MarsPrereq.Oxygen, 9)], new MarsScore(1, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Water Splitting Plant', '', null,
      [new MarsAction('Action 3 energy -> Spend 3 energy to raise oxygen 1 step.', null,
        new MarsResourceCount(MarsResourceType.Energy, -3), null, [new MarsResourceCount(MarsResourceType.Oxygen, 1)])],
      12, [], [], [MarsTags.Builder], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Symbiotic Fungus', '', null,
      [new MarsAction('Action -> Add a microbe to another card.', null, null, null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, true)])],
      4, [], [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -14)]));
    this.projects.push(new MarsActionProject(this.projects.length, 'Electro Catapult', '', null,
      [new MarsAction('Action 1 plant -> Spend 1 plant to gain 7 megacredits.', null, new MarsResourceCount(MarsResourceType.Plants, -1), null, [new MarsResourceCount(MarsResourceType.Megacredits, 7)]),
        new MarsAction('Action 1 steel -> Spend 1 steel to gain 7 megacredits.', null, new MarsResourceCount(MarsResourceType.Steel, -1), null, [new MarsResourceCount(MarsResourceType.Megacredits, 7)])],
      17, [new MarsResourceCount(MarsResourceType.Energy, -1)], [], [MarsTags.Builder], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 8, true)], new MarsScore(1)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Nitrite Reducing Bacteria', 'Start with 3 microbes on this card', null,
      [new MarsAction('Action -> Add 1 microbe to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false, MarsTags.None,
        false, MarsTiles.None, false, false, false, 0, 'nitrite')]),
        new MarsAction('Action 3 microbes -> Remove 3 microbes to increase your terraforming 1 step.', null, new MarsResourceCount(MarsResourceType.MicrobeToken, -3, false, false,
          [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'nitrite'), null,
          [new MarsResourceCount(MarsResourceType.Terraforming, 1)])], 11, [], [new MarsResourceCount(MarsResourceType.MicrobeToken, 3, false, false, [], null, false, MarsTags.None, false,
            MarsTiles.None, false, false, false, 0, 'nitrite')], [MarsTags.Microbe], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Water Import from Europa', '1 VP per Jovian tag you have.', null,
      [new MarsAction('Action 12 megacredits -> Spend 12 megacredits to place 1 ocean tile. TITANIUM MAY BE USED as if playing a space card.', null,
        new MarsResourceCount(MarsResourceType.Megacredits, -12, false, false, [new MarsResourceCount(MarsResourceType.Titanium, 0)]), null, [], MarsTiles.Ocean)],
      25, [], [], [MarsTags.Jovian, MarsTags.Space], [], [], new MarsScore(1, MarsResourceType.None, false, MarsTiles.None, false, true, MarsTags.Jovian)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Extreme-Cold Fungus', '', null,
      [new MarsAction('Action -> Gain 1 plant.', null, null, null, [new MarsResourceCount(MarsResourceType.Plants, 1)]),
      new MarsAction('Action -> Add 2 microbes to another card.', null, null, null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 2, false, true)])],
      13, [], [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -10, true)]));
    this.projects.push(new MarsActionProject(this.projects.length, 'GHG Producing Bacteria', '', null,
      [new MarsAction('Action -> Add 1 microbe to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'ghg bacteria')]),
        new MarsAction('Action 2 microbes -> Remove 2 microbes to raise temperature 1 step.', null, new MarsResourceCount(MarsResourceType.MicrobeToken, -2, false, false, [], null, false,
          MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'ghg bacteria'), null,
          [new MarsResourceCount(MarsResourceType.Temperature, 1)])], 8, [], [], [MarsTags.Microbe, MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 4)]));
    this.projects.push(new MarsActionProject(this.projects.length, 'Ore Processor', '', null,
      [new MarsAction('Action 4 energy -> Spend 4 energy to gain 1 titanium and increase oxygen 1 step.', null,
        new MarsResourceCount(MarsResourceType.Energy, -4), null, [new MarsResourceCount(MarsResourceType.Titanium, 1), new MarsResourceCount(MarsResourceType.Oxygen, 1)])],
      13, [], [], [MarsTags.Builder], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Birds', '1 VP for each animal on this card.', null,
      [new MarsAction('Action -> Add 1 animal to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false, MarsTags.None,
        false, MarsTiles.None, false, false, false, 0, 'birds')])],
      10, [new MarsResourceCount(MarsResourceType.Plants, -2, true)], [], [MarsTags.Animal], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 13)], new MarsScore(1, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Space Elevator', '', null,
      [new MarsAction('Action 1 steel -> Spend 1 steel to gain 5 megacredits.', null,
        new MarsResourceCount(MarsResourceType.Steel, -1), null, [new MarsResourceCount(MarsResourceType.Megacredits, 5)])],
      27, [new MarsResourceCount(MarsResourceType.Titanium, 1)], [], [MarsTags.Builder, MarsTags.Space], [], [], new MarsScore(2)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Steelworks', '', null,
      [new MarsAction('Action 4 energy -> Spend 4 energy to gain 2 steel and increase oxygen 1 step.', null,
        new MarsResourceCount(MarsResourceType.Energy, -4), null, [new MarsResourceCount(MarsResourceType.Steel, 2), new MarsResourceCount(MarsResourceType.Oxygen, 1)])],
      15, [], [], [MarsTags.Builder], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Anti-Gravity Technology', 'When you play a card, you may pay 2 megacredits less for it.',
      new MarsReaction(null, 2), [], 14, [], [], [MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Tags, 7, false, [MarsTags.Science])],
      new MarsScore(3)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Viral Enhancers',
      'When you play a plant, animal, or microbe tag, including this, gain 1 plant or add 1 animal or microbe resource TO THAT CARD.',
      new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Animal, MarsTags.Microbe, MarsTags.Plant],
        [new MarsResourceCount(MarsResourceType.Plants, 1, false, false,
          [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, '', false, false, true),
          new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, '', false, false, true)])])),
      [], 9, [], [], [MarsTags.Microbe, MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Media Group', 'After you play an event card, you gain 3 megacredits.',
      new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Event], [new MarsResourceCount(MarsResourceType.Megacredits, 3)])), [],
      6, [], [], [MarsTags.Earth, MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Shuttles', 'When you play a space card, you may pay 2 megacredits less for it.',
      new MarsReaction(null, 2, MarsTags.Space), [], 10, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], [MarsTags.Space], [],
      [new MarsPrereqCondition(MarsPrereq.Oxygen, 5)], new MarsScore(1)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Decomposers',
      'When you play an animal, plant, or microbe tag, including this, add a microbe to this card. 1 VP per 3 microbes on this card.',
      new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Animal, MarsTags.Microbe, MarsTags.Plant], [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [],
        null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'decomposers')])), [],
      5, [], [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 3)], new MarsScore(0.34, MarsResourceType.MicrobeToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Earth Office', 'When you play an Earth tag, you may pay 3 megacredits less for it.',
      new MarsReaction(null, 3, MarsTags.Earth), [], 1, [], [], [MarsTags.Earth], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Space Station', 'When you play a space card, you may pay 2 megacredits less for it.',
      new MarsReaction(null, 2, MarsTags.Space), [], 10, [], [], [MarsTags.Space], [], [], new MarsScore(1)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Mass Converter', 'When you play a space card, you may pay 2 megacredits less for it.',
      new MarsReaction(null, 2, MarsTags.Space), [], 8, [new MarsResourceCount(MarsResourceType.Energy, 6)], [], [MarsTags.Energy, MarsTags.Science], [],
      [new MarsPrereqCondition(MarsPrereq.Tags, 5, false, [MarsTags.Science])]));
    this.projects.push(new MarsActionProject(this.projects.length, 'Research Outpost', 'When you play a card, you may pay 1 megacredits less for it. Place a city tile NEXT TO NO OTHER TILE.',
      new MarsReaction(null, 1), [], 18, [], [], [MarsTags.Builder, MarsTags.City, MarsTags.Science], [MarsTiles.ResearchOutpost], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Quantum Extractor', 'When you play a space card, you may pay 2 megacredits less for it.',
      new MarsReaction(null, 2, MarsTags.Space), [], 13, [new MarsResourceCount(MarsResourceType.Energy, 4)], [], [MarsTags.Energy, MarsTags.Science], [],
      [new MarsPrereqCondition(MarsPrereq.Tags, 4, false, [MarsTags.Science])]));
    this.projects.push(new MarsActionProject(this.projects.length, 'Optimal Aerobraking', 'When you play a space event, you gain 3 megacredits and 3 heat.',
      new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Space], [new MarsResourceCount(MarsResourceType.Megacredits, 3), new MarsResourceCount(MarsResourceType.Heat, 3)],
        [], MarsTags.Event)), [], 7, [], [], [MarsTags.Space], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Herbivores', 'Whenever you place a greenery tile, add an animal to this card. 1 VP per 2 animals on this card.',
      new MarsReaction(null, 0, MarsTags.None, null, new MarsTilePlayed([MarsTiles.Greenery, MarsTiles.Mangrove, MarsTiles.ProtectedValley],
        new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'herbivores'))), [], 12,
      [new MarsResourceCount(MarsResourceType.Plants, -1, true)], [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false, MarsTags.None, false,
        MarsTiles.None, false, false, false, 0, 'herbivores')], [MarsTags.Animal], [],
      [new MarsPrereqCondition(MarsPrereq.Oxygen, 8)], new MarsScore(0.5, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Ecological Zone',
      'When you play a plant or an animal tag, including these 2, add an animal to this card. 1 VP per 2 animals on this card. Play this next to any greenery tile.',
      new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Animal, MarsTags.Plant], [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'ecological')])), [], 12, [], [],
      [MarsTags.Animal, MarsTags.Plant], [MarsTiles.EcologicalZone], [new MarsPrereqCondition(MarsPrereq.Tile, 1, false, [], [], MarsTiles.Greenery, true)],
      new MarsScore(0.5, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Standard Technology', 'After you pay for a standard project, except selling patents, you gain 3 megacredits.',
      new MarsReaction(null, 0, MarsTags.None, null, null, 3, false, false, false, false, new MarsPrereqCondition(MarsPrereq.StandardProject, 0)), [], 6, [], [], [MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Earth Catapult', 'When you play a card, you may pay 2 megacredits less for it.',
      new MarsReaction(null, 2), [], 23, [], [], [MarsTags.Earth], [], [], new MarsScore(2)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Immigrant City', 'Each time a city tile is placed, including this, increase your megacredit production 1 step.',
      new MarsReaction(null, 0, MarsTags.None, null,
        new MarsTilePlayed([MarsTiles.City, MarsTiles.Capital, MarsTiles.Ganymede, MarsTiles.LavaTube, MarsTiles.Noctis, MarsTiles.Phobos, MarsTiles.ResearchOutpost,
        MarsTiles.UrbanizedArea], null, new MarsResourceCount(MarsResourceType.Megacredits, 1), false, true)), [], 13,
      [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, -2)], [], [MarsTags.Builder, MarsTags.City],
      [MarsTiles.City], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Pets',
      'When any city tile is placed, add an animal to this card. Animals may not be removed from this card. 1 Vp per 2 animals on this card.',
      new MarsReaction(null, 0, MarsTags.None, null, new MarsTilePlayed([MarsTiles.City, MarsTiles.Capital, MarsTiles.Ganymede, MarsTiles.LavaTube, MarsTiles.Noctis, MarsTiles.Phobos, MarsTiles.ResearchOutpost,
        MarsTiles.UrbanizedArea], new MarsResourceCount(MarsResourceType.AnimalToken, 1, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'pets'), null,
        false, true)), [], 10, [], [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'pets', true)],
      [MarsTags.Animal, MarsTags.Earth], [], [], new MarsScore(0.5, MarsResourceType.AnimalToken)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Rover Construction', 'When any city tile is placed, gain 2 megacredits.',
      new MarsReaction(null, 0, MarsTags.None, null, new MarsTilePlayed([MarsTiles.City, MarsTiles.Capital, MarsTiles.Ganymede, MarsTiles.LavaTube, MarsTiles.Noctis, MarsTiles.Phobos, MarsTiles.ResearchOutpost,
      MarsTiles.UrbanizedArea], new MarsResourceCount(MarsResourceType.Megacredits, 2), null, false, true)), [], 8, [], [],
      [MarsTags.Builder], [], [], new MarsScore(1)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Arctic Algea', 'When anyone places an ocean tile, gain 2 plants.',
      new MarsReaction(null, 0, MarsTags.None, null, new MarsTilePlayed([MarsTiles.Ocean, MarsTiles.ArtificialLake],
        new MarsResourceCount(MarsResourceType.Plants, 2), null, false, true)), [], 12, [], [new MarsResourceCount(MarsResourceType.Plants, 1)],
      [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -12, true)]));
    this.projects.push(new MarsActionProject(this.projects.length, 'Adaptation Technology', 'Your global requirements are +2 or -2 steps, your choice in each case.',
      new MarsReaction(null, 0, MarsTags.None, null, null, 0, true), [], 12, [], [], [MarsTags.Science], [], [], new MarsScore(1)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Protected Habitats', 'Opponents may not remove your plants, animals, or microbes.',
      new MarsReaction(null, 0, MarsTags.None, null, null, 0, false, true), [], 5, [], [], [], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Olympus Conference',
      'When you play a science tag, including this, either add a science resource to this card, or remove a science resource from this card and draw a card.',
      new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Science], [new MarsResourceCount(MarsResourceType.ScienceToken, 1, false, false,
      [new MarsResourceCount(MarsResourceType.ScienceToken, -1, false, false, [new MarsResourceCount(MarsResourceType.Project, 1)], new MarsPrereqCondition(MarsPrereq.Token, 1),
      false, MarsTags.Science, false, MarsTiles.None, false, false, false, 0, 'olympus')], null, false, MarsTags.Science, false, MarsTiles.None, false, false, false, 0, 'olympus')])),
      [], 12, [], [], [MarsTags.Builder, MarsTags.Earth, MarsTags.Science], [], [], new MarsScore(1)));
    this.projects.push(new MarsActionProject(this.projects.length, 'Advanced Alloys', 'Each titanium and steel you have is worth 1 megacredit extra.',
      new MarsReaction(null, 0, MarsTags.None, null, null, 0, false, false, true), [], 9, [], [], [MarsTags.Science], [], []));
    this.projects.push(new MarsActionProject(this.projects.length, 'Mars University', 'When you play a science tag, including this, you may discard a card from hand to draw a card.',
      new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Science],
      [new MarsResourceCount(MarsResourceType.Project, -1, false, true, [new MarsResourceCount(MarsResourceType.Project, 1)])])), [], 8, [], [],
      [MarsTags.Builder, MarsTags.Science], [], [], new MarsScore(1)));

    // prelude expansion - blue cards
    this.projects.push(new MarsActionProject(this.projects.length, 'Psychrophiles', 'When paying for a plant card, microbes here may be used as 2 megacredits each.',
      new MarsReaction(new MarsAlternateCredits(MarsResourceType.MicrobeToken, 2, 'psychrophiles', MarsTags.Plant)),
      [new MarsAction('Action -> Add a microbe to this card.', null, null, null, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, false, false, false, 0, 'psychrophiles')])],
      2, [], [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -20, true)], null, MarsExpansion.Prelude));

    // promo - green cards
    this.projects.push(new MarsProject(this.projects.length, 'Snow Algae', '', 12, [new MarsResourceCount(MarsResourceType.Plants, 1), new MarsResourceCount(MarsResourceType.Heat, 1)], [],
      [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 2)], new MarsScore(0), false, false, MarsExpansion.Promos));

    // base set - green cards
    this.projects.push(new MarsProject(this.projects.length, 'Trees', '', 13, [new MarsResourceCount(MarsResourceType.Plants, 3)], [new MarsResourceCount(MarsResourceType.Plants, 1)],
      [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -4)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Bushes', '', 10, [new MarsResourceCount(MarsResourceType.Plants, 2)], [new MarsResourceCount(MarsResourceType.Plants, 2)],
      [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -10)]));
    this.projects.push(new MarsProject(this.projects.length, 'Great Escarpment Consortium', '', 6, [new MarsResourceCount(MarsResourceType.Steel, -1, true, false, [], null, false,
      MarsTags.None, false, MarsTiles.None, true), new MarsResourceCount(MarsResourceType.Steel, 1)], [], [], [],
      [new MarsPrereqCondition(MarsPrereq.Production, 1, false, [], [MarsResourceType.Steel])]));
    this.projects.push(new MarsProject(this.projects.length, 'Hackers', '', 3, [new MarsResourceCount(MarsResourceType.Energy, -1),
      new MarsResourceCount(MarsResourceType.Megacredits, -2, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true),
      new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], [], [], [], new MarsScore(-1)));
    this.projects.push(new MarsProject(this.projects.length, 'Nitrophilic Moss', '', 8, [new MarsResourceCount(MarsResourceType.Plants, 2)],
      [new MarsResourceCount(MarsResourceType.Plants, -2)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 3)]));
    this.projects.push(new MarsProject(this.projects.length, 'Lightning Harvest', '', 8, [new MarsResourceCount(MarsResourceType.Energy, 1), new MarsResourceCount(MarsResourceType.Megacredits, 1)],
      [], [MarsTags.Energy], [], [new MarsPrereqCondition(MarsPrereq.Tags, 3, false, [MarsTags.Science])], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Energy Saving', 'Increase your energy production 1 step for each city tile in play.', 15,
      [new MarsResourceCount(MarsResourceType.Energy, 1, false, false, [], null, false, MarsTags.None, true, MarsTiles.City, false, false)], [], [MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Fuel Factory', '', 6,
      [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Titanium, 1), new MarsResourceCount(MarsResourceType.Megacredits, 1)], [],
      [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Nuclear Power', '', 10, [new MarsResourceCount(MarsResourceType.Megacredits, -2), new MarsResourceCount(MarsResourceType.Energy, 3)],
      [], [MarsTags.Builder, MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Tectonic Stress Power', '', 18, [new MarsResourceCount(MarsResourceType.Energy, 3)],
      [], [MarsTags.Builder, MarsTags.Energy], [], [new MarsPrereqCondition(MarsPrereq.Tags, 2, false, [MarsTags.Science])], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Sponsors', '', 6, [new MarsResourceCount(MarsResourceType.Megacredits, 2)],
      [], [MarsTags.Earth]));
    this.projects.push(new MarsProject(this.projects.length, 'Heather', '', 6, [new MarsResourceCount(MarsResourceType.Plants, 1)],
      [new MarsResourceCount(MarsResourceType.Plants, 1)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -14)]));
    this.projects.push(new MarsProject(this.projects.length, 'Lagrange Observatory', '', 9, [], [new MarsResourceCount(MarsResourceType.Project, 1)],
      [MarsTags.Science, MarsTags.Space], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Phobos Space Haven', '', 25, [new MarsResourceCount(MarsResourceType.Titanium, 1)], [],
      [MarsTags.City, MarsTags.Space], [MarsTiles.Phobos], [], new MarsScore(3)));
    this.projects.push(new MarsProject(this.projects.length, 'Vesta Shipyard', '', 15, [new MarsResourceCount(MarsResourceType.Titanium, 1)], [],
      [MarsTags.Jovian, MarsTags.Space], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Fusion Power', '', 14, [new MarsResourceCount(MarsResourceType.Energy, 3)], [],
      [MarsTags.Builder, MarsTags.Energy, MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Tags, 2, false, [MarsTags.Energy])]));
    this.projects.push(new MarsProject(this.projects.length, 'Eos Chasma National Park', '', 16, [new MarsResourceCount(MarsResourceType.Megacredits, 2)],
      [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, true), new MarsResourceCount(MarsResourceType.Plants, 3)],
      [MarsTags.Builder, MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -12)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Tundra Farming', '', 16, [new MarsResourceCount(MarsResourceType.Megacredits, 2), new MarsResourceCount(MarsResourceType.Plants, 1)],
      [new MarsResourceCount(MarsResourceType.Plants, 1)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -6)], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Natural Preserve', 'Place this tile next to no other tile.', 9, [new MarsResourceCount(MarsResourceType.Megacredits, 1)],
      [], [MarsTags.Builder, MarsTags.Science], [MarsTiles.NaturalPreserve], [new MarsPrereqCondition(MarsPrereq.Temperature, -4, true)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Insects', 'Increase your plant production 1 step for each plant tag you have.', 9,
      [new MarsResourceCount(MarsResourceType.Plants, 1, false, false, [], null, true, MarsTags.Plant)], [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 6)]));
    this.projects.push(new MarsProject(this.projects.length, 'Cloud Seeding', '', 11,
      [new MarsResourceCount(MarsResourceType.Heat, -1, true), new MarsResourceCount(MarsResourceType.Megacredits, -1), new MarsResourceCount(MarsResourceType.Plants, 2)],
      [], [], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 3)]));
    this.projects.push(new MarsProject(this.projects.length, 'Asteroid Mining Consortium', '', 13, [new MarsResourceCount(MarsResourceType.Titanium, -1, true, false, [], null, false,
      MarsTags.None, false, MarsTiles.None, true), new MarsResourceCount(MarsResourceType.Titanium, 1)],
      [], [MarsTags.Jovian], [], [new MarsPrereqCondition(MarsPrereq.Production, 1, false, [], [MarsResourceType.Titanium])], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Plantation', '', 15, [], [], [MarsTags.Plant], [MarsTiles.Greenery],
      [new MarsPrereqCondition(MarsPrereq.Tags, 2, false, [MarsTags.Science])]));
    this.projects.push(new MarsProject(this.projects.length, 'Terraforming Ganymede', 'Raise your TR 1 step for each Jovian tag you have, including this.', 33, [],
      [new MarsResourceCount(MarsResourceType.Terraforming, 1, false, false, [], null, true, MarsTags.Jovian)], [MarsTags.Jovian, MarsTags.Space], [], [], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Industrial Microbes', '', 12, [new MarsResourceCount(MarsResourceType.Energy, 1), new MarsResourceCount(MarsResourceType.Steel, 1)],
      [], [MarsTags.Builder, MarsTags.Microbe]));
    this.projects.push(new MarsProject(this.projects.length, 'Tropical Resort', '', 13, [new MarsResourceCount(MarsResourceType.Heat, -2), new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [], [MarsTags.Builder], [], [], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Algae', '', 10, [new MarsResourceCount(MarsResourceType.Plants, 2)],
      [new MarsResourceCount(MarsResourceType.Plants, 1)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 5)]));
    this.projects.push(new MarsProject(this.projects.length, 'Noctis City', 'Place a city tile ON THE RESERVED AREA, disregarding normal placement restrictions.', 18,
      [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 3)], [], [MarsTags.Builder, MarsTags.City], [MarsTiles.Noctis]));
    this.projects.push(new MarsProject(this.projects.length, 'Lake Marineris', '', 18, [], [], [], [MarsTiles.Ocean, MarsTiles.Ocean], [new MarsPrereqCondition(MarsPrereq.Temperature, 0)],
      new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Protected Valley', 'Place a greenery tile ON AN AREA RESERVED FOR OCEAN, disregarding normal placement restrictions.', 23,
      [new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], [MarsTags.Builder, MarsTags.Plant], [MarsTiles.ProtectedValley]));
    this.projects.push(new MarsProject(this.projects.length, 'Mine', '', 4, [new MarsResourceCount(MarsResourceType.Steel, 1)], [], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Media Archives', 'Gain 1 megacredit for each event EVER PLAYED by all players', 8, [],
      [new MarsResourceCount(MarsResourceType.Megacredits, 1, true, false, [], null, true, MarsTags.Event)], [MarsTags.Earth]));
    this.projects.push(new MarsProject(this.projects.length, 'Corporate Stronghold', '', 11, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [], [MarsTags.Builder, MarsTags.City], [MarsTiles.City], [], new MarsScore(-2)));
    this.projects.push(new MarsProject(this.projects.length, 'Grass', '', 11, [new MarsResourceCount(MarsResourceType.Plants, 1)],
      [new MarsResourceCount(MarsResourceType.Plants, 3)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -16)]));
    this.projects.push(new MarsProject(this.projects.length, 'Heat Trappers', '', 6, [new MarsResourceCount(MarsResourceType.Heat, -2, true), new MarsResourceCount(MarsResourceType.Energy, 1)],
      [], [MarsTags.Builder, MarsTags.Energy], [], [], new MarsScore(-1)));
    this.projects.push(new MarsProject(this.projects.length, 'Black Polar Dust', '', 15, [new MarsResourceCount(MarsResourceType.Megacredits, -2), new MarsResourceCount(MarsResourceType.Heat, 3)],
      [], [], [MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Fueled Generators', '', 1, [new MarsResourceCount(MarsResourceType.Megacredits, -1), new MarsResourceCount(MarsResourceType.Energy, 1)],
      [], [MarsTags.Builder, MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Cartel', 'Increase your megacredit production 1 step for each Earth tag you have, including this.', 8,
      [new MarsResourceCount(MarsResourceType.Megacredits, 1, false, false, [], null, true, MarsTags.Earth)], [], [MarsTags.Earth]));
    this.projects.push(new MarsProject(this.projects.length, 'Titanium Mine', '', 7, [new MarsResourceCount(MarsResourceType.Titanium, 1)],
      [], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'GHG Factories', '', 11, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Heat, 4)],
      [], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Magnetic Field Dome', '', 5, [new MarsResourceCount(MarsResourceType.Energy, -2), new MarsResourceCount(MarsResourceType.Plants, 1)],
      [new MarsResourceCount(MarsResourceType.Terraforming, 1)], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Solar Wind Power', '', 11, [new MarsResourceCount(MarsResourceType.Energy, 1)],
      [new MarsResourceCount(MarsResourceType.Titanium, 2)], [MarsTags.Energy, MarsTags.Science, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Biomass Combusters', '', 4, [new MarsResourceCount(MarsResourceType.Plants, -1, true), new MarsResourceCount(MarsResourceType.Energy, 2)],
      [], [MarsTags.Builder, MarsTags.Energy], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 6)], new MarsScore(-1)));
    this.projects.push(new MarsProject(this.projects.length, 'Archaebacteria', '', 6, [new MarsResourceCount(MarsResourceType.Plants, 1)],
      [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -10, true)]));
    this.projects.push(new MarsProject(this.projects.length, 'Open City', '', 23, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 4)],
      [new MarsResourceCount(MarsResourceType.Plants, 2)], [MarsTags.Builder, MarsTags.City], [MarsTiles.City], [new MarsPrereqCondition(MarsPrereq.Oxygen, 12)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Greenhouses', 'Gain 1 plant for each city tile in play.', 6, [],
      [new MarsResourceCount(MarsResourceType.Plants, 1, false, false, [], null, false, MarsTags.None, true, MarsTiles.City, false, false)], [MarsTags.Builder, MarsTags.Plant]));
    this.projects.push(new MarsProject(this.projects.length, 'Giant Space Mirror', '', 17, [new MarsResourceCount(MarsResourceType.Energy, 3)],
      [], [MarsTags.Energy, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Worms', 'Increase your plant production 1 step for every 2 microbe tags you have, including this.', 8,
      [new MarsResourceCount(MarsResourceType.Plants, 0.5, false, false, [], null, true, MarsTags.Microbe)], [], [MarsTags.Microbe], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 4)]));
    this.projects.push(new MarsProject(this.projects.length, 'Moss', '', 4, [new MarsResourceCount(MarsResourceType.Plants, 1)],
      [new MarsResourceCount(MarsResourceType.Plants, -1)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 3)]));
    this.projects.push(new MarsProject(this.projects.length, 'Insulation', 'Decrease your heat production any number of steps and increase your megacredit production the same number of steps.', 2,
      [new MarsResourceCount(MarsResourceType.Heat, 0), new MarsResourceCount(MarsResourceType.Megacredits, 0)],
      [], [], [], [], new MarsScore(0)));
    this.projects.push(new MarsProject(this.projects.length, 'Solar Power', '', 11, [new MarsResourceCount(MarsResourceType.Energy, 1)],
      [], [MarsTags.Builder, MarsTags.Energy], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Wave Power', '', 8, [new MarsResourceCount(MarsResourceType.Energy, 1)],
      [], [MarsTags.Energy], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Magnetic Field Generator', '', 20, [new MarsResourceCount(MarsResourceType.Energy, -4), new MarsResourceCount(MarsResourceType.Plants, 2)],
      [new MarsResourceCount(MarsResourceType.Terraforming, 3)], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Breathing Filters', '', 11, [], [], [MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 7)], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Advanced Ecosystems', '', 11, [], [], [MarsTags.Animal, MarsTags.Microbe, MarsTags.Plant], [],
      [new MarsPrereqCondition(MarsPrereq.Tags, 1, false, [MarsTags.Animal, MarsTags.Microbe, MarsTags.Plant])], new MarsScore(3)));
    this.projects.push(new MarsProject(this.projects.length, 'Urbanized Area', 'Place a city tile ADJACENT TO AT LEAST 2 OTHER CITY TILES.', 10,
      [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 2)],
      [], [MarsTags.Builder, MarsTags.City], [MarsTiles.UrbanizedArea]));
    this.projects.push(new MarsProject(this.projects.length, 'Trans-Neptune Probe', '', 6, [], [], [MarsTags.Science, MarsTags.Space], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Lichen', '', 7, [new MarsResourceCount(MarsResourceType.Plants, 1)], [], [MarsTags.Plant], [],
      [new MarsPrereqCondition(MarsPrereq.Temperature, -24)]));
    this.projects.push(new MarsProject(this.projects.length, 'Toll Station', '', 12,
      [new MarsResourceCount(MarsResourceType.Megacredits, 1, false, false, [], null, true, MarsTags.Space, false, MarsTiles.None, true)],
      [], [MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Power Plant', '', 7, [new MarsResourceCount(MarsResourceType.Energy, 1)],
      [], [MarsTags.Builder, MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Mining Rights', 'Place this tile on an area with a steel or titanium placement bonus. Gain 1 production of that resource type.', 9,
      [], [], [MarsTags.Builder], [MarsTiles.Mining]));
    this.projects.push(new MarsProject(this.projects.length, 'Zeppelins', '', 13,
      [new MarsResourceCount(MarsResourceType.Megacredits, 1, false, false, [], null, false, MarsTags.Animal, true, MarsTiles.City, false, true)],
      [], [], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 5)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Artificial Lake', 'Place an ocean on a spot not reserved for oceans', 15, [], [], [MarsTags.Builder], [MarsTiles.ArtificialLake], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Research', '', 11, [], [new MarsResourceCount(MarsResourceType.Project, 2)], [MarsTags.Science, MarsTags.Science],
      [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Callisto Penal Mines', '', 24, [new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [], [MarsTags.Jovian, MarsTags.Space], [], [], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Gene Repair', '', 12, [new MarsResourceCount(MarsResourceType.Megacredits, 2)],
      [], [MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Tags, 3, false, [MarsTags.Science])], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Power Supply Consortium', '', 5, [new MarsResourceCount(MarsResourceType.Energy, -1, true, false, [], null, false,
      MarsTags.None, false, MarsTiles.None, true), new MarsResourceCount(MarsResourceType.Energy, 1)], [], [MarsTags.Energy], [],
      [new MarsPrereqCondition(MarsPrereq.Tags, 2, false, [MarsTags.Energy])]));
    this.projects.push(new MarsProject(this.projects.length, 'Asteriod Mining', '', 30, [new MarsResourceCount(MarsResourceType.Titanium, 2)],
      [], [MarsTags.Jovian, MarsTags.Space], [], [], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Capital', '1 ADDITIONAL VP FOR EACH OCEAN TILE ADJACENT TO THIS CITY TILE.', 26,
      [new MarsResourceCount(MarsResourceType.Energy, -2), new MarsResourceCount(MarsResourceType.Megacredits, 5)], [], [MarsTags.Builder, MarsTags.City], [MarsTiles.Capital],
      [new MarsPrereqCondition(MarsPrereq.Ocean, 4)], new MarsScore(1, MarsResourceType.None, true, MarsTiles.Ocean, true)));
    this.projects.push(new MarsProject(this.projects.length, 'Artificial Photosynthesis', '', 12,
      [new MarsResourceCount(MarsResourceType.Plants, 1, false, false, [new MarsResourceCount(MarsResourceType.Energy, 2)])],
      [], [MarsTags.Science]));
    this.projects.push(new MarsProject(this.projects.length, 'Beam from a Thorium Asteroid', '', 32, [new MarsResourceCount(MarsResourceType.Heat, 3), new MarsResourceCount(MarsResourceType.Energy, 3)],
      [], [MarsTags.Energy, MarsTags.Jovian, MarsTags.Space], [], [new MarsPrereqCondition(MarsPrereq.Tags, 1, false, [MarsTags.Jovian])], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Micro-Mills', '', 3, [new MarsResourceCount(MarsResourceType.Heat, 1)], [], []));
    this.projects.push(new MarsProject(this.projects.length, 'Carbonate Processing', '', 6, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Heat, 3)],
      [], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Ganymede Colony', 'Place a city tile ON THE RESERVED AREA. 1 VP per Jovian tag you have.', 20,
      [], [], [MarsTags.City, MarsTags.Jovian, MarsTags.Space], [MarsTiles.Ganymede], [], new MarsScore(1, MarsResourceType.None, false, MarsTiles.None, false, true, MarsTags.Jovian)));
    this.projects.push(new MarsProject(this.projects.length, 'Energy Tapping', '', 3,
      [new MarsResourceCount(MarsResourceType.Energy, -1, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true),
        new MarsResourceCount(MarsResourceType.Energy, 1)],
      [], [MarsTags.Energy], [], [], new MarsScore(-1)));
    this.projects.push(new MarsProject(this.projects.length, 'Immigration Shuttles', '1 VP for every 3rd city tile in play.', 31, [new MarsResourceCount(MarsResourceType.Megacredits, 5)],
      [], [MarsTags.Earth, MarsTags.Space], [], [], new MarsScore(0.34, MarsResourceType.None, true, MarsTiles.City)));
    this.projects.push(new MarsProject(this.projects.length, 'Robotic Workforce', 'Duplicate the production box of one of your building cards.', 9,
      [new MarsResourceCount(MarsResourceType.RoboticWorkforce, 1)], [], [MarsTags.Science], [], []));
    this.projects.push(new MarsProject(this.projects.length, 'Adapted Lichen', '', 9, [new MarsResourceCount(MarsResourceType.Plants, 1)],
      [], [MarsTags.Plant]));
    this.projects.push(new MarsProject(this.projects.length, 'Nuclear Zone', '', 10, [], [new MarsResourceCount(MarsResourceType.Temperature, 2)], [MarsTags.Earth], [MarsTiles.NuclearZone],
      [], new MarsScore(-2)));
    this.projects.push(new MarsProject(this.projects.length, 'Domed Crater', '', 24, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [new MarsResourceCount(MarsResourceType.Plants, 3)], [MarsTags.Builder, MarsTags.City], [MarsTiles.City], [new MarsPrereqCondition(MarsPrereq.Oxygen, 7, true)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Noctis Farming', '', 10, [new MarsResourceCount(MarsResourceType.Megacredits, 1)],
      [new MarsResourceCount(MarsResourceType.Plants, 2)], [MarsTags.Builder, MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -20)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Mining Area', 'Place this tile on an area with a steel or titanium placement bonus, adjacent to another of your tiles. Gain 1 production of that resource type.', 4,
      [], [], [MarsTags.Builder], [MarsTiles.AdjacentMining]));
    this.projects.push(new MarsProject(this.projects.length, 'Medical Lab', 'Increase your megacredit production 1 step for every 2 building tags you have, including this.', 13,
      [new MarsResourceCount(MarsResourceType.Megacredits, 0.5, false, false, [], null, true, MarsTags.Builder)], [], [MarsTags.Builder, MarsTags.Science], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Designed Microorganisms', '', 16, [new MarsResourceCount(MarsResourceType.Plants, 2)],
      [], [MarsTags.Microbe, MarsTags.Science], [], [new MarsPrereqCondition(MarsPrereq.Temperature, -14, true)]));
    this.projects.push(new MarsProject(this.projects.length, 'Mangrove', 'Place a greenery tile ON AN AREA RESERVED FOR OCEAN. Disregard the normal placement restrictions.', 12,
      [], [], [MarsTags.Plant], [MarsTiles.Mangrove], [new MarsPrereqCondition(MarsPrereq.Temperature, 4)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Kelp Farming', '', 17, [new MarsResourceCount(MarsResourceType.Megacredits, 2), new MarsResourceCount(MarsResourceType.Plants, 3)],
      [new MarsResourceCount(MarsResourceType.Plants, 2)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 6)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Soletta', '', 35, [new MarsResourceCount(MarsResourceType.Heat, 7)],
      [], [MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Io Mining Industries', '1 VP per Jovian tag you have.', 41,
      [new MarsResourceCount(MarsResourceType.Titanium, 2), new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], [MarsTags.Jovian, MarsTags.Space],
      [], [], new MarsScore(1, MarsResourceType.None, false, MarsTiles.None, false, true, MarsTags.Jovian)));
    this.projects.push(new MarsProject(this.projects.length, 'Colonizer Training Camp', '', 7, [], [], [MarsTags.Builder, MarsTags.Jovian], [],
      [new MarsPrereqCondition(MarsPrereq.Oxygen, 5, true)], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Miranda Resort', 'Increase your megacredit production 1 step for each Earth tag you have.', 12,
      [new MarsResourceCount(MarsResourceType.Megacredits, 1, false, false, [], null, true, MarsTags.Earth)],
      [], [MarsTags.Jovian, MarsTags.Space], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Great Dam', '', 12, [new MarsResourceCount(MarsResourceType.Energy, 2)],
      [], [MarsTags.Builder, MarsTags.Energy], [], [new MarsPrereqCondition(MarsPrereq.Ocean, 4)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Power Grid', 'Increase your energy production 1 step for each power tag you have, including this.', 18,
      [new MarsResourceCount(MarsResourceType.Energy, 1, false, false, [], null, true, MarsTags.Energy)], [], [MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Mohole Area', 'Place this tile ON AN AREA RESERVED FOR OCEAN.', 20, [new MarsResourceCount(MarsResourceType.Heat, 4)],
      [], [MarsTags.Builder], [MarsTiles.Mohole]));
    this.projects.push(new MarsProject(this.projects.length, 'Peroxide Power', '', 7, [new MarsResourceCount(MarsResourceType.Megacredits, -1), new MarsResourceCount(MarsResourceType.Energy, 2)],
      [], [MarsTags.Builder, MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Windmills', '', 6, [new MarsResourceCount(MarsResourceType.Energy, 1)],
      [], [MarsTags.Builder, MarsTags.Energy], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 7)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Satellites', 'Increase your megacredit production 1 step for each space tag you have, including this.', 10,
      [new MarsResourceCount(MarsResourceType.Megacredits, 1, false, false, [], null, true, MarsTags.Space)], [], [MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Dust Seals', '', 2, [], [], [], [],
      [new MarsPrereqCondition(MarsPrereq.Ocean, 3, true)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Commercial District', '1 VP PER ADJACENT CITY TILE.', 16,
      [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 4)], [], [MarsTags.Builder], [MarsTiles.Commercial],
      [], new MarsScore(1, MarsResourceType.None, true, MarsTiles.City, true)));
    this.projects.push(new MarsProject(this.projects.length, 'Rad-Chem Factory', '', 7, [new MarsResourceCount(MarsResourceType.Energy, -1)],
      [new MarsResourceCount(MarsResourceType.Terraforming, 2)], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Soil Factory', '', 9, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Plants, 1)],
      [], [MarsTags.Builder], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Geothermal Power', '', 11, [new MarsResourceCount(MarsResourceType.Energy, 2)],
      [], [MarsTags.Builder, MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Deep Well Heating', '', 13, [new MarsResourceCount(MarsResourceType.Energy, 1)],
      [new MarsResourceCount(MarsResourceType.Temperature, 1)], [MarsTags.Builder, MarsTags.Energy]));
    this.projects.push(new MarsProject(this.projects.length, 'Strip Mine', '', 25, [new MarsResourceCount(MarsResourceType.Energy, -2),
      new MarsResourceCount(MarsResourceType.Steel, 2), new MarsResourceCount(MarsResourceType.Titanium, 1)],
      [new MarsResourceCount(MarsResourceType.Oxygen, 2)], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Food Factory', '', 12, [new MarsResourceCount(MarsResourceType.Plants, -1), new MarsResourceCount(MarsResourceType.Megacredits, 4)],
      [], [MarsTags.Builder], [], [], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Farming', '', 16, [new MarsResourceCount(MarsResourceType.Megacredits, 2), new MarsResourceCount(MarsResourceType.Plants, 2)],
      [new MarsResourceCount(MarsResourceType.Plants, 2)], [MarsTags.Plant], [], [new MarsPrereqCondition(MarsPrereq.Temperature, 4)], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Acquired Company', '', 10, [new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [], [MarsTags.Earth]));
    this.projects.push(new MarsProject(this.projects.length, 'Cupola City', '', 16, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 3)],
      [], [MarsTags.Builder, MarsTags.City], [MarsTiles.City], [new MarsPrereqCondition(MarsPrereq.Oxygen, 9, true)]));
    this.projects.push(new MarsProject(this.projects.length, 'Methane from Titan', '', 28, [new MarsResourceCount(MarsResourceType.Heat, 2), new MarsResourceCount(MarsResourceType.Plants, 2)],
      [], [MarsTags.Jovian, MarsTags.Space], [], [new MarsPrereqCondition(MarsPrereq.Oxygen, 2)], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Rad-Suits', '', 6, [new MarsResourceCount(MarsResourceType.Megacredits, 1)],
      [], [], [], [new MarsPrereqCondition(MarsPrereq.Tile, 2, false, [], [], MarsTiles.City)], new MarsScore(1)));
    this.projects.push(new MarsProject(this.projects.length, 'Lunar Beam', '', 13, [new MarsResourceCount(MarsResourceType.Megacredits, -2),
      new MarsResourceCount(MarsResourceType.Heat, 2), new MarsResourceCount(MarsResourceType.Energy, 2)], [], [MarsTags.Earth, MarsTags.Earth]));
    this.projects.push(new MarsProject(this.projects.length, 'Building Industries', '', 6, [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Steel, 2)],
      [], [MarsTags.Builder]));
    this.projects.push(new MarsProject(this.projects.length, 'Underground City', '', 18, [new MarsResourceCount(MarsResourceType.Energy, -2), new MarsResourceCount(MarsResourceType.Steel, 2)],
      [], [MarsTags.Builder, MarsTags.City], [MarsTiles.City]));

    // prelude expansion - green cards
    this.projects.push(new MarsProject(this.projects.length, 'Space Hotels', '', 12, [new MarsResourceCount(MarsResourceType.Megacredits, 4)],
      [], [MarsTags.Earth, MarsTags.Space], [], [new MarsPrereqCondition(MarsPrereq.Tags, 2, false, [MarsTags.Earth])], new MarsScore(0), false, false, MarsExpansion.Prelude));
    this.projects.push(new MarsProject(this.projects.length, 'Lava Tube Settlement', 'Place a city tile ON A VOLCANIC AREA, regardless of adjacent cities.', 15,
      [new MarsResourceCount(MarsResourceType.Energy, -1), new MarsResourceCount(MarsResourceType.Megacredits, 2)], [], [MarsTags.Builder, MarsTags.City],
      [MarsTiles.LavaTube], [], new MarsScore(0), false, false, MarsExpansion.Prelude));
    this.projects.push(new MarsProject(this.projects.length, 'House Printing', '', 10, [new MarsResourceCount(MarsResourceType.Steel, 1)],
      [], [MarsTags.Builder], [], [], new MarsScore(1), false, false, MarsExpansion.Prelude));
    this.projects.push(new MarsProject(this.projects.length, 'SF Memorial', '', 7, [], [new MarsResourceCount(MarsResourceType.Project, 1)], [MarsTags.Builder],
      [], [], new MarsScore(1), false, false, MarsExpansion.Prelude));
    this.projects.push(new MarsProject(this.projects.length, 'Research Coordination', 'After being played, when you perform an action, the wild tag counts as any tag of your choice.', 4,
      [], [], [MarsTags.Wild], [], [], new MarsScore(0), false, false, MarsExpansion.Prelude));

    // promo - event cards
    this.projects.push(new MarsProject(this.projects.length, 'Small Asteroid', '', 10, [], [new MarsResourceCount(MarsResourceType.Temperature, 1),
      new MarsResourceCount(MarsResourceType.Plants, -2, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true)], [MarsTags.Event, MarsTags.Space],
      [], [], new MarsScore(0), false, false, MarsExpansion.Promos));
    
    // base game - event cards
    this.projects.push(new MarsProject(this.projects.length, 'Aerobraked Ammonia Asteroid', '', 26,
      [new MarsResourceCount(MarsResourceType.Heat, 3), new MarsResourceCount(MarsResourceType.Plants, 1)],
      [new MarsResourceCount(MarsResourceType.MicrobeToken, 2, false, true)], [MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Deimos Down', '', 31, [], [new MarsResourceCount(MarsResourceType.Steel, 4),
      new MarsResourceCount(MarsResourceType.Plants, -8, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true),
      new MarsResourceCount(MarsResourceType.Temperature, 3)], [MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Flooding', 'You may remove 4 megacredits from the owner of a tile this is placed next to.', 7, [],
      [new MarsResourceCount(MarsResourceType.Megacredits, -4, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true, false, false, 0, null, false, true)],
      [MarsTags.Event], [MarsTiles.Ocean], [], new MarsScore(-1)));
    this.projects.push(new MarsProject(this.projects.length, 'Land Claim', 'Place your marker on a non-reserved area. Only you may place a tile here.', 1, [], [],
      [MarsTags.Event], [MarsTiles.LandClaim]));
    this.projects.push(new MarsProject(this.projects.length, 'Imported GHG', '', 7, [new MarsResourceCount(MarsResourceType.Heat, 1)],
      [new MarsResourceCount(MarsResourceType.Heat, 3)], [MarsTags.Earth, MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Sabotage', '', 1, [], [new MarsResourceCount(MarsResourceType.Titanium, -3, true, false,
      [new MarsResourceCount(MarsResourceType.Steel, -4, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true),
        new MarsResourceCount(MarsResourceType.Megacredits, -7, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true)], null, false, MarsTags.None, false, MarsTiles.None, true)],
      [MarsTags.Event]));
    this.projects.push(new MarsProject(this.projects.length, 'Indentured Workers', 'The next card you play this generation cost 8 megacredits less.', 0, [], [],
      [MarsTags.Event], [], [], new MarsScore(-1), false, true));
    this.projects.push(new MarsProject(this.projects.length, 'Import of Advanced GHG', '', 9, [new MarsResourceCount(MarsResourceType.Heat, 2)], [],
      [MarsTags.Earth, MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Large Convoy', '', 36, [],
      [new MarsResourceCount(MarsResourceType.Project, 2), new MarsResourceCount(MarsResourceType.Plants, 5, false, false, [new MarsResourceCount(MarsResourceType.AnimalToken, 4, false, true)])],
      [MarsTags.Earth, MarsTags.Event, MarsTags.Space], [MarsTiles.Ocean], [], new MarsScore(2)));
    this.projects.push(new MarsProject(this.projects.length, 'Permafrost Extraction', '', 8, [], [], [MarsTags.Event], [MarsTiles.Ocean],
      [new MarsPrereqCondition(MarsPrereq.Temperature, -8)]));
    this.projects.push(new MarsProject(this.projects.length, 'Giant Ice Asteroid', '', 36, [],
      [new MarsResourceCount(MarsResourceType.Temperature, 2), new MarsResourceCount(MarsResourceType.Plants, -6, true, false)],
      [MarsTags.Event, MarsTags.Space], [MarsTiles.Ocean, MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Virus', '', 1, [],
      [new MarsResourceCount(MarsResourceType.AnimalToken, -2, true, true, [new MarsResourceCount(MarsResourceType.Plants, -5, true, false, [], null, false,
        MarsTags.None, false, MarsTiles.None, true)], null, false, MarsTags.None, false, MarsTiles.None, true)], [MarsTags.Event, MarsTags.Microbe]));
    this.projects.push(new MarsProject(this.projects.length, 'Special Design', 'The next card you play this generation is +2 or -2 in global requirements, your choice.', 4, [],
      [], [MarsTags.Event, MarsTags.Science], [], [], null, true));
    this.projects.push(new MarsProject(this.projects.length, 'Business Contacts', 'Look at the top 4 cards from the deck. Take 2 of them into hand and discard the other two.', 7, [],
      [new MarsResourceCount(MarsResourceType.Project, 4, false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 2)], [MarsTags.Earth, MarsTags.Event]));
    this.projects.push(new MarsProject(this.projects.length, 'Comet', '', 21, [],
      [new MarsResourceCount(MarsResourceType.Temperature, 1), new MarsResourceCount(MarsResourceType.Plants, -3, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true)],
      [MarsTags.Event, MarsTags.Space], [MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Lava Flows', 'Place this on either THARSIS THOLUS, ASCRAEUS MONS, PAVONIS MONS OR ARSIA MONS', 18, [],
      [new MarsResourceCount(MarsResourceType.Temperature, 2)], [MarsTags.Event], [MarsTiles.LavaFlows]));
    this.projects.push(new MarsProject(this.projects.length, 'Ceo\'s Favorite Project', 'Add 1 resource to a card with at least 1 resouce on it.', 1, [],
      [new MarsResourceCount(MarsResourceType.AnimalToken, 1, false, true, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, true),
        new MarsResourceCount(MarsResourceType.FleetToken, 1, false, true), new MarsResourceCount(MarsResourceType.ScienceToken, 1, false, true)])],
      [MarsTags.Event], [], [new MarsPrereqCondition(MarsPrereq.Token, 1)], new MarsScore(0)));
    this.projects.push(new MarsProject(this.projects.length, 'Technology Demonstration', '', 5, [],
      [new MarsResourceCount(MarsResourceType.Project, 2)], [MarsTags.Event, MarsTags.Science, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Investment Loan', '', 3, [new MarsResourceCount(MarsResourceType.Megacredits, -1)],
      [new MarsResourceCount(MarsResourceType.Megacredits, 10)], [MarsTags.Earth, MarsTags.Event]));
    this.projects.push(new MarsProject(this.projects.length, 'Interstellar Colony Ship', '', 24, [], [], [MarsTags.Earth, MarsTags.Event, MarsTags.Space], [],
      [new MarsPrereqCondition(MarsPrereq.Tags, 5, false, [MarsTags.Science])], new MarsScore(4)));
    this.projects.push(new MarsProject(this.projects.length, 'Invention Contest', 'Look at the top 3 cards from the deck. Take 1 of them into hand and discard the other 2.', 2, [],
      [new MarsResourceCount(MarsResourceType.Project, 3, false, false, [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 1)],
      [MarsTags.Event, MarsTags.Science]));
    this.projects.push(new MarsProject(this.projects.length, 'Imported Hydrogen', '', 16, [],
      [new MarsResourceCount(MarsResourceType.Plants, 3, false, false, [new MarsResourceCount(MarsResourceType.MicrobeToken, 3, false, true), new MarsResourceCount(MarsResourceType.AnimalToken, 2, false, true)])],
      [MarsTags.Earth, MarsTags.Event, MarsTags.Space], [MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Imported Nitrogen', '', 23, [],
      [new MarsResourceCount(MarsResourceType.Terraforming, 1), new MarsResourceCount(MarsResourceType.Plants, 4),
        new MarsResourceCount(MarsResourceType.MicrobeToken, 3, false, true), new MarsResourceCount(MarsResourceType.AnimalToken, 2, false, true)],
      [MarsTags.Earth, MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Local Heat Trapping', '', 1, [],
      [new MarsResourceCount(MarsResourceType.Heat, -5), new MarsResourceCount(MarsResourceType.Plants, 4, false, false, [new MarsResourceCount(MarsResourceType.AnimalToken, 2, false, true)])],
      [MarsTags.Event]));
    this.projects.push(new MarsProject(this.projects.length, 'Big Asteroid', '', 27, [],
      [new MarsResourceCount(MarsResourceType.Temperature, 2), new MarsResourceCount(MarsResourceType.Titanium, 4),
        new MarsResourceCount(MarsResourceType.Plants, -4, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true)], [MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Release of Inert Gases', '', 14, [], [new MarsResourceCount(MarsResourceType.Terraforming, 2)],
      [MarsTags.Event]));
    this.projects.push(new MarsProject(this.projects.length, 'Bribed Committee', '', 7, [], [new MarsResourceCount(MarsResourceType.Terraforming, 2)],
      [MarsTags.Earth, MarsTags.Event], [], [], new MarsScore(-2)));
    this.projects.push(new MarsProject(this.projects.length, 'Ice Cap Melting', '', 5, [], [], [MarsTags.Event], [MarsTiles.Ocean],
      [new MarsPrereqCondition(MarsPrereq.Temperature, 2)]));
    this.projects.push(new MarsProject(this.projects.length, 'Hired Raiders', 'Steal up to 2 steel, or 3 megacredits from any player.', 1, [],
      [new MarsResourceCount(MarsResourceType.Steel, -2, true, false, [new MarsResourceCount(MarsResourceType.Megacredits, -3, true, false, [], null, false, MarsTags.None, false,
        MarsTiles.None, true, false, false, 0, null, false, false, false, true)], null, false, MarsTags.None, false,
        MarsTiles.None, true, false, false, 0, null, false, false, false, true)], [MarsTags.Event]));
    this.projects.push(new MarsProject(this.projects.length, 'Subterranean Reservoir', '', 11, [], [], [MarsTags.Event], [MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Mining Expidition', '', 12, [],
      [new MarsResourceCount(MarsResourceType.Oxygen, 1), new MarsResourceCount(MarsResourceType.Plants, -2, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true),
      new MarsResourceCount(MarsResourceType.Steel, 2)], [MarsTags.Event]));
    this.projects.push(new MarsProject(this.projects.length, 'Nitrogen-Rich Asteroid', 'Increase your plant production 1 step, or 4 steps if you have 3 plant tags.', 31,
      [new MarsResourceCount(MarsResourceType.Plants, 1),
      new MarsResourceCount(MarsResourceType.Plants, 3, false, false, [], new MarsPrereqCondition(MarsPrereq.Tags, 3, false, [MarsTags.Plant]))],
      [new MarsResourceCount(MarsResourceType.Terraforming, 2), new MarsResourceCount(MarsResourceType.Temperature, 1)],
      [MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Ice Asteroid', '', 23, [], [], [MarsTags.Event, MarsTags.Space], [MarsTiles.Ocean, MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Towing a Comet', '', 23, [],
      [new MarsResourceCount(MarsResourceType.Plants, 2), new MarsResourceCount(MarsResourceType.Oxygen, 1)],
      [MarsTags.Event, MarsTags.Space], [MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Asteroid', '', 14, [],
      [new MarsResourceCount(MarsResourceType.Temperature, 1), new MarsResourceCount(MarsResourceType.Titanium, 2),
        new MarsResourceCount(MarsResourceType.Plants, -3, true, false, [], null, false, MarsTags.None, false, MarsTiles.None, true)],
      [MarsTags.Event, MarsTags.Space]));
    this.projects.push(new MarsProject(this.projects.length, 'Convoy from Europa', '', 15, [], [new MarsResourceCount(MarsResourceType.Project, 1)],
      [MarsTags.Event, MarsTags.Space], [MarsTiles.Ocean]));
    this.projects.push(new MarsProject(this.projects.length, 'Mineral Deposit', '', 5, [], [new MarsResourceCount(MarsResourceType.Steel, 5)], [MarsTags.Event]));

    // prelude expansion - events cards
    this.projects.push(new MarsProject(this.projects.length, 'Martian Survey', '', 9, [],
      [new MarsResourceCount(MarsResourceType.Project, 2)], [MarsTags.Event, MarsTags.Science], [], [], new MarsScore(1), false, false, MarsExpansion.Prelude));
  }

  private initializeStandardProjects() {
    this.standardProjects = new Array<MarsStandardProject>();

    this.standardProjects.push(new MarsStandardProject(this.standardProjects.length, 'Sell Patents', 0, null, null, MarsTiles.None, true));
    this.standardProjects.push(new MarsStandardProject(this.standardProjects.length, 'Power Plant', 11, new MarsResourceCount(MarsResourceType.Energy, 1)));
    this.standardProjects.push(new MarsStandardProject(this.standardProjects.length, 'Asteroid', 14, null, new MarsResourceCount(MarsResourceType.Temperature, 1)));
    this.standardProjects.push(new MarsStandardProject(this.standardProjects.length, 'Aquifier', 18, null, null, MarsTiles.Ocean));
    this.standardProjects.push(new MarsStandardProject(this.standardProjects.length, 'Greenery', 23, null, null, MarsTiles.Greenery));
    this.standardProjects.push(new MarsStandardProject(this.standardProjects.length, 'City', 25, new MarsResourceCount(MarsResourceType.Megacredits, 1), null, MarsTiles.City));
  }

  private initializeMilestonesAndAwards() {
    this.milestones = new Array<MarsMilestones>();

    // base
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Terraformer', new MarsPrereqCondition(MarsPrereq.Resource, 35, false, [], [MarsResourceType.Terraforming])));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Mayor', new MarsPrereqCondition(MarsPrereq.Tile, 3, false, [], [], MarsTiles.City, true)));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Gardener', new MarsPrereqCondition(MarsPrereq.Tile, 3, false, [], [], MarsTiles.Greenery, true)));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Builder', new MarsPrereqCondition(MarsPrereq.Tags, 8, false, [MarsTags.Builder])));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Planner', new MarsPrereqCondition(MarsPrereq.Resource, 16, false, [], [MarsResourceType.Project])));

    // hellas
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Diversifier', new MarsPrereqCondition(MarsPrereq.UniqueTags, 8), MarsMap.Hellas));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Tactician', new MarsPrereqCondition(MarsPrereq.PrereqCards, 5), MarsMap.Hellas));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Polar Explorer', new MarsPrereqCondition(MarsPrereq.PolarTiles, 3), MarsMap.Hellas));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Energizer', new MarsPrereqCondition(MarsPrereq.Production, 6, false, [], [MarsResourceType.Energy]), MarsMap.Hellas));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Rim Settler', new MarsPrereqCondition(MarsPrereq.Tags, 3, false, [MarsTags.Jovian]), MarsMap.Hellas));

    // elysium
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Generalist', new MarsPrereqCondition(MarsPrereq.Production, 1, false, [],
      [MarsResourceType.Megacredits, MarsResourceType.Steel, MarsResourceType.Titanium, MarsResourceType.Plants, MarsResourceType.Energy, MarsResourceType.Heat]), MarsMap.Elysium));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Specialist', new MarsPrereqCondition(MarsPrereq.Production, 10, false, [],
      [MarsResourceType.Megacredits, MarsResourceType.Steel, MarsResourceType.Titanium, MarsResourceType.Plants, MarsResourceType.Energy, MarsResourceType.Heat], MarsTiles.None, false, true),
      MarsMap.Elysium));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Ecologist', new MarsPrereqCondition(MarsPrereq.Tags, 4, false, [MarsTags.Animal, MarsTags.Plant, MarsTags.Microbe],
      [], MarsTiles.None, false, true), MarsMap.Elysium));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Tycoon', new MarsPrereqCondition(MarsPrereq.NonEventCards, 15), MarsMap.Elysium));
    this.milestones.push(new MarsMilestones(this.milestones.length, 'Legend', new MarsPrereqCondition(MarsPrereq.Tags, 5, false, [MarsTags.Event]), MarsMap.Elysium));

    this.awards = new Array<MarsAwards>();

    // base
    this.awards.push(new MarsAwards(this.awards.length, 'Landlord', new MarsPrereqCondition(MarsPrereq.Landlord, 0)));
    this.awards.push(new MarsAwards(this.awards.length, 'Banker', new MarsPrereqCondition(MarsPrereq.Production, 0, false, [], [MarsResourceType.Megacredits])));
    this.awards.push(new MarsAwards(this.awards.length, 'Scientist', new MarsPrereqCondition(MarsPrereq.Tags, 0, false, [MarsTags.Science])));
    this.awards.push(new MarsAwards(this.awards.length, 'Global Warmer', new MarsPrereqCondition(MarsPrereq.Resource, 0, false, [], [MarsResourceType.Heat])));
    this.awards.push(new MarsAwards(this.awards.length, 'Miner', new MarsPrereqCondition(MarsPrereq.Resource, 0, false, [], [MarsResourceType.Steel, MarsResourceType.Titanium])));

    // hellas
    this.awards.push(new MarsAwards(this.awards.length, 'Cultivator', new MarsPrereqCondition(MarsPrereq.Tile, 0, false, [], [], MarsTiles.Greenery), MarsMap.Hellas));
    this.awards.push(new MarsAwards(this.awards.length, 'Magnate', new MarsPrereqCondition(MarsPrereq.GreenCards, 0), MarsMap.Hellas));
    this.awards.push(new MarsAwards(this.awards.length, 'Space Baron', new MarsPrereqCondition(MarsPrereq.Tags, 0, false, [MarsTags.Space]), MarsMap.Hellas));
    this.awards.push(new MarsAwards(this.awards.length, 'Excentric', new MarsPrereqCondition(MarsPrereq.Token, 0), MarsMap.Hellas));
    this.awards.push(new MarsAwards(this.awards.length, 'Contractor', new MarsPrereqCondition(MarsPrereq.Tags, 0, false, [MarsTags.Builder]), MarsMap.Hellas));

    // elysium
    this.awards.push(new MarsAwards(this.awards.length, 'Celebrity', new MarsPrereqCondition(MarsPrereq.NonEventCards, 20), MarsMap.Elysium));
    this.awards.push(new MarsAwards(this.awards.length, 'Industrialist', new MarsPrereqCondition(MarsPrereq.Resource, 0, false, [], [MarsResourceType.Steel, MarsResourceType.Energy]), MarsMap.Elysium));
    this.awards.push(new MarsAwards(this.awards.length, 'Desert Settler', new MarsPrereqCondition(MarsPrereq.PolarTiles, 0), MarsMap.Elysium));
    this.awards.push(new MarsAwards(this.awards.length, 'Estate Dealer', new MarsPrereqCondition(MarsPrereq.EstateDealer, 0), MarsMap.Elysium));
    this.awards.push(new MarsAwards(this.awards.length, 'Benefactor', new MarsPrereqCondition(MarsPrereq.Resource, 0, false, [], [MarsResourceType.Terraforming]), MarsMap.Elysium));
  }
}
