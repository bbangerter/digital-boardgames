import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSliderModule } from '@angular/material/slider';
import { MatSortModule } from '@angular/material/sort';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { TerraformingMarsGameControllerComponent } from './terraforming-mars-game-controller/terraforming-mars-game-controller.component';
import { TerraformingMarsBoardComponent } from './terraforming-mars-board/terraforming-mars-board.component';
import { TerraformingMarsSetupComponent } from './terraforming-mars-setup/terraforming-mars-setup.component';
import { TerraformingMarsStartingHandComponent } from './terraforming-mars-starting-hand/terraforming-mars-starting-hand.component';
import { TerraformingMarsProjectsComponent } from './terraforming-mars-projects/terraforming-mars-projects.component';
import { TerraformingMarsPlayerMatComponent } from './terraforming-mars-player-mat/terraforming-mars-player-mat.component';
import { TerraformingMarsGenerationComponent } from './terraforming-mars-generation/terraforming-mars-generation.component';
import { TerraformingMarsCorporationsComponent } from './terraforming-mars-corporations/terraforming-mars-corporations.component';
import { TerraformingMarsTagsComponent } from './terraforming-mars-tags/terraforming-mars-tags.component';
import { TerraformingMarsResourceTargetComponent } from './terraforming-mars-resource-target/terraforming-mars-resource-target.component';
import { TerraformingMarsTilePlacementComponent } from './terraforming-mars-tile-placement/terraforming-mars-tile-placement.component';
import { TerraformingMarsFinalScoreComponent } from './terraforming-mars-final-score/terraforming-mars-final-score.component';
import { TerraformingMarsPreludeComponent } from './terraforming-mars-prelude/terraforming-mars-prelude.component';

@NgModule({
  declarations: [
    TerraformingMarsGameControllerComponent,
    TerraformingMarsBoardComponent,
    TerraformingMarsSetupComponent,
    TerraformingMarsStartingHandComponent,
    TerraformingMarsProjectsComponent,
    TerraformingMarsPlayerMatComponent,
    TerraformingMarsGenerationComponent,
    TerraformingMarsCorporationsComponent,
    TerraformingMarsTagsComponent,
    TerraformingMarsResourceTargetComponent,
    TerraformingMarsTilePlacementComponent,
    TerraformingMarsFinalScoreComponent,
    TerraformingMarsPreludeComponent
  ],
  exports: [
    TerraformingMarsGameControllerComponent
  ],
  imports: [
    CommonModule,
    DragDropModule,
    MatCheckboxModule,
    MatTableModule,
    MatRadioModule,
    MatSelectModule,
    MatCardModule,
    MatTabsModule,
    MatSliderModule,
    MatSortModule,
    FormsModule,
    CoreModule
  ]
})
export class TerraformingMarsModule { }
