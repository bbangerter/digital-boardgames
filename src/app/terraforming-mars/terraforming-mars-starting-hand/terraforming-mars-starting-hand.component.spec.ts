import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsStartingHandComponent } from './terraforming-mars-starting-hand.component';

describe('TerraformingMarsStartingHandComponent', () => {
  let component: TerraformingMarsStartingHandComponent;
  let fixture: ComponentFixture<TerraformingMarsStartingHandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsStartingHandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsStartingHandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
