import { Component } from '@angular/core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { PlayerStateService } from '../../core';
import { MarsCorp, MarsResourceCount } from '../dtos/terraforming-mars-dtos.model';
import { TerraformingMarsDecksService } from '../services/terraforming-mars-decks.service';
import { MarsMessageType, MarsResourceType, MarsExpansion, MarsTags } from '../terraforming-mars-constants';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';

@Component({
  selector: 'terraforming-mars-starting-hand',
  templateUrl: './terraforming-mars-starting-hand.component.html',
  styleUrls: [
    './terraforming-mars-starting-hand.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsStartingHandComponent {
  displayedColumns = ['Name', 'Production', 'Resources', 'Effect', 'Special', 'Tags', 'Select'];
  playerCorps: Array<MarsCorp>;

  constructor(private gameState: TerraformingMarsStateService, private playerState: PlayerStateService, private decks: TerraformingMarsDecksService,
    private marsPlayerState: TerraformingMarsPlayerStateService) {
    var corpIds = this.marsPlayerState.getAllPlayerCorporations().filter(c => c.playerId == this.playerState.getPlayerId()).map(c => c.cardId);
    this.playerCorps = this.decks.getCorporations(corpIds);
  }

  public usingPreludes(): boolean {
    return this.gameState.usingExpansion(MarsExpansion.Prelude);
  }

  public firstPlayer(): string {
    return this.gameState.getPlayerName(this.gameState.getFirstPlayer());
  }

  public getStartingProduction(id: number): Array<MarsResourceCount> {
    return this.playerCorps.find(c => c.id == id).production;
  }

  public getResourceImage(resource: MarsResourceType): string {
    return this.gameState.getResourceImage(resource);
  }

  public getResourceExtra(resource: MarsResourceCount) {
    return this.gameState.getResourceExtra(resource);
  }

  public getStartingResources(id: number): Array<MarsResourceCount> {
    return this.playerCorps.find(c => c.id == id).resources;
  }

  public getCorpTags(id: number): Array<MarsTags> {
    return this.playerCorps.find(c => c.id == id).tags;
  }

  public getTagImage(tag: MarsTags): string {
    return this.gameState.getTagImage(tag);
  }

  public discardCorp(id: number) {
    this.gameState.sendGameMessage(MarsMessageType.DiscardCorp, this.playerCorps.find(c => c.id == id).id);
    var index = this.playerCorps.findIndex(c => c.id == id);
    if (index >= -1) {
      this.playerCorps.splice(index, 1);
      this.displayedColumns.splice(this.displayedColumns.length - 1, 1);
    }
  }

  public canContinue(): boolean {
    if (this.playerCorps.length > 1) {
      return false;
    }

    if (this.gameState.usingExpansion(MarsExpansion.Prelude) && this.marsPlayerState.getPreludeCards(this.playerState.getPlayerId()).length > 2) {
      return false;
    }

    return this.playerCorps[0].resources.find(r => r.resource == MarsResourceType.Megacredits).amount + this.getPreludeCost() >= this.getProjectCost();
  }

  public getProjectCost(): number {
    var projects = this.marsPlayerState.getPlayerDealtProjects().filter(p => p.playerId == this.playerState.getPlayerId());
    return projects.length * 3;
  }

  public getPreludeCost(): number {
    var cost = 0;
    if (this.gameState.usingExpansion(MarsExpansion.Prelude)) {
      var preludes = this.marsPlayerState.getPreludeCards(this.playerState.getPlayerId());
      preludes.forEach((p) => {
        var card = this.decks.getPrelude(p);
        card.resources.forEach((r) => {
          if (r.resource == MarsResourceType.Megacredits) {
            cost += r.amount;
          }
        });
      });
    }

    return cost;
  }
}
