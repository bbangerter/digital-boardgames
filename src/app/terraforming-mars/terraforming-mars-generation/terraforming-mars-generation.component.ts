import { Component } from '@angular/core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { PlayerStateService } from '../../core';
import { MarsMessageType, MarsPhase } from '../terraforming-mars-constants';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';

@Component({
  selector: 'terraforming-mars-generation',
  templateUrl: './terraforming-mars-generation.component.html',
  styleUrls: [
    './terraforming-mars-generation.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsGenerationComponent {
  constructor(private gameState: TerraformingMarsStateService, private playerState: PlayerStateService, private marsPlayerState: TerraformingMarsPlayerStateService) { }

  public canTakeActions(): boolean {
    return this.gameState.playerCanTakeActions();
  }

  public allPlayersReady(): boolean {
    return this.gameState.allPlayersReady();
  }

  public hasDealtCards(): boolean {
    return this.marsPlayerState.getPlayerDealtProjects().filter(p => p.playerId == this.playerState.getPlayerId()).length > 0;
  }

  public generation(): number {
    return this.gameState.getGeneration();
  }

  public currentPlayer(): string {
    return this.gameState.getPlayerName(this.gameState.getCurrentPlayer());
  }

  public firstPlayer(): string {
    return this.gameState.getPlayerName(this.gameState.getFirstPlayer());
  }

  public isCurrentPlayer(): boolean {
    return this.gameState.getCurrentPlayer() == this.playerState.getPlayerId();
  }

  public getRecentEvents(): Array<string> {
    return this.gameState.getRecentEvents();
  }

  public actionsRemaining(): number {
    return this.gameState.getAvailableActions();
  }

  public skip() {
    if (this.gameState.getAvailableActions() == 1) {
      this.gameState.useAction();
    }
    else {
      this.gameState.sendGameMessage(MarsMessageType.Pass, this.playerState.getPlayerId());
    }
    this.gameState.hideMapButtons();
  }

  public getSkipText(): string {
    if (this.gameState.getAvailableActions() == 1) {
      return 'Skip 2nd action';
    }

    return 'Pass';
  }

  public showCardPurchase(): boolean {
    if (this.gameState.getFinalGeneration()) {
      return false;
    }
    return this.hasDealtCards() || !this.allPlayersReady() || this.gameState.getPurchaseMidTurnCards() || this.gameState.getSelectMidTurnCards() > 0;
  }

  public canSkip(): boolean {
    var corp = this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.action != null) {
      if (corp.action.firstTurn && !corp.action.firstTurnUsed) {
        return false;
      }
    }
    return this.canTakeActions() && this.isCurrentPlayer() && !this.hasDealtCards() && this.gameState.allPlayersReady() && this.getCurrentPhase() == MarsPhase.PlayGeneration;
  }

  public getCurrentPhase(): number {
    return this.gameState.getCurrentPhase();
  }
}
