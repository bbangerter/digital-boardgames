import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsGenerationComponent } from './terraforming-mars-generation.component';

describe('TerraformingMarsGenerationComponent', () => {
  let component: TerraformingMarsGenerationComponent;
  let fixture: ComponentFixture<TerraformingMarsGenerationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsGenerationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsGenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
