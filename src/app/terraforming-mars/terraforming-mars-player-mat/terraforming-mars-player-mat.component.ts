import { Component, Input, AfterViewInit } from '@angular/core';
import { PlayerScoreService, GamePlayer, PlayerStateService } from '../../core';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';
import { MarsResourceType, MarsMessageType, MarsTiles, MarsPhase } from '../terraforming-mars-constants';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { MarsResourceTarget, MarsResourceCount, MarsTargetType, MarsCorp } from '../dtos/terraforming-mars-dtos.model';

@Component({
  selector: 'terraforming-mars-player-mat',
  templateUrl: './terraforming-mars-player-mat.component.html',
  styleUrls: [
    './terraforming-mars-player-mat.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsPlayerMatComponent implements AfterViewInit {
  @Input() currentPlayer: boolean;

  targetSelect: boolean = false;
  displayedColumns = ['Name', 'Label', 'Megacredits', 'Steel', 'Titanium', 'Plants', 'Energy', 'Heat'];
  limitTargets: Array<string>;

  constructor(private score: PlayerScoreService, private marsPlayerState: TerraformingMarsPlayerStateService, private gameState: TerraformingMarsStateService,
    private playerState: PlayerStateService) {
    this.limitTargets = [this.playerState.getPlayerId()];
  }

  ngAfterViewInit(): void {
    if (this.hasCorpAction()) {
      this.displayedColumns.push('Action');
    }
  }

  public productionPicked(target: MarsTargetType) {
    if (target != null && target.resource != MarsResourceType.None) {
      this.targetSelect = false;
      this.gameState.setCanTakeActions(true);
      this.gameState.sendGameMessage(MarsMessageType.ApplyProduction, new MarsResourceTarget(new MarsResourceCount(target.resource, 1), this.playerState.getPlayerId(), -1));
      this.gameState.useAction();
    }
  }

  public canTakeCorpAction(playerId: string): boolean {
    if (this.gameState.getCurrentPhase() == MarsPhase.PlayPreludes || this.gameState.getCurrentPlayer() != this.playerState.getPlayerId() || !this.gameState.allPlayersReady() ||
      this.gameState.getFinalGeneration() || playerId != this.playerState.getPlayerId()) {
      return false;
    }

    var corp = this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.action == null) {
      return false;
    }
    if (corp.action.firstTurn && corp.action.firstTurnUsed && !corp.action.allTurns) {
      return false;
    }
    if (corp.action.firstTurn && !corp.action.allTurns) {
      return true;
    }

    if (this.marsPlayerState.getUNMIPlayer() == this.playerState.getPlayerId()) { // UNMI
      return this.gameState.getUNMITerraformRating() < this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Terraforming, true)) &&
        corp.lastCorpAction < this.gameState.getGeneration() && this.canPayCorpActionCost(corp) && this.gameState.getCurrentPlayer() == this.playerState.getPlayerId() && this.gameState.playerCanTakeActions();
    }
    else if (this.canPayCorpActionCost(corp)) {  // Robinson Industries
      return corp.lastCorpAction < this.gameState.getGeneration();
    }

    return false;
  }

  public getCorpAction(): string {
    var corp = this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.action == null) {
      return '';
    }

    return corp.action.action;
  }

  public getTargetProduction(): Array<MarsResourceCount> {
    var corp = this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId());
    if (corp.action != null && corp.action.productionResult != null && corp.action.productionResult.resource == MarsResourceType.AnyBasic) {
      return [corp.action.productionResult];
    }

    return [];
  }

  public corpAction(playerId: string) {
    var corp = this.marsPlayerState.getPlayerCorporation(playerId);
    if (corp.action != null) {
      this.gameState.sendGameMessage(MarsMessageType.RecordEvent, `${this.playerState.getPlayerName()} activates ${corp.name}`);
      this.gameState.sendGameMessage(MarsMessageType.TriggerCorp, this.gameState.getGeneration());
      corp.lastCorpAction = this.gameState.getGeneration();

      if (corp.action.productionCost != null) {
        this.gameState.sendGameMessage(MarsMessageType.ApplyProduction, new MarsResourceTarget(corp.action.productionCost, this.playerState.getPlayerId(), -1));
      }
      if (corp.action.resourceCost != null) {
        this.gameState.sendGameMessage(MarsMessageType.ApplyResources, new MarsResourceTarget(corp.action.resourceCost, this.playerState.getPlayerId(), -1));
      }
      if (corp.action.productionResult != null) {
        if (corp.action.productionResult.resource == MarsResourceType.AnyBasic) {
          this.gameState.setCanTakeActions(false);
          this.targetSelect = true;
          return;
        }
        else {
          this.gameState.sendGameMessage(MarsMessageType.ApplyProduction, new MarsResourceTarget(corp.action.productionResult, this.playerState.getPlayerId(), -1));
        }
      }
      corp.action.resourceResult.forEach((r) => {
        if (r.resource == MarsResourceType.Prelude) {
          // valley trust corp
          this.gameState.setCanTakeActions(false);
        }
        this.gameState.sendGameMessage(MarsMessageType.ApplyResources, new MarsResourceTarget(r, this.playerState.getPlayerId(), -1));
      });

      if (corp.action.tile != MarsTiles.None) {
        this.gameState.addPendingTile(corp.action.tile);
        this.gameState.setCanTakeActions(false);
      }

      corp.action.firstTurnUsed = true;
      if (corp.action.firstTurn && !corp.action.allTurns) {
        this.displayedColumns.pop();
      }
      
      if (corp.action.award) {
        this.gameState.showAwardsButtons(8);
        return;
      }

      // don't use the action for tharsis republic, we will use it when the tile is placed
      if (corp.action.tile == MarsTiles.None) {
        this.gameState.useAction();
      }
    }
  }

  private canPayCorpActionCost(corp: MarsCorp): boolean {
    if (corp.action != null) {
      if (corp.action.productionCost != null) {
        if (this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(corp.action.productionCost.resource, false)) < -corp.action.productionCost.amount) {
          return false;
        }
      }
      if (corp.action.resourceCost != null) {
        if (this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(corp.action.resourceCost.resource, true)) < -corp.action.resourceCost.amount) {
          return false;
        }
      }

      return true;
    }
  }

  public hasCorpAction(): boolean {
    return this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId()).action != null;
  }

  public isUNMIPlayer(): boolean {
    return this.currentPlayer && this.marsPlayerState.getUNMIPlayer() == this.playerState.getPlayerId();
  }

  public getPlayers(): Array<GamePlayer> {
    return this.gameState.getActivePlayers().filter(p => p.id == this.playerState.getPlayerId() && this.currentPlayer || !this.currentPlayer && p.id != this.playerState.getPlayerId());
  }

  public getCorporationName(playerId: string): string {
    return this.marsPlayerState.getPlayerCorporation(playerId).name;
  }

  public getPlayerColorClass(playerId: string): string {
    var color = this.gameState.getPlayerColorClass(playerId);
    var corp = this.marsPlayerState.getPlayerCorporation(playerId);
    if (corp != null && corp.action != null && (!corp.action.firstTurn || corp.action.allTurns) && corp.lastCorpAction < this.gameState.getGeneration()) {
      color += " project-highlight";
    }

    return color;
  }

  public getPlayerClass(playerId: string): string {
    if (playerId == this.playerState.getPlayerId()) {
      return `player-highlight ${this.getPlayerColorClass(playerId)}`;
    }

    return this.getPlayerColorClass(playerId);
  }

  public isPlayer(playerId: string): boolean {
    return playerId == this.playerState.getPlayerId();
  }

  public getMegacredit(playerId: string, supply: boolean): number {
    var x = this.score.getScore(playerId, this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, supply));
    return x;
  }

  public getSteel(playerId: string, supply: boolean): number {
    return this.score.getScore(playerId, this.marsPlayerState.getScoreTrack(MarsResourceType.Steel, supply));
  }

  public getTitanium(playerId: string, supply: boolean): number {
    return this.score.getScore(playerId, this.marsPlayerState.getScoreTrack(MarsResourceType.Titanium, supply));
  }

  public getPlant(playerId: string, supply: boolean): number {
    return this.score.getScore(playerId, this.marsPlayerState.getScoreTrack(MarsResourceType.Plants, supply));
  }

  public getEnergy(playerId: string, supply: boolean): number {
    return this.score.getScore(playerId, this.marsPlayerState.getScoreTrack(MarsResourceType.Energy, supply));
  }

  public getHeat(playerId: string, supply: boolean): number {
    return this.score.getScore(playerId, this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, supply));
  }
}
