import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsPlayerMatComponent } from './terraforming-mars-player-mat.component';

describe('TerraformingMarsPlayerMatComponent', () => {
  let component: TerraformingMarsPlayerMatComponent;
  let fixture: ComponentFixture<TerraformingMarsPlayerMatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsPlayerMatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsPlayerMatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
