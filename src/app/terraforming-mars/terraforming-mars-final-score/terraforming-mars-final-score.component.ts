import { Component } from '@angular/core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { PlayerStateService, PlayerScoreService } from '../../core';
import { MarsFinalScore } from '../dtos/terraforming-mars-dtos.model';
import { MarsScoreTrack } from '../terraforming-mars-constants';

@Component({
  selector: 'terraforming-mars-final-score',
  templateUrl: './terraforming-mars-final-score.component.html',
  styleUrls: [
    './terraforming-mars-final-score.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsFinalScoreComponent {
  displayedColumns = ['Name', 'Score', 'Projects', 'Board', 'Milestones', 'Awards'];

  private finalScore: Array<MarsFinalScore>;
  constructor(private gameState: TerraformingMarsStateService, private playerState: PlayerStateService, private score: PlayerScoreService) {
    this.finalScore = new Array<MarsFinalScore>();
    this.gameState.getActivePlayers().forEach((p) => {
      var score = this.score.getScore(p.id, MarsScoreTrack.Terraforming);
      var cards = this.score.getScore(p.id, MarsScoreTrack.Cards);
      var board = this.score.getScore(p.id, MarsScoreTrack.Board);
      var milestones = this.score.getScore(p.id, MarsScoreTrack.Milestones);
      var awards = this.score.getScore(p.id, MarsScoreTrack.Awards)
      this.finalScore.push(new MarsFinalScore(p.id, score, cards, board, milestones, awards));
    });
  }

  public getPlayers(): Array<MarsFinalScore> {
    return this.finalScore;
  }

  public getName(playerId: string): string {
    return this.gameState.getActivePlayers().find(p => p.id == playerId).name;
  }

  public getPlayerColorClass(playerId: string): string {
    return this.gameState.getPlayerColorClass(playerId);
  }

  public getPlayerClass(playerId: string): string {
    if (playerId == this.playerState.getPlayerId()) {
      return `player-highlight ${this.getPlayerColorClass(playerId)}`;
    }

    return this.getPlayerColorClass(playerId);
  }
}
