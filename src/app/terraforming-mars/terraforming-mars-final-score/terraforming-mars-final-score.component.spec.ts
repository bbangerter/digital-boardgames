import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsFinalScoreComponent } from './terraforming-mars-final-score.component';

describe('TerraformingMarsFinalScoreComponent', () => {
  let component: TerraformingMarsFinalScoreComponent;
  let fixture: ComponentFixture<TerraformingMarsFinalScoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsFinalScoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsFinalScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
