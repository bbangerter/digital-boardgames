import { Component, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { CanvasStateService, PlayerScoreService, GfxRectangle } from '../../core';
import { MarsScoreTrack, MarsMap } from '../terraforming-mars-constants';
import { Subscription } from 'rxjs';
import { MarsPlayerTerraformingMarker } from '../dtos/terraforming-mars-dtos.model';
import { TerraformingMarsBoardStateService } from '../services/terraforming-mars-board-state.service';
import { TerraformingMarsDecksService } from '../services/terraforming-mars-decks.service';

@Component({
  selector: 'terraforming-mars-board',
  templateUrl: './terraforming-mars-board.component.html',
  styleUrls: [
    './terraforming-mars-board.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsBoardComponent implements AfterViewInit, OnDestroy {
  @Input() map: string;

  private temperatureTracker: Subscription;
  private oxygenTracker: Subscription;
  private playerTrackers: Array<Subscription> = [];
  private oxygenMarker: GfxRectangle;
  private temperatureMarker: GfxRectangle;
  private playerMarkers: Array<MarsPlayerTerraformingMarker> = [];
  private oxygenPosition = [{ x: 240, y: 170 }, { x: 266, y: 149 }, { x: 298, y: 128 }, { x: 327, y: 113 }, { x: 361, y: 97 }, { x: 393, y: 91 }, { x: 426, y: 83 },
    { x: 458, y: 83 }, { x: 493, y: 83 }, { x: 526, y: 87 }, { x: 558, y: 94 }, { x: 590, y: 105 }, { x: 619, y: 121 }, { x: 648, y: 138 }, { x: 677, y: 163 }]

  constructor(private gameState: TerraformingMarsStateService, private canvas: CanvasStateService, private score: PlayerScoreService, private marsBoardState: TerraformingMarsBoardStateService,
    private decks: TerraformingMarsDecksService) { }

  ngAfterViewInit(): void {
    this.createBoard();
    this.temperatureTracker = this.score.subscribeScoreChange('global', MarsScoreTrack.Temperature, (data) => this.onTemperatureChange(data));
    this.oxygenTracker = this.score.subscribeScoreChange('global', MarsScoreTrack.Oxygen, (data) => this.onOxygenChange(data));

    this.gameState.getActivePlayers().forEach((p) => {
      this.playerTrackers.push(this.score.subscribeScoreChange(p.id, MarsScoreTrack.Terraforming, (data) => this.onTerraformingChange(data, p.id)));
    });
  }

  ngOnDestroy(): void {
    this.temperatureTracker.unsubscribe();
    this.oxygenTracker.unsubscribe();
    this.playerTrackers.forEach((t) => {
      t.unsubscribe();
    })
    this.playerMarkers.forEach((m) => {
      this.canvas.removeGfxObject(this.marsBoardState.canvasId, m.gfx.getId());
    })
  }

  private createBoard() {
    var board = this.marsBoardState.createBoard(parseInt(this.map));
    this.oxygenMarker = this.canvas.createRectangle(this.marsBoardState.canvasId, 20, 20, '#FFFFFF', false, 2, 0);
    this.temperatureMarker = this.canvas.createRectangle(this.marsBoardState.canvasId, 20, 20, '#FFFFFF', false, 2, 0);
    this.gameState.getActivePlayers().forEach((p) => {
      this.playerMarkers.push(new MarsPlayerTerraformingMarker(p.id, this.marsBoardState.createPlayerMarker(this.gameState.getPlayerColors().find(c => c.playerId == p.id).color, board)));
    });

    this.marsBoardState.initializeBoard(board, parseInt(this.map));
    this.decks.getStandardProjects().forEach((p) => {
      p.gfxId = this.marsBoardState.createStandardProjectButton(p.id, parseInt(this.map));
    });
    this.decks.getMilestones(parseInt(this.map)).forEach((m) => {
      m.gfxId = this.marsBoardState.createMilestoneButton(m.id);
    });
    this.decks.getAwards(parseInt(this.map)).forEach((a) => {
      a.gfxId = this.marsBoardState.createAwardButton(a.id);
    });
  }

  public marsCanvasId(): string {
    return this.marsBoardState.canvasId;
  }

  private onTemperatureChange(data: number) {
    var x = 830;
    if (parseInt(this.map) == MarsMap.Elysium) {
      x = 120;
    }
    this.temperatureMarker.setTranslation(x, 232 - data / 2 * 23);
    if (data == -30) {
      this.temperatureMarker.translate(0, 18);
    }
  }

  private onOxygenChange(data: number) {
    this.oxygenMarker.setTranslation(this.oxygenPosition[data].x, this.oxygenPosition[data].y);
  }

  private onTerraformingChange(data: number, playerId: string) {
    while (data >= 100) {
      data -= 100;
    }

    var index = this.gameState.getActivePlayers().findIndex(p => p.id == playerId);
    var marker = this.playerMarkers.find(m => m.playerId == playerId);

    if (data <= 25) {
      marker.gfx.setTranslation(10, 770 - data * 30.5);
    }
    else if (data <= 50) {
      marker.gfx.setTranslation(10 + (data - 25) * 36, 9);
    }
    else if (data <= 75) {
      marker.gfx.setTranslation(910, 10 + (data - 50) * 30.5);
    }
    else {
      marker.gfx.setTranslation(910 - (data - 75) * 36, 770);
    }

    marker.gfx.translate(index == 2 ? 5 : (index % 2 * 10), index == 2 ? 5 : (index < 2 ? 0 : 10));
  }
}
