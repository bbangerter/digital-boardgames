import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsBoardComponent } from './terraforming-mars-board.component';

describe('TerraformingMarsBoardComponent', () => {
  let component: TerraformingMarsBoardComponent;
  let fixture: ComponentFixture<TerraformingMarsBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
