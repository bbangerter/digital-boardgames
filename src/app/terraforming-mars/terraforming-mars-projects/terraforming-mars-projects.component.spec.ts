import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsProjectsComponent } from './terraforming-mars-projects.component';

describe('TerraformingMarsDealtProjectsComponent', () => {
  let component: TerraformingMarsProjectsComponent;
  let fixture: ComponentFixture<TerraformingMarsProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
