import { Component, Input, AfterViewInit, ViewChild } from '@angular/core';
import { TerraformingMarsDecksService } from '../services/terraforming-mars-decks.service';
import { PlayerStateService, PlayerScoreService, CanvasStateService, GfxClickData } from '../../core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';
import { MarsProject, MarsActionProject, MarsAction, MarsPlayProject, MarsPrereqCondition, MarsResourceCount, MarsResourceTarget, MarsTargetType, MarsScore, MarsPrelude, MarsReaction, MarsTagPlayed } from '../dtos/terraforming-mars-dtos.model';
import { MarsMessageType, MarsTags, MarsResourceType, MarsPrereq, MarsScoreTrack, MarsTiles, MarsPhase, MarsExpansion, MarsCorporationProject } from '../terraforming-mars-constants';
import { TerraformingMarsBoardStateService } from '../services/terraforming-mars-board-state.service';
import { Subscription } from 'rxjs';
import { Sort } from '@angular/material/sort';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { MatTable } from '@angular/material/table';

@Component({
  selector: 'terraforming-mars-projects',
  templateUrl: './terraforming-mars-projects.component.html',
  styleUrls: [
    './terraforming-mars-projects.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsProjectsComponent implements AfterViewInit {
  @Input() dealt: boolean;
  @Input() playedProjects: boolean;
  @Input() allPlayers: boolean;
  @Input() showPurchase: boolean;

  @ViewChild('table1') table1: MatTable<MarsProject>;

  displayedColumns = ['Name', 'Cost', 'Production', 'Resources', 'Description', 'Tiles', 'Tags', 'Prereqs', 'Score', 'Select'];
  steelValue: number = 0;
  titaniumValue: number = 0;
  heatValue: number = 0;
  psychrophileValue: number = 0;
  resourceValue: number = 0;
  errorState: string = '';
  afterTilePlacement: MarsResourceCount = null;
  limitTargets: Array<string> = [];
  lastProjectId: number = -1;

  private gfxClickListener: Subscription;
  private megacreditTracker: Subscription;
  private sellingPatents: boolean = false;
  private specialDesign: number = -1;
  private indenturedWorkers: number = -1;
  private sort: Sort = null;
  constructor(private gameState: TerraformingMarsStateService, private playerState: PlayerStateService, private decks: TerraformingMarsDecksService, private canvas: CanvasStateService,
    private marsPlayerState: TerraformingMarsPlayerStateService, private score: PlayerScoreService, private marsBoardState: TerraformingMarsBoardStateService) {
  }

  ngAfterViewInit(): void {
    if (this.allPlayers) {
      this.displayedColumns.splice(this.displayedColumns.length - 1, 1);
      this.displayedColumns.unshift('Player');
    }
    if (!this.dealt && !this.playedProjects && !this.allPlayers) {
      this.gfxClickListener = this.canvas.subscribeClickEvent(this.marsBoardState.canvasId, (data: GfxClickData) => this.onClick(data));
      this.megacreditTracker = this.score.subscribeScoreChange(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true), (data) => this.onMoneyChange(data));
      if (this.getPlantDiscount() > 0) {
        // update extra project greenery
        var greenery = this.decks.getExtraProjects().find(p => p.id == -1);
        greenery.action[0].action = 'Action 7 plants -> Spend 7 plants to place a greenery';
        greenery.action[0].resourceCost.amount = -7;
      }
    }
  }

  ngOnDestroy(): void {
    if (!this.dealt && !this.playedProjects && !this.allPlayers) {
      this.gfxClickListener.unsubscribe();
      this.megacreditTracker.unsubscribe();
    }
  }

  public showExtraPreludes(): boolean {
    return this.gameState.getCurrentPhase() == MarsPhase.PlayGeneration && this.marsPlayerState.getPlayerDealtPreludes().filter(p => p.playerId == this.playerState.getPlayerId()).length > 0;
  }

  public getCurrentPhase(): number {
    return this.gameState.getCurrentPhase();
  }

  public preludePlayed(cardId: number) {
    var card = this.decks.getPrelude(cardId);
    this.gameState.sendGameMessage(MarsMessageType.RecordEvent, `${this.playerState.getPlayerName()} plays ${card.name}`);
    if (card.playACard) {
      this.gameState.setPlayingPreludeProject(card);
      return;
    }
    else if (card.tiles.length > 0) {
      this.applyTiles(card.tiles);
    }

    if (this.showExtraPreludes()) {
      // add the specific prelude to the player cards
      this.gameState.sendGameMessage(MarsMessageType.PlayPrelude, cardId);
    }

    this.useAction();
  }

  public onSortData(sort: Sort) {
    this.sort = sort;
  }

  public getSelectColumnHeader() {
    if (this.sellingPatents) {
      return 'Sell';
    }
    return this.dealt ? 'Discard' : 'Play';
  }

  private onMoneyChange(data: number) {
    this.gameState.showMapButtons();
  }

  private onClick(data: GfxClickData) {
    if (data != null) {
      var megacredits = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true));
      var heatUsed = 0;

      var standardProject = this.decks.getStandardProjects().find(s => s.gfxId == data.objectId);
      if (standardProject != null) {
        if (standardProject.sellPatents) {
          this.sellingPatents = true;
          this.gameState.setCanTakeActions(false);
          this.gameState.sendGameMessage(MarsMessageType.RecordEvent, `${this.playerState.getPlayerName()} activates standard project ${standardProject.name}`);
          this.gameState.hideMapButtons();
          return;
        }
        else {
          var refund = 0;
          var corp = this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId());
          corp.reaction.forEach((c) => {
            if (c.refund > 0 && c.condition != null && c.condition.type == MarsPrereq.StandardProject && standardProject.cost >= c.condition.amount) {
              refund += c.refund;
            }
          });
          var projects = this.marsPlayerState.getPlayersActionProjects(this.playerState.getPlayerId()).filter(p => p.reaction != null && p.reaction.refund != 0);
          
          projects.forEach((p) => {
            if (p.reaction.condition != null && p.reaction.condition.type == MarsPrereq.StandardProject) {
              refund += p.reaction.refund;
            }
          });

          var cost = standardProject.cost - this.marsPlayerState.getStandardProjectDiscount(standardProject.production);
          if (cost > megacredits) {
            var heatUsed = cost - megacredits;
            cost = megacredits;
          }
          this.applyResources([new MarsResourceCount(MarsResourceType.Megacredits, -cost), new MarsResourceCount(MarsResourceType.Megacredits, refund),
            new MarsResourceCount(MarsResourceType.Heat, -heatUsed)]);
          if (standardProject.production != null) {
            this.applyProduction([standardProject.production]);
          }
          if (standardProject.resource != null) {
            this.applyResources([standardProject.resource]);
          }
        }
        if (standardProject.tile != MarsTiles.None) {
          this.applyTiles([standardProject.tile]);
        }

        this.gameState.sendGameMessage(MarsMessageType.RecordEvent, `${this.playerState.getPlayerName()} activates standard project ${standardProject.name}`);
        this.useAction();
      }

      var milestone = this.decks.getMilestones(this.gameState.getMarsMap()).find(s => s.gfxId == data.objectId);
      if (milestone != null) {
        if (megacredits < 8) {
          heatUsed = 8 - megacredits;
        }
        this.gameState.sendGameMessage(MarsMessageType.ClaimMilestone, milestone.id);
        this.applyResources([new MarsResourceCount(MarsResourceType.Megacredits, -8 + heatUsed), new MarsResourceCount(MarsResourceType.Heat, -heatUsed)]);
        this.gameState.sendGameMessage(MarsMessageType.RecordEvent, `${this.playerState.getPlayerName()} claims ${milestone.name} milestone`);
        this.useAction();
      }

      var award = this.decks.getAwards(this.gameState.getMarsMap()).find(s => s.gfxId == data.objectId);
      if (award != null) {
        cost = 8 + 6 * this.marsPlayerState.getClaimedAwards().length;
        if (megacredits < cost) {
          heatUsed = cost - megacredits;
        }
        this.gameState.sendGameMessage(MarsMessageType.ClaimAward, award.id);
        this.applyResources([new MarsResourceCount(MarsResourceType.Megacredits, -cost + heatUsed), new MarsResourceCount(MarsResourceType.Heat, -heatUsed)]);
        this.gameState.sendGameMessage(MarsMessageType.RecordEvent, `${this.playerState.getPlayerName()} activates ${award.name} award`);
        this.useAction();
      }
    }
  }

  public playerIsReady(): boolean {
    return this.gameState.playerIsReady(this.playerState.getPlayerId());
  }

  public getNewProjectCost(): number {
    var projects = this.marsPlayerState.getPlayerDealtProjects().filter(p => p.playerId == this.playerState.getPlayerId());
    return projects.length * 3;
  }

  public getProjectPurchaseCost() {
    if (this.sellingPatents) {
      return 'Done selling patents';
    }
    if (this.gameState.getSelectMidTurnCards() > 0) {
      return 'Keep cards';
    }
    return `Purchase Projects (${this.getNewProjectCost()} megacredits)`;
  }

  public purchaseProjects() {
    if (this.sellingPatents) {
      this.sellingPatents = false;
      this.useAction();
      this.gameState.showMapButtons();
    }
    else if (this.gameState.getPurchaseMidTurnCards()) {
      this.gameState.setPurchaseMidTurnCards(false);
      this.gameState.sendGameMessage(MarsMessageType.PurchaseMidTurnCards, this.playerState.getPlayerId());
      this.useAction();
    }
    else if (this.gameState.getSelectMidTurnCards() > 0) {
      this.gameState.setSelectMidTurnCards(0);
      this.gameState.sendGameMessage(MarsMessageType.SelectMidTurnCards, this.playerState.getPlayerId());
      this.useAction();
    }
    else {
      this.gameState.sendGameMessage(MarsMessageType.PurchaseProjectCards, this.playerState.getPlayerId());
    }
  }

  public getPlayerColorClass(projectId: number): string {
    var player = this.marsPlayerState.getProjectOwner(projectId);
    var playerColor = this.gameState.getPlayerColorClass(player);

    if (this.playedProjects || this.allPlayers) {
      var card = this.getTriggerActionProject(projectId);
      if (card != null && card.action.length > 0 && (card.triggerGeneration < this.gameState.getGeneration() && projectId >= 0 ||
        projectId < 0 && this.playerCanChoose(projectId))) {
        playerColor += " project-highlight";
      }
    }

    return playerColor;
  }

  public productionPicked(target: MarsTargetType) {
    if (target != null) {
      var production = this.gameState.getPendingProduction()[target.projectIndex];
      if (target.resourceIndex > 0) {
        production = production.or[target.resourceIndex - 1];
      }
      if (target.robotic) {
        var targetCard = this.decks.getProject(target.projectId);
        this.applyProduction(targetCard.production);
      }
      else {
        this.gameState.sendGameMessage(MarsMessageType.ApplyProductionTarget, new MarsResourceTarget(production, target.playerId, target.projectId));
      }
    }
    this.gameState.removePendingProduction();
    this.useAction();
  }

  public supplyPicked(target: MarsTargetType) {
    if (target != null) {
      var resource = this.gameState.getPendingResources()[target.projectIndex];
      if (target.resourceIndex > 0) {
        resource = resource.or[target.resourceIndex - 1];
        resource.or.forEach((o) => {
          this.gameState.sendGameMessage(MarsMessageType.ApplyResourceTarget, new MarsResourceTarget(o, target.playerId, target.projectId));
        });
      }
      // Mars University
      if (resource.resource == MarsResourceType.Project && resource.amount < 0) {
        this.gameState.sendGameMessage(MarsMessageType.DiscardProject, target.projectId);
        resource.or.forEach((o) => {
          this.gameState.sendGameMessage(MarsMessageType.ApplyResourceTarget, new MarsResourceTarget(o, target.playerId, -1));
        });
      }
      else {
        this.gameState.sendGameMessage(MarsMessageType.ApplyResourceTarget, new MarsResourceTarget(resource, target.playerId, target.projectId));
        if (resource.steal) {
          this.applyResources([new MarsResourceCount(resource.resource, -resource.amount)]);
        }
      }
    }
    this.gameState.removePendingResource();
    this.useAction();
  }

  public getPendingProduction(): Array<MarsResourceCount> {
    return this.gameState.getPendingProduction();
  }

  public getPendingResources(): Array<MarsResourceCount> {
    return this.gameState.getPendingResources();
  }

  public getPendingTiles(): Array<MarsTiles> {
    return this.gameState.getPendingTiles();
  }

  public tilePlaced(location: number) {
    var tile = this.gameState.removePendingTile();
    if (tile == MarsTiles.Greenery || tile == MarsTiles.Mangrove || tile == MarsTiles.ProtectedValley) {
      if (this.score.getScore('global', MarsScoreTrack.Oxygen) == 7 && this.score.getScore('global', MarsScoreTrack.Temperature) == -2 && this.score.getScore('global', MarsScoreTrack.Oceans) < 9) {
        this.applyTiles([MarsTiles.Ocean]);
      }
    }

    var tileLocation = this.marsBoardState.tileLocations.find(l => l.id == location);
    if (tileLocation.tile != MarsTiles.None && tile != MarsTiles.LandClaim && tile != MarsTiles.CommunityArea) {
      this.applyTiles([this.marsBoardState.tileLocations.find(l => l.id == location).tile]);
    }

    // special handling for flooding tile
    if (this.afterTilePlacement != null) {
      var adjacentTiles = this.marsBoardState.getAdjacentTiles(location);
      this.limitTargets = adjacentTiles.filter(o => o.tile != MarsTiles.Ocean && o.tile != MarsTiles.ArtificialLake).map(t => t.playerId);
      this.gameState.addPendingResource(this.afterTilePlacement);
      this.afterTilePlacement = null;
    }

    this.useAction();
  }

  public isPlayerTurn(): boolean {
    return this.gameState.getCurrentPlayer() == this.playerState.getPlayerId();
  }

  public getOwner(id: number): string {
    if (id < 0) {
      return this.playerState.getPlayerName();
    }
    var player = this.marsPlayerState.getProjectOwner(id);
    return this.gameState.getPlayerName(player);
  }

  public canUseSteel(projectId: number): boolean {
    if (this.dealt || this.allPlayers || !this.isPlayerTurn() || this.sellingPatents || projectId < 0) {
      return false;
    }
    if (this.playedProjects) {
      var card = this.getTriggerActionProject(projectId);
      if (card == null || card.action.length == 0 || card.action[0].resourceCost == null) {
        return false;
      }
      if (card.action[0].resourceCost.or.length > 0) {
        if (card.action[0].resourceCost.or[0].resource == MarsResourceType.Steel) {
          return true;
        }
      }

      return false;
    }
    return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Steel, true)) > 0 &&
      this.decks.getProject(projectId).tags.some(t => t == MarsTags.Builder);
  }

  public canUseTitanium(projectId: number): boolean {
    if (this.dealt || this.allPlayers || !this.isPlayerTurn() || this.sellingPatents || projectId < 0) {
      return false;
    }
    if (this.playedProjects) {
      var card = this.getTriggerActionProject(projectId);
      if (card == null || card.action.length == 0 || card.action[0].resourceCost == null) {
        return false;
      }
      if (card.action[0].resourceCost.or.length > 0) {
        if (card.action[0].resourceCost.or[0].resource == MarsResourceType.Titanium) {
          return true;
        }
      }

      return false;
    }
    return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Titanium, true)) > 0 &&
      this.decks.getProject(projectId).tags.some(t => t == MarsTags.Space);
  }

  public canUsePsychrophiles(projectId: number): boolean {
    if (this.dealt || this.allPlayers || !this.isPlayerTurn() || this.sellingPatents || projectId < 0) {
      return false;
    }
    return this.score.getScore(this.playerState.getPlayerId(), 'psychrophiles') > 0 &&
      this.decks.getProject(projectId).tags.some(t => t == MarsTags.Plant);
  }

  public canUseHeat(projectId: number): boolean {
    if (this.dealt || this.allPlayers || !this.isPlayerTurn() || this.sellingPatents || projectId < 0) {
      return false;
    }
    if (this.marsPlayerState.isHelion()) {
      if (this.playedProjects) {
        var card = this.getTriggerActionProject(projectId);
        if (card == null || card.action.length == 0 || card.action[0].resourceCost == null) {
          return false;
        }
        if (card.action[0].resourceCost.resource == MarsResourceType.Megacredits) {
          return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, true)) > 0;
        }
      }
      else {
        return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, true)) > 0;
      }
    }

    return false;
  }

  public canUseResource(projectId: number): boolean {
    if (this.dealt || this.allPlayers || !this.isPlayerTurn() || this.sellingPatents || projectId < 0) {
      return false;
    }

    var card = this.decks.getProject(projectId);
    if (card.production.length > 0 && card.production[0].amount == 0) {
      return true;  // insulation
    }

    if (!(card instanceof MarsActionProject)) {
      return false;
    }

    var actionCard = card as MarsActionProject;
    if (actionCard.action.length > 0 && actionCard.action[0].resourceCost != null && actionCard.action[0].resourceCost.amount == 0 && this.playedProjects) {
      return true;
    }

    return false;
  }

  public getMaxSteel(): number {
    return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Steel, true));
  }

  public getMaxTitanium(): number {
    return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Titanium, true));
  }

  public getMaxHeat(): number {
    return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, true));
  }

  public getMaxResource(projectId: number): number {
    var card = this.decks.getProject(projectId);
    if (card.production.length > 0 && card.production[0].amount == 0) {
      return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(card.production[0].resource, false));
    }

    if (card instanceof MarsActionProject) {

      var actionCard = card as MarsActionProject;
      if (actionCard.action.length > 0 && actionCard.action[0].resourceCost.amount == 0) {
        return this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(actionCard.action[0].resourceCost.resource, true));
      }
    }

    return 0;
  }

  public getResourceName(projectId: number): string {
    var card = this.decks.getProject(projectId);
    if (card.production.length > 0 && card.production[0].amount == 0) {
      return this.gameState.getResourceName(card.production[0].resource, 1);
    }

    if (card instanceof MarsActionProject) {
      var actionCard = card as MarsActionProject;
      if (actionCard.action.length > 0 && actionCard.action[0].resourceCost.amount == 0) {
        return this.gameState.getResourceName(actionCard.action[0].resourceCost.resource, 1);
      }
    }

    return '';
  }

  public getMaxPsychrophiles(): number {
    return this.score.getScore(this.playerState.getPlayerId(), 'psychrophiles');
  }

  public onListDrop(event: CdkDragDrop<MarsProject[]>) {
    // Swap the elements around
    var list = this.getPlayerCards();
    var currentId = list[event.currentIndex].id;
    var previousId = list[event.previousIndex].id;
    this.decks.dragAndDropProjectOrder(currentId, previousId);
  }

  public getPlayerCards(): Array<MarsProject> {
    var projectIds;
    if (this.dealt) {
      projectIds = this.marsPlayerState.getPlayerDealtProjects().filter(p => p.playerId == this.playerState.getPlayerId()).map(p => p.cardId);
    }
    else {
      if (this.playedProjects) {
        if (this.allPlayers) {
          projectIds = this.marsPlayerState.getPlayerProjectPlayed().filter(p => p.playerId != this.playerState.getPlayerId()).map(p => p.cardId);
        }
        else {
          projectIds = this.marsPlayerState.getPlayerProjectPlayed().filter(p => p.playerId == this.playerState.getPlayerId()).map(p => p.cardId);
        }
      }
      else {
        projectIds = this.marsPlayerState.getPlayerProjectHand().filter(p => p.playerId == this.playerState.getPlayerId()).map(p => p.cardId);
      }
    }

    var projects = this.decks.getProjects(projectIds);
    if (this.playedProjects && this.allPlayers && !this.gameState.getFinalGeneration()) {
      // remove played event cards
      projects = projects.filter(p => !p.tags.some(t => t == MarsTags.Event));
      projects.sort((a, b) => {
        var aPlayer = this.marsPlayerState.getPlayerProjectPlayed().find(p => p.cardId == a.id).playerId;
        var bPlayer = this.marsPlayerState.getPlayerProjectPlayed().find(p => p.cardId == b.id).playerId;
        var aIndex = this.gameState.getActivePlayers().findIndex(p => p.id == aPlayer);
        var bIndex = this.gameState.getActivePlayers().findIndex(p => p.id == bPlayer);
        if (bIndex == aIndex) {
          return a.id - b.id;
        }
        return aIndex - bIndex;
      });
    }

    if (this.playedProjects && !this.allPlayers) {
      var extras = this.decks.getExtraProjects();
      extras.forEach((e) => {
        projects.unshift(e);
      });
    }

    if (this.sort != null && this.sort.direction != '') {
      projects.sort((a, b) => {
        switch (this.sort.active) {
          case 'Name':
            if (b.name < a.name) {
              return 1 * (this.sort.direction == 'asc' ? 1 : -1);
            }
            else {
              return -1 * (this.sort.direction == 'asc' ? 1 : -1);
            }
          case 'Cost':
            return (b.cost - a.cost) * (this.sort.direction == 'asc' ? 1 : -1);
          case 'Score':
            return (this.marsPlayerState.getProjectScore(b.id) - this.marsPlayerState.getProjectScore(a.id)) * (this.sort.direction == 'asc' ? 1 : -1);
          default:
            return 0;
      }
      });
    }

    return projects;
  }

  // discount to placing a greenery with plants
  private getPlantDiscount(): number {
    var corp = this.marsPlayerState.getPlayerCorporation(this.playerState.getPlayerId());
    var plantDiscount = 0;
    corp.reaction.forEach((c) => {
      if (c.discountedGreenery) {
        plantDiscount = 1;
      }
    });

    return plantDiscount;
  }

  public showTilePlacement(): boolean {
    return !this.dealt && !this.playedProjects && !this.allPlayers;
  }

  public playerCanChoose(projectId: number): boolean {
    if (this.dealt) {
      return !this.playerIsReady() || this.gameState.getPurchaseMidTurnCards() || this.gameState.getSelectMidTurnCards() > 0 && this.marsPlayerState.getPlayerDealtProjects().length > this.gameState.getSelectMidTurnCards();
    }
    else if (!this.playedProjects) {
      if (this.getCurrentPhase() == MarsPhase.PlayPreludes && this.gameState.getPlayingPreludeProject() == null) {
        return false;
      }

      if (this.sellingPatents) {
        return true;
      }
      return this.canPlayProject(projectId) && !this.gameState.getFinalGeneration();
    }
    else if (!this.allPlayers) {
      return this.canTriggerAction(projectId);
    }

    return false;
  }

  private canTriggerAction(projectId: number, index: number = 0): boolean {
    if (this.getCurrentPhase() == MarsPhase.PlayPreludes || !this.canPlayerTakeActions()) {
      return false;
    }

    var card = this.getTriggerActionProject(projectId);
    if (projectId != -1 && this.gameState.getFinalGeneration()) {
      return false;
    }

    if (card != null) {
      if (card.action.length > index && (card.triggerGeneration < this.gameState.getGeneration() || card.id < 0)) {
        if (card.action[index].productionCost != null && !this.hasResource(card.id, card.action[index].productionCost, false)) {
          return false;
        }
        if (card.action[index].tile == MarsTiles.Ocean && this.score.getScore('global', MarsScoreTrack.Oceans) >= 9) {
          return false;
        }
        if (card.action[index].tile == MarsTiles.Greenery && this.marsBoardState.validTileLocations(MarsTiles.Greenery, this.playerState.getPlayerId()).length == 0) {
          return false;
        }
        if (card.id == -2 && this.score.getScore('global', MarsScoreTrack.Temperature) == 8) {
          return false;
        }
        if (card.action[index].resourceCost != null) {
          return this.hasResource(card.id, card.action[index].resourceCost, true);
        }
        if (card.action[index].resourceResult.length > 0 && (card.action[index].resourceResult[0].resource == MarsResourceType.AnimalToken || card.action[index].resourceResult[0].resource == MarsResourceType.MicrobeToken)) {
          if (card.action[index].resourceResult[0].anyCard) {
            // find all played cards that take the same token type
            var cards = this.marsPlayerState.getPlayedResourceCards(card.action[index].resourceResult[0].resource, card.action[index].resourceResult[0].amount < 0, card.id, -card.action[index].resourceResult[0].amount, card.action[index].resourceResult[0].anyCard);
            if (!card.action[index].resourceResult[0].anyPlayer) {
              cards = cards.filter(c => this.marsPlayerState.getProjectOwner(c.id) == this.playerState.getPlayerId());
            }
            return cards.length > 0;
          }
        }

        return true;
      }
    }

    return false;
  }

  private getTriggerActionProject(projectId: number): MarsActionProject {
    var card = this.decks.getProject(projectId);
    if (card instanceof MarsActionProject) {
      return card as MarsActionProject;
    }

    return null;
  }

  private hasResource(projectId: number, resource: MarsResourceCount, supply: boolean): boolean {
    if (resource.amount < 0 && (resource.resource == MarsResourceType.AnimalToken || resource.resource == MarsResourceType.MicrobeToken)) {
      return this.marsPlayerState.getPlayedResourceCards(resource.resource, true, projectId, -resource.amount, resource.anyCard).length > 0;
    }
    if (resource.amount == 0) {   // power infrastructure, and other cards that take a sliding scale
      return this.getMaxResource(projectId) > 0;
    }
    var amount = -resource.amount;
    if (this.canUseSteel(projectId)) {
      amount -= this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Steel, true)) * this.marsPlayerState.getSteelValue();
    }
    if (this.canUseTitanium(projectId)) {
      amount -= this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Titanium, true)) * this.marsPlayerState.getTitaniumValue()
    }
    if (this.canUseHeat(projectId) && resource.resource == MarsResourceType.Megacredits) {
      amount -= this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, true));
    }
    return this.score.getScore(this.playerState.getPlayerId(), resource.tokenScoreTrackId != null ? resource.tokenScoreTrackId : this.marsPlayerState.getScoreTrack(resource.resource, supply)) >= amount;
  }

  public getMissingProductionClass(projectId: number): string {
    var player = this.getPlayerColorClass(projectId);

    if (this.playedProjects == false && this.allPlayers == false) {
      var card = this.decks.getProject(projectId);
      if (!this.gameState.hasProduction(card)) {
        player = player + " missing-highlight";
      }
      else {
        var targetOtherPlayers = card.production.filter(p => p.amount < 0 && p.anyPlayer);
        for (var i = 0; i < targetOtherPlayers.length; i++) {
          var corps = this.marsPlayerState.getAllPlayerCorporations().filter(c => c.playerId != this.playerState.getPlayerId() || !targetOtherPlayers[i].onlyOpponents);
          if (!corps.some(c => this.score.canAdd(targetOtherPlayers[i].amount, c.playerId, this.marsPlayerState.getScoreTrack(targetOtherPlayers[i].resource, false)))) {
            player = player + " missing-highlight";
          }
        }
      }
    }

    return player;
  }

  public getMissingResourceClass(projectId: number): string {
    var player = this.getPlayerColorClass(projectId);
    if (this.playedProjects == false && this.allPlayers == false) {
      var card = this.decks.getProject(projectId);
      if (!this.gameState.hasResource(card)) {
        player = player + " missing-highlight";
      }
    }

    return player;
  }

  public getMissingPrereqsClass(projectId: number): string {
    var player = this.getPlayerColorClass(projectId);
    if (this.playedProjects == false && this.allPlayers == false) {
      var card = this.decks.getProject(projectId);
      if (!this.hasPrereqs(card, projectId)) {
        player = player + " missing-highlight";
      }
    }

    return player;
  }

  public getTilePlacementClass(projectId: number): string {
    var player = this.getPlayerColorClass(projectId);
    if (this.playedProjects == false && this.allPlayers == false) {
      var card = this.decks.getProject(projectId);
      if (card.tile.length > 0) {
        for (var t = 0; t < card.tile.length; t++) {
          if (this.marsBoardState.validTileLocations(card.tile[t], this.playerState.getPlayerId()).length == 0) {
            player = player + " missing-highlight";
          }
        }
      }
    }

    return player;
  }

  public getCostClass(projectId: number): string {
    var player = this.getPlayerColorClass(projectId);
    if (this.dealt == false && this.playedProjects == false && this.allPlayers == false) {
      var card = this.decks.getProject(projectId);
      if (!this.canPayCost(card, projectId)) {
        player = player + " missing-highlight";
      }
    }

    return player;
  }

  private canPlayProject(projectId: number): boolean {
    if (!this.canPlayerTakeActions()) {
      return false;
    }

    if (this.gameState.hasFirstTurnAction() && this.getCurrentPhase() == MarsPhase.PlayGeneration) {
      return false;
    }

    var card = this.decks.getProject(projectId);

    if (!this.gameState.hasProduction(card) || !this.gameState.hasResource(card)) {
      return false;
    }

    // if the card reduces any players production, make sure someone has production to be reduced
    var targetOtherPlayers = card.production.filter(p => p.amount < 0 && p.anyPlayer);
    for (var i = 0; i < targetOtherPlayers.length; i++) {
      var corps = this.marsPlayerState.getAllPlayerCorporations().filter(c => c.playerId != this.playerState.getPlayerId() || !targetOtherPlayers[i].onlyOpponents);
      if (!corps.some(c => this.score.canAdd(targetOtherPlayers[i].amount, c.playerId, this.marsPlayerState.getScoreTrack(targetOtherPlayers[i].resource, false)))) {
        return false;
      }
    }

    // check global or other conditions
    var meetsConditions = this.hasPrereqs(card, projectId);

    // for robotic workforce, make sure there is a card the player can duplicate
    card.production.forEach((p) => {
      if (p.resource == MarsResourceType.RoboticWorkforce) {
        if (this.gameState.roboticWorkforceTargets().length == 0) {
          meetsConditions = false;
        }
      }
    });

    if (!meetsConditions) {
      return false;
    }

    if (card.tile.length > 0) {
      for (var t = 0; t < card.tile.length; t++) {
        if (this.marsBoardState.validTileLocations(card.tile[t], this.playerState.getPlayerId()).length == 0 &&
          card.tile[t] != MarsTiles.Ocean && card.tile[t] != MarsTiles.ArtificialLake) {
          return false;
        }
      }
    }

    // calculate the money and other forms of income we can apply to the project
    return this.canPayCost(card, projectId);
  }

  private canPayCost(card: MarsProject, projectId: number): boolean {
    var megacredits = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true));
    var steel = 0;
    var titanium = 0;
    var alternateCredits = this.marsPlayerState.getAlternateCredits(card.tags);
    if (card.tags.some(t => t == MarsTags.Builder)) {
        steel = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Steel, true)) * this.marsPlayerState.getSteelValue();
    }
    if (card.tags.some(t => t == MarsTags.Space)) {
        titanium = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Titanium, true)) * this.marsPlayerState.getTitaniumValue();
    }
    var cost = this.getProjectCost(projectId);
    return cost <= megacredits + steel + titanium + alternateCredits;
  }

  private hasPrereqs(card: MarsProject, projectId: number): boolean {
    var meetsConditions = true;
    card.prereqs.forEach((p) => {
      switch (p.type) {
        case MarsPrereq.Temperature:
          if (!this.checkGlobalConditions(this.score.getScore('global', MarsScoreTrack.Temperature), p)) {
            meetsConditions = false;
          }
          break;
        case MarsPrereq.Ocean:
          if (!this.checkGlobalConditions(this.score.getScore('global', MarsScoreTrack.Oceans), p)) {
            meetsConditions = false;
          }
          break;
        case MarsPrereq.Oxygen:
          if (!this.checkGlobalConditions(this.score.getScore('global', MarsScoreTrack.Oxygen), p)) {
            meetsConditions = false;
          }
          break;
        case MarsPrereq.Production:
          p.production.forEach((r) => {
            if (!this.hasResource(projectId, new MarsResourceCount(r, -p.amount), false)) {
              meetsConditions = false;
            }
          });
          break;
        case MarsPrereq.Tags:
          if (!this.hasTags(p.amount, p.tag)) {
            meetsConditions = false;
          }
          break;
        case MarsPrereq.Tile:
          if (this.marsBoardState.getTotalTiles(p.tile, false, p.owned ? this.playerState.getPlayerId() : 'any') < p.amount) {
            meetsConditions = false;
          }
          break;
        case MarsPrereq.Token:
          var actions = this.marsPlayerState.getPlayersActionProjects(this.playerState.getPlayerId());
          if (!actions.some(a => this.marsPlayerState.getTokenCount(this.playerState.getPlayerId(), a.id) > 0)) {
            meetsConditions = false;
          }
          break;
      }
    });

    return meetsConditions;
  }

  private hasTags(amount: number, tags: Array<MarsTags>): boolean {
    for (var i = 0; i < tags.length; i++) {
      if (this.marsPlayerState.getTags(this.playerState.getPlayerId(), tags[i], true) < amount) {
        return false;
      }
    }

    return true;
  }

  private checkGlobalConditions(score: number, prereq: MarsPrereqCondition): boolean {
    var adaptation = this.marsPlayerState.adaptationCount();
    if (this.specialDesign == this.gameState.getGeneration()) {
      adaptation += 2;
    }
    if (prereq.type == MarsPrereq.Temperature) {
      adaptation *= 2;
    }
    if (this.gameState.getPlayingPreludeProject() != null && this.gameState.getPlayingPreludeProject().cardAdaptation) {
      adaptation += 100;
    }
    if (prereq.amount - adaptation <= score && !prereq.max || prereq.amount + adaptation >= score && prereq.max) {
      return true;
    }

    return false;
  }

  private getProjectCost(projectId: number): number {
    var card = this.decks.getProject(projectId);
    var discount = 0;
    if (this.gameState.getPlayingPreludeProject() != null) {
      discount = this.gameState.getPlayingPreludeProject().cardDiscount;
    }
    return Math.max(card.cost - this.marsPlayerState.getProjectDiscount(card.tags) - (this.indenturedWorkers == this.gameState.getGeneration() ? 8 : 0) - discount, 0);
  }

  public getButtonLabel(projectId: number, index: number = 0): string {
    if (this.dealt) {
      return 'Discard';
    }
    if (this.sellingPatents) {
      return 'Sell for 1 megacredit';
    }
    if (!this.playedProjects) {
      var cost = this.getProjectCost(projectId);
      if (this.canUseSteel(projectId)) {
        cost -= this.steelValue * this.marsPlayerState.getSteelValue();
      }
      if (this.canUseTitanium(projectId)) {
        cost -= this.titaniumValue * this.marsPlayerState.getTitaniumValue();
      }
      if (this.canUsePsychrophiles(projectId)) {
        cost -= this.marsPlayerState.getPsychrophileValue() * this.psychrophileValue;
      }
      if (this.canUseHeat(projectId)) {
        cost -= this.heatValue;
      }
      return `Purchase (${cost} megacredits)`;
    }
    if (!this.allPlayers) {
      if (this.canTriggerAction(projectId, index)) {
        var card = this.getTriggerActionProject(projectId);
        var actionCost = this.getProjectActionCost(card, index);
        if (actionCost != '') {
          return `${index + 1}) Activate (${actionCost})`;
        }
        return `${index + 1}) Activate`;
      }
    }

    return '';
  }

  private getProjectActionCost(card: MarsActionProject, index: number): string {
    var action = card.action[index];
    var amount = 0;
    if (action.productionCost != null) {
      amount = -action.productionCost.amount;
    }
    else if (action.resourceCost != null) {
      amount = -action.resourceCost.amount;
    }

    if (this.canUseSteel(card.id)) {
      amount -= this.steelValue * this.marsPlayerState.getSteelValue();
    }
    if (this.canUseTitanium(card.id)) {
      amount -= this.titaniumValue * this.marsPlayerState.getTitaniumValue();
    }
    if (this.canUseHeat(card.id)) {
      amount -= this.heatValue;
    }
    if (this.canUseResource(card.id)) {
      amount = this.resourceValue;
    }

    var cost = '';
    if (action.productionCost != null) {
      cost = `${amount} ${this.gameState.getResourceName(action.productionCost.resource, action.productionCost.amount)}`;
    }
    if (cost == '' && action.resourceCost != null) {
      cost = `${amount} ${this.gameState.getResourceName(action.resourceCost.resource, action.resourceCost.amount)}`;
    }

    return cost;
  }

  public getActions(projectId: number): Array<MarsAction> {
    if (!this.isActionCard(projectId)) {
      return [];
    }

    var card = this.decks.getProject(projectId) as MarsActionProject;
    return card.action;
  }

  public isActionCard(projectId: number): boolean {
    var card = this.decks.getProject(projectId);
    if (card instanceof MarsActionProject) {
      return true;
    }
    return false;
  }

  public getCardClass(projectId: number): string {
    var player = this.marsPlayerState.getProjectOwner(projectId);
    var playerColor = this.gameState.getPlayerColorClass(player);

    if (this.isActionCard(projectId)) {
      return `action-card ${playerColor}`;
    }

    if (this.decks.getProject(projectId).tags.some(t => t == MarsTags.Event)) {
      return `event-card ${playerColor}`;
    }

    return `standard-card ${playerColor}`;
  }

  public getProduction(projectId: number): Array<MarsResourceCount> {
    return this.decks.getProject(projectId).production;
  }

  public getResourceImage(resource: MarsResourceType): string {
    return this.gameState.getResourceImage(resource);
  }

  public getResourceExtra(resource: MarsResourceCount) {
    return this.gameState.getResourceExtra(resource);
  }

  public getResources(projectId: number): Array<MarsResourceCount> {
    return this.decks.getProject(projectId).resources;
  }

  public getTiles(projectId: number): string {
    return this.gameState.getTilesString(this.decks.getProject(projectId).tile);
  }

  public getTagImage(tag: MarsTags): string {
    return this.gameState.getTagImage(tag);
  }

  public getTags(projectId: number): Array<MarsTags> {
    return this.decks.getProject(projectId).tags
  }
  
  public getPrereqs(projectId: number): string {
    return this.gameState.getPrereqsString(this.decks.getProject(projectId).prereqs);
  }

  public showPurchaseButton(): boolean {
    if (this.sellingPatents) {
      return true;
    }
    var megacredits = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true));
    if (this.marsPlayerState.isHelion()) {
      megacredits += this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, true));
    }
    if (this.gameState.getPurchaseMidTurnCards() && megacredits < this.getNewProjectCost()) {
      return false;
    }
    if (this.gameState.getSelectMidTurnCards() > 0 && this.marsPlayerState.getPlayerDealtProjects().length > this.gameState.getSelectMidTurnCards()) {
      return false;
    }
    return this.dealt && (!this.playerIsReady() && this.showPurchase || this.gameState.getPurchaseMidTurnCards() || this.gameState.getSelectMidTurnCards() > 0);
  }

  public getScore(id: number): string {
    var score = this.marsPlayerState.getProjectScore(id);
    var tokenCount = this.marsPlayerState.getTokenCount(this.marsPlayerState.getProjectOwner(id), id);
    var scoreString = score.toString();
    if (tokenCount > 0) {
      if (score == 0) {
        return `(${tokenCount} resources)`;
      }
      scoreString += ` (${tokenCount} resources)`;
    }

    return scoreString;
  }

  public selectProject(projectId: number, index: number = 0) {
    if (this.dealt) {
      this.gameState.sendGameMessage(MarsMessageType.DiscardProject, projectId);
    }
    else if (!this.playedProjects) {
      if (this.sellingPatents) {
        this.gameState.sendGameMessage(MarsMessageType.DiscardProject, projectId);
        this.applyResources([new MarsResourceCount(MarsResourceType.Megacredits, 1)]);
        return;
      }
      this.playProjectCard(projectId);
    }
    else if (!this.allPlayers) {
      this.playProjectAction(projectId, index);
    }

    this.steelValue = 0;
    this.titaniumValue = 0;
    this.heatValue = 0;
    this.psychrophileValue = 0;
    this.resourceValue = 0;
    this.errorState = '';
  }

  public hasSecondAction(projectId: number): boolean {
    if (this.allPlayers || !this.playedProjects) {
      return false;
    }
    if (!this.canPlayerTakeActions()) {
      return false;
    }

    var card = this.getTriggerActionProject(projectId);
    if (card == null) {
      return false;
    }

    if (card.action.length > 1) {
      return this.canTriggerAction(projectId, 1);
    }

    return false;
  }

  private canPlayerTakeActions(): boolean {
    if (!this.gameState.playerCanTakeActions() || this.marsPlayerState.getPlayerDealtProjects().length > 0 || !this.gameState.allPlayersReady()) {
      return false;
    }
    if (this.gameState.getPendingProduction().length > 0 || this.gameState.getPendingResources().length > 0 || this.gameState.getPendingTiles().length > 0) {
      return false;
    }
    if (this.gameState.getCurrentPlayer() != this.playerState.getPlayerId()) {
      return false;
    }

    return true;
  }

  public playProjectAction(projectId: number, index: number) {
    this.lastProjectId = projectId;
    var card = this.getTriggerActionProject(projectId);
    if (card != null) {
      var action = card.action[index];

      if (action.productionCost != null) {
        this.applyProduction([action.productionCost]);
      }
      if (action.resourceCost != null) {
        var cost = new MarsResourceCount(action.resourceCost.resource, action.resourceCost.amount, action.resourceCost.anyPlayer, action.resourceCost.anyCard, [],
          action.resourceCost.condition, action.resourceCost.perTag, action.resourceCost.tag, action.resourceCost.perTile, action.resourceCost.tile, action.resourceCost.onlyOpponents,
          action.resourceCost.onMars, action.resourceCost.mustPurchase, action.resourceCost.keepProject, action.resourceCost.tokenScoreTrackId, action.resourceCost.protectedAnimals,
          action.resourceCost.adjacentPlayers)
        if (this.canUseSteel(projectId)) {
          var steel = this.marsPlayerState.getSteelValue();
          var steelNeeded = Math.ceil(-cost.amount / steel);
          if (steelNeeded < this.steelValue) {
            this.steelValue = steelNeeded;
          }
          cost.amount = -Math.max(-cost.amount - this.steelValue * steel, 0);
          if (cost.resource == MarsResourceType.Megacredits && this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true)) < cost.amount) {
            return;
          }
          this.applyResources([cost, new MarsResourceCount(MarsResourceType.Steel, -this.steelValue)]);
          this.steelValue = 0;
        }
        else if (this.canUseTitanium(projectId)) {
          var titanium = this.marsPlayerState.getTitaniumValue();
          var titaniumNeeded = Math.ceil(-cost.amount / titanium);
          if (titaniumNeeded < this.titaniumValue) {
            this.titaniumValue = titaniumNeeded;
          }
          cost.amount = -Math.max(-cost.amount - this.titaniumValue * titanium, 0);
          if (cost.resource == MarsResourceType.Megacredits && this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true)) < cost.amount) {
            return;
          }
          this.applyResources([cost, new MarsResourceCount(MarsResourceType.Titanium, -this.titaniumValue)]);
          this.titaniumValue = 0;
        }
        else if (this.canUseHeat(projectId)) {
          if (this.heatValue >= -cost.amount) {
            this.heatValue = -cost.amount;
            cost.amount = 0;
          }
          cost.amount = -Math.max(-cost.amount - this.heatValue, 0);
          if (cost.resource == MarsResourceType.Megacredits && this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true)) < cost.amount) {
            return;
          }
          this.applyResources([cost, new MarsResourceCount(MarsResourceType.Heat, -this.heatValue)]);
          this.heatValue = 0;
        }
        else if (this.canUseResource(projectId)) {
          cost.amount = -this.resourceValue;
          if (cost.resource == MarsResourceType.Megacredits && this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true)) < cost.amount) {
            return;
          }
          this.applyResources([cost, new MarsResourceCount(action.resourceResult[0].resource, this.resourceValue)]);
          this.resourceValue = 0;
        }
        else {
          this.applyResources([cost]);
        }
      }
      if (action.productionResult != null) {
        this.applyProduction([action.productionResult]);
      }
      if (action.resourceResult.length > 0) {
        this.applyResources(action.resourceResult);
      }
      if (action.tile != MarsTiles.None) {
        this.applyTiles([action.tile]);
      }

      if (projectId >= 0) { // temperature and greeneries aren't limited to 1 per turn
        card.triggerGeneration = this.gameState.getGeneration();
        this.gameState.sendGameMessage(MarsMessageType.TriggerProject, projectId);
      }

      this.steelValue = 0;
      this.titaniumValue = 0;
      this.heatValue = 0;

      if (card.action.length > 0 && card.action[0].resourceResult.length > 0 && card.action[0].resourceResult[0].mustPurchase) {
        this.gameState.setPurchaseMidTurnCards(true);
      }

      this.gameState.sendGameMessage(MarsMessageType.RecordEvent, `${this.playerState.getPlayerName()} activates ${card.name}`);
      this.useAction();
    }
  }

  private playProjectCard(projectId: number) {
    var cost = this.getProjectCost(projectId);
    if (this.canUseSteel(projectId)) {
      var steel = this.marsPlayerState.getSteelValue();
      var steelNeeded = Math.ceil(cost / steel);
      if (this.steelValue > steelNeeded) {
        this.steelValue = steelNeeded;
      }
      cost = Math.max(cost - this.steelValue * steel, 0);
    }
    else {
      this.steelValue = 0;
    }
    if (this.canUseTitanium(projectId)) {
      var titanium = this.marsPlayerState.getTitaniumValue();
      var titaniumNeeded = Math.ceil(cost / titanium);
      if (this.titaniumValue > titaniumNeeded) {
        this.titaniumValue = titaniumNeeded;
      }
      cost = Math.max(cost - this.titaniumValue * titanium, 0);
    }
    else {
      this.titaniumValue = 0;
    }
    if (this.canUsePsychrophiles(projectId)) {
      var psychrophile = this.marsPlayerState.getPsychrophileValue();
      var psychrophileNeeded = Math.ceil(cost / psychrophile);
      if (this.psychrophileValue > psychrophileNeeded) {
        this.psychrophileValue = psychrophileNeeded;
      }
      cost = Math.max(cost - this.psychrophileValue * psychrophile, 0);
    }
    else {
      this.psychrophileValue = 0;
    }
    if (this.canUseHeat(projectId)) {
      if (this.heatValue >= cost) {
        this.heatValue = cost;
        cost = 0;
      }
      cost = Math.max(cost - this.heatValue, 0);
    }
    else {
      this.heatValue = 0;
    }

    if (cost > this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, true))) {
      this.errorState = "Not enough megacredits. You must spend additional resources.";
      return;
    }

    var playCard = new MarsPlayProject(projectId, cost, this.steelValue, this.titaniumValue, this.heatValue, this.psychrophileValue);
    this.gameState.sendGameMessage(MarsMessageType.PlayProject, playCard);

    var card = this.decks.getProject(projectId);
    if (card.production.length > 0 && card.production[0].amount == 0) {
      this.applyProduction([new MarsResourceCount(card.production[0].resource, -this.resourceValue), new MarsResourceCount(card.production[1].resource, this.resourceValue)]);
    }
    else {
      this.applyProduction(card.production);
    }
    this.applyResources(card.resources);
    this.applyTiles(card.tile);

    card.resources.forEach((c) => {
      if (c.resource == MarsResourceType.Project && c.keepProject < c.amount) {
        this.gameState.setSelectMidTurnCards(this.gameState.getSelectMidTurnCards() + c.keepProject);
      }
    });
    
    var cards = this.marsPlayerState.getPlayersActionProjects(this.playerState.getPlayerId());
    var reactionCard = cards.filter(c => c.reaction != null && c.reaction.tagPlayed != null && c.reaction.tagPlayed.resource.length > 0 && c.reaction.tagPlayed.resource[0].or.length > 0);
    if (card instanceof MarsActionProject) {
      if (card.reaction != null && card.reaction.tagPlayed != null && card.reaction.tagPlayed.resource.length > 0 && card.reaction.tagPlayed.resource[0].or.length > 0) {
        reactionCard.push(card);
      }
    }

    // check if SPLICE is in play
    if (this.marsPlayerState.getSplicePlayer() != null) {
      var tempCard = new MarsActionProject(MarsCorporationProject.Splice, '', '',
        new MarsReaction(null, 0, MarsTags.None, new MarsTagPlayed([MarsTags.Microbe],
          [new MarsResourceCount(MarsResourceType.Megacredits, 2, false, false, [new MarsResourceCount(MarsResourceType.MicrobeToken, 1, false, false, [], null, false, MarsTags.None,
            false, MarsTiles.None, false, false, false, 0, '', false, false, true)])])), [], 0, [], [], []);
      reactionCard.push(tempCard);
    }

    reactionCard.forEach((c) => {
      // player needs to make a choice about what to do (olympus conference), applied for each tag played
      card.tags.forEach((t) => {
        // Viral Enhancers
        if (c.reaction.tagPlayed.tag.some(r => r == t)) {
          if (c.reaction.tagPlayed.resource[0].or[0].targetPlayedCard) {
            if (card instanceof MarsActionProject && this.marsPlayerState.getProjectScoreTrack(card as MarsActionProject) != null) {
              var actionCard = card as MarsActionProject;
              var options: MarsResourceCount;
              if (c.id == MarsCorporationProject.Splice) {
                options = new MarsResourceCount(MarsResourceType.Megacredits, 2, false, false, [new MarsResourceCount(this.marsPlayerState.getProjectTokenType(actionCard), 1, false, false,
                  [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, this.marsPlayerState.getProjectScoreTrack(actionCard), false, false, true)]);
              }
              else {
                options = new MarsResourceCount(MarsResourceType.Plants, 1, false, false, [new MarsResourceCount(this.marsPlayerState.getProjectTokenType(actionCard), 1, false, false,
                  [], null, false, MarsTags.None, false, MarsTiles.None, false, false, false, 0, this.marsPlayerState.getProjectScoreTrack(actionCard), false, false, true)]);
              }
              this.applyResources([options]);
            }
            else {
              if (c.id == MarsCorporationProject.Splice) {
                this.applyResources([new MarsResourceCount(MarsResourceType.Megacredits, 2)]);
              }
              else {
                this.applyResources([new MarsResourceCount(MarsResourceType.Plants, 1)]);
              }
            }
          }
          else {
            this.applyResources(c.reaction.tagPlayed.resource);
          }
        }
      });
    });

    if (card.specialDesign) {
      this.specialDesign = this.gameState.getGeneration();
    }
    else {
      this.specialDesign = -1;
    }

    if (card.indenturedWorkers) {
      this.indenturedWorkers = this.gameState.getGeneration();
    }
    else {
      this.indenturedWorkers = -1;
    }

    this.gameState.setPlayingPreludeProject(null);

    this.useAction();
  }

  private useAction() {
    this.gameState.hideMapButtons();
    if (this.gameState.getPendingProduction().length == 0 && this.gameState.getPendingResources().length == 0 && this.gameState.getPendingTiles().length == 0 && !this.gameState.getPurchaseMidTurnCards() &&
      this.gameState.getSelectMidTurnCards() == 0) {
      this.gameState.useAction();
      this.gameState.setCanTakeActions(true);
    }
    else {
      this.gameState.setCanTakeActions(false);
    }
  }

  private applyProduction(production: Array<MarsResourceCount>) {
    production.forEach((p) => {
      if (p.anyCard || p.anyPlayer || p.or.some(o => o.amount != 0) || p.resource == MarsResourceType.RoboticWorkforce) {
        this.gameState.addPendingProduction(p);
      }
      else {
        if (p.condition != null && p.condition.type == MarsPrereq.Tags) {
          p.condition.tag.forEach((t) => {
            if (this.marsPlayerState.getTags(this.playerState.getPlayerId(), t, true) >= p.condition.amount) {
              this.gameState.sendGameMessage(MarsMessageType.ApplyProduction, new MarsResourceTarget(p, this.playerState.getPlayerId(), -1));
            }
          });
        }
        else {
          this.gameState.sendGameMessage(MarsMessageType.ApplyProduction, new MarsResourceTarget(p, this.playerState.getPlayerId(), -1));
        }
      }
    });
  }

  public showProductionTargets(): boolean {
    return this.gameState.getPendingProduction().length > 0;
  }

  public showResourceTagets(): boolean {
    return this.gameState.getPendingProduction().length == 0 && this.gameState.getPendingResources().length > 0
  }

  public showTileTargets(): boolean {
    return this.gameState.getPendingProduction().length == 0 && this.gameState.getPendingResources().length == 0 && this.getPendingTiles().length > 0
  }

  private applyResources(resources: Array<MarsResourceCount>) {
    this.limitTargets = this.gameState.getActivePlayers().map(p => p.id);
    resources.forEach((p) => {
      // if amount != 0 and condition != null we are triggering search for life, which has no choices to be made
      var choice = false;
      if (p.anyCard || p.anyPlayer && !p.perTag || p.or.some(o => o.amount != 0) && p.condition == null) {
        choice = true;
      }
      if (choice) {
        if (p.adjacentPlayers) {
          this.afterTilePlacement = p;
        }
        else {
          this.gameState.addPendingResource(p);
        }
      }
      else {
        // Determine if our results will give us an ocean from raised temperature, if so add an ocean tile
        if (p.resource == MarsResourceType.Oxygen) {
          var oxygenScore = this.score.getScore('global', MarsScoreTrack.Oxygen);
          if (oxygenScore < 8 && oxygenScore + p.amount >= 8) {
            this.addOceanForTemperatureIncrease(1);
          }
        }
        if (p.resource == MarsResourceType.Temperature) {
          this.addOceanForTemperatureIncrease(p.amount);
        }
        this.gameState.sendGameMessage(MarsMessageType.ApplyResources, new MarsResourceTarget(p, this.playerState.getPlayerId(), -1));
      }
    });
  }

  private addOceanForTemperatureIncrease(increase: number) {
    var temperatureScore = this.score.getScore('global', MarsScoreTrack.Temperature);
    if (temperatureScore < 0 && temperatureScore + increase * 2 >= 0) {
      this.applyTiles([MarsTiles.Ocean]);
    }
  }

  private applyTiles(tiles: Array<MarsTiles>) {
    tiles.forEach((t) => {
      if (t != MarsTiles.ArtificialLake && t != MarsTiles.Ocean ||
        this.score.getScore('global', MarsScoreTrack.Oceans) < 9 - this.gameState.getPendingTiles().filter(p => p == MarsTiles.Ocean || p == MarsTiles.ArtificialLake).length) {
        this.gameState.addPendingTile(t);
      }
    });
  }
}
