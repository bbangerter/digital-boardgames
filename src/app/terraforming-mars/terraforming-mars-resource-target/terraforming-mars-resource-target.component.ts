import { Component, Input, EventEmitter, Output } from '@angular/core';
import { MarsResourceCount, MarsTargetType, MarsActionProject } from '../dtos/terraforming-mars-dtos.model';
import { MarsResourceType, MarsTags, MarsCorporationProject } from '../terraforming-mars-constants';
import { PlayerScoreService, PlayerStateService } from '../../core';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { TerraformingMarsDecksService } from '../services/terraforming-mars-decks.service';

@Component({
  selector: 'terraforming-mars-resource-target',
  templateUrl: './terraforming-mars-resource-target.component.html',
  styleUrls: [
    './terraforming-mars-resource-target.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsResourceTargetComponent {
  displayedColumns = ['Name', 'Current', 'Effect', 'Select'];

  @Input() targetSupply: boolean;
  @Input() resource: Array<MarsResourceCount>;
  @Input() excludeId: number;
  @Input() limitTargets: Array<string>;
  @Output() resourceSelected = new EventEmitter<MarsTargetType>();

  constructor(private score: PlayerScoreService, private marsPlayerState: TerraformingMarsPlayerStateService, private gameState: TerraformingMarsStateService,
    private decks: TerraformingMarsDecksService, private playerState: PlayerStateService) { }

  public skip() {
    this.resourceSelected.emit(null);
  }

  public getValidTargets(): Array<MarsTargetType> {
    var prod = this.resource[0];
    var targets = this.generateValidTargets(prod, 0, 0);
    return targets.filter(t => this.limitTargets.some(l => l == t.playerId));
  }

  public select(target: MarsTargetType) {
    this.resourceSelected.emit(target);
  }

  private generateValidTargets(prod: MarsResourceCount, index: number, resourceIndex: number, depth: number = 0): Array<MarsTargetType> {
    var targets = new Array<MarsTargetType>();
    if (prod.amount < 0) {  // removing something from another player or card
      switch (prod.resource) {
        case MarsResourceType.Megacredits:
        case MarsResourceType.Steel:
        case MarsResourceType.Titanium:
        case MarsResourceType.Plants:
        case MarsResourceType.Energy:
        case MarsResourceType.Heat:
          targets = this.basicResource(prod, true, index, resourceIndex);
          break;
        case MarsResourceType.AnimalToken:
        case MarsResourceType.MicrobeToken:
          targets = this.tokenResource(prod, true, index, resourceIndex, true, this.excludeId);
          break;
        case MarsResourceType.ScienceToken:
          targets = this.tokenResource(prod, true, index, resourceIndex, true, -1, MarsTags.Science);
          break;
        case MarsResourceType.Project:
          targets = this.selectDiscardCards();
          break;
      }
    }
    else {
      switch (prod.resource) {
        case MarsResourceType.Megacredits:
        case MarsResourceType.Steel:
        case MarsResourceType.Titanium:
        case MarsResourceType.Plants:
        case MarsResourceType.Energy:
        case MarsResourceType.Heat:
          targets = this.basicResource(prod, false, index, resourceIndex);
          break;
        case MarsResourceType.AnimalToken:
        case MarsResourceType.MicrobeToken:
        case MarsResourceType.FleetToken:
          targets = this.tokenResource(prod, false, index, resourceIndex, false, this.excludeId);
          break;
        case MarsResourceType.ScienceToken:
          targets = this.tokenResource(prod, false, index, resourceIndex, false, -1, MarsTags.Science);
          break;
        case MarsResourceType.RoboticWorkforce:
          targets = this.builderTags();
          break;
        case MarsResourceType.AnyBasic:
          targets = this.productionIncrease();
          break;
      }
    }

    if (depth == 0) {
      prod.or.forEach((p, i) => {
        targets = targets.concat(this.generateValidTargets(p, index, i + 1, depth + 1));
      });
    }

    return targets;
  }

  public getPlayerColorClass(target: MarsTargetType): string {
    return this.gameState.getPlayerColorClass(target.playerId);
  }

  private isProductionIncrease(): boolean {
    return this.resource[0].resource == MarsResourceType.AnyBasic;
  }

  public getTargetName(target: MarsTargetType): string {
    var playerName = this.gameState.getPlayerName(target.playerId);
    if (target.projectId < 0) {
      return playerName;
    }

    if (this.isProductionIncrease()) {
      return this.playerState.getPlayerName();
    }

    var card = this.decks.getProject(target.projectId);
    return `${card.name} (${playerName})`;
  }

  public getCurrentAmount(target: MarsTargetType): string {
    var prod = this.resource[target.projectIndex];
    if (target.resourceIndex > 0) {
      prod = prod.or[target.resourceIndex - 1];
    }
    switch (prod.resource) {
      case MarsResourceType.Megacredits:
      case MarsResourceType.Steel:
      case MarsResourceType.Titanium:
      case MarsResourceType.Plants:
      case MarsResourceType.Energy:
      case MarsResourceType.Heat:
        var amount = this.score.getScore(target.playerId, this.marsPlayerState.getScoreTrack(prod.resource, this.targetSupply));
        return `${amount} ${this.gameState.getResourceName(prod.resource, amount)} ${this.targetSupply ? '' : ' production'}`
      case MarsResourceType.AnimalToken:
      case MarsResourceType.MicrobeToken:
      case MarsResourceType.ScienceToken:
      case MarsResourceType.FleetToken:
        var tokens = this.marsPlayerState.getTokenCount(target.playerId, target.projectId);
        return `${tokens} ${this.gameState.getResourceName(prod.resource, tokens)}`;
      case MarsResourceType.RoboticWorkforce:
        var card = this.decks.getProject(target.projectId);
        return this.gameState.getResourceString(card.production);
      case MarsResourceType.AnyBasic:
        var currentProd = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(prod.resource, false));
        return `${currentProd} ${this.gameState.getResourceName(target.resource, 1)}`;
    }

    return '';
  }

  public getProjectEffect(target: MarsTargetType): string {
    if (this.isProductionIncrease()) {
      return `Increase ${this.gameState.getResourceName(target.resource, 1)} production`;
    }
    var prod = this.resource[target.projectIndex];
    if (target.resourceIndex > 0) {
      prod = prod.or[target.resourceIndex - 1];
    }
    if (prod.resource == MarsResourceType.Project && prod.amount < 0) {
      return 'Discard to draw 1 card';
    }
    var effect = this.gameState.getResourceString([prod], true);
    if (target.resourceIndex > 0 && prod.or.length > 0) {
      effect += `, ${this.gameState.getResourceString(prod.or, true)}`;
    }
    return effect;
  }

  private basicResource(production: MarsResourceCount, allPlayers: boolean, projectIndex: number, resourceIndex: number): Array<MarsTargetType> {
    var targets = new Array<MarsTargetType>();
    var players = this.gameState.getActivePlayers().filter(p => allPlayers || p.id == this.playerState.getPlayerId());
    var targetPlayers = players.filter(p => (this.targetSupply || this.score.canAdd(production.amount, p.id, this.marsPlayerState.getScoreTrack(production.resource, this.targetSupply))) &&
      (!production.onlyOpponents || p.id != this.playerState.getPlayerId()));
    targetPlayers.forEach((p) => {
      if (production.resource != MarsResourceType.Plants || !this.marsPlayerState.isProtected(p.id) || !this.targetSupply) {
        targets.push(new MarsTargetType(p.id, -1, projectIndex, resourceIndex));
      }
    });

    return targets;
  }

  private tokenResource(production: MarsResourceCount, allPlayers: boolean, projectIndex: number, resourceIndex: number, remove: boolean, excludeId: number, tag: MarsTags = MarsTags.None): Array<MarsTargetType> {
    var targets = new Array<MarsTargetType>();
    var players = this.gameState.getActivePlayers().filter(p => allPlayers && production.anyPlayer || p.id == this.playerState.getPlayerId());
    players.forEach((p) => {
      if (!this.marsPlayerState.isProtected(p.id)) {
        var projects = this.marsPlayerState.getPlayersActionProjects(p.id).filter(c => this.projectTakesTokensOfType(production.resource, c, tag) && c.id != excludeId);
        if (this.marsPlayerState.getRecyclonPlayer() == p.id) {
          var corp = this.marsPlayerState.getPlayerCorporation(this.marsPlayerState.getRecyclonPlayer());
          projects.push(new MarsActionProject(MarsCorporationProject.Recyclon, 'Recyclon', '', corp.reaction[0], [], 0, [], [], []));
        }
        projects = projects.filter(c => !remove || c.resources.length == 0 || !c.resources[0].protectedAnimals);
        projects = projects.filter(c => production.amount < 0 || production.tokenScoreTrackId == null || production.tokenScoreTrackId == this.marsPlayerState.getProjectScoreTrack(c));
        projects = projects.filter(c => this.score.getScore(this.marsPlayerState.getProjectOwner(c.id), this.marsPlayerState.getProjectScoreTrack(c)) >= (allPlayers ? 1 : -production.amount));
        projects = projects.filter(c => !production.targetPlayedCard || this.marsPlayerState.getProjectScoreTrack(c) == production.tokenScoreTrackId);
        projects = projects.filter(c => production.anyCard || this.marsPlayerState.getProjectScoreTrack(c) == production.tokenScoreTrackId);
        projects.forEach((c) => {
          targets.push(new MarsTargetType(p.id, c.id, projectIndex, resourceIndex));
        });
      }
    });

    return targets;
  }

  private builderTags(): Array<MarsTargetType> {
    var targets = new Array<MarsTargetType>();
    var projects = this.gameState.roboticWorkforceTargets();
    projects.forEach((p) => {
      targets.push(new MarsTargetType(this.playerState.getPlayerId(), p.id, 0, 0, true));
    });

    return targets;
  }

  private selectDiscardCards(): Array<MarsTargetType> {
    var targets = new Array<MarsTargetType>();
    var projects = this.marsPlayerState.getPlayerProjectHand().filter(p => p.playerId == this.playerState.getPlayerId());
    projects.forEach((p) => {
      targets.push(new MarsTargetType(p.playerId, p.cardId, 0, 0));
    });

    return targets;
  }

  private projectTakesTokensOfType(type: MarsResourceType, card: MarsActionProject, tag: MarsTags): boolean {
    if (card.action.some(a => a.resourceResult.some(r => r.tokenScoreTrackId != null && r.resource == type))) {
      return true;
    }

    if (card.reaction != null && card.reaction.tilePlayed != null && card.reaction.tilePlayed.resource != null &&
      card.reaction.tilePlayed.resource.tokenScoreTrackId != null && card.reaction.tilePlayed.resource.resource == type && !card.reaction.tilePlayed.resource.protectedAnimals) {
      return true;
    }

    if (card.reaction != null && card.reaction.tagPlayed != null && card.reaction.tagPlayed.resource != null &&
      card.reaction.tagPlayed.resource[0].tokenScoreTrackId != null && card.reaction.tagPlayed.resource[0].tag == tag &&
      card.reaction.tagPlayed.resource[0].resource == type) {
      return true;
    }

    return false;
  }

  private productionIncrease(): Array<MarsTargetType> {
    var megacredits = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Megacredits, false));
    var steel = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Steel, false));
    var titanium = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Titanium, false));
    var plants = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Plants, false));
    var energy = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Energy, false));
    var heat = this.score.getScore(this.playerState.getPlayerId(), this.marsPlayerState.getScoreTrack(MarsResourceType.Heat, false));

    var min = Math.min(megacredits, steel);
    min = Math.min(min, titanium);
    min = Math.min(min, plants);
    min = Math.min(min, energy);
    min = Math.min(min, heat);

    var targets = new Array<MarsTargetType>();

    if (megacredits == min) {
      targets.push(new MarsTargetType(this.playerState.getPlayerId(), 0, 0, 0, false, MarsResourceType.Megacredits));
    }
    if (steel == min) {
      targets.push(new MarsTargetType(this.playerState.getPlayerId(), 0, 0, 0, false, MarsResourceType.Steel));
    }
    if (titanium == min) {
      targets.push(new MarsTargetType(this.playerState.getPlayerId(), 0, 0, 0, false, MarsResourceType.Titanium));
    }
    if (plants == min) {
      targets.push(new MarsTargetType(this.playerState.getPlayerId(), 0, 0, 0, false, MarsResourceType.Plants));
    }
    if (energy == min) {
      targets.push(new MarsTargetType(this.playerState.getPlayerId(), 0, 0, 0, false, MarsResourceType.Energy));
    }
    if (heat == min) {
      targets.push(new MarsTargetType(this.playerState.getPlayerId(), 0, 0, 0, false, MarsResourceType.Heat));
    }

    return targets;
  }
}
