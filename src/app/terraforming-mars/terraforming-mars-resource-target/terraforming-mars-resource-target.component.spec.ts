import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsResourceTargetComponent } from './terraforming-mars-resource-target.component';

describe('TerraformingMarsResourceTargetComponent', () => {
  let component: TerraformingMarsResourceTargetComponent;
  let fixture: ComponentFixture<TerraformingMarsResourceTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsResourceTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsResourceTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
