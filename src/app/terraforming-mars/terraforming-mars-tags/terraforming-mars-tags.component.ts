import { Component } from '@angular/core';
import { TerraformingMarsPlayerStateService } from '../services/terraforming-mars-player-state.service';
import { MarsPlayerCard, MarsCorp } from '../dtos/terraforming-mars-dtos.model';
import { MarsTags } from '../terraforming-mars-constants';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { TerraformingMarsDecksService } from '../services/terraforming-mars-decks.service';
import { PlayerStateService } from '../../core';

@Component({
  selector: 'terraforming-mars-tags',
  templateUrl: './terraforming-mars-tags.component.html',
  styleUrls: [
    './terraforming-mars-tags.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsTagsComponent {
  displayedColumns = ['Name', 'Animal', 'Builder', 'City', 'Earth', 'Energy', 'Event', 'Jovian', 'Microbe', 'Plant', 'Science', 'Space', 'Wild', 'Projects', 'ProjectsPlayed'];

  private playerCorps: Array<MarsCorp>;;
  constructor(private marsPlayerState: TerraformingMarsPlayerStateService, private gameState: TerraformingMarsStateService, private decks: TerraformingMarsDecksService,
    private playerState: PlayerStateService) {
    var corpIds = this.marsPlayerState.getAllPlayerCorporations().map(c => c.cardId);
    this.playerCorps = this.decks.getCorporations(corpIds);
  }

  public isPlayer(playerId: string): boolean {
    return playerId == this.playerState.getPlayerId();
  }

  public getPlayerClass(playerId: string): string {
    if (this.isPlayer(playerId)) {
      return `player-highlight ${this.getPlayerColorClass(playerId)}`;
    }

    return this.getPlayerColorClass(playerId);
  }

  public getPlayerColorClass(playerId: string): string {
    return this.gameState.getPlayerColorClass(playerId);
  }

  public getPlayers(): Array<MarsPlayerCard> {
    return this.marsPlayerState.getAllPlayerCorporations();
  }

  public getPlayerName(playerId: string): string {
    return this.gameState.getPlayerName(playerId);
  }

  public getCorpName(corpId: number): string {
    return this.playerCorps.find(c => c.id == corpId).name;
  }

  public getTags(playerId: string, tag: MarsTags): number {
    return this.marsPlayerState.getTags(playerId, tag, false);
  }

  public getProjects(playerId: string): number {
    return this.marsPlayerState.getPlayerProjectHand().filter(p => p.playerId == playerId).length;
  }

  public getProjectPlayed(playerId: string): string {
    return this.marsPlayerState.getPlayerNonActionProjects(playerId).length + '/' + this.marsPlayerState.getPlayersActionProjects(playerId).length;
  }
}
