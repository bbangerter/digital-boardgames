import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsTagsComponent } from './terraforming-mars-tags.component';

describe('TerraformingMarsTagsComponent', () => {
  let component: TerraformingMarsTagsComponent;
  let fixture: ComponentFixture<TerraformingMarsTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
