import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerraformingMarsGameControllerComponent } from './terraforming-mars-game-controller.component';

describe('TerraformingMarsGameControllerComponent', () => {
  let component: TerraformingMarsGameControllerComponent;
  let fixture: ComponentFixture<TerraformingMarsGameControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerraformingMarsGameControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerraformingMarsGameControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
