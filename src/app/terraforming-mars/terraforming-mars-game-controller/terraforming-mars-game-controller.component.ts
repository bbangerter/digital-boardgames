import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { GameComponent, GameStart, PlayerStateService, CanvasStateService } from '../../core';
import { TerraformingMarsStateService } from '../services/terraforming-mars-state.service';
import { TerraformingMarsBoardStateService } from '../services/terraforming-mars-board-state.service';
import { MarsMap } from '../terraforming-mars-constants';

@Component({
  selector: 'terraforming-mars-game-controller',
  templateUrl: './terraforming-mars-game-controller.component.html',
  styleUrls: [
    './terraforming-mars-game-controller.component.scss',
    './../terraforming-mars.scss'
  ]
})
export class TerraformingMarsGameControllerComponent implements OnInit, OnDestroy, GameComponent  {
  @Input() gameData: GameStart;
  constructor(private playerState: PlayerStateService, private gameState: TerraformingMarsStateService, private canvas: CanvasStateService, private marsBoardState: TerraformingMarsBoardStateService) { }

  ngOnInit(): void {
    this.gameState.startGameHub(this.playerState.getPlayerName(), this.gameData.gameId, this.playerState.getPlayerId());
    this.gameState.setCreatorId(this.gameData.creatorId);
  }

  ngOnDestroy(): void {
    this.gameState.closeGameHub();
    this.canvas.removeCanvas(this.marsBoardState.canvasId);
  }

  public getCurrentPhase(): number {
    return this.gameState.getCurrentPhase();
  }

  public getMap(): string {
    return this.gameState.getMarsMap().toString();
  }
}
