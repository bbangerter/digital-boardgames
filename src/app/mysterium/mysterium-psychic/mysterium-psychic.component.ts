import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { MysteriumStateService } from '../services/mysterium-state.service';
import { CanvasStateService, GfxClickData } from '../../core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'mysterium-psychic',
  templateUrl: './mysterium-psychic.component.html',
  styleUrls: [
    './mysterium-psychic.component.scss',
    './../mysterium.scss'
  ]
})
export class MysteriumPsychicComponent implements AfterViewInit, OnDestroy {
  private clickListener: Subscription;
  constructor(private gameState: MysteriumStateService, private canvas: CanvasStateService) { }

  ngOnDestroy(): void {
    this.clickListener.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.gameState.setupPsychicBoard();
    this.clickListener = this.canvas.subscribeClickEvent(this.mysteriumCanvasId(), (data: GfxClickData) => this.onClick(data));
  }

  public mysteriumCanvasId(): string {
    return this.gameState.canvasId;
  }

  private onClick(data: GfxClickData) {
    if (data != null) {
      this.gameState.clickPsychicButton(data.objectId);
    }
  }
}
