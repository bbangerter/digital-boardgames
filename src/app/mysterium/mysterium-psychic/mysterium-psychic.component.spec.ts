import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysteriumPsychicComponent } from './mysterium-psychic.component';

describe('MysteriumPsychicComponent', () => {
  let component: MysteriumPsychicComponent;
  let fixture: ComponentFixture<MysteriumPsychicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysteriumPsychicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysteriumPsychicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
