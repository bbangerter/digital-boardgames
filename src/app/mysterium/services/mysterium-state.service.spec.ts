import { TestBed } from '@angular/core/testing';

import { MysteriumStateService } from './mysterium-state.service';

describe('MysteriumStateService', () => {
  let service: MysteriumStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MysteriumStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
