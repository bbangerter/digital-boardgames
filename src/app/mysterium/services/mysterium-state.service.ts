import { Injectable } from '@angular/core';
import { SignalRService, GamePlayer, PlayerStateService, GameMessage, GameUtilsService, CanvasStateService, GfxTextureAtlas, GfxText, GfxObject, GfxTexture, PlayerScoreService, GfxScrollable } from '../../core';
import { Subscription } from 'rxjs';
import { MysteriumPhase, MysteriumMessageType } from '../mysterium-constants';
import { MysteriumSettings, MysteriumPlayerCard, MysteriumGhostCard, MysteriumVisions, MysteriumCardSelect, MysteriumPlayerMarker, MysteriumMarkerSelect, MysteriumVote } from '../dtos/mysterium-dtos-models';

@Injectable({
  providedIn: 'root'
})
export class MysteriumStateService {
  creatorId: string;
  activePlayers: Array<GamePlayer> = [];
  currentPhase: MysteriumPhase = MysteriumPhase.Setup;
  settings: MysteriumSettings;
  canvasId: string = 'Mysterium';
  ghostCards: Array<MysteriumGhostCard>;
  playerCards: Array<MysteriumPlayerCard>;
  playerVisions: Array<MysteriumVisions>;
  ravens: Array<GfxTexture>;
  victory: boolean = false;

  private characters: Array<number>;
  private locations: Array<number>;
  private objects: Array<number>;
  private visionsDeck: Array<number>;
  private discardedVisions: Array<number>;
  private originalCharacters: Array<number>;
  private originalLocations: Array<number>;
  private originalObjects: Array<number>;
  private gameSubscription: Subscription;
  private hubName: string = 'game';
  private background: number;
  private visionScroller: GfxScrollable;
  private countdownTimer: any = null;
  private countdownTime: number = 120;
  private timer: GfxText;
  private playerMarkers: Array<MysteriumPlayerMarker>;
  private playerVotes: Array<MysteriumMarkerSelect>;
  private usedVotes: Array<MysteriumMarkerSelect>;
  private hour: number = 1;
  private hourGfx: GfxText;
  private clairvoyancyRank: Array<GfxText>;
  private cardCovers: Array<number>;
  private finalVotes: Array<MysteriumVote>;
  constructor(private signalR: SignalRService, private playerState: PlayerStateService, private gameUtils: GameUtilsService, private canvas: CanvasStateService,
    private score: PlayerScoreService) {
    this.settings = new MysteriumSettings('', 4, 1, true, '', 2);
    this.playerCards = new Array<MysteriumPlayerCard>();
    this.playerVisions = new Array<MysteriumVisions>();
    this.cardCovers = new Array<number>();
  }

  public startGameHub(playerName: string, groupName: string, playerId: string) {
    this.signalR.connect(this.hubName, `PlayerName=${playerName}&GroupName=${groupName}&PlayerId=${playerId}`);
    this.gameSubscription = this.signalR.subscribeToMessage(this.hubName, (data) => this.onMessage(data));
  }

  public closeGameHub() {
    this.gameSubscription.unsubscribe();
    this.signalR.close(this.hubName);
  }

  public isGameCreator() {
    return this.creatorId == this.playerState.getPlayerId();
  }

  public startGame() {
    this.characters = this.shuffleCards();
    this.sendGameMessage(MysteriumMessageType.ShuffledCharacters, this.characters);
    this.locations = this.shuffleCards();
    this.sendGameMessage(MysteriumMessageType.ShuffledLocations, this.locations);
    this.objects = this.shuffleCards();
    this.sendGameMessage(MysteriumMessageType.ShuffledObjects, this.objects);
    this.sendGameMessage(MysteriumMessageType.StartGame, '');
  }

  public updateDifficulty() {
    this.sendGameMessage(MysteriumMessageType.UpdateSettings, this.settings);
  }

  public sendGameMessage(type: MysteriumMessageType, message: any) {
    var gameMessage = new GameMessage(type, JSON.stringify(message), this.playerState.getPlayerId());
    this.signalR.sendMessage(this.hubName, gameMessage);
  }

  public clickPsychicButton(buttonGfxId: number) {
    if (this.currentPhase == MysteriumPhase.Visions) {
      var characterIndex = this.playerCards.findIndex(c => c.characterGfxId == buttonGfxId);
      var locationIndex = this.playerCards.findIndex(c => c.locationGfxId == buttonGfxId);
      var objIndex = this.playerCards.findIndex(c => c.objectGfxId == buttonGfxId);

      if (characterIndex != -1 || locationIndex != -1 || objIndex != -1) {
        this.sendGameMessage(MysteriumMessageType.SelectCard, new MysteriumCardSelect(this.playerState.getPlayerId(), characterIndex, locationIndex, objIndex));
      }

      var marker = this.playerMarkers.find(m => m.gfxId == buttonGfxId);
      if (marker != null) {
        this.sendGameMessage(MysteriumMessageType.SelectMarker, new MysteriumMarkerSelect(this.playerState.getPlayerId(), marker.playerId));
      }
    }
    else if (this.playerVisions.length == 1) {
      var vote = this.playerCards.find(p => p.tagGfxId == buttonGfxId);
      this.sendGameMessage(MysteriumMessageType.FinalVote, new MysteriumVote(this.playerState.getPlayerId(), vote.playerId));
    }
  }

  public getPsychicIndex(playerId: string): number {
    var index = this.activePlayers.findIndex(p => p.id == playerId);
    var ghostIndex = this.activePlayers.findIndex(p => p.id == this.settings.ghost);
    if (ghostIndex < index) {
      index--;
    }

    return index;
  }        

  public useRaven() {
    this.discardedVisions = this.discardedVisions.concat(this.ghostCards.map(c => c.card));
    this.ghostCards.forEach((c) => {
      c.card = -1;
      c.selected = false;
      this.canvas.removeGfxObject(this.canvasId, c.gfxId);
    });
    this.dealVisions();
    this.sendGameMessage(MysteriumMessageType.UseRaven, '');
  }

  public getPlayerColor(playerId: string, transparent: string = 'FF'): string {
    var index = this.getPsychicIndex(playerId);
    switch (index) {
      case 0:
        return `#800080${transparent}`;
      case 1:
        return `#ffff00${transparent}`;
      case 2:
        return `#0000ff${transparent}`;
      case 3:
        return `#111111${transparent}`;
      case 4:
        return `#ff0000${transparent}`;
      case 5:
        return `#ffffff${transparent}`;
    }

    return `#111111${transparent}`;
  }

  public addPlayerCards(index: number, containerId: number, playerId: string, tag: number) {
    var charIndex = this.characters[index];
    var character = this.canvas.createTextureAtlas(this.canvasId, this.getCharacterImage(charIndex), 3, 3, charIndex % 9, containerId);
    character.translate(0, 35);
    character.setEnabled(!this.isGhost());

    var locIndex = this.locations[index];
    var location = this.canvas.createTextureAtlas(this.canvasId, this.getLocationImage(locIndex), 3, 3, locIndex % 9, containerId);
    location.translate(0, 150);
    location.setEnabled(false);

    var objIndex = this.objects[index];
    var playerObject = this.canvas.createTextureAtlas(this.canvasId, this.getObjectImage(objIndex), 3, 3, objIndex % 9, containerId);
    playerObject.translate(0, 265);
    playerObject.setEnabled(false);

    this.playerCards.push(new MysteriumPlayerCard(playerId, character.getId(), location.getId(), playerObject.getId(), tag, containerId));
  }

  public isGhost(): boolean {
    return this.settings.ghost == this.playerState.getPlayerId();
  }

  public sendVisions(playerId: string) {
    if (this.currentPhase == MysteriumPhase.End) {
      return;
    }
    var visions = this.ghostCards.filter(c => c.selected).map(c => c.card);
    if (this.currentPhase == MysteriumPhase.Revelation && (visions.length != 3 || this.playerVisions.length != 0)) {
      return;
    }
    this.gameUtils.shuffle(visions);
    this.sendGameMessage(MysteriumMessageType.SendVisions, new MysteriumVisions(playerId, visions));
    this.ghostCards.forEach((c) => {
      if (c.selected) {
        c.card = -1;
        c.selected = false;
        this.canvas.removeGfxObject(this.canvasId, c.gfxId);
      }
    });
    this.discardedVisions = this.discardedVisions.concat(visions);

    this.dealVisions();
  }

  public setupPsychicBoard() {
    this.background = this.canvas.createTexture(this.canvasId, './assets/mysterium/background.jpg', 0).getId();

    for (var index = 0; index < this.settings.cards; index++) {
      this.createEmptyMat(index);
    }

    this.addRavens();
    this.addHour();
    this.addTimer();
    this.addPlayerMarkers();
    this.addClairvoyancyGfx();

    this.visionScroller = this.canvas.createScrollArea(this.canvasId, 1530, 310, false, './assets/mysterium/left.png', -32, 300, './assets/mysterium/right.png', 1532, 300, 600, this.background);
    this.visionScroller.translate(35, 390);
  }

  public setupGhostBoard() {
    this.background = this.canvas.createTexture(this.canvasId, './assets/mysterium/background.jpg', 0).getId();

    var index = 0;
    this.activePlayers.forEach((p) => {
      if (p.id == this.settings.ghost) {
        return;
      }

      this.createPlayerMat(index, p.id);
      index++;
    });

    for (; index < this.settings.cards; index++) {
      this.createEmptyMat(index);
    }

    this.addRavens();
    this.addHour();
    this.addTimer();
    this.addPlayerMarkers();
    this.addClairvoyancyGfx();

    this.dealVisions();
  }

  private addRavens() {
    this.ravens = new Array<GfxTexture>();
    for (var i = 0; i < this.settings.ravens; i++) {
      var raven = this.canvas.createTexture(this.canvasId, './assets/mysterium/raven.png', this.background);
      raven.translate(1400 + i * 70, 738);
      raven.setEnabled(this.isGhost());
      this.ravens.push(raven);
    }
  }

  private addClairvoyancyGfx() {
    this.clairvoyancyRank = new Array<GfxText>();
    var label = this.canvas.createText(this.canvasId, 'Clairvoyancy:', '20px Arial', 'white', this.background);
    label.translate(5, 785);
    this.activePlayers.forEach((p) => {
      if (p.id != this.settings.ghost) {
        var gfx = this.canvas.createText(this.canvasId, '0', '20px Arial', this.getPlayerColor(p.id), this.background);
        var index = this.getPsychicIndex(p.id);
        gfx.translate(135 + index * 30, 785);
        this.clairvoyancyRank.push(gfx);
      }
    });
  }

  private addHour() {
    this.hourGfx = this.canvas.createText(this.canvasId, 'Hour: 1', '20px Arial', 'white', this.background);
    this.hourGfx.translate(1450, 20);
  }

  private updateHourGfx() {
    this.hourGfx.setText(`Hour: ${this.hour}`);
  }

  private addPlayerMarkers() {
    this.playerVotes = new Array<MysteriumMarkerSelect>();
    this.usedVotes = new Array<MysteriumMarkerSelect>();
    this.playerMarkers = new Array<MysteriumPlayerMarker>();
    this.activePlayers.forEach((p) => {
      if (p.id != this.settings.ghost) {
        var marker = this.canvas.createCircle(this.canvasId, 0, 2 * Math.PI, 20, false, this.getPlayerColor(p.id, '66'), true, 1, this.background);
        marker.setVisible(false);
        marker.setEnabled(p.id != this.playerState.getPlayerId() && !this.isGhost());
        var index = this.getPsychicIndex(p.id);
        var x = 25 + 45 * ((index % 3));
        var y = 25 + 45 * (Math.floor(index / 3));
        marker.translate(x, y);
        this.playerMarkers.push(new MysteriumPlayerMarker(p.id, marker.getId()));
      }
    });
  }

  private addTimer() {
    this.timer = this.canvas.createText(this.canvasId, '', '20px Arial', 'white', this.background);
    this.timer.translate(1550, 20);
    this.timer.setVisible(false);
  }

  private createPlayerMat(index: number, playerId: string) {
    var container = this.canvas.createContainer(this.canvasId, this.background);
    container.translate(index * 160 + 2, 0);

    var player = this.canvas.createButton(this.canvasId, 155, 30, 'grey', true, 1, 5, 20, this.activePlayers.find(p => p.id == playerId).name, '16px Arial',
      this.getPlayerColor(playerId), container.getId());
    player.setVisible(this.isGhost() || this.currentPhase == MysteriumPhase.Revelation);

    this.addPlayerCards(index, container.getId(), playerId, player.getId());
  }

  private createEmptyMat(index: number) {
    var container = this.canvas.createContainer(this.canvasId, this.background);
    container.translate(index * 160 + 2, 0);

    this.addPlayerCards(index, container.getId(), '', -1);
  }
  
  private advancePhase() {
    this.currentPhase++;
    if (this.currentPhase == MysteriumPhase.Revelation) {
      this.finalVotes = new Array<MysteriumVote>();
      this.canvas.removeGfxObject(this.canvasId, this.hourGfx.getId());
      this.playerCards.forEach((c) => {
        if (c.playerId == '') {
          this.canvas.removeGfxObject(this.canvasId, c.containerId);
        }
      });
      this.cardCovers.forEach((c) => {
        this.canvas.removeGfxObject(this.canvasId, c);
      });

      if (this.isGhost()) {
        this.sendGameMessage(MysteriumMessageType.RevelationCharacters, this.characters);
        this.sendGameMessage(MysteriumMessageType.RevelationLocations, this.locations);
        this.sendGameMessage(MysteriumMessageType.RevelationObjects, this.objects);
      }
    }
  }

  private dealVisions() {
    if (this.isGhost()) {
      while (this.ghostCards.filter(c => c.card != -1).length < 7) {
        if (this.visionsDeck.length == 0) {
          this.shuffleVisions();
        }

        var card = this.visionsDeck.shift();
        var index = this.ghostCards.findIndex(c => c.card == -1);
        var gfx = this.getVisionTextureAtlas(card, this.background);
        gfx.translate(index * 200, 430);
        gfx.setEnabled(true);
        var highlight = this.canvas.createRectangle(this.canvasId, 190, 286, 'red', false, 2, gfx.getId());
        highlight.setVisible(false);
        this.ghostCards[index] = new MysteriumGhostCard(card, gfx.getId(), highlight.getId());
      }
    }
  }

  private getVisionTextureAtlas(index: number, parent: number): GfxTextureAtlas {
    return this.canvas.createTextureAtlas(this.canvasId, `./assets/mysterium/visions-${Math.floor(index / 4) + 1}.jpg`, 2, 2, index % 4, parent);
  }

  private onMessage(data: GameMessage) {
    if (data != null) {
      var message = JSON.parse(data.message);
      switch (data.type) {
        case MysteriumMessageType.UpdatePlayerList:
          this.activePlayers = message;
          if (this.activePlayers.length > 0 && this.currentPhase == MysteriumPhase.Setup) {
            this.settings.ghost = this.activePlayers[0].id;
          }
          break;
        case MysteriumMessageType.UpdateSettings:
          this.settings = message;
          break;
        case MysteriumMessageType.StartGame:
          this.advancePhase();
          this.shuffleVisions();
          this.settings.voteTokens += this.activePlayers.length > 5 ? 1 : 0;
          break;
        case MysteriumMessageType.ShuffledCharacters:
          this.characters = message;
          if (this.isGhost()) {
            this.originalCharacters = [...message];
            this.gameUtils.shuffle(this.characters);
          }
          break;
        case MysteriumMessageType.ShuffledLocations:
          this.locations = message;
          if (this.isGhost()) {
            this.originalLocations = [...message];
            this.gameUtils.shuffle(this.locations);
          }
          break;
        case MysteriumMessageType.ShuffledObjects:
          this.objects = message;
          if (this.isGhost()) {
            this.originalObjects = [...message];
            this.gameUtils.shuffle(this.objects);
          }
          break;
        case MysteriumMessageType.SendVisions:
          this.addVisionImages(message);
          this.playerVisions.push(message);
          this.startTimer();
          break;
        case MysteriumMessageType.SelectCard:
          this.markPlayerSelection(message);
          break;
        case MysteriumMessageType.SelectMarker:
          this.markPlayerMarker(message);
          break;
        case MysteriumMessageType.CorrectCharacter:
          this.markCorrectCharacter(message);
          break;
        case MysteriumMessageType.CorrectLocation:
          this.markCorrectLocation(message);
          break;
        case MysteriumMessageType.CorrectObject:
          this.markCorrectObject(message);
          break;
        case MysteriumMessageType.CorrectVote:
          this.addClairvoyancy(message);
          break;
        case MysteriumMessageType.UseRaven:
          this.hideRaven();
          break;
        case MysteriumMessageType.EndHour:
          this.checkForRevelation();
          break;
        case MysteriumMessageType.AdvancePhase:
          this.advancePhase();
          break;
        case MysteriumMessageType.RevelationCharacters:
          this.characters = message;
          break;
        case MysteriumMessageType.RevelationLocations:
          this.locations = message;
          break;
        case MysteriumMessageType.RevelationObjects:
          this.objects = message;
          this.setupFinalCards();
          break;
        case MysteriumMessageType.FinalVote:
          this.addFinalVote(message);
          break;
      }
    }
  }

  private addFinalVote(vote: MysteriumVote) {
    if (this.finalVotes.some(v => v.playerId == vote.playerId)) {
      return;
    }

    this.finalVotes.push(vote);

    if (this.finalVotes.length == this.activePlayers.length - 1) {
      this.finalVotes.forEach((v) => {
        this.playerCards.find(p => p.playerId == v.targetId).voteScore += 100 + this.score.getScore(v.playerId, 'clairvoyancy');
      });

      var maxScore = 0;
      var targetId = null;
      this.playerCards.forEach((p) => {
        if (p.voteScore > maxScore) {
          maxScore = p.voteScore;
          targetId = p.playerId;
        }
      });

      this.victory = this.playerVisions[0].playerId == targetId;

      this.advancePhase();
    }
  }

  private setupFinalCards() {
    if (!this.isGhost()) {
      this.activePlayers.forEach((p) => {
        if (p.id != this.settings.ghost) {
          this.createPlayerMat(this.getPsychicIndex(p.id), p.id);
        }
      });
    }
  }

  private checkForRevelation() {
    if (this.getUnfinishedPlayerCount() == 0 && this.isGhost()) {
      this.sendGameMessage(MysteriumMessageType.AdvancePhase, '');
    }
    else if (this.hour == 8) {
      this.currentPhase = MysteriumPhase.End;
    }
  }

  private hideRaven() {
    if (!this.isGhost()) {
      this.ravens.find(r => r.isVisible()).setVisible(false);
    }
  }

  private addClairvoyancy(playerId: string, amount: number = 1) {
    this.score.addScore(amount, playerId, 'clairvoyancy');
    var index = this.getPsychicIndex(playerId);
    this.clairvoyancyRank[index].setText(`${this.score.getScore(playerId, 'clairvoyancy')}`);
  }

  private markCorrectCharacter(player: MysteriumPlayerMarker) {
    if (this.isGhost()) {
      player.cardIndex = this.getCharacterIndex(player.cardIndex);
    }

    var card = this.playerCards[player.cardIndex];
    card.characterIndex = player.cardIndex;
    var cardGfx = this.canvas.getGfxObject(this.canvasId, card.characterGfxId) as GfxTexture;
    this.coverCard(cardGfx, player.playerId);

    if (this.playerState.getPlayerId() == player.playerId) {
      this.playerCards.forEach((c) => {
        var location = this.canvas.getGfxObject(this.canvasId, c.locationGfxId) as GfxTexture
        location.setEnabled(!this.playerCards.some(c => c.locationIndex != -1 && c.locationGfxId == location.getId()));
        var character = this.canvas.getGfxObject(this.canvasId, c.characterGfxId) as GfxTexture;
        character.setEnabled(false);
      });
    }
  }

  private markCorrectLocation(player: MysteriumPlayerMarker) {
    if (this.isGhost()) {
      player.cardIndex = this.getLocationIndex(player.cardIndex);
    }

    var card = this.playerCards[player.cardIndex];
    card.locationIndex = player.cardIndex;
    var cardGfx = this.canvas.getGfxObject(this.canvasId, card.locationGfxId) as GfxTexture;
    this.coverCard(cardGfx, player.playerId);

    if (this.playerState.getPlayerId() == player.playerId) {
      this.playerCards.forEach((c) => {
        var objectGfx = this.canvas.getGfxObject(this.canvasId, c.objectGfxId) as GfxTexture
        objectGfx.setEnabled(!this.playerCards.some(c => c.objectIndex != -1 && c.objectGfxId == objectGfx.getId()));
        var location = this.canvas.getGfxObject(this.canvasId, c.locationGfxId) as GfxTexture
        location.setEnabled(false);
      });
    }
  }

  private markCorrectObject(player: MysteriumPlayerMarker) {
    if (this.isGhost()) {
      player.cardIndex = this.getObjectIndex(player.cardIndex);
    }

    var card = this.playerCards[player.cardIndex];
    card.objectIndex = player.cardIndex;
    var cardGfx = this.canvas.getGfxObject(this.canvasId, card.objectGfxId) as GfxTexture;
    this.coverCard(cardGfx, player.playerId);

    this.addClairvoyancy(player.playerId, 7 - this.hour + 1);

    if (this.playerState.getPlayerId() == player.playerId) {
      this.playerCards.forEach((c) => {
        var objectGfx = this.canvas.getGfxObject(this.canvasId, c.objectGfxId) as GfxTexture
        objectGfx.setEnabled(false);
      });
    }
  }

  private coverCard(cardGfx: GfxTexture, playerId: string) {
    cardGfx.setEnabled(false);
    var cover = this.canvas.createRectangle(this.canvasId, cardGfx.getWidth(), cardGfx.getHeight(), this.getPlayerColor(playerId, '66'), true, 1, cardGfx.getId());
    this.cardCovers.push(cover.getId());
  }

  private getVotesUsed(playerId: string, positive: boolean): number {
    var used = this.usedVotes.filter(v => v.playerId == playerId && v.vote == positive).length;
    var current = this.playerVotes.filter(v => v.playerId == playerId && v.vote == positive).length;
    return used + current;
  }

  private markPlayerMarker(selection: MysteriumMarkerSelect) {
    var playerMarker = this.playerMarkers.find(m => m.playerId == selection.targetId);

    var currentVote = this.playerVotes.find(v => v.playerId == selection.playerId && v.targetId == selection.targetId);
    if (currentVote != null) {
      this.canvas.removeGfxObject(this.canvasId, currentVote.voteId);
      if (currentVote.vote && this.getVotesUsed(selection.playerId, false) < this.settings.voteTokens) {
        currentVote.vote = false;
      }
      else {
        var index = this.playerVotes.findIndex(v => v.playerId == selection.playerId && v.targetId == selection.targetId);
        this.playerVotes.splice(index, 1);
        return;
      }
    }
    else {
      selection.vote = true;
      this.playerVotes.push(selection);
      currentVote = selection;
      // If we have exceeded our max votes, convert this to a negative icon
      if (this.getVotesUsed(selection.playerId, true) > this.settings.voteTokens) {
        this.markPlayerMarker(selection);
        return;
      }
    }

    this.createVoteIcon(currentVote, playerMarker.gfxId);
  }

  private createVoteIcon(vote: MysteriumMarkerSelect, parent: number) {
    var voteMarker = this.canvas.createCircle(this.canvasId, 0, 2 * Math.PI, 10, false, this.getPlayerColor(vote.playerId, '66'), true, 1, parent);
    var playerIndex = this.getPsychicIndex(vote.playerId);
    var angle = Math.PI * playerIndex / 3;
    var x = 20 * Math.cos(angle);
    var y = 20 * Math.sin(angle);
    voteMarker.translate(x, y);
    vote.voteId = voteMarker.getId();

    var mark = this.canvas.createTexture(this.canvasId, `./assets/mysterium/${vote.vote ? 'checkmark.png' : 'xmark.png'}`, voteMarker.getId());
    mark.translate(-8, -8);
  }

  private getCharacterIndex(index: number): number {
    return this.characters.findIndex(c => c == this.originalCharacters[index]);
  }

  private getLocationIndex(index: number): number {
    return this.locations.findIndex(l => l == this.originalLocations[index]);
  }

  private getObjectIndex(index: number): number {
    return this.objects.findIndex(o => o == this.originalObjects[index]);
  }

  private getOriginalCharacterIndex(index: number): number {
    return this.originalCharacters.findIndex(c => c == this.characters[index]);
  }

  private getOriginalLocationIndex(index: number): number {
    return this.originalLocations.findIndex(c => c == this.locations[index]);
  }

  private getOriginalObjectIndex(index: number): number {
    return this.originalObjects.findIndex(c => c == this.objects[index]);
  }

  private markPlayerSelection(selection: MysteriumCardSelect) {
    if (this.isGhost()) {
      if (selection.characterIndex != -1) {
        selection.characterIndex = this.getCharacterIndex(selection.characterIndex);
      }
      else if (selection.locationIndex != -1) {
        selection.locationIndex = this.getLocationIndex(selection.locationIndex);
      }
      else if (selection.objectIndex != -1) {
        selection.objectIndex = this.getObjectIndex(selection.objectIndex);
      }
    }

    var marker = this.playerMarkers[this.getPsychicIndex(selection.playerId)];
    var markerGfx = this.canvas.getGfxObject(this.canvasId, marker.gfxId);

    var cardGfx: GfxObject = null;
    if (selection.characterIndex != -1) {
      cardGfx = this.canvas.getGfxObject(this.canvasId, this.playerCards[selection.characterIndex].characterGfxId);
      if (marker.cardIndex != selection.characterIndex) {
        this.clearVotes(marker.playerId);
      }
      marker.cardIndex = selection.characterIndex;
    }
    else if (selection.locationIndex != -1) {
      cardGfx = this.canvas.getGfxObject(this.canvasId, this.playerCards[selection.locationIndex].locationGfxId);
      if (marker.cardIndex != selection.locationIndex) {
        this.clearVotes(marker.playerId);
      }
      marker.cardIndex = selection.locationIndex;
    }
    else if (selection.objectIndex != -1) {
      cardGfx = this.canvas.getGfxObject(this.canvasId, this.playerCards[selection.objectIndex].objectGfxId);
      if (marker.cardIndex != selection.objectIndex) {
        this.clearVotes(marker.playerId);
      }
      marker.cardIndex = selection.objectIndex;
    }

    this.canvas.removeGfxObject(this.canvasId, markerGfx.getId());

    if (cardGfx != null) {
      markerGfx.setVisible(true);
      cardGfx.addChild(markerGfx);
    }
  }

  private clearVotes(playerId: string) {
    var votes = this.playerVotes.filter(v => v.targetId == playerId);
    votes.forEach((v) => {
      this.canvas.removeGfxObject(this.canvasId, v.voteId);
    });

    this.playerVotes = this.playerVotes.filter(v => v.targetId != playerId);
  }

  private getUnfinishedPlayerCount(): number {
    return this.activePlayers.length - 1 - this.playerCards.filter(c => c.characterIndex != -1 && c.locationIndex != -1 && c.objectIndex != -1).length;
  }

  private startTimer() {
    if (this.playerVisions.length == this.getUnfinishedPlayerCount() && this.currentPhase == MysteriumPhase.Visions) {
      this.countdownTime = 120 - (this.isGhost() ? 0 : 1);
      this.countdownTimer = setInterval(() => this.updateTimer(), 1000);
      this.timer.setText('2:00');
      this.timer.setVisible(true);
    }
  }

  private updateTimer() {
    this.countdownTime--;
    if (this.countdownTime < 1) {
      this.timer.setVisible(false);
      clearInterval(this.countdownTimer);
      this.advanceHour();
    }
    else {
      this.timer.setText(`${Math.floor(this.countdownTime / 60)}:${(this.countdownTime % 60).toString().padStart(2, '0')}`);
    }
  }

  private advanceHour() {
    if (this.isGhost()) {
      this.playerMarkers.forEach((p) => {
        var cards = this.playerCards.find(c => c.playerId == p.playerId);
        var votes = this.playerVotes.filter(v => v.targetId == p.playerId);
        if (cards.characterIndex == -1) {
          if (p.cardIndex == this.getPsychicIndex(p.playerId)) {
            p.cardIndex = this.getOriginalCharacterIndex(p.cardIndex);
            this.sendGameMessage(MysteriumMessageType.CorrectCharacter, p);
            votes.filter(v => v.vote).forEach((v) => {
              this.sendGameMessage(MysteriumMessageType.CorrectVote, v.playerId);
            });
          }
          else {
            votes.filter(v => !v.vote).forEach((v) => {
              this.sendGameMessage(MysteriumMessageType.CorrectVote, v.playerId);
            });
          }
        }
        else if (cards.locationIndex == -1) {
          if (p.cardIndex == this.getPsychicIndex(p.playerId)) {
            p.cardIndex = this.getOriginalLocationIndex(p.cardIndex);
            this.sendGameMessage(MysteriumMessageType.CorrectLocation, p);
            votes.filter(v => v.vote).forEach((v) => {
              this.sendGameMessage(MysteriumMessageType.CorrectVote, v.playerId);
            });
          }
          else {
            votes.filter(v => !v.vote).forEach((v) => {
              this.sendGameMessage(MysteriumMessageType.CorrectVote, v.playerId);
            });
          }
        }
        else if (cards.objectIndex == -1) {
          if (p.cardIndex == this.getPsychicIndex(p.playerId)) {
            p.cardIndex = this.getOriginalObjectIndex(p.cardIndex);
            this.sendGameMessage(MysteriumMessageType.CorrectObject, p);
            votes.filter(v => v.vote).forEach((v) => {
              this.sendGameMessage(MysteriumMessageType.CorrectVote, v.playerId);
            });
          }
          else {
            votes.filter(v => !v.vote).forEach((v) => {
              this.sendGameMessage(MysteriumMessageType.CorrectVote, v.playerId);
            });
          }
        }
      });
    }

    this.playerVotes.forEach((v) => {
      this.canvas.removeGfxObject(this.canvasId, v.voteId);
    });
    this.usedVotes = this.usedVotes.concat(this.playerVotes);
    this.playerVotes = new Array<MysteriumMarkerSelect>();

    this.hour++;
    if (this.hour == 4) {
      this.usedVotes = new Array<MysteriumMarkerSelect>();
    }

    this.playerMarkers.forEach((m) => {
      var gfx = this.canvas.getGfxObject(this.canvasId, m.gfxId);
      if (gfx != null) {
        gfx.setVisible(false);
      }
      m.cardIndex = -1;
    });

    this.visionScroller.removeAllChildren();
    this.playerVisions = new Array<MysteriumVisions>();

    this.updateHourGfx();

    if (this.settings.perTurn) {
      this.ravens.forEach((r) => {
        r.setVisible(true);
      });
    }

    if (this.isGhost()) {
      this.sendGameMessage(MysteriumMessageType.EndHour, '');
    }
  }

  private addVisionImages(message: MysteriumVisions) {
    if (this.isGhost()) {
      return;
    }

    var index = 0;
    this.playerVisions.forEach((v) => {
      index += v.visions.length;
    });

    if (this.currentPhase == MysteriumPhase.Visions) {
      var player = this.canvas.createText(this.canvasId, `Visions for ${this.activePlayers.find(p => p.id == message.playerId).name}`, '10px Arial',
        this.getPlayerColor(message.playerId), this.visionScroller.getId());
      player.translate(index * 200, 10);
    }
    
    message.visions.forEach((v, i) => {
      if (this.currentPhase == MysteriumPhase.Visions || this.getClairvoyancyIndex() > i) {
        var gfx = this.getVisionTextureAtlas(v, this.visionScroller.getId());
        gfx.translate(index * 200, 20);
        index++;
      }
    });
  }

  private getClairvoyancyIndex() {
    var score = this.score.getScore(this.playerState.getPlayerId(), 'clairvoyancy');
    if (this.activePlayers.length > 5 && score >= 10 || this.activePlayers.length < 6 && score >= 7) {
      return 3;
    }
    if (this.activePlayers.length > 5 && score >= 7 || this.activePlayers.length < 6 && score >= 5) {
      return 2;
    }

    return 1;
  }

  private shuffleVisions() {
    if (this.discardedVisions == null) {
      this.ghostCards = new Array<MysteriumGhostCard>();
      for (var j = 0; j < 7; j++) {
        this.ghostCards.push(new MysteriumGhostCard(-1, -1, -1));
      }
      this.visionsDeck = new Array<number>();
      for (var i = 0; i < 84; i++) {
        this.visionsDeck.push(i);
      }
      this.gameUtils.shuffle(this.visionsDeck);
    }
    else {
      this.gameUtils.shuffle(this.discardedVisions);
      this.visionsDeck = this.visionsDeck.concat(this.discardedVisions);
    }

    this.discardedVisions = new Array<number>();
  }

  private shuffleCards(): Array<number> {
    var cards = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
    this.gameUtils.shuffle(cards);
    cards.splice(this.settings.cards, 18 - this.settings.cards);
    return cards;
  }

  private getCharacterImage(index: number): string {
    if (index < 9) {
      return './assets/mysterium/character-1.png';
    }
    return './assets/mysterium/character-2.png';
  }

  private getLocationImage(index: number): string {
    if (index < 9) {
      return './assets/mysterium/location-1.png';
    }
    return './assets/mysterium/location-2.png';
  }

  private getObjectImage(index: number): string {
    if (index < 9) {
      return './assets/mysterium/objects-1.png';
    }
    return './assets/mysterium/objects-2.png';
  }
}
