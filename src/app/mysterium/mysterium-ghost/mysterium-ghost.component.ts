import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { MysteriumStateService } from '../services/mysterium-state.service';
import { CanvasStateService, GfxClickData } from '../../core';
import { Subscription } from 'rxjs';
import { MysteriumPhase } from '../mysterium-constants';

@Component({
  selector: 'mysterium-ghost',
  templateUrl: './mysterium-ghost.component.html',
  styleUrls: [
    './mysterium-ghost.component.scss',
    './../mysterium.scss'
  ]
})
export class MysteriumGhostComponent implements AfterViewInit, OnDestroy {
  private clickSubscription: Subscription;
  constructor(private gameState: MysteriumStateService, private canvas: CanvasStateService) { }

  ngAfterViewInit(): void {
    this.gameState.setupGhostBoard();
    this.clickSubscription = this.canvas.subscribeClickEvent(this.mysteriumCanvasId(), (data: GfxClickData) => this.onClick(data));
  }

  ngOnDestroy(): void {
    this.clickSubscription.unsubscribe();
    this.canvas.removeCanvas(this.mysteriumCanvasId());
  }

  public mysteriumCanvasId(): string {
    return this.gameState.canvasId;
  }

  private onClick(data: GfxClickData) {
    if (data != null) {
      var vision = this.gameState.ghostCards.find(c => c.gfxId == data.objectId);
      if (vision != null) {
        var highlight = this.canvas.getGfxObject(this.mysteriumCanvasId(), vision.highlightGfxId);
        vision.selected = !vision.selected;
        highlight.setVisible(vision.selected);
      }

      var player = this.gameState.playerCards.find(c => c.tagGfxId == data.objectId);
      if (player != null && this.gameState.ghostCards.some(c => c.selected) && !this.gameState.playerVisions.some(v => v.playerId == player.playerId)) {
        if (this.gameState.playerCards.find(c => c.playerId == player.playerId &&
          ((c.characterIndex == -1 || c.locationIndex == -1 || c.objectIndex == -1)) || this.gameState.currentPhase == MysteriumPhase.Revelation)) {
          this.gameState.sendVisions(player.playerId);
        }
      }

      var raven = this.gameState.ravens.find(r => r.getId() == data.objectId);
      if (raven != null) {
        raven.setVisible(false);
        this.gameState.useRaven();
      }
    }
  }
}
