import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysteriumGhostComponent } from './mysterium-ghost.component';

describe('MysteriumGhostComponent', () => {
  let component: MysteriumGhostComponent;
  let fixture: ComponentFixture<MysteriumGhostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysteriumGhostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysteriumGhostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
