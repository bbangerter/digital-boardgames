import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysteriumEndGameComponent } from './mysterium-end-game.component';

describe('MysteriumEndGameComponent', () => {
  let component: MysteriumEndGameComponent;
  let fixture: ComponentFixture<MysteriumEndGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysteriumEndGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysteriumEndGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
