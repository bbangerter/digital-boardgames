import { Component, OnInit } from '@angular/core';
import { MysteriumStateService } from '../services/mysterium-state.service';

@Component({
  selector: 'mysterium-end-game',
  templateUrl: './mysterium-end-game.component.html',
  styleUrls: [
    './mysterium-end-game.component.scss',
    './../mysterium.scss'
  ]
})
export class MysteriumEndGameComponent {
  constructor(private gameState: MysteriumStateService) { }

  public isVictory(): boolean {
    return this.gameState.victory;
  }
}
