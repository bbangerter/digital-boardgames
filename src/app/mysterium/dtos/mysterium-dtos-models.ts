export class MysteriumSettings {
  constructor(public difficulty: string, public cards: number, public ravens: number, public perTurn: boolean, public ghost: string, public voteTokens: number) { }
}

export class MysteriumPlayerCard {
  constructor(public playerId: string, public characterGfxId: number, public locationGfxId: number, public objectGfxId: number, public tagGfxId: number, public containerId: number,
    public characterIndex: number = -1, public locationIndex: number = -1, public objectIndex: number = -1, public voteScore: number = 0) { }
}

export class MysteriumGhostCard {
  constructor(public card: number, public gfxId: number, public highlightGfxId: number, public selected: boolean = false) { }
}

export class MysteriumVisions {
  constructor(public playerId: string, public visions: Array<number>) { }
}

export class MysteriumCardSelect {
  constructor(public playerId: string, public characterIndex: number, public locationIndex: number, public objectIndex: number) { }
}

export class MysteriumPlayerMarker {
  constructor(public playerId: string, public gfxId: number, public cardIndex: number = -1) { }
}

export class MysteriumMarkerSelect {
  constructor(public playerId: string, public targetId: string, public vote: boolean = false, public voteId: number = -1) { }
}

export class MysteriumVote {
  constructor(public playerId: string, public targetId: string) { }
}
