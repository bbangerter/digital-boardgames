import { Component, Input } from '@angular/core';
import { MysteriumStateService } from '../services/mysterium-state.service';
import { GameStart, GamePlayer } from '../../core';
import { MysteriumSettings } from '../dtos/mysterium-dtos-models';

@Component({
  selector: 'mysterium-setup',
  templateUrl: './mysterium-setup.component.html',
  styleUrls: [
    './mysterium-setup.component.scss',
    './../mysterium.scss'
  ]
})
export class MysteriumSetupComponent {
  @Input() gameData: GameStart;

  constructor(private gameState: MysteriumStateService) { }

  public gameCanStart(): boolean {
    return this.setDifficulty() && this.allPlayersReady();
  }

  public setDifficulty(): boolean {
    return this.gameState.settings.difficulty != '';
  }

  public allPlayersReady(): boolean {
    for (var i = 0; i < this.gameData.playerIds.length; i++) {
      if (!this.gameState.activePlayers.some(p => p.id == this.gameData.playerIds[i]))
        return false;
    }

    return true;
  }

  public startGame() {
    this.gameState.startGame();
  }

  public isGameCreator(): boolean {
    return this.gameState.isGameCreator();
  }

  public getSettings(): MysteriumSettings {
    return this.gameState.settings;
  }

  public getPlayers(): Array<GamePlayer> {
    return this.gameState.activePlayers;
  }

  public ghostChanged(ghost: any) {
    this.gameState.settings.ghost = ghost.value;
    this.gameState.updateDifficulty();
  }

  public difficultyChange(difficulty: any) {
    this.gameState.settings.difficulty = difficulty.value;
    switch (difficulty.value) {
      case "0":
        this.gameState.settings.cards = this.gameState.activePlayers.length + 2;
        if (this.gameState.activePlayers.length >= 4) {
          this.gameState.settings.cards--;
        }
        if (this.gameState.activePlayers.length >= 6) {
          this.gameState.settings.cards--;
        }
        this.gameState.settings.ravens = 1;
        this.gameState.settings.perTurn = true;
        break;
      case "1":
        this.gameState.settings.cards = this.gameState.activePlayers.length + 3;
        if (this.gameState.activePlayers.length >= 4) {
          this.gameState.settings.cards--;
        }
        if (this.gameState.activePlayers.length >= 7) {
          this.gameState.settings.cards--;
        }
        this.gameState.settings.ravens = 3;
        this.gameState.settings.perTurn = false;
        break;
      case "2":
        this.gameState.settings.cards = this.gameState.activePlayers.length + 4;
        if (this.gameState.activePlayers.length >= 4) {
          this.gameState.settings.cards--;
        }
        if (this.gameState.activePlayers.length >= 7) {
          this.gameState.settings.cards--;
        }
        this.gameState.settings.ravens = 1;
        this.gameState.settings.perTurn = false;
        break;
    }

    this.gameState.updateDifficulty();
  }

  public onCustomChanged() {
    if (this.gameState.settings.cards < this.gameState.activePlayers.length) {
      this.gameState.settings.cards = this.gameState.activePlayers.length;
    }
    if (this.gameState.settings.cards > 10) {
      this.gameState.settings.cards = 10;
    }
    if (this.gameState.settings.ravens > 3) {
      this.gameState.settings.ravens = 3;
    }
    this.gameState.updateDifficulty();
  }
}
