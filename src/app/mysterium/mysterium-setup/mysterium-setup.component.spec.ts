import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysteriumSetupComponent } from './mysterium-setup.component';

describe('MysteriumSetupComponent', () => {
  let component: MysteriumSetupComponent;
  let fixture: ComponentFixture<MysteriumSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysteriumSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysteriumSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
