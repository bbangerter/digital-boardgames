import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSliderModule } from '@angular/material/slider';
import { MatSortModule } from '@angular/material/sort';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';

import { MysteriumGameControllerComponent } from './mysterium-game-controller/mysterium-game-controller.component';
import { MysteriumSetupComponent } from './mysterium-setup/mysterium-setup.component';
import { MysteriumGhostComponent } from './mysterium-ghost/mysterium-ghost.component';
import { MysteriumPsychicComponent } from './mysterium-psychic/mysterium-psychic.component';
import { MysteriumEndGameComponent } from './mysterium-end-game/mysterium-end-game.component';

@NgModule({
  declarations: [
    MysteriumGameControllerComponent,
    MysteriumSetupComponent,
    MysteriumGhostComponent,
    MysteriumPsychicComponent,
    MysteriumEndGameComponent
  ],
  exports: [
    MysteriumGameControllerComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatTableModule,
    MatRadioModule,
    MatSelectModule,
    MatCardModule,
    MatTabsModule,
    MatSliderModule,
    MatSortModule,
    FormsModule,
    CoreModule
  ]
})
export class MysteriumModule { }
