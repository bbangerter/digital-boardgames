import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysteriumGameControllerComponent } from './mysterium-game-controller.component';

describe('MysteriumGameControllerComponent', () => {
  let component: MysteriumGameControllerComponent;
  let fixture: ComponentFixture<MysteriumGameControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysteriumGameControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysteriumGameControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
