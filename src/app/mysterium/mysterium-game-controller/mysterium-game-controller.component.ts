import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { GameComponent, GameStart, PlayerStateService } from '../../core';

import { MysteriumStateService } from '../services/mysterium-state.service';

@Component({
  selector: 'mysterium-game-controller',
  templateUrl: './mysterium-game-controller.component.html',
  styleUrls: [
    './mysterium-game-controller.component.scss',
    './../mysterium.scss'
  ]
})
export class MysteriumGameControllerComponent implements OnInit, OnDestroy, GameComponent {
  @Input() gameData: GameStart;
  constructor(private playerState: PlayerStateService, private gameState: MysteriumStateService) { }

  ngOnInit(): void {
    this.gameState.startGameHub(this.playerState.getPlayerName(), this.gameData.gameId, this.playerState.getPlayerId());
    this.gameState.creatorId = this.gameData.creatorId;
  }

  ngOnDestroy(): void {
    this.gameState.closeGameHub();
  }

  public getCurrentPhase(): number {
    return this.gameState.currentPhase;
  }

  public isGhost(): boolean {
    return this.gameState.isGhost();
  }
}
