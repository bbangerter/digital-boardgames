export enum MysteriumMessageType {
  UpdatePlayerList = 0,
  UpdateSettings = 1,
  StartGame = 2,
  ShuffledCharacters = 3,
  ShuffledLocations = 4,
  ShuffledObjects = 5,
  SendVisions = 6,
  SelectCard = 7,
  SelectMarker = 8,
  CorrectCharacter = 9,
  CorrectLocation = 10,
  CorrectObject = 11,
  CorrectVote = 12,
  UseRaven = 13,
  EndHour = 14,
  AdvancePhase = 15,
  RevelationCharacters = 16,
  RevelationLocations = 17,
  RevelationObjects = 18,
  FinalVote = 19
}

export enum MysteriumPhase {
  Setup = 0,
  Visions = 1,
  Revelation = 2,
  End = 3
}
